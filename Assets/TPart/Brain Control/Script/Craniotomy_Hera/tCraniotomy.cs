﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tCraniotomy : Singleton<tCraniotomy>
{
    public Transform oldHera = null;
    public heraO herao = null;

    public Transform trHera = null;
    public Craniotomy crain = null;
    public tHera thera = null;

    public Transform cylinder = null;

    private void Start()
    {
        AllOff();        
    }

    public void On_oldHera()
    {
        GetComponent<tCraniotomyMethod>().On_oldHera();
    }

    public void On_tHera()
    {
        GetComponent<tCraniotomyMethod>().On_tHera();
    }

    public void AllOff()
    {
        GetComponent<tCraniotomyMethod>().AllOff();
    }
}
