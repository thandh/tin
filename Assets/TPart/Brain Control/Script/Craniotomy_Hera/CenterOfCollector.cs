﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CenterOfCollector : MonoBehaviour
{
    public iListVec3 listVec3 = null;

    [ContextMenu("Center")]
    public void Center()
    {
        if (listVec3 == null || listVec3.pointList().Count == 0) return;
        Vector3 center = Vector3.zero;
        for (int i = 0; i < listVec3.pointList().Count; i++) center += listVec3.pointList()[i];
        center /= (float)listVec3.pointList().Count;
        transform.position = center;
    }
}
