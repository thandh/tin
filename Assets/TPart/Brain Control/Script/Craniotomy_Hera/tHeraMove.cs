﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class tHeraMove : MonoBehaviour
{
    [SerializeField]
    float time = 0f;

    [ContextMenu("GoAutoDeform")]
    public void GoAutoDeform()
    {
        StartCoroutine(C_GoAutoDeform());
    }

    IEnumerator C_GoAutoDeform()
    {
        float time = 0f;

        while (time < 5)
        { 
            time += Time.deltaTime;
            transform.localPosition = new Vector3(0, -2f + 0.16f * time, 0);
            yield return new WaitForEndOfFrame();
        }

        time = 5;

        yield break;
    }

    [ContextMenu("GoReset")]
    public void GoReset()
    {
        StartCoroutine(C_GoReset());
    }

    IEnumerator C_GoReset()
    {
        time = 5;

        while (time >= 5 && time < 10)
        {
            time += Time.deltaTime;
            transform.localPosition = new Vector3(0, -1.2f - 0.16f * (time - 5), 0);
            yield return new WaitForEndOfFrame();
        }

        time = 10;

        yield break;
    }
}