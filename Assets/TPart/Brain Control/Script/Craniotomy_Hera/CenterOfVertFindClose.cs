﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CenterOfVertFindClose: MonoBehaviour
{
    public FindVertClose findVert = null;

    [ContextMenu("Center")]
    public void Center()
    {
        transform.position = findVert.CurrentCenter();
    }
}
