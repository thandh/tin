﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvarageAngleOfTransform : MonoBehaviour
{
    public List<Transform> trList = null;

    [ContextMenu("Avarage")]
    public void Avarage()
    {
        if (trList == null || trList.Count == 0) return;
        Quaternion center = trList[0].rotation;
        for (int i = 1; i < trList.Count; i++) center = Quaternion.Lerp(center, trList[i].rotation, 0.5f);
        transform.rotation = center;
    }
}
