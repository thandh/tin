﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tHeraImpact : MonoBehaviour
{
    public bool isDebug = false;
    public tHera tHera = null;

    [Header("DeformUsePrediction")]
    public bool doneDeformUsePrediction = true;

    [Header("Prediction")]
    public HeraPrediction prediction = null;
    public MeshColliderByMeshFilter cereColliderByMesh = null;

    tCerebellum _tCerebellum = null;
    tCerebellum tCerebellum { get { if (_tCerebellum == null) _tCerebellum = FindObjectOfType<tCerebellum>(); return _tCerebellum; } }

    private void Start()
    {
        cereColliderByMesh = cerebellum_180416.Instance.colliderByFilter;
    }

    [ContextMenu("DeformUsePrediction")]
    public void DeformUsePrediction()
    {
        StopAllCoroutines();
        StartCoroutine(C_DeformUsePrediction());
    }

    IEnumerator C_DeformUsePrediction()
    {
        doneDeformUsePrediction = false;

        //while (prediction.collector.contactList.Count == 0)
        //{
        //    ///move a step down
        //    Vector3 downPoint = trHera.position + trHera.up * stepToDeformPoint;
        //    trHera.position = new Vector3(Mathf.Lerp(trHera.position.x, downPoint.x, Time.deltaTime * stepToDeformPoint),
        //        Mathf.Lerp(trHera.position.y, downPoint.y, Time.deltaTime * stepToDeformPoint),
        //        Mathf.Lerp(trHera.position.z, downPoint.z, Time.deltaTime * stepToDeformPoint));

        //    yield return new WaitForEndOfFrame();
        //}

        tHera.wanderHera.WanderUntilGetPoint();
        yield return new WaitUntil(() => tHera.wanderHera.doneWander == true);

        //lineDistribute_away.Distribute();
        //lineDistribute_surface.Distribute();
        DoImpact_AllImpactLinePoints(prediction.distributeCircle.listForce);
        tHera.MoveAStepToDeformPoint();

        yield return new WaitUntil(() => tCerebellum.md.idle == true);

        cereColliderByMesh.ColliderSyncFilter();

        doneDeformUsePrediction = true;

        yield break;
    }

    void DoImpact_AllImpactLinePoints(List<CereImpactPoint> listForce)
    {
        if (isDebug) Debug.Log("Impact " + force);
        for (int i = 0; i < listForce.Count; i++)
        {
            listForce[i].force.force = force;
            listForce[i].force.AddThatForce();
        }
    }

    void DoImpact_AllImpactCirclePoints()
    {
        for (int i = 0; i < prediction.distributeCircle.listForce.Count; i++)
        {
            prediction.distributeCircle.listForce[i].force.AddThatForce();
        }
    }

    bool CheckAllDone_IPointAddForce(List<CereImpactPoint> points)
    {
        bool done = true;

        foreach (CereImpactPoint p in points)
            if (p.force.doneAdd == false) return false;

        return done;
    }

    [Header("DeformUseLineDistribute")]
    public bool doneDeformUseLineDistribute = true;
    public List<HeraLineImpactDistribute> lineDistribute_aways = null;
    public List<HeraLineImpactDistribute> lineDistribute_surfaces = null;
    public MeshVertCollector collector = null;
    public tPlane planeHera = null;
    itPlane _iplaneHera = null;
    itPlane iplaneHera { get { if (_iplaneHera == null) _iplaneHera = planeHera.GetComponent<itPlane>(); return _iplaneHera; } }
    public NextStep_ByDirection heraNextStepDir = null;
    public NextStep_ByTransform heraNextStepTR = null;
    public MoveMethod rotateZ = null;

    public float distanceCanAddForce = 1f;
    public float force = -200f;
    public int numberAwayToBreakWander = 10;

    [ContextMenu("DeformUseLineDistribute")]
    public void DeformUseLineDistribute()
    {
        StopAllCoroutines();
        StartCoroutine(C_DeformUseLineDistribute());
    }

    IEnumerator C_DeformUseLineDistribute()
    {
        doneDeformUseLineDistribute = false;

        //collector.Shake();
        //yield return new WaitUntil(() => collector.doneShake == true);

        //if (collector.pointListCount == 0)
        //{
        //    tHera.wanderHera.WanderUntilGetPoint();
        //    yield return new WaitUntil(() => tHera.wanderHera.doneWander == true);
        //} else
        //{
        //    tHera.wanderHera.WanderUntilGetNoPoint();
        //    yield return new WaitUntil(() => tHera.wanderHera.doneWander == true);
        //}

        if (isDebug) Debug.Log("Do Wander!!");
        tHera.wanderPrediction.GetComponent<TransformMethod>().ResetLocal();

        tHera.wanderPrediction.boolStop = false;
        tHera.wanderPrediction.WanderUntilBoolStop();

        while (!tHera.wanderPrediction.boolStop)
        {
            foreach (HeraLineImpactDistribute surface in lineDistribute_surfaces) surface.Distribute();
            foreach (HeraLineImpactDistribute away in lineDistribute_aways) away.Distribute();

            int countCanAdd = 0;
            for (int i = 0; i < lineDistribute_surfaces.Count; i++)
            {
                for (int j = 0; j < lineDistribute_aways[i].listForce.Count; j++)
                {
                    bool canAdd = CheckCloseToHera(lineDistribute_aways[i].listForce[j], lineDistribute_surfaces[i].listForce[j]);
                    if (canAdd) countCanAdd++;
                }
            }

            tHera.wanderPrediction.boolStop = countCanAdd >= numberAwayToBreakWander;

            yield return new WaitForEndOfFrame();
        }

        //StartCoroutine(C_RunTouchWithCere());
        if (isDebug) Debug.Log("heraNextStepTR.StepForward() + rotateZ.RotateZ()");
        heraNextStepTR.StepForward();
        rotateZ.RotateZ();

        foreach (HeraLineImpactDistribute away in lineDistribute_aways)
        {
            DoImpact_AllImpactLinePoints(away.listForce);
        }

        yield return new WaitUntil(() => tCerebellum.md.idle == true && doneRunTouchWithCere == true);

        //collector.Shake();
        //yield return new WaitUntil(() => collector.doneShake == true);

        //tHera.wanderPrediction.GetComponent<TransformMethod>().ResetLocal();

        cereColliderByMesh.ColliderSyncFilter();

        doneDeformUseLineDistribute = true;

        yield break;
    }

    bool doneRunTouchWithCere = true;

    IEnumerator C_RunTouchWithCere()
    {
        doneRunTouchWithCere = false;
        Plane heraPlane = iplaneHera.GetPlane();

        while (!tCerebellum.md.idle)
        {
            foreach (HeraLineImpactDistribute surface in lineDistribute_surfaces)
            {
                CereImpactPoint closestPoint = surface.closestPointTo(heraPlane);
                Vector3 pointInHera = heraPlane.ClosestPointOnPlane(closestPoint.transform.position);
                if (closestPoint != null)
                {
                    Vector3 direction = closestPoint.transform.position - pointInHera;
                    heraNextStepDir.direction = direction;
                    heraNextStepDir.StepForward();
                }
            }

            yield return new WaitForEndOfFrame();
        }

        doneRunTouchWithCere = true;

        yield break;
    }

    bool CheckCloseToHera(CereImpactPoint pAway, CereImpactPoint pSurface)
    {
        //if (isDebug) Debug.Log(pSurface.transform.parent.name + " " + pSurface.transform.name + " distance to Hera " + Mathf.Abs(tHera.tPlane_Local.GetPlane().GetDistanceToPoint(pSurface.transform.position)));
        bool canAdd = Mathf.Abs(tHera.tPlane_Local.GetPlane().GetDistanceToPoint(pSurface.transform.position)) < distanceCanAddForce;

        if (pAway.force.canAdd == true)
        {
            pAway.force.canAdd = canAdd;
            pAway.SetColor(canAdd ? pAway.vColor : Color.black);
        }

        return canAdd;
    }

    private void OnEnable()
    {
        prediction.collector.afterAdd += HandleTouchFX;
    }

    private void OnDisable()
    {
        if (IsQuiting) return;
        prediction.collector.afterAdd -= HandleTouchFX;
    }

    [Header("TouchFX")]
    public tHeraTouchFX touchFX = null;

    void HandleTouchFX()
    {
        touchFX.isTouch = prediction.collector.pointList().Count > 0;
    }

    bool IsQuiting = false;

    private void OnApplicationQuit()
    {
        IsQuiting = true;
    }

    [Header("Show")]
    public bool showImpactPoint = true;

    [ContextMenu("ToggleShow")]
    public void ToggleShowImpactPoint()
    {
        showImpactPoint = !showImpactPoint;
        foreach (HeraLineImpactDistribute away in lineDistribute_aways) away.ShowRenderer(showImpactPoint);
        foreach (HeraLineImpactDistribute surface in lineDistribute_surfaces) surface.ShowRenderer(showImpactPoint);
    }

    [ContextMenu("ShowCurrentState_ImpactPoint")]
    public void ShowCurrentState_ImpactPoint()
    {
        foreach (HeraLineImpactDistribute away in lineDistribute_aways) away.ShowRenderer(showImpactPoint);
        foreach (HeraLineImpactDistribute surface in lineDistribute_surfaces) surface.ShowRenderer(showImpactPoint);
    }

    [ContextMenu("Stop")]
    public void Stop()
    {
        StopAllCoroutines();
    }
}
