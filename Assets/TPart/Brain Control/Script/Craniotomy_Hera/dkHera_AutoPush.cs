﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class dkHera_AutoPush : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;
    
    public enum eMoveType { Linear, Rotate }

    [Serializable]
    public class AMove
    {
        public eMoveType moveType = eMoveType.Linear;
        public int countPush = 0;
    }

    [Header("Movement Script")]
    public List<AMove> moveList = new List<AMove>();
    public bool doneScriptMove = true;

    [ContextMenu("ScriptMove")]
    public void ScriptMove()
    {
        StartCoroutine(C_ScriptMove());        
    }

    public IEnumerator C_ScriptMove()
    {
        if (isDebug) Debug.Log("Start C_ScriptMove()");

        doneScriptMove = false;

        for (int i = 0; i < moveList.Count; i++)
        {
            doneAutoMove = false;
            MoveAMove(moveList[i]);
            if (isDebug) Debug.Log("yielding " + moveList[i].moveType + " doneAutoMove == true");
            yield return new WaitUntil(() => doneAutoMove == true);
            if (isDebug) Debug.Log("done yield at " + i + " doneAutoMove == true");
        }

        doneScriptMove = true;
        if (isDebug) Debug.Log("Done C_ScriptMove()");

        yield break;
    }

    void MoveAMove(AMove move)
    {
        if (isDebug) Debug.Log("MoveAMove() : " + move.moveType + ", " + move.countPush);

        switch (move.moveType)
        {
            case eMoveType.Linear:
                currentCountPush = move.countPush;
                AutoPush();
                break;

            case eMoveType.Rotate:
                currentCountPush = move.countPush;
                AutoRotate();
                break;
        }
    }

    [Header("Current")]
    public int currentCountPush = 5;
    public float dStep = 0.1f;

    [Header("Params")]
    public TransparentJob brainTransparent = null;
    public tRig_autoCreate_Brain brainDKRig = null;
    public BrainResetMesh brainReset = null;

    [Header("Params Push")]
    public TransRelationship relation = null;        
    public NextStep_ByTransform nextStepLinear = null;

    [Header("Process")]
    public bool doneAutoMove = true;

    [ContextMenu("AutoPush")]
    public void AutoPush()
    {        
        if (brainReset.doneReset == false) brainReset.Stop();
        brainDKRig.tRig.updateFromNodes = true;
        StartCoroutine(C_AutoPush());
    }

    [ContextMenu("Jumpforward")]
    public void Jumpforward()
    {
        nextStepLinear.d = dStep;
        transform.position = transform.position + currentCountPush * dStep * (nextStepLinear.trEnd.position - nextStepLinear.trStart.position).normalized;
    }

    IEnumerator C_AutoPush()
    {
        if (isDebug) Debug.Log("Start C_AutoPush!!");

        doneAutoMove = false;
        brainDKRig.tRig.UpdateNodeToMM();

        nextStepLinear.d = dStep;
        Vector3 finDes = transform.position + currentCountPush * dStep * (nextStepLinear.trEnd.position - nextStepLinear.trStart.position).normalized;

        //brainTransparent.Set50();

        nextStepLinear.StartCoroutine((nextStepLinear.C_Move(finDes)));
        yield return new WaitUntil(() => nextStepLinear.doneMoving == true);

        brainDKRig.tRig.UpdateMeshMethod();
        brainTransparent.Set100();

        doneAutoMove = true;
        if (isDebug) Debug.Log("Done C_AutoPush!!");

        yield break;
    }

    [ContextMenu("UnAutoPushAtCurrent")]
    public void UnAutoPushAtCurrent()
    {        
        StartCoroutine(C_UnAutoPushAtCurrent());
    }

    [ContextMenu("Jumpbackward")]
    public void Jumpbackward()
    {
        nextStepLinear.d = dStep;
        transform.position = transform.position - currentCountPush * dStep * (nextStepLinear.trEnd.position - nextStepLinear.trStart.position).normalized;
    }

    IEnumerator C_UnAutoPushAtCurrent()
    {
        if (isDebug) Debug.Log("Start C_UnAutoPushAtCurrent!!");

        doneAutoMove = false;

        nextStepLinear.d = dStep;
        Vector3 finDes = transform.position - currentCountPush * dStep * (nextStepLinear.trEnd.position - nextStepLinear.trStart.position).normalized;

        nextStepLinear.StartCoroutine((nextStepLinear.C_Slerp(finDes)));
        yield return new WaitUntil(() => nextStepLinear.doneMoving == true);

        doneAutoMove = true;
        if (isDebug) Debug.Log("Done C_UnAutoPushAtCurrent!!");

        yield break;
    }

    [Header("Params Rotate")]
    public NextStep_ByDirection nextStepRotation = null;

    [ContextMenu("AutoRotate")]
    public void AutoRotate()
    {        
        if (brainReset.doneReset == false) brainReset.Stop();
        brainDKRig.tRig.updateFromNodes = true;
        StartCoroutine(C_AutoRotate());
    }

    [ContextMenu("Rotateforward")]
    public void Rotateforward()
    {
        nextStepRotation.d = dStep;
        transform.rotation = Quaternion.Euler(transform.eulerAngles + currentCountPush * dStep * nextStepRotation.direction.normalized);
    }

    IEnumerator C_AutoRotate()
    {
        if (isDebug) Debug.Log("Start C_AutoRotate!!");

        doneAutoMove = false;
        brainDKRig.tRig.UpdateNodeToMM();

        nextStepRotation.d = dStep;
        Vector3 finRot = transform.eulerAngles + currentCountPush * dStep * nextStepRotation.direction.normalized;

        //brainTransparent.Set50();

        nextStepRotation.StartCoroutine((nextStepRotation.C_Rotate(finRot)));
        yield return new WaitUntil(() => nextStepRotation.doneRotate == true);

        brainDKRig.tRig.UpdateMeshMethod();
        brainTransparent.Set100();

        doneAutoMove = true;
        if (isDebug) Debug.Log("Done C_AutoRotate!!");

        yield break;
    }

    [ContextMenu("UnAutoRotateAtCurrent")]
    public void UnAutoRotateAtCurrent()
    {        
        StartCoroutine(C_UnAutoPushAtCurrent());
    }

    [ContextMenu("Rotatebackward")]
    public void Rotatebackward()
    {
        nextStepRotation.d = dStep;
        transform.rotation = Quaternion.Euler(transform.eulerAngles - currentCountPush * dStep * nextStepRotation.direction.normalized);
    }

    IEnumerator C_UnAutoRotateAtCurrent()
    {
        if (isDebug) Debug.Log("Start C_UnAutoRotateAtCurrent!!");

        doneAutoMove = false;

        nextStepRotation.d = dStep;
        Vector3 finRot = transform.eulerAngles - currentCountPush * dStep * nextStepRotation.direction.normalized;

        nextStepRotation.StartCoroutine((nextStepRotation.C_Slerp(finRot)));
        yield return new WaitUntil(() => nextStepRotation.doneRotate == true);

        doneAutoMove = true;
        if (isDebug) Debug.Log("Done C_UnAutoRotateAtCurrent!!");

        yield break;
    }

    [ContextMenu("Stop")]
    public void Stop()
    {
        if (isDebug) Debug.Log("Stop dkHera_AutoPush!!");

        doneAutoMove = true;
        doneScriptMove = true;

        StopAllCoroutines();
    }
}
