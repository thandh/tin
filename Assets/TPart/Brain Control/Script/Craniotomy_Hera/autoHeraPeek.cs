﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class autoHeraPeek : MonoBehaviour
{
    public GameObject REZ = null;

    public float distanceToREZ = -1f;
    
    [ContextMenu("GetdistanceToREZ")]
    public void GetdistanceToREZ()
    {
        distanceToREZ = (REZ.transform.position - transform.position).magnitude;
    }
}
