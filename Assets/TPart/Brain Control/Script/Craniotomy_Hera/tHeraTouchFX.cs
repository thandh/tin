﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tHeraTouchFX : MonoBehaviour
{
    [Header("Inputs")]
    public Renderer r = null;

    [Header("Params")]
    public Color normalColor = Color.white;
    public Color touchColor = Color.red;

    [SerializeField] bool _isTouch = false;
    public bool isTouch
    {
        set
        {
            _isTouch = value;
            r.sharedMaterials[0].color = _isTouch ? touchColor : normalColor;
        }
    }

    [ContextMenu("TestColor")]
    public void TestColor()
    {
        r.material.color = _isTouch ? touchColor : normalColor;
    }
}
