﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface iListVec3
{
    List<Vector3> pointList();
}
