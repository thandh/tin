﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrainStepback : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Inputs")]
    public Transform peek = null;
    public tHeraAppear hera = null;    

    [ContextMenu("StepbackFromRezCurrentDir")]
    public void StepbackFromRezCurrentDir()
    {
        if (isDebug) Debug.Log("StepbackFromRezCurrentDir");
        Rez.Instance.CalculateLength();
        float lengthRez = Rez.Instance.radius;
        Vector3 dirToRez = transform.position - Rez.Instance.transform.position;
        Vector3 dirToPeek = transform.position - peek.position;
        float lengthHeraTip = (hera.leftTip.position - hera.rightTip.position).magnitude;
        transform.position = Rez.Instance.transform.position + dirToRez.normalized * (lengthRez + lengthHeraTip + dirToPeek.magnitude);        
    }
}
