﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public delegate void afterAddPointList();

[Serializable]
public class MeshVertCollector : MonoBehaviour, iListVec3
{
    public bool isDebug = false;

    [Header("Params")]
    public Collider _thiscollider = null;
    public Collider _othercollider = null;

    public afterAddPointList afterAdd = null;

    public int contactListCount = 0;
    List<ContactPoint> _contactList = new List<ContactPoint>();
    public List<ContactPoint> contactList { get { return _contactList; } set { _contactList = value; HandleContactList(_contactList); } }

    void HandleContactList(List<ContactPoint> contactList)
    {
        if (isDebug) Debug.Log("HandleContactList");
        _pointList = new List<Vector3>();
        for (int i = 0; i < contactList.Count; i++)
        {
            _pointList.Add(contactList[i].point);
        }
        if (afterAdd != null) afterAdd.Invoke();
        contactListCount = contactList.Count;
        pointListCount = _pointList.Count;
    }

    [Header("Process")]
    public bool allowCollectVert = false;

    void CollectVert(Collision col)
    {
        if (isDebug) Debug.Log("CollectVert Collision");
        if (!allowCollectVert) return;
        contactList = col.contacts.ToList();
    }

    void CollectVert(Collider col)
    {
        if (isDebug) Debug.Log("CollectVert Collider");
        if (!allowCollectVert) return;
    }

    [Header("Output")]
    public int pointListCount = 0;
    List<Vector3> __pointList = new List<Vector3>();
    public List<Vector3> _pointList { get { return __pointList; } set { __pointList = value; pointListCount = __pointList.Count; } }

    public List<Vector3> pointList()
    {
        return _pointList;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (_othercollider == null) return;
        if (isDebug) Debug.Log("OnCollisionEnter " + collision.gameObject.name);
        if (collision.gameObject != _othercollider.gameObject) return;
        CollectVert(collision);
    }

    private void OnCollisionStay(Collision collision)
    {
        if (_othercollider == null) return;
        if (isDebug) Debug.Log("OnCollisionStay " + collision.gameObject.name);        
        if (collision.gameObject != _othercollider.gameObject) return;
        CollectVert(collision);
    }

    private void OnCollisionExit(Collision collision)
    {
        if (_othercollider == null) return;
        if (isDebug) Debug.Log("OnCollisionExit " + collision.gameObject.name);
        if (collision.gameObject != _othercollider.gameObject) return;
        CollectVert(collision);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (_othercollider == null) return;
        if (isDebug) Debug.Log("OnTriggerEnter " + other.gameObject.name);
        if (other != _othercollider) return;
        CollectVert(other);
    }

    private void OnTriggerStay(Collider other)
    {
        if (_othercollider == null) return;
        if (isDebug) Debug.Log("OnTriggerStay " + other.gameObject.name);
        if (other != _othercollider) return;
        CollectVert(other);
    }

    private void OnTriggerExit(Collider other)
    {
        if (_othercollider == null) return;
        if (isDebug) Debug.Log("OnTriggerExit " + other.gameObject.name);
        if (other != _othercollider) return;
        CollectVert(other);
    }

    public bool doneShake = true;
    public float stepShake = 0.1f;

    [ContextMenu("Shake")]
    public void Shake()
    {
        StartCoroutine(C_Shake());
    }

    IEnumerator C_Shake()
    {
        doneShake = false;

        _thiscollider.transform.position += new Vector3(stepShake, 0, 0);
        yield return new WaitForEndOfFrame();

        _thiscollider.transform.position += new Vector3(0, stepShake, 0);
        yield return new WaitForEndOfFrame();

        _thiscollider.transform.position += new Vector3(-stepShake, 0, 0);
        yield return new WaitForEndOfFrame();

        _thiscollider.transform.position += new Vector3(0, -stepShake, 0);
        yield return new WaitForEndOfFrame();

        doneShake = true;

        yield break;
    }
}
