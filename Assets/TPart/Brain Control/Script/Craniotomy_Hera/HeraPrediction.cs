﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeraPrediction : MonoBehaviour
{
    public bool isDebug = false;

    [Header("Predict")]
    public bool isPredict = true;

    [Header("Params")]
    public MeshVertCollector collector = null;
    public CenterOfCollector center = null;
    public HitByRay hitByRaySurfacePoint = null;
    public DistributeInCircle distributeCircle = null;
    public DistributeInLine distributeLine = null;

    public Transform centerPoint = null;
    public Transform surfacePoint = null;

    void OnEnable()
    {
        center.listVec3 = collector.GetComponent<iListVec3>();
        collector.afterAdd += DefineHit;
    }

    private void OnDisable()
    {
        collector.afterAdd -= DefineHit;
    }

    void DefineHit()
    {
        if (!isPredict) return;
        //center.Center();
        //surfacePoint.position = centerPoint.position;
        //hitByRaySurfacePoint.HitUseFinalToFinal();
        //distributeCircle.Distribute();
        distributeLine.Distribute();
    }
}
