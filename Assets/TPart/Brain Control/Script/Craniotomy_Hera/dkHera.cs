﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class dkHera : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Params")]
    public Transform dkHeraTr = null;
    public ApplybyLocalTransform localScale = null;

    [ContextMenu("SetScaleByCurrentLocal")]
    public void SetScaleByCurrentLocal()
    {
        
    }

    [Header("Setup Trans Close")]
    public Snap hitSnap = null;
    public HitByRay hitBrain = null;    

    [ContextMenu("SetupBrainWithNewTransClose")]
    public void SetupBrainWithNewTransClose()
    {
        hitSnap.SnapPosition();
        hitSnap.SnapRotation();
        hitBrain.HitUseDir();

        tRig_autoCreate_Brain setupBrain = FindObjectOfType<tRig_autoCreate_Brain>();
        setupBrain.findClose.trClose.position = hitBrain.transform.position;
        setupBrain.FullAuto();
    }
}
