﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowDirectionToStickSurface : MonoBehaviour
{
    [Header("Input")]
    public Collider _collider = null;
    public Transform trDir = null;

    [Header("Params")]
    public float stepBack = 999f;
    [SerializeField] LayerMask lm;

    private void Start()
    {
        lm = _collider.gameObject.layer;
    }

    [ContextMenu("Stick")]
    public void Stick()
    {

    }    
}
