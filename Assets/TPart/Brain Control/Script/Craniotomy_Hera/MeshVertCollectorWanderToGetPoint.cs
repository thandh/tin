﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MeshVertCollectorWanderToGetPoint : MonoBehaviour
{
    public bool isDebug = false;

    [Header("Params")]
    public Transform trINextStep = null;
    INextStep _iNextStep = null;
    INextStep iNextStep { get { if (_iNextStep == null) _iNextStep = trINextStep.GetComponent<INextStep>(); return _iNextStep; } }

    public MeshVertCollector collector = null;

    [Header("Wander To Get Point")]
    public bool doneWander = true;    

    public void WanderUntilGetPoint()
    {
        StartCoroutine(C_WanderUntilGetPoint());
    }

    IEnumerator C_WanderUntilGetPoint()
    {
        doneWander = false;
        while (!IsNextStepHasPoint()) yield return new WaitForEndOfFrame();
        doneWander = true;
        yield break;
    }

    public bool IsNextStepHasPoint()
    {
        iNextStep.StepForward();
        return collector.contactList.Count > 0;
    }

    [Header("Wander Until BoolStop")]
    public bool boolStop = true;

    public void WanderUntilBoolStop()
    {
        StartCoroutine(C_WanderUntilBoolStop());
    }

    IEnumerator C_WanderUntilBoolStop()
    {
        doneWander = false;
        while (!boolStop) { iNextStep.StepForward(); yield return new WaitForEndOfFrame(); }
        doneWander = true;
        yield break;
    }

    public void WanderUntilGetNoPoint()
    {
        StartCoroutine(C_WanderUntilGetNoPoint());
    }

    IEnumerator C_WanderUntilGetNoPoint()
    {
        doneWander = false;
        while (IsBackStepHasPoint()) yield return new WaitForEndOfFrame();
        doneWander = true;
        yield break;
    }

    public bool IsBackStepHasPoint()
    {
        iNextStep.StepBackward();
        return collector.contactList.Count > 0;
    }
}
