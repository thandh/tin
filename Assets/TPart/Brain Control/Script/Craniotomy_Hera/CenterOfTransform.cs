﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CenterOfTransform : MonoBehaviour
{
    public List<Transform> trList = null;

    [ContextMenu("Center")]
    public void Center()
    {
        if (trList == null || trList.Count == 0) return;
        Vector3 center = Vector3.zero;
        for (int i = 0; i < trList.Count; i++) center += trList[i].position;
        center /= (float)trList.Count;
        transform.position = center;
    }
}
