﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Craniotomy : Singleton<Craniotomy>
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Whole Crain")]
	public Transform CraniotomyObj = null;
    public ShapeJob shapeJob = null;
	//public GameObject targetRev, targetRevWorld;
	
	void Start ()
	{
		a = 0;
		isStartSet = false;
	}

    [Header("Appropriated position")]
    public bool isMove = false;
    public bool isStartSet = false;
    public bool isStartMove = false;
    public int a = 0;
    public float dis;
    public bool hasOutput = false;

    private void OnEnable()
    {
        isStartSet = false;
        isStartMove = false;
        hasOutput = false;
    }

    void Update ()
	{
		if (isMove)
        {
			if (!isStartSet)
            {
				isStartSet = true;
                a = 0;
				CraniotomyObj.transform.position = cerebellumControl.Instance.linelist [0];
			}

			CraniotomyObj.rotation = Quaternion.LookRotation (cerebellumControl.Instance.rezTarget.position - CraniotomyObj.position);

            if (isStartMove)
            {
				dis = Vector3.Distance (CraniotomyObj.transform.position, cerebellumControl.Instance.linelist [a + 1]);

				if (dis >= 0.3f)
                {
					CraniotomyObj.transform.position = Vector3.MoveTowards(CraniotomyObj.transform.position, cerebellumControl.Instance.linelist [a + 1], 0.5f * Time.deltaTime);
				}
                else
                {
					CraniotomyObj.transform.position = cerebellumControl.Instance.linelist [a + 1];
					a++;
				}
			}
		}
	}

	void OnTriggerEnter (Collider other)
	{
		if (other.gameObject.tag == "sinus")
        {
			isStartMove = true;
			//Debug.LogError("Enter");
		}
	}

	void OnTriggerStay (Collider other)
	{
		if (other.gameObject.tag == "sinus" && isMove)
        {
			//Debug.LogError("Stay");
			isStartMove = true;
		}
	}

    [Header("Stepback")]
    public ApplybyLocalTransform localPrefab = null;

    public delegate void DoneCylinder();
    public DoneCylinder doneCylinder = null;

    [Header("Stepback")]
    public CrainStepback stepBack = null;

    void OnTriggerExit (Collider other)
	{
		if (other.gameObject.tag == "sinus" && isMove)
        {            
            if (isDebug) Debug.Log("Done OnTriggerExit cylinder!!");

			isMove = false;
            hasOutput = true;
            Debug.Log("Cylinder has output : " + hasOutput);

            if (a != 0) CraniotomyObj.transform.position = cerebellumControl.Instance.linelist [a];
            stepBack.StepbackFromRezCurrentDir();
            //targetRevWorld.transform.position = targetRev.transform.position;
            //targetRevWorld.transform.rotation = targetRev.transform.rotation;

            if (doneCylinder != null) doneCylinder.Invoke();
		}
	}

	public void ClickHoleSetButton ()
	{
		CraniotomyObj.gameObject.SetActive (true);
		isMove = true;
	}

    [Header("Peeks")]
    public Direction crainDir = null;
}
