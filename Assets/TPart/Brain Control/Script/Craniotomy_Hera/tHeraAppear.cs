﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tHeraAppear : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Inputs")]    
    public Transform cutHole = null;
    public Transform dirVIII = null;
    public Transform leftTip = null;
    public Transform rightTip = null;

    [Header("Use Local Prefab")]
    public TransRelationship relationHera = null;

    [ContextMenu("Appear")]
    public void Appear()
    {
        if (isDebug) Debug.Log("Start Appear!!");
        #region
        ///Nhìn về Rez và vuông góc với VII
        ///Nguyên do bỏ: vì quá vuông góc nên dẫn đến hạn chế.

        //if (isDebug) Debug.Log("Start Appear");

        //transform.position = skull.Instance.cutHole.lastHole.position;
        //transform.rotation = Quaternion.LookRotation(Rez.Instance.transform.position - transform.position, -dirVIII.right);
        //Vector3 dirFromRez = transform.position - Rez.Instance.transform.position;
        //float lengthTip = Vector3.Distance(leftTip.position, rightTip.position);
        //float disToTip = Vector3.Distance((leftTip.position + rightTip.position) / 2, transform.position);
        //transform.position = Rez.Instance.transform.position + dirFromRez.normalized * (Rez.Instance.radius + lengthTip + disToTip);
        #endregion        

        relationHera.Compare();
        if (isDebug) Debug.Log("Done Appear!!");
    }

    [ContextMenu("TouchRez")]
    public void TouchRez()
    {
        Vector3 dirToRez = Rez.Instance.transform.position - transform.position;
        transform.forward = dirToRez;
        float lengthTip = Vector3.Distance(leftTip.position, rightTip.position);
        float disToTip = Vector3.Distance((leftTip.position + rightTip.position) / 2, transform.position);
        transform.position = Rez.Instance.transform.position - dirToRez.normalized * (lengthTip + disToTip);
    }

    [ContextMenu("PerpendicularVIII")]
    public void PerpendicularVIII()
    {
        transform.rotation = Quaternion.LookRotation(Rez.Instance.transform.position - transform.position, -dirVIII.right);
    }

    [ContextMenu("TouchNPerpen")]
    public void TouchNPerpen()
    {
        TouchRez();
        PerpendicularVIII();
    }

    #region AutoAppear
    //[Header("AutoAppear")]
    //public CollisionDetect colDetect = null;

    //[Header("AutoAppear Params")]
    //public bool doneAutoAppear = true;
    //public float stepUp = -0.5f;
    //public List<tPlane> heraTPlanes = new List<tPlane>();
    //public float maxDisFromHole = 5f;

    //[ContextMenu("AutoAppear")]
    //public void AutoAppear()
    //{
    //    StartCoroutine(C_AutoAppear());
    //}

    //IEnumerator C_AutoAppear()
    //{
    //    doneAutoAppear = false;
    //    colDetect.CheckOverlapBox();

    //    while (colDetect.collis.Exists((x) => x.layerName == "cerebellum"))
    //    {
    //        transform.position += transform.up.normalized * stepUp;
    //        TouchNPerpen();
    //        colDetect.CheckOverlapBox();
    //        yield return new WaitForSeconds(0.05f);
    //    }

    //    doneAutoAppear = true;
    //    yield break;
    //}
    #endregion AutoAppear

    bool CheckDistanceToHole()
    {
        //foreach (tPlane p in heraTPlanes)
        //    if (p.GetComponent<itPlane>().DistanceTo()
        return true;
    }

    [ContextMenu("Stop")]
    public void Stop()
    {
        StopAllCoroutines();
    }
}
