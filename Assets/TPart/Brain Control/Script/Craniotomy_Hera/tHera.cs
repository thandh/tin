﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class tHera : Singleton<tHera>, IDragHandler, IEndDragHandler
{   
    public Plane tHeraPlane
    {
        get
        {
            tPlane_Global._inNormal = transform.right;
            tPlane_Global._inPoint = transform.position;
            return new Plane(transform.right, transform.position);
        }
    }

    [ContextMenu("DebugHeraPlane")]
    public void DebugHeraPlane()
    {
        Debug.Log(transform.position + " " + tHeraPlane.normal);
    }

    public tPlane_inPoint_inNormal tPlane_Global = null;
    public tPlane_trPoint_trNormal tPlane_Local = null;

    public tHeraImpact heraImpact = null;
    public bool tHeraMove = true;    

    //CutHole _cuthole = null;
    //CutHole cuthole { get { if (_cuthole == null) _cuthole = FindObjectOfType<CutHole>(); return _cuthole; } }
    CutHole cuthole = null;

    [SerializeField] Transform firstHitSphere = null;
    [SerializeField] Transform hittingSphere = null;
    [SerializeField] Transform offsetSphere = null;
    [SerializeField] Transform muzzleSphere = null;
    [SerializeField] float muzzleDistance = 0.1f;

    [SerializeField] Transform hardoffsetSphere = null;    

    [Header("Sphere45")]
    public MeshDeformerMethod sph45 = null;
    public AutoTranslate tHeraTranslate = null;
    public AutoTranslateTemplate manualTranslateTemplate = null;
    public AutoTranslateTemplate ivoryTranslateTemplate = null;    

    [Header("Hits")]
    [SerializeField] Vector3 _firstHit = -Vector3.one;
    Vector3 firstHit
    {
        get { return _firstHit; }
        set
        {
            _firstHit = value;
            firstHitSphere.position = _firstHit;
        }
    }

    [SerializeField] Vector3 _hitting = -Vector3.one;
    Vector3 hitting
    {
        get { return _hitting; }
        set
        {
            _hitting = value;
            hittingSphere.position = _hitting;
            direction = firstHitSphere.position - hittingSphere.position;
            directionLength = direction.magnitude;
            offsetSphere.position = hittingSphere.position + direction.normalized * forceOffset;
        }
    }
    [SerializeField] Vector3 lasthitting = -Vector3.one;
    [SerializeField] Vector3 direction = -Vector3.one;
    [SerializeField] float directionLength = -1f;

    public float lastFrameShow = -1f;
    public float frameStep = 0.5f;

    [SerializeField] bool isSetupInit = true;
    tCerebellum _tCerebellum = null;
    tCerebellum tCerebellum { get { if (_tCerebellum == null) _tCerebellum = FindObjectOfType<tCerebellum>(); return _tCerebellum; } }    
    MeshDeformer deformer = null;

    public bool deformAsGo = false;

    public float force = 10f;
    public float forceOffset = 7f;

    public Transform trHera = null;
    public Transform meshHera = null;
    public Transform offset = null;
    public Transform meshOffset = null;
    public float offsetLength = -1f;

    //public Transform tSphereCerebullumPusher = null;
    public float offset1stPosition = 0f;

    private void Start()
    {
        deformer = tCerebellum.md;
        if (isSetupInit) Invoke("SetupInit", 1f);        
    }

    [ContextMenu("SetupInit")]
    public void SetupInit()
    {
        Debug.Log("Setup Init");
        Color c = tCerebellum.mr.material.color;
        tCerebellum.mr.material.color = new Color(c.r, c.g, c.b, 0.5f);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "cerebellum")
        {            
            firstHit = collision.contacts[0].point;
            hitting = collision.contacts[0].point;
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "cerebellum")
        {
            hitting = collision.contacts[0].point;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == "cerebellum")
        {
            firstHit = -Vector3.one;
            hitting = -Vector3.one;
            direction = -Vector3.one;
            directionLength = -1f;
        }
    }

    private void Update()
    {
        if (!tHeraMove) return;

        if (Input.GetKey(KeyCode.UpArrow) && Input.GetKey(KeyCode.LeftShift))
        {
            trHera.transform.position -= trHera.transform.right.normalized;
        }

        if (Input.GetKey(KeyCode.DownArrow) && Input.GetKey(KeyCode.LeftShift))
        {
            trHera.transform.position += trHera.transform.right.normalized;
        }

        if (Input.GetKey(KeyCode.UpArrow) && !Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.LeftControl))
        {
            trHera.transform.position -= trHera.transform.up.normalized;
        }

        if (Input.GetKey(KeyCode.DownArrow) && !Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.LeftControl))
        {
            trHera.transform.position += trHera.transform.up.normalized;
        }

        if (Input.GetKey(KeyCode.LeftArrow) && !Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.LeftControl))
        {
            trHera.transform.position += trHera.transform.forward.normalized;
        }

        if (Input.GetKey(KeyCode.RightArrow) && !Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.LeftControl))
        {
            trHera.transform.position -= trHera.transform.forward.normalized;
        }

        if (Input.GetKey(KeyCode.UpArrow) && Input.GetKey(KeyCode.LeftControl))
        {
            trHera.transform.Rotate(Vector3.forward);
        }

        if (Input.GetKey(KeyCode.DownArrow) && Input.GetKey(KeyCode.LeftControl))
        {
            trHera.transform.Rotate(Vector3.back);
        }

        if (Input.GetKey(KeyCode.LeftArrow) && Input.GetKey(KeyCode.LeftControl))
        {
            trHera.transform.Rotate(Vector3.up);            
        }

        if (Input.GetKey(KeyCode.RightArrow) && Input.GetKey(KeyCode.LeftControl))
        {
            trHera.transform.Rotate(Vector3.down);            
        }

        if (Input.GetKey(KeyCode.Space) && !Input.GetKey(KeyCode.LeftShift))
        {
            //if (deformAsGo && firstHit != -Vector3.one && hitting != -Vector3.one)
            {
                //Vector3 point = hitting + direction.normalized * forceOffset;
                Vector3 point = hardoffsetSphere.transform.position;
                deformer.AddDeformingForce(point, force);
            }
        }

        if (Input.GetKey(KeyCode.Space) && Input.GetKey(KeyCode.LeftShift))
        {
            if (deformer.allowDeform == false)
                deformer.allowDeform = true;
        } else if (Input.GetKey(KeyCode.Space) && !Input.GetKey(KeyCode.LeftShift))
        {
            if (deformer.allowDeform == true)
                deformer.allowDeform = false;
        }

        if (Input.GetKey(KeyCode.KeypadEnter))
        {
            TryMoveIn();
        }

        //if (Time.time - lastFrameShow > frameStep) return;
        //if (firstHit == -Vector3.one) return;
        //if (hitting == -Vector3.one) return;
        //if (hitting == lastShown) return;

            //Deform(firstHit, hitting);

            //lastShown = hitting;
            //lastFrameShow = Time.time;
    }

    [ContextMenu("Deform")]
    void Deform()
    {
        MeshDeformer md = tCerebellum.md;
        md.allowDeform = false;

        md._point.transform.position = _firstHit;
        md._force = directionLength * -1000f;

        md.AddDeformingForce();
    }

    void Deform(Vector3 firstHit, Vector3 hitting)
    {
        Debug.Log("Deforming");
    }

    public void OnDrag(PointerEventData eventData)
    {
        Debug.Log("OnDrag");        
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Debug.Log("OnEndDrag");

        if (hitting != lasthitting)
        {
            Deform(firstHit, hitting);
            lasthitting = hitting;
        }
    }

    [ContextMenu("DebugOffsetLength")]
    public void DebugOffsetLength()
    {
        Debug.Log((trHera.position - offset.position).magnitude);
    }

    private void OnEnable()
    {        

    }

    [SerializeField] bool IsQuiting = false;

    private void OnApplicationQuit()
    {
        IsQuiting = true;
    }

    private void OnDisable()
    {
        if (IsQuiting) return;        
    }

    private void AlignAB()
    {
        if (cuthole == null) cuthole = skull.Instance.cutHole;

        if (cuthole != null && cuthole.lastHole != null && cuthole.lastHole.position != -Vector3.one)
        {
            //tSphereCerebullumPusher.gameObject.SetActive(false);

            offset.position = cuthole.lastHole.position;

            Vector3 direction = offset.position - tCerebellum.origin.position;
            float length = (trHera.position - offset.position).magnitude;
            trHera.position = cuthole.lastHole.position + direction.normalized * offsetLength;

            trHera.LookAt(tCerebellum.origin);
            trHera.Rotate(Vector3.up, 90f);
            trHera.Translate(offset1stPosition, 0, 0, Space.Self);

            offset.position = cuthole.lastHole.position;

            //tSphereCerebullumPusher.gameObject.SetActive(true);
        }
        else
        {
            Debug.Log("cuthole " + cuthole);
        }
    }

    [ContextMenu("TryMoveIn")]
    public void TryMoveIn()
    {
        StartCoroutine(C_TryMoveIn());
    }

    IEnumerator C_TryMoveIn()
    {
        while (hitting != -Vector3.one && (hitting - muzzleSphere.position).magnitude > muzzleDistance)
        {
            trHera.transform.position -= trHera.transform.right.normalized;
            yield return new WaitForEndOfFrame();
        }

        yield break;
    }

    public void SnapOldHera()
    {
        meshOffset.GetComponent<Follow>().SnapToTarget();
    }

    [Header("Tịnh tiến")]
    public float stepToDeformPoint = 1f;

    public Transform deformPoint = null;
    public Transform deformHeraPoint = null;

    public void MoveAStepToDeformPoint()
    {
        trHera.position += (deformPoint.position - deformHeraPoint.position) * stepToDeformPoint;
    }

    [ContextMenu("Get_AutoTranslate_Manual")]
    public void Get_AutoTranslate_Manual()
    {        
        manualTranslateTemplate = tHeraTranslate.transform.GetComponents<AutoTranslateTemplate>().ToList().Find((x) => x.templateDefine == "Manual");
    }

    [ContextMenu("Get_AutoTranslate_Ivori")]
    public void Get_AutoTranslate_Ivori()
    {
        ivoryTranslateTemplate = tHeraTranslate.GetComponents<AutoTranslateTemplate>().ToList().Find((x) => x.templateDefine == "Ivori");
    }        

    [Header("Wander")]
    public MeshVertCollectorWanderToGetPoint wanderHera = null;
    public MeshVertCollectorWanderToGetPoint wanderPrediction = null;

    public void On()
    {
        gameObject.SetActive(true);
    }

    public void Off()
    {
        gameObject.SetActive(false);
    }

    public void ToggleOnOff()
    {
        gameObject.SetActive(!gameObject.activeSelf);
    }
}
