﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HeraLineImpactDistribute : MonoBehaviour
{
    [Header("Params")]
    public bool isDebug = false;   

    [Header("Predict")]
    public List<CereImpactPoint> listForce = new List<CereImpactPoint>();

    [ContextMenu("Get List Force")]
    public void GetListForce()
    {
        listForce = GetComponentsInChildren<CereImpactPoint>().ToList();
    }

    [Header("Distribute")]
    public Transform startPeek = null;
    public Transform endPeek = null;
    public float dDistribute = 3f;
    public float dToCere = 3f;

    [ContextMenu("Distribute")]
    public void Distribute()
    {
        Vector3 dir = endPeek.position - startPeek.position;

        for (int i = 0; i < listForce.Count; i++)
        {
            listForce[i].transform.position = startPeek.position + i * dDistribute * dir.normalized;
            listForce[i].StepFromCere(dToCere);
        }        
    }

    public void ShowRenderer(bool show)
    {
        foreach (CereImpactPoint point in listForce) point.ShowRenderer(show);
    }

    public CereImpactPoint closestPointTo(Plane plane)
    {
        CereImpactPoint pClose = null;
        float dClose = Mathf.Infinity;
        foreach (CereImpactPoint p in listForce)
        {
            if (plane.GetDistanceToPoint(p.transform.position) < dClose)
            {
                pClose = p;
            }
        }
        return pClose;
    }
}
