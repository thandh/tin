﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineCut_ReduceByOne : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Params")]
    public LineRenderer lr = null;

    [ContextMenu("RemoveByOne")]
    public void RemoveByOne()
    {
        Vector3[] posList = new Vector3[lr.positionCount];
        int k = lr.GetPositions(posList);

        List<Vector3> transferList = new List<Vector3>();
        for (int i = 0; i < posList.Length / 2; i++) transferList.Add(posList[i * 2]);

        posList = transferList.ToArray();
        lr.positionCount = posList.Length;
        lr.SetPositions(posList);
    }
}
