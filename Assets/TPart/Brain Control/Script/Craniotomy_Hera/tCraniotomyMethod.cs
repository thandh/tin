﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class tCraniotomyMethod : MonoBehaviour
{
    Transform oldHera = null;
    Transform trHera = null;
    tHera thera = null;
    Transform cylinder = null;

    private void Awake()
    {
        oldHera = tCraniotomy.Instance.oldHera;
        trHera = tCraniotomy.Instance.trHera;
        thera = tCraniotomy.Instance.thera;
        cylinder = tCraniotomy.Instance.cylinder;
    }

    public void On_oldHera()
    {
        oldHera.gameObject.SetActive(true);
        trHera.gameObject.SetActive(false);
        thera.tHeraMove = false;
        OnOff_cylinder(false);
    }

    public void On_tHera()
    {
        oldHera.gameObject.SetActive(false);
        OnOff_cylinder(false);
        trHera.gameObject.SetActive(true);
        thera.tHeraMove = false;
    }

    public void AllOff()
    {
        oldHera.gameObject.SetActive(false);
        trHera.gameObject.SetActive(false);
        thera.tHeraMove = false;
        OnOff_cylinder(false);
    }

    public void OnOff_cylinder(bool On)
    {
        cylinder.gameObject.SetActive(On);        
    }

    public void OnOff_cylinder(SwitchImageButton s)
    {
        cylinder.gameObject.SetActive(s.GetComponent<Image>().sprite == s.buttonOn);
    }
}
