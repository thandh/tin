﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class tHeraUIAppear : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Inputs")]
    public tHeraAppear appear = null;
    public SwitchImageButton imageSwitch = null;

    [ContextMenu("Appear")]
    public void Appear()
    {
        imageSwitch.SwitchImage();

        if (imageSwitch.GetComponent<Image>().sprite == imageSwitch.buttonOn)
        {
            appear.gameObject.SetActive(true);
            appear.Appear();            
        } else
        {
            appear.gameObject.SetActive(false);
        }
    }
}
