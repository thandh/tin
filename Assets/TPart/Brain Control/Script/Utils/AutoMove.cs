﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

//Not working
public class AutoMove : MonoBehaviour
{
    [Header("Move")]
    public float stepPos = 0.1f;

    public Vector3 beginPos = -Vector3.one;
    public Vector3 endPos = -Vector3.one;

    [ContextMenu("MoveForward")]
    public void MoveForward()
    {
        Move(beginPos, endPos);
    }

    [ContextMenu("MoveBackward")]
    public void MoveBackward()
    {
        Move(endPos, beginPos);
    }

    public void Move(Vector3 beginPos, Vector3 endPos)
    {        
        StartCoroutine(C_Move(beginPos, endPos));
    }

    IEnumerator C_Move(Vector3 beginPos, Vector3 endPos)
    {
        transform.position = beginPos;

        while (checkBetween(beginPos, endPos, transform.position) && transform.position != endPos)
        {
            transform.position += (endPos - beginPos).normalized * stepPos;
            yield return new WaitForEndOfFrame();
        }

        yield break;
    }

    bool checkBetween(Vector3 a, Vector3 b, Vector3 c)
    {
        return (((a - c).magnitude + (b - c).magnitude) == (a - b).magnitude);
    }

    public void Stop()
    {
        StopAllCoroutines();
    }

    [Header("Move Until")]
    public bool boolStopMoveUntil = true;

    public void MoveUntil(Vector3 beginPos, Vector3 endPos)
    {   
        StartCoroutine(C_MoveUntil(beginPos, endPos));
    }

    IEnumerator C_MoveUntil(Vector3 beginPos, Vector3 endPos)
    {
        transform.position = beginPos;

        while (checkBetween(beginPos, endPos, transform.position) && transform.position != endPos && !boolStopMoveUntil)
        {
            transform.position += (endPos - beginPos).normalized * stepPos;
            yield return new WaitForEndOfFrame();
        }

        yield break;
    }
}
