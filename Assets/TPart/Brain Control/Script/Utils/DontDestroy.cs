﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroy : Singleton<DontDestroy>
{
    private void Awake()
    {
        if (Instance != null) DontDestroyOnLoad(this.gameObject);
        else { if (Application.isPlaying) Destroy(this.gameObject); else DestroyImmediate(this.gameObject); }
    }
}
