﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveMethod : MonoBehaviour
{    
    public Transform tr = null;
    public float stepRot = 1;
    public float stepPos = 1;    

    [ContextMenu("RotateX")]
    public void RotateX()
    {
        tr.Rotate(tr.transform.right, stepRot, Space.World);
    }

    [ContextMenu("RotateNX")]
    public void RotateNX()
    {
        tr.Rotate(-tr.transform.right, stepRot, Space.World);
    }

    [ContextMenu("RotateNY")]
    public void RotateY()
    {
        tr.Rotate(tr.transform.up, stepRot, Space.World);
    }

    [ContextMenu("RotateNY")]
    public void RotateNY()
    {
        tr.Rotate(-tr.transform.up, stepRot, Space.World);
    }

    [ContextMenu("RotateZ")]
    public void RotateZ()
    {
        tr.Rotate(tr.transform.forward, stepRot, Space.World);        
    }

    [ContextMenu("RotateNZ")]
    public void RotateNZ()
    {
        tr.Rotate(-tr.transform.forward, stepRot, Space.World);
    }

    [ContextMenu("MoveX")]
    public void MoveX()
    {
        tr.Translate(tr.transform.right * stepPos, Space.World);
    }

    [ContextMenu("MoveNX")]
    public void MoveNX()
    {
        tr.Translate(-tr.transform.right * stepPos, Space.World);
    }

    [ContextMenu("MoveY")]
    public void MoveY()
    {
        tr.Translate(tr.transform.up * stepPos, Space.World);
    }

    [ContextMenu("MoveNY")]
    public void MoveNY()
    {
        tr.Translate(-tr.transform.up * stepPos, Space.World);
    }

    [ContextMenu("MoveZ")]
    public void MoveZ()
    {
        tr.Translate(tr.transform.forward * stepPos, Space.World);
    }

    [ContextMenu("MoveNZ")]
    public void MoveNZ()
    {
        tr.Translate(-tr.transform.forward * stepPos, Space.World);
    }
}
