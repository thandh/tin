﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Direction : MonoBehaviour
{    
    public Transform trOrigin = null;
    public Transform trTarget = null;

    public bool isReverse = false;

    public Direction(Transform trOrigin, Transform trTarget)
    {
        this.trOrigin = trOrigin;
        this.trTarget = trTarget;
    }

    public Vector3 GetDir()
    {
        return !isReverse ? trTarget.position - trOrigin.position : trOrigin.position - trTarget.position;
    }

    [ContextMenu("DebugMagnitude")]
    public void DebugMagnitude()
    {
        Debug.Log("Magnitude : " + GetDir().magnitude);
    }

    [ContextMenu("SnapZ")]
    public void SnapZ()
    {
        transform.forward = trTarget.position - trOrigin.position;
    }
}
