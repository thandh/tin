﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class AutoTranslate : MonoBehaviour
{
    public float stepPos = 0.1f;
    public float stepRot = 0.1f;
    public int stepTime = 10;
    public float stepDelay = 0.1f;

    public Vector3 beginPos = -Vector3.one;
    public Vector3 endPos = -Vector3.one;

    public Quaternion beginRot = Quaternion.identity;
    public Quaternion endRot = Quaternion.identity;

    [ContextMenu("SetBeginFromFollow")]
    public void SetBeginFromFollow()
    {
        GetComponent<Follow>().SnapToTarget();
        beginPos = transform.position;
        beginRot = transform.rotation;

        Debug.Log(beginPos);
        Debug.Log(beginRot);
    }

    [ContextMenu("SetEndFromFollow")]
    public void SetEndFromFollow()
    {
        GetComponent<Follow>().SnapToTarget();
        endPos = transform.position;
        endRot = transform.rotation;
    }

    [ContextMenu("SnapBegin")]
    public void SnapBegin()
    {
        transform.position = beginPos;
        transform.rotation = beginRot;
    }

    [ContextMenu("SnapEnd")]
    public void SnapEnd()
    {
        transform.position = endPos;
        transform.rotation = endRot;
    }

    [Header("Process")]
    public bool doneMove = true;

    [ContextMenu("MoveCurrentWithinSteps")]
    public void MoveCurrentWithinSteps()
    {
        StartCoroutine(C_MoveWithinSteps(beginPos, beginRot, endPos, endRot, true));
    }

    [ContextMenu("MoveReachWithinSteps")]
    public void MoveReachWithinSteps()
    {
        StartCoroutine(C_MoveWithinSteps(beginPos, beginRot, endPos, endRot, false));
    }

    public void MoveWithinSteps(Vector3 beginPos, Quaternion beginRot, Vector3 endPos, Quaternion endRot, bool ResetTrans = false)
    {        
        StartCoroutine(C_MoveWithinSteps(beginPos, beginRot, endPos, endRot, ResetTrans));
    }

    IEnumerator C_MoveWithinSteps(Vector3 beginPos, Quaternion beginRot, Vector3 endPos, Quaternion endRot, bool ResetTrans = false)
    {
        doneMove = false;

        if (ResetTrans)
        {
            transform.position = beginPos;
            transform.rotation = beginRot;
        }

        int time = stepTime;

        while (time > 0)
        {
            time--;
            transform.position += (endPos - beginPos).normalized * stepPos;
            transform.rotation = Quaternion.Lerp(transform.rotation, endRot, Time.deltaTime);
            yield return new WaitForSeconds(stepDelay);
        }

        doneMove = true;

        yield break;
    }

    //[Header("MoveReach Params")]
    //public float delPos = 0.01f;
    //public float delRot = 0.01f;
    //public float timeDelayMoveReach = 0.05f;    

    //public void MoveReach(Vector3 endPos, Quaternion endRot)
    //{
    //    StartCoroutine(C_MoveReach(endPos, endRot));
    //}

    //IEnumerator C_MoveReach(Vector3 endPos, Quaternion endRot)
    //{
    //    doneMove = false;

    //    while (!checkReach(transform.position, transform.rotation, endPos, endRot))
    //    {
    //        transform.position = Vector3.Lerp(transform.position, endPos, Time.deltaTime);
    //        transform.rotation = Quaternion.Lerp(transform.rotation, endRot, Time.deltaTime);
    //        yield return new WaitForSeconds(timeDelayMoveReach);
    //    }

    //    doneMove = true;

    //    yield break;
    //}

    //bool checkReach(Vector3 posA, Quaternion rotA, Vector3 posB, Quaternion rotB)
    //{
    //    return (((posA - posB).magnitude < delRot) && 
    //        (Mathf.Abs(rotA.x - rotB.x) < delRot) &&
    //        (Mathf.Abs(rotA.y - rotB.y) < delRot) &&
    //        (Mathf.Abs(rotA.z - rotB.z) < delRot) &&
    //        (Mathf.Abs(rotA.w - rotB.w) < delRot));
    //}

    //bool checkBetween(Vector3 a, Vector3 b, Vector3 c)
    //{
    //    return (((a - c).magnitude + (b - c).magnitude) == (a - b).magnitude);
    //}

    public void Stop()
    {
        StopAllCoroutines();
        doneMove = true;
    }
}
