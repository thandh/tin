﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using tMeshIO;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class MeshOut : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Editor")]
    public TextAsset textAsset = null;

    [Header("Runtime")]
    public string filename = string.Empty;

    [Header("GetParams")]
    public bool donemeshOut = true;

    [Header("Params View Only")]
    [SerializeField]
    string _currentFolder = string.Empty;
    string currentFolder
    {
        get
        {
            if (isDebug) { Debug.Log("Current : " + _currentFolder + " and " + Head.Instance.caseInfo.GetCurrentCaseFolder()); }
            if (_currentFolder != Head.Instance.caseInfo.GetCurrentCaseFolder()) _currentFolder = Head.Instance.caseInfo.GetCurrentCaseFolder();
            return _currentFolder;
        }
        set { _currentFolder = value; }
    }

    [ContextMenu("ResetParams")]
    public void ResetParams()
    {
        _currentFolder = string.Empty;
        Debug.LogWarning("Reseting : " + currentFolder);
    }

    IEnumerator Start()
    {
        yield return new WaitUntil(() => Head.Instance != null);

        ResetParams();
        CheckExisted();
        yield break;
    }

    [Header("Force Create File")]
    public bool forceCheckExisted = true;

    [ContextMenu("CheckExisted")]
    public void CheckExisted()
    {
        if (string.IsNullOrEmpty(filename)) { Debug.Log("filename empty"); return; }

        if (!File.Exists(currentFolder + filename))
        {
            File.Create(currentFolder + filename);
        }
    }

    bool Existed()
    {
        if (isDebug && string.IsNullOrEmpty(filename)) { Debug.Log("filename empty"); return false; }
        bool existed = File.Exists(currentFolder + filename);
        return existed;
    }

    [Header("Empty")]
    public bool doneEmpty = true;

    [ContextMenu("Empty")]
    public void Empty()
    {
        StartCoroutine(C_Empty());
    }

    IEnumerator C_Empty()
    {
        if (isDebug) Debug.Log("Start Empty !!");

        if (isDebug && string.IsNullOrEmpty(filename)) { Debug.Log("filename empty"); yield break; }

        if (!File.Exists(currentFolder + filename))
        {
            File.Create(currentFolder + filename);
            yield return new WaitForSeconds(2f);
        }

        yield return new WaitUntil(() => Existed() == true);

        doneEmpty = false;

        yield return new WaitUntil(() => IsFileLocked(new FileInfo(currentFolder + filename)) == false);
        File.WriteAllText(currentFolder + filename, string.Empty);
        yield return new WaitUntil(() => IsFileLocked(new FileInfo(currentFolder + filename)) == false);

        doneEmpty = true;
        if (isDebug) Debug.Log("Done Empty");

        yield break;
    }

    [Header("Current Params")]
    [HideInInspector]
    public Vector3[] currentVertsWorld = new Vector3[0];
    [HideInInspector]
    public string currentOutMesh = string.Empty;

    [ContextMenu("meshOut_Current")]
    public void meshOut_Current()
    {
        StartCoroutine(C_MeshOut(this.currentVertsWorld, this.currentOutMesh));
    }

    public void meshOut(Vector3[] vertsWorld, string outMesh)
    {
        StartCoroutine(C_MeshOut(vertsWorld, outMesh));
    }

    IEnumerator C_MeshOut(Vector3[] vertsWorld, string outMesh)
    {
        if (isDebug) Debug.Log("Start meshOut : " + vertsWorld.Length + " verts, " + outMesh.Length + " string length");

        if (isDebug && string.IsNullOrEmpty(filename)) { Debug.Log("filename empty"); yield break; }

        yield return new WaitUntil(() => Existed() == true);

        donemeshOut = false;

        string output = new tMesh(vertsWorld).toJson();

        yield return new WaitUntil(() => IsFileLocked(new FileInfo(currentFolder + filename)) == false);
        File.WriteAllText(currentFolder + filename, output);
        yield return new WaitUntil(() => IsFileLocked(new FileInfo(currentFolder + filename)) == false);

        donemeshOut = true;
        if (isDebug) Debug.Log("Done meshOut");

        yield break;
    }

    bool IsFileLocked(FileInfo file)
    {
        FileStream stream = null;

        try
        {
            stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
        }
        catch (IOException)
        {
            return true;
        }
        finally
        {
            if (stream != null)
                stream.Close();
        }

        return false;
    }

    [ContextMenu("Stop")]
    public void Stop()
    {
        StopAllCoroutines();
    }

    [Header("Delete")]
    public bool doneDelete = true;

    [ContextMenu("DeleteCurrentFile")]
    public void DeleteCurrentFile()
    {
        StartCoroutine(C_DeleteCurrentFile());
    }

    IEnumerator C_DeleteCurrentFile()
    {
        doneDelete = false;

        if (File.Exists(currentFolder + filename)) File.Delete(currentFolder + filename);
        else Debug.Log(currentFolder + filename + " not existed");

        yield return new WaitUntil(() => File.Exists(currentFolder + filename) == false);

        doneDelete = true;

        yield break;
    }
}