﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawLineGLViewer : MonoBehaviour
{
    private void OnPostRender()
    {
        DrawLineGL.CreateLineMaterial();
        // set the current material
        DrawLineGL.lineMaterial.SetPass(0);

        GL.Begin(GL.LINES);

        if (DrawLineGL.linesGL.Length > 0)
        {
            for (int i = 0; i < DrawLineGL.linesGL.Length; i++)
		    {
                GL.Color(DrawLineGL.linesGL[i].c);
                GL.Vertex3(DrawLineGL.linesGL[i].st.x, DrawLineGL.linesGL[i].st.y, DrawLineGL.linesGL[i].st.z);
                GL.Vertex3(DrawLineGL.linesGL[i].ed.x, DrawLineGL.linesGL[i].ed.y, DrawLineGL.linesGL[i].ed.z);

                if (Time.time > DrawLineGL.linesGL[i].lt)
                {
                    DrawLineGL.ResizeLines(i);
                }
            }
        }

        GL.End();
    }
}
