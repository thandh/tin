﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderIO : MonoBehaviour
{
    public enum coliderType
    {
        box,
        sphere,
        mesh
    }

    public coliderType type = coliderType.mesh;

    public MeshFilter _meshFilter = null;
    public Collider _collider = null;

    [ContextMenu("ToggleOnOff")]
    public void ToggleOnOff()
    {
        if (_collider == null)
        {
            _collider = _meshFilter.GetComponent<Collider>();
        }

        if (_collider != null)
        {
            if (Application.isPlaying) Destroy(_collider); else DestroyImmediate(_collider);
        } else
        {
            switch (type)
            {
                case coliderType.box: _collider = _meshFilter.gameObject.AddComponent<BoxCollider>(); break;
                case coliderType.sphere: _collider = _meshFilter.gameObject.AddComponent<SphereCollider>(); break;
                case coliderType.mesh:
                    {
                        _collider = _meshFilter.gameObject.AddComponent<MeshCollider>();
                        ((MeshCollider)_collider).sharedMesh = _meshFilter.sharedMesh;
                        break;
                    }
            }
        }
    }
}
