﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Follow : MonoBehaviour
{
    [Header("note")]
    public string note = string.Empty;

    [Header("Debug")]
    public bool isDebug = false;

    [Header("Process")]
    public bool IsLock = false;
    public float step = -1f;
    public bool followPosition = true;
    public bool followRotation = true;

    [Header("Target")]
    public Transform target = null;

    private void Update()
    {
        if (IsLock) return;

        if (target == null) return;

        if (followPosition && transform.position != target.position)
        {
            if (step == -1) transform.position = target.position;
            else
            {
                transform.position = new Vector3(Mathf.Lerp(transform.position.x, target.position.x, step),
                    Mathf.Lerp(transform.position.y, target.position.y, step),
                    Mathf.Lerp(transform.position.z, target.position.z, step));
            }
        }

        if (followRotation && transform.rotation != target.rotation)
        {
            if (step == -1) transform.rotation = target.rotation;
            else
            {
                transform.rotation = target.rotation;
            }
        }
    }

    [ContextMenu("SnapToTarget")]
    public void SnapToTarget()
    {
        transform.position = target.position;
        transform.rotation = target.rotation;

        if (isDebug)
        {
            Debug.Log("SnapToTarget " + transform.position + " " + transform.rotation.eulerAngles);
        }
    }

    [ContextMenu("SnapPosition")]
    public void SnapPosition()
    {
        transform.position = target.position;
    }

    [ContextMenu("SnapRotation")]
    public void SnapRotation()
    {
        transform.rotation = target.rotation;
    }

    [Header("Control Global")]
    public List<Follow> followList = new List<Follow>();

    [ContextMenu("DisableAll")]
    public void DisableAll()
    {
        followList = FindObjectsOfType<Follow>().ToList();
        foreach (Follow f in followList) f.enabled = false;
    }

    [ContextMenu("nableAll")]
    public void EnableAll()
    {
        followList = FindObjectsOfType<Follow>().ToList();
        foreach (Follow f in followList) f.enabled = true;
    }
}
