#pragma strict

function Awake()
{
	DrawLineGL.CreateLinesGL();
}

function OnPostRender() {
    DrawLineGL.CreateLineMaterial();
    // set the current material
    DrawLineGL.lineMaterial.SetPass( 0 );
    
    GL.Begin( GL.LINES );

	if(DrawLineGL.linesGL.Length > 0)
	{
		for(var i : int = 0; i<DrawLineGL.linesGL.length; i++)
		{
			GL.Color (DrawLineGL.linesGL[i].c);
			GL.Vertex3 (DrawLineGL.linesGL[i].st.x,DrawLineGL.linesGL[i].st.y,DrawLineGL.linesGL[i].st.z);
			GL.Vertex3 (DrawLineGL.linesGL[i].ed.x,DrawLineGL.linesGL[i].ed.y,DrawLineGL.linesGL[i].ed.z);
			
			if(Time.time > DrawLineGL.linesGL[i].lt)
			{
				DrawLineGL.ResizeLines(i);
			}
		}
	}

    GL.End();
}