﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class TINCaseInfo : MonoBehaviour
{
    [Header("Path")]
    public bool isDebug = false;

    [Header("Path")]
    public string DIRasset = string.Empty;
    public string DIRname = string.Empty;
    public string DIRfullPath = string.Empty;

    [Header("Current")]
    [SerializeField] string _currentCaseFolder = string.Empty;
    public string currentCaseFolder
    {
        get { if (string.IsNullOrEmpty(_currentCaseFolder)) PrepareFolder(); return _currentCaseFolder; }
        set { _currentCaseFolder = value; if (isDebug) Debug.Log("Setting new value 4 currentCaseFolder : " + _currentCaseFolder); }
    }

    [Header("Temp fix current CaseFolder")]
    public bool fixPathAtAwake = true;

    private void Awake()
    {
        if (fixPathAtAwake) PrepareFolder();
    }

    public string GetCurrentCaseFolder()
    {
        if (string.IsNullOrEmpty(this.currentCaseFolder)) Debug.LogError("NULL!!");        
        return this.currentCaseFolder;
    }

    [ContextMenu("DebugCurrentFolder")]
    public void DebugCurrentFolder()
    {
        Debug.Log(this.currentCaseFolder);
    }

    [ContextMenu("PrepareFolder")]
    public void PrepareFolder()
    {
        if (isDebug) Debug.Log("Start PrepareFolder");

        if (!Directory.Exists(Application.persistentDataPath + "/" + "Data/"))
            Directory.CreateDirectory(Application.persistentDataPath + "/" + "Data/");

        if (!Directory.Exists(Application.persistentDataPath + "/" + "Data/" + DIRname + "/"))
            Directory.CreateDirectory(Application.persistentDataPath + "/" + "Data/" + DIRname + "/");

        if (isDebug)
        {
            Debug.Log("pData path : " + Application.persistentDataPath);
            Debug.Log("DIRname : " + this.DIRname);
        }

        currentCaseFolder = Application.persistentDataPath + "/" + "Data/" + DIRname + "/";

        if (isDebug) Debug.Log("Done PrepareFolder");
    }
}