﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Follow_Vertice : MonoBehaviour
{
    [Header("Control")]
    public bool isDebug = false;
    public bool IsLock = false;

    [Header("Params")]
    public int vertID = 0;
    public MeshFilter mf = null;
    [SerializeField] Transform _mfTrans = null;
    Transform mfTrans { get { if (_mfTrans == null) _mfTrans = mf.transform; return _mfTrans; } }

    private void Update()
    {
        if (IsLock) return;
        SnapPosition();
    }

    [ContextMenu("SnapPosition")]
    public void SnapPosition()
    {
        if (Application.isPlaying)
            transform.position = mfTrans.TransformPoint(mf.mesh.vertices[vertID]);
        else transform.position = mfTrans.TransformPoint(mf.sharedMesh.vertices[vertID]);
    }
}
