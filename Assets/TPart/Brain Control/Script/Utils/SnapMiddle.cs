﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapMiddle : MonoBehaviour
{
    [Header("snapMiddle")]
    public Transform target1 = null;
    public Transform target2 = null;

    public BoxCollider bc = null;

    [ContextMenu("snapMiddle")]
    public void snapMiddle()
    {
        transform.position = (target1.position + target2.position) / 2f;

        ///x = 2, z = 4 are constants
        bc.size = new Vector3(2f, (target1.position - target2.position).magnitude, 4f);
    }
}
