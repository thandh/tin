﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawLineGL : MonoBehaviour
{
    public static Material lineMaterial;
    public static lineGL[] linesGL;

    public class lineGL
    {
        public float lt;
        public Vector3 st;
        public Vector3 ed;
        public Color c;

        public lineGL(Vector3 nst, Vector3 ned, Color nc, float nlt)
        {
            lt = Time.time + nlt;
            st = nst;
            ed = ned;
            c = nc;
        }
    }

    public static void CreateLineMaterial()
    {
        if (!lineMaterial)
        {
            lineMaterial = new Material("Shader \"Lines/Colored Blended\" {" +
                "SubShader { Pass { " +
                "    Blend SrcAlpha OneMinusSrcAlpha " +
                "    ZWrite Off Cull Off Fog { Mode Off } " +
                "    BindChannels {" +
                "      Bind \"vertex\", vertex Bind \"color\", color }" +
                "} } }");
            lineMaterial.hideFlags = HideFlags.HideAndDontSave;
            lineMaterial.shader.hideFlags = HideFlags.HideAndDontSave;
        }
    }

    static void CreateLinesGL()
    {
        if (linesGL == null)
        {
            linesGL = new lineGL[0];
        }
    }

    public static void DrawLine(Vector3 st, Vector3 ed, Color c, float lt)
    {
        lineGL nlGL = new lineGL(st, ed, c, lt);

        lineGL[] nlinesGL = new lineGL[linesGL.Length + 1];

        for (int i = 0; i < nlinesGL.Length; i++)
        {
            if (i < linesGL.Length)
            {
                nlinesGL[i] = linesGL[i];
            }
            else
            {
                nlinesGL[i] = nlGL;
            }
        }

        linesGL = nlinesGL;
    }

    public static void DrawLine(Vector3 st, Vector3 ed, Color c)
    {
        lineGL nlGL = new lineGL(st, ed, c, 0);

        lineGL[] nlinesGL = new lineGL[linesGL.Length + 1];

        for (int i = 0; i < nlinesGL.Length; i++)
        {
            if (i < linesGL.Length)
            {
                nlinesGL[i] = linesGL[i];
            }
            else
            {
                nlinesGL[i] = nlGL;
            }
        }

        linesGL = nlinesGL;
    }

    public static void ResizeLines(float index)
    {
        lineGL[] nlinesGL = new lineGL[linesGL.Length - 1];

        for (int i = 0; i < linesGL.Length; i++)
        {
            if (i < index)
            {
                nlinesGL[i] = linesGL[i];
            }
            else
            if (i < nlinesGL.Length)
            {
                nlinesGL[i] = linesGL[i + 1];
            }
        }
        linesGL = nlinesGL;
    }
}