﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetPivotMethod : MonoBehaviour
{
    Vector3 p; 
    Vector3 last_p;

    [Header("Inputs")]
    public MeshFilter mf = null;
    [SerializeField] Mesh _m = null;
    Mesh m
    {
        get
        {
            if (_m == null)
            {
                if (mf != null) _m = Application.isPlaying ? mf.mesh : mf.sharedMesh;
            }
            return _m;
        }

        set { _m = value; }
    }

    [ContextMenu("ResetMesh")]
    public void ResetMesh()
    {
        _m = null;
    }

    [ContextMenu("SyncMesh")]
    public void SyncMesh()
    {
        if (mf != null) m = mf.sharedMesh;
    }

    [ContextMenu("Reset0")]
    public void Reset0()
    {
        ResetMesh();
        SyncMesh();
    }

    [ContextMenu("ZeroPosition")]
    public void ZeroPosition()
    {
        transform.localPosition = Vector3.zero;

        Vector3 origin = Vector3.zero;
        Vector3[] vertsWorld = new Vector3[m.vertices.Length];

        for (int i = 0; i < vertsWorld.Length; i++)
        {
            vertsWorld[i] = transform.TransformPoint(m.vertices[i]);
            origin += vertsWorld[i];
        }

        origin /= vertsWorld.Length;

        Vector3 diff = transform.position - origin;

        Vector3[] vertsLocal = m.vertices;
        for (int i = 0; i < vertsLocal.Length; i++)
        {
            vertsLocal[i] += transform.InverseTransformVector(diff);
        }

        m.vertices = vertsLocal;
        m.RecalculateBounds();
    }

    [Header("Test_SetPivotTo")]
    public Transform testPivot = null;

    [ContextMenu("Test_SetPivotTo")]
    public void Test_SetPivotTo()
    {
        SetPivotTo(testPivot.position);
    }

    public void SetPivotTo(Vector3 pivot)
    {
        Vector3 diff = transform.position - pivot;

        Vector3[] vertsLocal = m.vertices;
        for (int i = 0; i < vertsLocal.Length; i++)
        {
            vertsLocal[i] += transform.InverseTransformVector(diff);
        }

        m.vertices = vertsLocal;
        m.RecalculateBounds();

        transform.position -= diff;
    }
}
