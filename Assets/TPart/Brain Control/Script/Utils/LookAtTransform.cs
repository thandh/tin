﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LookAtTransform : MonoBehaviour
{
    public bool isDebug = false;

    [Header("lookAt")]
    public Transform lookAt = null;

    [ContextMenu("LookAt")]
    public void LookAt()
    {
        transform.LookAt(lookAt);
    }

    [ContextMenu("LookAtX")]
    public void LookAtX()
    {
        transform.right = lookAt.position - transform.position;
    }

    [ContextMenu("LookAtY")]
    public void LookAtY()
    {
        transform.up = lookAt.position - transform.position;
    }

    [ContextMenu("LookAtZ")]
    public void LookAtZ()
    {
        transform.forward = lookAt.position - transform.position;
    }

    [ContextMenu("LookAtNX")]
    public void LookAtNX()
    {
        transform.right = -(lookAt.position - transform.position);
    }

    [ContextMenu("LookAtNY")]
    public void LookAtNY()
    {
        transform.up = -(lookAt.position - transform.position);
    }

    [ContextMenu("LookAtNZ")]
    public void LookAtNZ()
    {
        transform.forward = -(lookAt.position - transform.position);
    }    
}
