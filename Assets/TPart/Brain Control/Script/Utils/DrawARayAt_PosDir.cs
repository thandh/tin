﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class DrawARayAt_PosDir : MonoBehaviour
{
    [Header("note")]
    public string note = string.Empty;

    [Header("Inputs")]
    public bool useUpdate = false;
    public Vector3 pos = Vector3.zero;
    public Vector3 dir = Vector3.zero;

    [Header("Params")]
    public float length = 10f;
    public Color color = Color.green;

    private void Update()
    {
        if (!useUpdate) return;
        Debug.DrawRay(pos, dir.normalized * length, color);
    }    
}
