﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeScale : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [ContextMenu("Scale0")]
    public void vScale0()
    {
        if (isDebug) Debug.Log("vScale0p1");
        Scale0();
    }

    public static void Scale0()
    {        
        Time.timeScale = 0f;
    }

    [ContextMenu("Scale1")]
    public void vScale1()
    {
        if (isDebug) Debug.Log("vScale1");
        Scale1();
    }

    public static void Scale1()
    {        
        Time.timeScale = 1f;
    }
}
