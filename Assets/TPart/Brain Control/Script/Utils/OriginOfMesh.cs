﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OriginOfMesh : MonoBehaviour
{
    public bool isDebug = false;

    [Header("Params")]
    public MeshFilter meshFilter = null;

    [Header("Center")]
    public bool centerAtStart = false;

    private void Start()
    {
        if (centerAtStart) Center();
    }

    [ContextMenu("Center")]
    public void Center()
    {
        if (isDebug) Debug.Log("Start finding center");

        Vector3 origin = Vector3.zero;

        Transform trMesh = meshFilter.transform;
        for (int i = 0; i < meshFilter.sharedMesh.vertices.Length; i++) origin += trMesh.TransformPoint(meshFilter.sharedMesh.vertices[i]);

        origin /= meshFilter.sharedMesh.vertices.Length;

        transform.position = origin;

        if (isDebug) Debug.Log("done finding center");
    }
}
