/*
HOW TO USE, Put DrawLineGLViewer.js on the cameras you want the lines to be rendered

THIS SCRIPT SHOULD BE IN YOUR PLUGINS FOLDER

DrawLineGL.DrawLine(StartPos:Vector3, EndPos:Vector3, Color:Color, LifeTime:float);

LifeTime is optional, if none then it'll be rendered 1 frame
*/

static var lineMaterial : Material;
static var linesGL:lineGL[];

class lineGL
{
	var lt: float;
	var st: Vector3;
	var ed: Vector3;
	var c: Color;
	
	function lineGL(nst:Vector3, ned:Vector3, nc:Color, nlt:float)
	{
		lt = Time.time + nlt;
		st = nst;
		ed = ned;
		c = nc;
	}
}

static function CreateLineMaterial() {
    if( !lineMaterial ) {
        lineMaterial = new Material( "Shader \"Lines/Colored Blended\" {" +
            "SubShader { Pass { " +
            "    Blend SrcAlpha OneMinusSrcAlpha " +
            "    ZWrite Off Cull Off Fog { Mode Off } " +
            "    BindChannels {" +
            "      Bind \"vertex\", vertex Bind \"color\", color }" +
            "} } }" );
        lineMaterial.hideFlags = HideFlags.HideAndDontSave;
        lineMaterial.shader.hideFlags = HideFlags.HideAndDontSave;
    }
}

static function CreateLinesGL()
{
	if(!linesGL)
	{
		linesGL = new lineGL[0];
	}
}

static public function DrawLine(st:Vector3, ed:Vector3, c:Color, lt:float)
{
	var nlGL : lineGL = new lineGL(st,ed,c,lt);
	
	var nlinesGL = new lineGL[linesGL.length + 1];
	
	for(var i : int = 0; i<nlinesGL.length;i++)
	{
		if(i<linesGL.length)
		{
			nlinesGL[i] = linesGL[i];
		}else
		{
			nlinesGL[i] = nlGL;
		}
	}
	
	linesGL = nlinesGL;
}

static public function DrawLine(st:Vector3, ed:Vector3, c:Color)
{
	var nlGL : lineGL = new lineGL(st,ed,c,0);
	
	var nlinesGL = new lineGL[linesGL.length + 1];
	
	for(var i : int = 0; i<nlinesGL.length;i++)
	{
		if(i<linesGL.length)
		{
			nlinesGL[i] = linesGL[i];
		}else
		{
			nlinesGL[i] = nlGL;
		}
	}
	
	linesGL = nlinesGL;
}

static public function ResizeLines(index:float)
{
	var nlinesGL = new lineGL[linesGL.length - 1];
	
	for(var i : int = 0; i<linesGL.length; i++)
	{
		if(i < index)
		{
			nlinesGL[i] = linesGL[i];
		}else
		if(i < nlinesGL.length)
		{
			nlinesGL[i] = linesGL[i+1];
		}
	}
	
	linesGL = nlinesGL;
}
