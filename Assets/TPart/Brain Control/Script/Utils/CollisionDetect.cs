﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void onChange_collis();

public class CollisionDetect: MonoBehaviour
{
    public bool isDebug = false;

    [Serializable]
    public class ArColli
    {
        public string layerName = string.Empty;
        public List<GameObject> trList = new List<GameObject>();
        public ArColli() { }
    }

    [Header("Output")]
    public int collisCount = 0;
    public List<ArColli> _collis = new List<ArColli>();
    public List<ArColli> collis { get { return _collis; } set { _collis = value; collisCount = _collis.Count; } }

    public onChange_collis onchange_collis = null;

    private void OnCollisionEnter(Collision collision)
    {
        if (isDebug) Debug.Log(gameObject.name + " OnCollisionEnter add " + collision.gameObject.name);
        Add(collision.gameObject);
    }

    private void OnCollisionStay(Collision collision)
    {
        if (isDebug) Debug.Log(gameObject.name + " OnCollisionStay add " + collision.gameObject.name);
        Add(collision.gameObject);
    }

    private void OnCollisionExit(Collision collision)
    {
        if (isDebug) Debug.Log(gameObject.name + " OnCollisionExit remove " + collision.gameObject.name);
        Remove(collision.gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (isDebug) Debug.Log(gameObject.name + " OnTriggerEnter add " + other.gameObject.name);
        Add(other.gameObject);
    }

    private void OnTriggerStay(Collider other)
    {
        if (isDebug) Debug.Log(gameObject.name + " OnTriggerStay add " + other.gameObject.name);
        Add(other.gameObject);
    }

    private void OnTriggerExit(Collider other)
    {
        if (isDebug) Debug.Log(gameObject.name + " OnTriggerExit remove " + other.gameObject.name);
        Remove(other.gameObject);
    }

    [ContextMenu("CheckOverlapBox")]
    public void CheckOverlapBox()
    {
        collis = new List<ArColli>();
        BoxCollider box = GetComponent<BoxCollider>();
        if (box == null) { if (isDebug) Debug.Log("Box null!!"); return; }
        Collider[] cols = Physics.OverlapBox(transform.position, box.size / 2f);
        foreach (Collider col in cols) Add(col.gameObject);
    }

    void Add(GameObject go)
    {
        int indexAr = collis.FindIndex((x) => x.layerName == LayerMask.LayerToName(go.layer));
        if (indexAr == -1)
        {
            ArColli arColli = new ArColli();
            arColli.layerName = LayerMask.LayerToName(go.layer);
            arColli.trList.Add(go);
            collis.Add(arColli);
        }
        else
        {
            int indexGO = collis[indexAr].trList.FindIndex((x) => x == go);
            if (indexGO == -1)
            {
                collis[indexAr].trList.Add(go);
                if (onchange_collis != null) onchange_collis.Invoke();
            }
        }
        collisCount = collis.Count;
    }

    void Remove(GameObject go)
    {
        int indexAr = collis.FindIndex((x) => x.layerName == LayerMask.LayerToName(go.layer));
        if (indexAr == -1) return;
        else
        {
            int indexGO = collis[indexAr].trList.FindIndex((x) => x == go);
            if (indexGO == -1) return;
            else
            {
                collis[indexAr].trList.RemoveAt(indexGO);
                if (onchange_collis != null) onchange_collis.Invoke();
            }
        }
        collisCount = collis.Count;
    }
}
