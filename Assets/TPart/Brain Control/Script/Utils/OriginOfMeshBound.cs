﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OriginOfMeshBound : MonoBehaviour
{
    public bool isDebug = false;

    [Header("Params")]
    public MeshFilter meshFilter = null;

    [Header("Center")]
    public bool centerAtStart = false;

    private void Start()
    {
        if (centerAtStart) Center();
    }

    [ContextMenu("Center")]
    public void Center()
    {
        transform.position = meshFilter.transform.TransformPoint(meshFilter.sharedMesh.bounds.center);
        if (isDebug) Debug.Log("OriginOfMeshBound " + transform.position);
    }
}
