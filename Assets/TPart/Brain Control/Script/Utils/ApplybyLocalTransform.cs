﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplybyLocalTransform : MonoBehaviour
{
    [Header("Note")]
    public string note = string.Empty;

    [Header("Position")]
    public Vector3 applyPos = Vector3.zero;

    [ContextMenu("GetCurrent")]
    public void GetCurrent()
    {
        GetCurrentPos();
        GetCurrentRot();
        GetCurrentSca();
    }

    [ContextMenu("GetCurrentPos")]
    public void GetCurrentPos()
    {
        applyPos = transform.localPosition;        
    }

    [ContextMenu("GetCurrentRot")]
    public void GetCurrentRot()
    {
        applyRotQuad = transform.localRotation;
        applyRotEuler = transform.localRotation.eulerAngles;
    }

    [ContextMenu("GetCurrentPos")]
    public void GetCurrentSca()
    {
        applyScale = transform.localScale;
    }

    [ContextMenu("applyAll")]
    public void applyAll()
    {
        Debug.Log("Đã bỏ");
        //applyPosition();
        //applyRotation_byQuaternion();     
    }

    [ContextMenu("ApplyPosition")]
    public void ApplyPosition()
    {
        transform.localPosition = applyPos;
    }

    [Header("Rotation")]
    public Quaternion applyRotQuad = Quaternion.identity;
    public Vector3 applyRotEuler = Vector3.zero;

    [ContextMenu("ApplyRotation_byQuaternion")]
    public void ApplyRotation_byQuaternion()
    {
        transform.localRotation = applyRotQuad;
    }

    [ContextMenu("ApplyRotation_byEuler")]
    public void ApplyRotation_byEuler()
    {
        transform.localRotation = Quaternion.Euler(applyRotEuler);
    }

    [ContextMenu("DebugPosition")]
    public void DebugPosition()
    {
        Debug.Log("Local Pos " + transform.localPosition);
    }

    [ContextMenu("DebugQuaternion")]
    public void DebugQuaternion()
    {
        Debug.Log("Local Quad " + transform.localRotation);
    }

    [ContextMenu("DebugEuler")]
    public void DebugEuler()
    {
        Debug.Log("Local Euler " + transform.localRotation.eulerAngles);
    }

    [ContextMenu("ResetParams")]
    public void ResetParams()
    {
        applyPos = Vector3.zero;
        applyRotQuad = Quaternion.identity;
        applyRotEuler = Vector3.zero;
        applyScale = Vector3.one;
    }

    [Header("Scale")]
    public Vector3 applyScale = Vector3.one;

    [ContextMenu("ApplyScale")]
    public void ApplyScale()
    {
        transform.localScale = applyScale;
    }
}
