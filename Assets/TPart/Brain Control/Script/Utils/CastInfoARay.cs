﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CastInfoARay : MonoBehaviour
{
    public Transform from = null;
    public Transform to = null;

    [Header("Input")]
    public string layermask = string.Empty;

    [Header("Output")]
    public bool boolHit = false;

    [Header("Local")]
    [SerializeField] LayerMask lm = -1;

    [ContextMenu("Cast")]
    public void Cast()
    {        
        if (from != null && to != null)
        {
            lm = LayerMask.GetMask(new string[] { layermask });
            RaycastHit hit;
            boolHit = Physics.Raycast(new Ray(from.position, to.position - from.position), out hit, 9999f, lm);
            if (boolHit) { Debug.Log("hit " + hit.transform.name); }
            else { Debug.Log("eo hit"); }
        }
    }
}
