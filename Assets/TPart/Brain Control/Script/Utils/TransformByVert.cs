﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformByVert : MonoBehaviour
{
    [Header("Params")]
    public int index = -1;
    public MeshFilter mf = null;

    [ContextMenu("Go")]
    public void Go()
    {
        if (index == -1) return;
        Transform tr = mf.transform;
        Vector3[] verts = Application.isPlaying ? mf.mesh.vertices : mf.sharedMesh.vertices;
        transform.position = tr.TransformPoint(verts[index]);
    }
}
