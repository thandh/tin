﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationFollow2Dir : MonoBehaviour
{
    [Header("Params1")]
    public Transform from1 = null;
    public Transform to1 = null;

    [Header("Params2")]
    public Transform from2 = null;
    public Transform to2 = null;
    
    [ContextMenu("Snap_Forward_Upward")]
    public void Snap_Forward_Upward()
    {
        transform.rotation = Quaternion.LookRotation(to1.position - from1.position, to2.position - from2.position);        
    }
}
