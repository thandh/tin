﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace tMeshIO
{
    public class autoXMeshIO : MonoBehaviour
    {
        [Header("Inputs")]
        public MeshFilter mf = null;
        [SerializeField]
        Mesh _m = null;
        Mesh m
        {
            get
            {
                if (_m == null)
                {
                    if (mf != null) _m = Application.isPlaying ? mf.mesh : mf.sharedMesh;
                }
                return _m;
            }

            set { _m = value; }
        }

        [ContextMenu("ResetMesh")]
        public void ResetMesh()
        {
            _m = null;
        }

        [ContextMenu("SyncMesh")]
        public void SyncMesh()
        {
            if (mf != null) m = mf.sharedMesh;
        }

        public MeshOut mo = null;

        [Header("Params")]
        public Vector3 origin = Vector3.zero;
        public Transform trOrigin = null;

        public int iPos0 = -1;
        public int iPos1 = -1;
        public int iPos2 = -1;

        [ContextMenu("Reset0")]
        public void Reset0()
        {
            iPos0 = -1;
            iPos1 = -1;
            iPos2 = -1;
            origin = Vector3.zero;

            ResetMesh();
            SyncMesh();
        }

        [Header("GetParams")]
        public bool doneGetParams = true;

        [ContextMenu("GetParams")]
        public void GetParams()
        {
            StartCoroutine(C_GetParams());
        }

        IEnumerator C_GetParams()
        {
            Debug.Log("start C_GetParams");

            doneGetParams = false;
            float time = Time.time;

            origin = Vector3.zero;
            iPos0 = -1; iPos1 = -1; iPos2 = -1;

            Vector3[] vertsWorld = new Vector3[m.vertices.Length];
            vertsWorld = m.vertices;

            Vector3 worldPos0 = transform.TransformPoint(m.vertices[0]);
            Vector3 size = mf.GetComponent<MeshRenderer>().bounds.size;
            Vector3 midPlane0 = worldPos0 + new Vector3(size.x, 0, 0); float minDisToPlane0 = float.MaxValue; int iPlane0 = -1;
            Vector3 midPlane1 = worldPos0 + new Vector3(-size.x, 0, 0); float minDisToPlane1 = float.MaxValue; int iPlane1 = -1;
            Vector3 midPlane2 = worldPos0 + new Vector3(0, size.y, 0); float minDisToPlane2 = float.MaxValue; int iPlane2 = -1;
            Vector3 midPlane3 = worldPos0 + new Vector3(0, -size.y, 0); float minDisToPlane3 = float.MaxValue; int iPlane3 = -1;
            Vector3 midPlane4 = worldPos0 + new Vector3(0, 0, size.z); float minDisToPlane4 = float.MaxValue; int iPlane4 = -1;
            Vector3 midPlane5 = worldPos0 + new Vector3(0, 0, -size.z); float minDisToPlane5 = float.MaxValue; int iPlane5 = -1;

            for (int i = 0; i < vertsWorld.Length; i++)
            {
                Vector3 worldPosI = transform.TransformPoint(m.vertices[i]);

                if (minDisToPlane0 > (midPlane0 - worldPosI).magnitude) { iPlane0 = i; minDisToPlane0 = (midPlane0 - worldPosI).magnitude; }
                if (minDisToPlane1 > (midPlane1 - worldPosI).magnitude) { iPlane1 = i; minDisToPlane1 = (midPlane1 - worldPosI).magnitude; }
                if (minDisToPlane2 > (midPlane2 - worldPosI).magnitude) { iPlane2 = i; minDisToPlane2 = (midPlane2 - worldPosI).magnitude; }
                if (minDisToPlane3 > (midPlane3 - worldPosI).magnitude) { iPlane3 = i; minDisToPlane3 = (midPlane3 - worldPosI).magnitude; }
                if (minDisToPlane4 > (midPlane4 - worldPosI).magnitude) { iPlane4 = i; minDisToPlane4 = (midPlane4 - worldPosI).magnitude; }
                if (minDisToPlane5 > (midPlane5 - worldPosI).magnitude) { iPlane5 = i; minDisToPlane5 = (midPlane5 - worldPosI).magnitude; }
            }

            List<int> iList = new List<int>() { iPlane0, iPlane1, iPlane2, iPlane3, iPlane4, iPlane5 };

            float bestArea = 0f;

            for (int i = 0; i < iList.Count - 2; i++)
            {
                for (int j = i + 1; j < iList.Count - 1; j++)
                {
                    for (int k = j + 1; k < iList.Count; k++)
                    {
                        float area = Multiple3 (transform.TransformPoint(vertsWorld[iList[i]]),
                            transform.TransformPoint(vertsWorld[iList[j]]),
                            transform.TransformPoint(vertsWorld[iList[k]]));
                        if (bestArea < area)
                        {
                            bestArea = area;
                            iPos0 = iList[i]; iPos1 = iList[j]; iPos2 = iList[k];
                        }
                    }
                }
            }

            Debug.Log("start C_MeshOut");
            mo.meshOut(vertsWorld, new tMesh(vertsWorld).toJson());
            Debug.Log("yielding C_MeshOut");
            yield return new WaitUntil(() => mo.donemeshOut == true);
            Debug.Log("done C_MeshOut");

            tMesh tMesh = JsonUtility.FromJson<tMesh>(mo.textAsset.text);
            Debug.Log("MeshOut from : " + vertsWorld.Length + " verts to " + tMesh.verts.list.Count + " verts");

            doneGetParams = true;
            Debug.Log("done C_GetParams in " + (Time.time - time));

            yield break;
        }

        float Multiple3(Vector3 p1, Vector3 p2, Vector3 p3)
        {
            return ((p1 - p2).magnitude * (p1 - p3).magnitude * (p2 - p3).magnitude);
        }

        [ContextMenu("OutputToMO")]
        public void OutputToMO()
        {
            mo.currentVertsWorld = new Vector3[m.vertices.Length];
            mo.currentOutMesh = new tMesh(mo.currentVertsWorld).toJson();
        }

        List<int> arrangedList_ItoPlane(int i0, int i1, int i2, int i3, int i4, int i5,
            float dis0, float dis1, float dis2, float dis3, float dis4, float dis5)
        {
            List<int> listIndex = new List<int> { i0, i1, i2, i3, i4, i5 };
            List<float> listDistance = new List<float> { dis0, dis1, dis2, dis3, dis4, dis5 };

            for (int i = 0; i < listIndex.Count - 1; i++)
                for (int j = i + 1; j < listIndex.Count; j++)
                {
                    if (listDistance[i] < listDistance[j])
                    {
                        int itemp = listIndex[i]; float dtemp = listDistance[i];
                        listIndex[i] = listIndex[j]; listDistance[i] = listDistance[j];
                        listIndex[j] = itemp; listDistance[j] = dtemp;
                    }
                }

            return listIndex;
        }

        [Header("Params View Only")]
        [SerializeField]
        string _currentFolder = string.Empty;
        string currentFolder { get { if (_currentFolder != Head.Instance.caseInfo.GetCurrentCaseFolder()) _currentFolder = Head.Instance.caseInfo.GetCurrentCaseFolder(); return _currentFolder; } }

        [ContextMenu("MeshIn")]
        public void MeshIn()
        {
            StreamReader sr = new StreamReader(currentFolder + mo.filename);
            //tMesh tMesh = JsonUtility.FromJson<tMesh>(mo.textAsset.text);
            tMesh tMesh = JsonUtility.FromJson<tMesh>(sr.ReadToEnd());
            sr.Close();

            Debug.Log("MeshIn : " + tMesh.verts.list.Count + " verts");

            Vector3[] vertsLocal = tMesh.GetVerts();
            for (int i = 0; i < vertsLocal.Length; i++)
            {
                vertsLocal[i] = transform.InverseTransformPoint(vertsLocal[i]);
            }

            m.vertices = vertsLocal;
            m.RecalculateBounds();
        }

        [ContextMenu("CalculateOrigin")]
        public void CalculateOrigin()
        {
            Vector3[] vertsWorld = new Vector3[m.vertices.Length];

            for (int i = 0; i < vertsWorld.Length; i++)
            {
                vertsWorld[i] = transform.TransformPoint(m.vertices[i]);
                origin += vertsWorld[i];
            }

            origin /= vertsWorld.Length;
        }

        [ContextMenu("PasteToOrigin")]
        public void PasteToOrigin()
        {
            trOrigin.position = origin;
        }

        [Header("Delete")]
        public bool doneDelete = true;

        [ContextMenu("DeleteCurrentFile")]
        public void DeleteCurrentFile()
        {
            StartCoroutine(C_DeleteCurrentFile());
        }

        IEnumerator C_DeleteCurrentFile()
        {
            doneDelete = false;

            if (File.Exists(currentFolder + mo.filename)) File.Delete(currentFolder + mo.filename);
            else Debug.Log(currentFolder + mo.filename + " not existed");

            yield return new WaitUntil(() => File.Exists(currentFolder + mo.filename) == false);

            doneDelete = true;

            yield break;
        }
    }
}