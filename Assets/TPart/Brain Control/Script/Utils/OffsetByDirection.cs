﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OffsetByDirection : MonoBehaviour
{
    public Transform trPoint = null;
    public Direction dir = null;
    public float d = 0f;

    [ContextMenu("Offset")]
    public void Offset()
    {
        transform.position = trPoint.position + dir.GetDir().normalized * d;
    }
}
