﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformMethod : MonoBehaviour
{
    [ContextMenu("ResetLocal")]
    public void ResetLocal()
    {
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        transform.localScale = Vector3.one;
    }
}
