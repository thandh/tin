﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TINFileOutput : FileOutput
{
    [Header("Params View Only")]
    string _currentFolder = string.Empty;
    public override string currentFolder { get { if (_currentFolder != Head.Instance.caseInfo.GetCurrentCaseFolder()) _currentFolder = Head.Instance.caseInfo.GetCurrentCaseFolder(); return _currentFolder; } }

    [ContextMenu("DebugCurrentFolder")]
    public override void DebugCurrentFolder()
    {
        Debug.Log("Current Folder : " + this.currentFolder);
    }
}
