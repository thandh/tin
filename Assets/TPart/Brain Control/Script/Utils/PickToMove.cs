﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PickToMove : MonoBehaviour, IDragHandler, IEndDragHandler
{
    public float step = 0.1f;

    public void OnDrag(PointerEventData eventData)
    {
        Debug.Log("OnDrag");
        if (Input.GetMouseButton(1)) return;

        if (!Input.GetKey(KeyCode.LeftShift))
            transform.position += new Vector3(-eventData.delta.x * step, -eventData.delta.y * step, 0);
        else
            transform.position += new Vector3(0,0, (eventData.delta.x + eventData.delta.y) * step);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Debug.Log("OnEndDrag");
    }
}
