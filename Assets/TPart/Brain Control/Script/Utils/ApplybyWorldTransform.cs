﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplybyWorldTransform : MonoBehaviour
{
    [Header("Note")]
    public string note = string.Empty;

    [Header("Position")]
    public Vector3 applyPos = Vector3.zero;    

    [ContextMenu("applyAll")]
    public void applyAll()
    {
        Debug.Log("Đã bỏ");
        //applyPosition();
        //applyRotation_byQuaternion();     
    }

    [ContextMenu("applyPosition")]
    public void applyPosition()
    {
        transform.position = applyPos;
    }

    [Header("Rotation")]
    public Quaternion applyRotQuad = Quaternion.identity;
    public Vector3 applyRotEuler = Vector3.zero;

    [ContextMenu("applyRotation_byQuaternion")]
    public void applyRotation_byQuaternion()
    {
        transform.rotation = applyRotQuad;
    }

    [ContextMenu("applyRotation_byEuler")]
    public void applyRotation_byEuler()
    {
        transform.rotation = Quaternion.Euler(applyRotEuler);
    }

    [ContextMenu("DebugPosition")]
    public void DebugPosition()
    {
        Debug.Log("World Pos " + transform.position);
    }

    [ContextMenu("DebugQuaternion")]
    public void DebugQuaternion()
    {
        Debug.Log("World Quad " + transform.rotation);
    }

    [ContextMenu("DebugEuler")]
    public void DebugEuler()
    {
        Debug.Log("World Euler " + transform.rotation.eulerAngles);
    }

    [ContextMenu("GetCurrent")]
    public void GetCurrent()
    {
        applyPos = transform.position;
        applyRotQuad = transform.rotation;
        applyRotEuler = transform.rotation.eulerAngles;
    }
}
