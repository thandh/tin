﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class AutoTranslateTemplate : MonoBehaviour
{
    public string templateDefine = string.Empty;

    public float stepPos = 0.1f;
    public float stepRot = 0.1f;
    public int stepTime = 10;
    public float stepDelay = 0.1f;

    public Vector3 beginPos = -Vector3.one;
    public Vector3 endPos = -Vector3.one;

    public Quaternion beginRot = Quaternion.identity;
    public Quaternion endRot = Quaternion.identity;

    AutoTranslate auto = null;

    private void Start()
    {
        
    }

    [ContextMenu("Copy")]
    public void Copy()
    {
        if (auto == null) auto = GetComponent<AutoTranslate>();
        stepPos = auto.stepPos;
        stepRot = auto.stepRot;
        stepTime = auto.stepTime;
        stepDelay = auto.stepDelay;
        beginPos = auto.beginPos;
        endPos = auto.endPos;
        beginRot = auto.beginRot;
        endRot = auto.endRot;
    }

    [ContextMenu("Paste")]
    public void Paste()
    {
        if (auto == null) auto = GetComponent<AutoTranslate>();
        auto.stepPos = stepPos;
        auto.stepRot = stepRot;
        auto.stepTime = stepTime;
        auto.stepDelay = stepDelay;
        auto.beginPos = beginPos;
        auto.endPos = endPos;
        auto.beginRot = beginRot;
        auto.endRot = endRot;
    }

    [ContextMenu("SetBeginFromFollow")]
    public void SetBeginFromFollow()
    {
        GetComponent<Follow>().SnapToTarget();
        beginPos = transform.position;
        beginRot = transform.rotation;

        Debug.Log(beginPos);
        Debug.Log(beginRot);
    }

    [ContextMenu("SetEndFromFollow")]
    public void SetEndFromFollow()
    {
        GetComponent<Follow>().SnapToTarget();
        endPos = transform.position;
        endRot = transform.rotation;
    }

    [ContextMenu("SnapBegin")]
    public void SnapBegin()
    {
        transform.position = beginPos;
        transform.rotation = beginRot;
    }

    [ContextMenu("SnapEnd")]
    public void SnapEnd()
    {
        transform.position = endPos;
        transform.rotation = endRot;
    }

    [ContextMenu("GetBeginByCurrent")]
    public void GetBeginByCurrent()
    {
        beginPos = transform.position;
        beginRot = transform.rotation;
    }

    [ContextMenu("GetEndByCurrent")]
    public void GetEndByCurrent()
    {
        endPos = transform.position;
        endRot = transform.rotation;
    }
}
