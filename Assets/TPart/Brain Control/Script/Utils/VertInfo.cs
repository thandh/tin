﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VertInfo : MonoBehaviour
{
    [Header("Params")]
    public int index = -1;
    public MeshFilter mf = null;

    [ContextMenu("DebugIndex")]
    public void DebugIndex()
    {
        if (index == -1) return;
        Transform tr = mf.transform;
        Vector3[] verts = Application.isPlaying ? mf.mesh.vertices : mf.sharedMesh.vertices;
        Debug.Log("world pos : " + tr.TransformPoint(verts[index]));
    }

    [ContextMenu("DebugAll")]
    public void DebugAll()
    {
        Vector3[] verts = Application.isPlaying ? mf.mesh.vertices : mf.sharedMesh.vertices;
        int[] tris = Application.isPlaying ? mf.mesh.triangles : mf.sharedMesh.triangles;
        Debug.Log("Total verts : " + verts.Length);
        Debug.Log("Total tris : " + tris.Length);
    }
}
