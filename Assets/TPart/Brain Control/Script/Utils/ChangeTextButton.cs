﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class ChangeTextButton : MonoBehaviour
{
    [Header("ChangeText")]
    public List<Button> btnList = new List<Button>();
    public string btnString = "";

    [ContextMenu("SearchIt")]
    public void SearchIt()
    {
        btnList = FindObjectsOfType<Button>().Where(btn => btn.transform.name == btnString).ToList();
    }

    [Header("ChangeText Params")]    
    public string btnText = "";

    [ContextMenu("ChangeIt")]
    public void ChangeIt()
    {
        foreach (Button btn in btnList) btn.GetComponentInChildren<Text>().text = btnText;
    }
}
