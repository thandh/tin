﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class FileOutput : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Params")]
    public string filename = string.Empty;        
    public virtual string currentFolder { get; set; }

    [ContextMenu("DebugCurrentFolder")]
    public virtual void DebugCurrentFolder()
    {
        Debug.Log("Current Folder : " + this.currentFolder);
    }

    [Header("Get Params")]
    public bool doneOutFile = true;
    [HideInInspector] public string data = string.Empty;

    [ContextMenu("DebugCurrentData")]
    public virtual void DebugCurrentData()
    {
        Debug.Log("Current data : " + this.data);
    }

    [ContextMenu("ResetParams")]
    public void ResetParams()
    {
        data = string.Empty;
    }

    [Header("Empty")]
    public bool doneEmpty = true;

    [ContextMenu("Empty")]
    public void Empty()
    {
        StartCoroutine(C_Empty());
    }

    IEnumerator C_Empty()
    {
        if (isDebug) Debug.Log("Start Empty !!");

        if (isDebug && string.IsNullOrEmpty(filename)) { Debug.Log("filename empty"); yield break; }

        if (!File.Exists(currentFolder + filename))
        {
            File.Create(currentFolder + filename);
            yield return new WaitForSeconds(2f);
        }

        yield return new WaitUntil(() => Existed() == true);

        doneEmpty = false;

        File.WriteAllText(currentFolder + filename, string.Empty);
        yield return new WaitUntil(() => IsFileLocked(new FileInfo(currentFolder + filename)) == false);

        this.data = string.Empty;

        doneEmpty = true;
        if (isDebug) Debug.Log("Done Empty");

        yield break;
    }

    public bool Existed()
    {
        if (isDebug) Debug.Log("Checking existed : " + currentFolder + filename);
        return File.Exists(currentFolder + filename);
    }

    bool IsFileLocked(FileInfo file)
    {
        FileStream stream = null;

        try
        {
            stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
        }
        catch (IOException)
        {
            return true;
        }
        finally
        {
            if (stream != null)
                stream.Close();
        }

        return false;
    }

    [ContextMenu("outCurrentData")]
    public void outCurrentData()
    {
        StartCoroutine(C_outFile(this.data));
    }

    public void outFile(string output)
    {
        StartCoroutine(C_outFile(output));
    }

    IEnumerator C_outFile(string output)
    {
        doneOutFile = false;

        if (isDebug) Debug.Log("start C_outFile : " + currentFolder + filename);        

        if (!Existed())
        {
            if (isDebug) Debug.Log("No File, empty it!!" + currentFolder + filename);
            Empty();

            if (isDebug) Debug.Log("Start yield => doneEmpty == true);");
            yield return new WaitUntil(() => doneEmpty == true);
            if (isDebug) Debug.Log("Done yield => doneEmpty == true);");

            if (isDebug) Debug.Log("Start yield IsFileLocked() == false);");
            yield return new WaitUntil(() => IsFileLocked(new FileInfo(currentFolder + filename)) == false);
            if (isDebug) Debug.Log("Done yield IsFileLocked() == false);");
        }

        if (isDebug) Debug.Log("start yield return new WaitUntil(() => IsFileLocked()");
        yield return new WaitUntil(() => IsFileLocked(new FileInfo(currentFolder + filename)) == false);
        if (isDebug) Debug.Log("done yield return new WaitUntil(() => IsFileLocked()");

        if (isDebug) Debug.Log("data : " + output);
        this.data = output;
        File.WriteAllText(currentFolder + filename, output);        

        if (isDebug) Debug.Log("start yield return new WaitUntil(() => IsFileLocked()");
        yield return new WaitUntil(() => IsFileLocked(new FileInfo(currentFolder + filename)) == false);
        if (isDebug) Debug.Log("done yield return new WaitUntil(() => IsFileLocked()");        

        doneOutFile = true;
        if (isDebug) Debug.Log("done C_outFile");

        yield break;
    }

    [Header("Import")]
    public bool doneImport = true;

    [ContextMenu("Import")]
    public void Import()
    {
        StartCoroutine(C_Import());
    }

    IEnumerator C_Import()
    {
        doneImport = false;

        if (!Existed()) { if (isDebug) Debug.Log("File not existed!"); doneImport = true; yield break; }

        if (isDebug) Debug.Log("start yield return new WaitUntil(() => IsFileLocked()");
        yield return new WaitUntil(() => IsFileLocked(new FileInfo(currentFolder + filename)) == false);
        if (isDebug) Debug.Log("done yield return new WaitUntil(() => IsFileLocked()");

        this.data = File.ReadAllText(currentFolder + filename);

        if (isDebug) Debug.Log(currentFolder + filename + " : " + this.data);
        doneImport = true;

        yield break;
    }
}
