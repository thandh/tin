﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineRendererInfo : MonoBehaviour
{
    [Header("Params")]
    public LineRenderer lr = null;

    [ContextMenu("DebugPoints")]
    public void DebugPoints()
    {
        string strOut = string.Empty;

        for (int i = 0; i < lr.positionCount; i++)
        {
            strOut += lr.GetPosition(i);
        }

        Debug.Log(strOut);
    }
}
