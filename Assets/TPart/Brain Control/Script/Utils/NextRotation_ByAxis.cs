﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextRotation_ByAxis : MonoBehaviour, INextStep
{
    [Header("Debug")]
    public bool isDebug = false;
    
    public enum eAxis { x, y, z }
    [Header("Input")]
    public eAxis eaxis = eAxis.z;

    public Vector3 v3Axis(eAxis eaxis)
    {
        Vector3 v3axis = Vector3.zero;
        switch (eaxis)
        {
            
        }
        return v3axis;
    }

    [Header("Params")]
    public float d = 1f;

    [ContextMenu("StepForward")]
    public void StepForward()
    {        
        
    }

    [ContextMenu("StepBackward")]
    public void StepBackward()
    {
        
    }
}
