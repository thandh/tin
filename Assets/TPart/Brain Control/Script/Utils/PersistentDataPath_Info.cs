﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersistentDataPath_Info : MonoBehaviour
{
    [ContextMenu("DebugPath")]
    public void DebugPath()
    {
        Debug.Log("Path : " + Application.persistentDataPath);
    }	
}
