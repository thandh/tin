﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LookQuaternion : MonoBehaviour
{
    public bool isDebug = false;

    [Header("Note")]
    public string note = string.Empty;

    [Header("look Quaternion")]
    public DirectionType forwardDir = new DirectionType(eDirType.forward);
    public DirectionType upDir = new DirectionType(eDirType.up);

    [ContextMenu("DoLook")]
    public void DoLook()
    {
        if (forwardDir.type == upDir.type) if (isDebug) { Debug.Log("forwardDir.type == upDir.type"); return; }
        transform.rotation = Quaternion.LookRotation(forwardDir.GetDir(), upDir.GetDir());
    }
}
