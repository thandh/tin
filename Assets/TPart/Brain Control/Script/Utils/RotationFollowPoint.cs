﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationFollowPoint : MonoBehaviour
{
    [Header("Params")]
    public Vector3 from = Vector3.zero;
    public Vector3 to = Vector3.zero;

    [ContextMenu("Snap x-axis")]
    public void SnapXAxis()
    {
        transform.right = to - from;
    }
}
