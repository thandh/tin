﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface INextStep
{
    void StepForward();
    void StepBackward();
}
