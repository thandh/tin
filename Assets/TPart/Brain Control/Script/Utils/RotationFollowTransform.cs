﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationFollowTransform : MonoBehaviour
{
    [Header("Params")]
    public Transform from = null;
    public Transform to = null;

    [ContextMenu("Snap x-axis")]
    public void SnapXAxis()
    {
        transform.right = to.position - from.position;
    }

    [ContextMenu("Snap y-axis")]
    public void SnapYAxis()
    {
        transform.up = to.position - from.position;
    }

    [ContextMenu("Snap z-axis")]
    public void SnapZAxis()
    {
        transform.forward = to.position - from.position;
    }
}
