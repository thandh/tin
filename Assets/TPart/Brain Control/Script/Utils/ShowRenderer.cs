﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowRenderer : MonoBehaviour
{
    [Header("note")]
    public string note = string.Empty;

    [Header("Inputs")]
    public List<Renderer> renList = new List<Renderer>();

    [ContextMenu("Show")]
    public void Show()
    {
        foreach (Renderer r in renList) if (r != null) r.enabled = true;
    }

    [ContextMenu("Shut")]
    public void Shut()
    {
        foreach (Renderer r in renList) if (r != null) r.enabled = false;
    }
}
