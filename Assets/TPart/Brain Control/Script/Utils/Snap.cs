﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Snap : MonoBehaviour
{
    [Header("note")]
    public string note = string.Empty;

    [Header("Params")]
    public Transform target = null;

    [ContextMenu("SnapToTarget")]
    public void SnapToTarget()
    {
        transform.position = target.position;
        transform.rotation = target.rotation;
    }

    [ContextMenu("SnapPosition")]
    public void SnapPosition()
    {
        transform.position = target.position;
    }

    [ContextMenu("SnapRotation")]
    public void SnapRotation()
    {
        transform.rotation = target.rotation;
    }

    [ContextMenu("DebugAll")]
    public void DebugAll()
    {
        Debug.Log("world pos : " + transform.position);
        Debug.Log("world rot : " + transform.rotation);
        Debug.Log("world eul : " + transform.rotation.eulerAngles);
        Debug.Log("world sca : " + transform.lossyScale);
        Debug.Log("local pos : " + transform.localPosition);
        Debug.Log("local rot : " + transform.localRotation);
        Debug.Log("local eul : " + transform.localRotation.eulerAngles);
        Debug.Log("local sca : " + transform.localScale);
    }
}
