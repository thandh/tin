﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextStep_ByTransformNRigid : MonoBehaviour, INextStep
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Input")]
    public Transform trStart = null;
    public Transform trEnd = null;
    [SerializeField] Rigidbody _rb = null;
    public Rigidbody rb { get { if (_rb == null) _rb = GetComponent<Rigidbody>(); return _rb; }}

    [Header("Params")]
    public float f = 100f;

    [ContextMenu("StepForward")]
    public void StepForward()
    {        
        rb.AddForce((trEnd.position - trStart.position).normalized * f);
    }

    [ContextMenu("StepBackward")]
    public void StepBackward()
    {
        rb.AddForce(-(trEnd.position - trStart.position).normalized * f);
    }

    [Header("Slerp")]
    public bool doneSlerp = true;
    public float delaySlerp = 0.02f;
    public float deltaDis = 0.01f;

    [ContextMenu("SlerpForward")]
    public void SlerpForward()
    {
        if (!doneSlerp) { if (isDebug) Debug.Log("Not done Slerp!!"); return; }
        StartCoroutine(C_Slerp(transform.position + (trEnd.position - trStart.position).normalized * f));
    }

    [ContextMenu("SlerpBackward")]
    public void SlerpBackward()
    {
        if (!doneSlerp) { if (isDebug) Debug.Log("Not done Slerp!!"); return; }
        StartCoroutine(C_Slerp(transform.position - (trEnd.position - trStart.position).normalized * f));
    }    

    IEnumerator C_Slerp(Vector3 pos)
    {
        doneSlerp = false;
        while ((transform.position - pos).magnitude > deltaDis)
        {
            transform.position = Vector3.Lerp(transform.position, pos, Time.deltaTime);
            yield return new WaitForSeconds(delaySlerp);
        }
        doneSlerp = true;
        yield break;
    }

    [ContextMenu("Stop")]
    public void Stop()
    {
        StopAllCoroutines();
    }
}
