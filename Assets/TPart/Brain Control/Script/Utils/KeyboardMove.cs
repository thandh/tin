﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class KeyboardMove : MonoBehaviour
{
    public enum Type
    {
        Transform,
        CameraMain
    }

    public KeyboardMove.Type type = Type.CameraMain;

    public bool Allow = true;

    public float step = 1f;

    private void Update()
    {
        if (!Allow) return;
        if (!Input.anyKey) return;

        Transform trForward = null;

        switch (type)
        {
            case Type.Transform: trForward = transform; break;
            case Type.CameraMain: trForward = Camera.main.transform; break;
        }

        if (Input.GetKey(KeyCode.UpArrow) && Input.GetKey(KeyCode.LeftShift))
        {
            transform.position -= trForward.right.normalized * step;
        }

        if (Input.GetKey(KeyCode.DownArrow) && Input.GetKey(KeyCode.LeftShift))
        {
            transform.position += trForward.right.normalized * step;
        }

        if (Input.GetKey(KeyCode.UpArrow) && !Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.LeftControl))
        {
            transform.position -= trForward.up.normalized * step;
        }

        if (Input.GetKey(KeyCode.DownArrow) && !Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.LeftControl))
        {
            transform.position += trForward.up.normalized * step;
        }

        if (Input.GetKey(KeyCode.LeftArrow) && !Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.LeftControl))
        {
            transform.position += trForward.forward.normalized * step;
        }

        if (Input.GetKey(KeyCode.RightArrow) && !Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.LeftControl))
        {
            transform.position -= trForward.forward.normalized * step;
        }

        if (Input.GetKey(KeyCode.UpArrow) && Input.GetKey(KeyCode.LeftControl))
        {
            transform.Rotate(trForward.forward * step);
        }

        if (Input.GetKey(KeyCode.DownArrow) && Input.GetKey(KeyCode.LeftControl))
        {
            //transform.Rotate(trForward. * step);
        }

        if (Input.GetKey(KeyCode.LeftArrow) && Input.GetKey(KeyCode.LeftControl))
        {
            transform.Rotate(Vector3.up * step);
        }

        if (Input.GetKey(KeyCode.RightArrow) && Input.GetKey(KeyCode.LeftControl))
        {
            transform.Rotate(Vector3.down * step);
        }
    }
}
