﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class DrawARay : MonoBehaviour
{
    public bool useUpdate = false;

    public Transform from = null;
    public Transform to = null;
        
    public Color color = Color.green;

    private void Update()
    {
        if (!useUpdate) return;

        if (from != null && to != null)
        {
            Debug.DrawRay(from.position, to.position - from.position, color);
        }
    }
}
