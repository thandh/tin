﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public delegate void delBefore_UseRayToMove();
public delegate void delAfter_UseRayToMove();

public class UseRayToMove : MonoBehaviour
{
    public bool isLock = true;

    public LayerMask maskChild = -99;

    public Transform lastHit = null;
    public Vector3 lastPos = -Vector3.one;
    public Vector3 lastMousePos = -Vector3.one;

    GameObject guidance = null;
    public float stepPos = 0.5f;

    public delBefore_UseRayToMove delBefore = null;
    public delBefore_UseRayToMove delAfter = null;

    private void Update()
    {
        if (isLock) return;

        if (Input.GetMouseButton(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Debug.DrawRay(ray.origin, ray.direction * Mathf.Infinity);

            if (lastHit == null && lastPos == -Vector3.one && lastMousePos == -Vector3.one)
            {
                if (Physics.Raycast(ray, out hit, Mathf.Infinity, maskChild.value))
                {
                    if (lastHit == null)
                    {
                        //cmc.IsOn = false;

                        lastHit = hit.transform;
                        lastPos = hit.transform.position;
                        lastMousePos = ray.origin;

                        guidance = new GameObject();
                        guidance.transform.position = lastPos;
                        return;
                    }
                }
            }
            else if (lastHit != null && lastPos != -Vector3.one && lastMousePos != -Vector3.one)
            {
                //keyboardController(lastHit);                

                Vector3 reflect = (ray.origin - lastMousePos) * (lastPos - Camera.main.transform.position).magnitude / (lastMousePos - Camera.main.transform.position).magnitude;
                guidance.transform.position = lastPos + reflect;

                float step = Time.deltaTime * stepPos;
                lastHit.transform.position = new Vector3(Mathf.Lerp(lastHit.transform.position.x, guidance.transform.position.x, step),
                    Mathf.Lerp(lastHit.transform.position.y, guidance.transform.position.y, step),
                    Mathf.Lerp(lastHit.transform.position.z, guidance.transform.position.z, step));

                if (delAfter != null) delAfter.Invoke();
            }
        }
        else
        {
            if (lastHit == null) return;

            //cmc.IsOn = true;

            lastHit = null;
            lastPos = -Vector3.one;
            lastMousePos = -Vector3.one;
            DestroyImmediate(guidance);
        }
    }
}
