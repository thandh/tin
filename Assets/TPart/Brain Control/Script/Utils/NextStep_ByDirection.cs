﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextStep_ByDirection : MonoBehaviour, INextStep
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Input")]
    public Vector3 direction = Vector3.zero;

    [Header("Params")]
    public float d = 0.1f;

    [ContextMenu("StepForward")]
    public void StepForward()
    {
        transform.Rotate(direction, d);
    }

    [ContextMenu("StepBackward")]
    public void StepBackward()
    {
        transform.Rotate(direction, -d);
    }

    [Header("Rotate")]
    public bool doneRotate = true;
    public float delayRotating = 0.02f;
    public float deltaDis = 0.01f;

    [ContextMenu("SlerpForward")]
    public void SlerpForward()
    {
        if (!doneRotate) { if (isDebug) Debug.Log("Not done Slerp!!"); return; }
        StartCoroutine(C_Slerp(transform.rotation.eulerAngles + direction.normalized * d));
    }

    [ContextMenu("SlerpBackward")]
    public void SlerpBackward()
    {
        if (!doneRotate) { if (isDebug) Debug.Log("Not done Slerp!!"); return; }
        StartCoroutine(C_Slerp(transform.rotation.eulerAngles - direction.normalized * d));
    }

    public IEnumerator C_Slerp(Vector3 rot)
    {
        doneRotate = false;
        while ((transform.eulerAngles - rot).magnitude > deltaDis)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(rot), Time.deltaTime);
            yield return new WaitForSeconds(delayRotating);
        }
        doneRotate = true;
        yield break;
    }

    [ContextMenu("RotateForward")]
    public void RotateForward()
    {
        if (!doneRotate) { if (isDebug) Debug.Log("Not done Rotate!!"); return; }
        StartCoroutine(C_Rotate(transform.eulerAngles + direction.normalized * d));
    }

    [ContextMenu("RotateBackward")]
    public void RotateBackward()
    {
        if (!doneRotate) { if (isDebug) Debug.Log("Not done Rotate!!"); return; }
        StartCoroutine(C_Rotate(transform.eulerAngles - direction.normalized * d));
    }

    public IEnumerator C_Rotate(Vector3 rot)
    {
        doneRotate = false;
        while ((transform.eulerAngles - rot).magnitude > deltaDis)
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(rot), Time.deltaTime);
            yield return new WaitForSeconds(delayRotating);
        }
        doneRotate = true;
        yield break;
    }

    [ContextMenu("Stop")]
    public void Stop()
    {
        doneRotate = true;
        StopAllCoroutines();
    }
}
