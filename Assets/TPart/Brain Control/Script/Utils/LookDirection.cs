﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookDirection : MonoBehaviour
{
    public Transform trOrigin = null;
    public Transform trTarget = null;
    public DirectionType core = new DirectionType(eDirType.forward, null);

    [ContextMenu("Look")]
    public void Look()
    {
        core.SetDir(trTarget.position - trOrigin.position);
    }
}
