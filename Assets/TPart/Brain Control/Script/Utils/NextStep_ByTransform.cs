﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextStep_ByTransform : MonoBehaviour, INextStep
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Input")]
    public Transform trStart = null;
    public Transform trEnd = null;

    [Header("Params")]
    public float d = 1f;

    [ContextMenu("StepForward")]
    public void StepForward()
    {
        transform.position += (trEnd.position - trStart.position).normalized * d;
    }

    [ContextMenu("StepBackward")]
    public void StepBackward()
    {
        transform.position -= (trEnd.position - trStart.position).normalized * d;
    }

    [Header("Moving")]
    public bool doneMoving = true;
    public float delayMoving = 0.02f;
    public float deltaDis = 0.01f;

    [ContextMenu("SlerpForward")]
    public void SlerpForward()
    {
        if (!doneMoving) { if (isDebug) Debug.Log("Not done Slerp!!"); return; }
        StartCoroutine(C_Slerp(transform.position + (trEnd.position - trStart.position).normalized * d));
    }    

    [ContextMenu("SlerpBackward")]
    public void SlerpBackward()
    {
        if (!doneMoving) { if (isDebug) Debug.Log("Not done Slerp!!"); return; }
        StartCoroutine(C_Slerp(transform.position - (trEnd.position - trStart.position).normalized * d));
    }    

    public IEnumerator C_Slerp(Vector3 pos)
    {
        doneMoving = false;
        while ((transform.position - pos).magnitude > deltaDis)
        {
            transform.position = Vector3.Lerp(transform.position, pos, Time.deltaTime);
            yield return new WaitForSeconds(delayMoving);
        }
        doneMoving = true;
        yield break;
    }

    [ContextMenu("MoveForward")]
    public void MoveForward()
    {
        if (!doneMoving) { if (isDebug) Debug.Log("Not done Move!!"); return; }
        StartCoroutine(C_Move(transform.position + (trEnd.position - trStart.position).normalized * d));
    }

    [ContextMenu("MoveBackward")]
    public void MoveBackward()
    {
        if (!doneMoving) { if (isDebug) Debug.Log("Not done Move!!"); return; }
        StartCoroutine(C_Move(transform.position - (trEnd.position - trStart.position).normalized * d));
    }

    public IEnumerator C_Move(Vector3 pos)
    {
        doneMoving = false;
        while ((transform.position - pos).magnitude > deltaDis)
        {
            transform.position = Vector3.MoveTowards(transform.position, pos, Time.deltaTime);
            yield return new WaitForSeconds(delayMoving);
        }
        doneMoving = true;
        yield break;
    }

    [ContextMenu("Stop")]
    public void Stop()
    {
        doneMoving = true;
        StopAllCoroutines();
    }
}
