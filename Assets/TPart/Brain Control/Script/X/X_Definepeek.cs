﻿using System.Collections;
using System.Collections.Generic;
using tMeshIO;
using UnityEngine;

public class X_Definepeek : MonoBehaviour
{
    [Header("X")]
    public Transform peek = null;

    public MeshFilter meshFilter = null;
    public Transform trOrigin = null;
    public Transform trPicaMovementPeek = null;

    public Transform trSkullAtXPeek = null;

    [ContextMenu("DefinePeek")]
    public void DefinePeek()
    {
        float maxDistance = 0;
        Vector3 maxPeek = -Vector3.one;
        Vector3 dirToCerebellum = trPicaMovementPeek.position - trOrigin.position;

        for (int i = 0; i < meshFilter.sharedMesh.vertices.Length; i++)
        {
            Vector3 worldPos = transform.TransformPoint(meshFilter.sharedMesh.vertices[i]);
            Vector3 dir = worldPos - trOrigin.position;
            if (dir.magnitude > maxDistance && Vector3.Dot(dir, dirToCerebellum) > 0)
            {
                maxDistance = dir.magnitude;
                maxPeek = worldPos;
            }
        }

        peek.transform.position = maxPeek;
        peek.rotation = trOrigin.rotation;
    }
}
