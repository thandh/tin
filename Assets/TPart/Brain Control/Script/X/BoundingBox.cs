﻿using UnityEngine;
using System.Collections;

public class BoundingBox : MonoBehaviour
{
    [Header("Input")]
    public MeshFilter meshFilter = null;

    [Header("temp Peeks")]
    public Transform peekX = null;
    public Transform peekNX = null;
    public Transform peekY = null;
    public Transform peekNY = null;
    public Transform peekZ = null;
    public Transform peekNZ = null;    

    [Header("Output")]
    public Vector3 meshCenterBound = Vector3.zero;    

    [ContextMenu("DefinePeek")]
    public void DefinePeek()
    {   
        meshCenterBound = meshFilter.transform.TransformPoint(meshFilter.sharedMesh.bounds.center);

        peekX.position = meshCenterBound + meshFilter.transform.right * meshFilter.sharedMesh.bounds.extents.x;        
        peekNX.position = meshCenterBound + meshFilter.transform.right * -meshFilter.sharedMesh.bounds.extents.x;        
        peekY.position = meshCenterBound + meshFilter.transform.up * meshFilter.sharedMesh.bounds.extents.y;        
        peekNY.position = meshCenterBound + meshFilter.transform.up * -meshFilter.sharedMesh.bounds.extents.y;        
        peekZ.position = meshCenterBound + meshFilter.transform.forward * meshFilter.sharedMesh.bounds.extents.z;        
        peekNZ.position = meshCenterBound + meshFilter.transform.forward * -meshFilter.sharedMesh.bounds.extents.z;        
    }

    [ContextMenu("MeshOnOff")]
    public void MeshOnOff()
    {
        peekX.GetComponent<MeshRenderer>().enabled = !peekX.GetComponent<MeshRenderer>().enabled;
        peekNX.GetComponent<MeshRenderer>().enabled = !peekNX.GetComponent<MeshRenderer>().enabled;
        peekY.GetComponent<MeshRenderer>().enabled = !peekY.GetComponent<MeshRenderer>().enabled;
        peekNY.GetComponent<MeshRenderer>().enabled = !peekNY.GetComponent<MeshRenderer>().enabled;
        peekZ.GetComponent<MeshRenderer>().enabled = !peekZ.GetComponent<MeshRenderer>().enabled;
        peekNZ.GetComponent<MeshRenderer>().enabled = !peekNZ.GetComponent<MeshRenderer>().enabled;
    }

    public Quaternion QuaternionOfTheLongest()
    {
        Vector3 dir = dir = peekNX.position - peekX.position; ;
        Vector3 dirCross = peekNY.position - peekY.position;

        if ((peekNY.position - peekY.position).magnitude > dir.magnitude)
        {
            dir = peekNY.position - peekY.position;
            dirCross = peekNZ.position - peekZ.position;
        }

        if ((peekNZ.position - peekZ.position).magnitude > dir.magnitude)
        {
            dir = peekNZ.position - peekZ.position;
            dirCross = peekNX.position - peekX.position;
        }

        return Quaternion.LookRotation(dir, dirCross);
    }
}