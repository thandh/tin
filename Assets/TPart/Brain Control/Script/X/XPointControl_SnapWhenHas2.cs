﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class XPointControl_SnapWhenHas2 : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Clickable")]
    public bool ClickHere = false;

    [Header("Input")]
    public XPointControl xPointControl = null;

    [Header("Process")]    
    public Snap rTrSnap = null;
    public Direction rTrDirection = null;

    [Header("Local")]
    public Transform cTr = null;
    public Transform c1Tr = null;
    public Transform xTr = null;
    public Transform xTrParent = null;

    [Header("Update to snap Angle")]
    public bool boolUpdate = false;
    public bool boolSnaping = false;

    [Header("Snap angle params")]
    float lastnBit = 0f;

    [SerializeField] float stepRot = 0.1f; 
    [SerializeField] float stepRotBegin = 1f;

    [SerializeField] float deltaDis = 1f;
    [SerializeField] float deltaDisBegin = 1f;

    [SerializeField] float stepRotFinal = 0.01f;
    [SerializeField] float deltaDisFinal = 0.01f;

    [SerializeField] float deltaDelta = 0.01f;

    [SerializeField] int directionMultiple = 1;
    [SerializeField] int countChangeDirection = 0;
    
    public void CheckNGet(Transform aTr, Transform a1Tr, Transform bTr, Transform b1Tr, Transform cTr, Transform c1Tr, Transform xTr)
    {
        List<Direction> dirList = new List<Direction>()
        {
            new Direction(aTr, a1Tr),
            new Direction(bTr, b1Tr),
            new Direction(cTr, c1Tr)
        };

        dirList.Sort((a, b) => a.GetDir().magnitude.CompareTo(b.GetDir().magnitude));

        Transform a2Tr = dirList[0].trOrigin;
        Transform b2Tr = dirList[1].trOrigin;
        Transform c2Tr = dirList[2].trOrigin;

        rTrSnap.target = a2Tr;
        rTrSnap.SnapPosition();

        rTrDirection.trOrigin = a2Tr;
        rTrDirection.trTarget = b2Tr;
        rTrDirection.SnapZ();

        this.cTr = c2Tr;
        this.c1Tr = dirList[2].trTarget;
        this.xTr = xTr;

        xTrParent = xTr.parent;
        xTr.parent = this.rTrSnap.transform;
    }

    [ContextMenu("SnapAngle_Update")]
    public void SnapAngle_Update()
    {        
        lastnBit = 0f;
        stepRot = stepRotBegin;
        deltaDis = deltaDisBegin;
        countChangeDirection = 0;

        boolUpdate = true;
        boolSnaping = true;
    }

    private void Update()
    {
        if (!boolUpdate) return;

        if (boolSnaping)
        {
            if (Mathf.Abs(stepRot - stepRotFinal) < deltaDelta && Mathf.Abs(deltaDis - deltaDisFinal) < deltaDelta)
            {
                if (CC1Distance() < deltaDis)
                {
                    Debug.Log("Done update snaping 2 " + CC1Distance());
                    boolUpdate = false;
                    boolSnaping = false;
                    xPointControl.doneSnap = true;
                    AfterDoneRotate();
                }
            }            

            if (CC1Distance() > deltaDis)
            {
                bool has = TryRotate(stepRot * directionMultiple);
                if (has)
                {
                    RotatenBit(stepRot * directionMultiple);
                    ClickHere = !ClickHere;
                }
                else
                {
                    directionMultiple *= -1;
                    countChangeDirection++;

                    if (countChangeDirection == 2)
                    {
                        boolUpdate = false;
                        boolSnaping = false;
                        xPointControl.doneSnap = true;
                        AfterDoneRotate();
                    }
                }
            }
            else
            {
                if (stepRot != stepRotFinal || deltaDis != deltaDisFinal)
                {
                    stepRot = Mathf.Lerp(stepRot, stepRotFinal, Time.deltaTime);
                    deltaDis = Mathf.Lerp(deltaDis, deltaDisFinal, Time.deltaTime);

                    countChangeDirection = 0;
                }
                else
                {
                    Debug.Log("Done update snaping " + CC1Distance());
                    boolUpdate = false;
                    boolSnaping = false;
                }
            }
                        
        }
    }

    [ContextMenu("AfterDoneRotate")]
    public void AfterDoneRotate()
    {
        xTr.parent = xTrParent;
        xTr.SetAsFirstSibling();
    }

    bool TryRotate(float nBit)
    {
        if (isDebug) Debug.Log("TryRotate nBit : " + nBit);

        Vector3 lastPos = this.rTrSnap.transform.position;
        Quaternion lastQua = this.rTrSnap.transform.rotation;

        float lastDistance = CC1Distance();

        RotatenBit(nBit);

        float currentDistance = CC1Distance();

        //Debug.Log("eulerAngle " + eulerAngle + " last dis " + lastDistance + " current dis " + currentDistance);

        if (!Application.isPlaying)
        {
            this.rTrSnap.transform.position = lastPos;
            this.rTrSnap.transform.rotation = lastQua;
        }

        return currentDistance < lastDistance;
    }

    [ContextMenu("RotateStepBit")]
    public void RotateStepBit()
    {
        rTrSnap.transform.localRotation = rTrSnap.transform.localRotation * Quaternion.AngleAxis(stepRot, rTrSnap.transform.InverseTransformDirection(rTrSnap.transform.forward));
    }

    [ContextMenu("RotateNStepBit")]
    public void RotateNStepBit()
    {
        rTrSnap.transform.localRotation = rTrSnap.transform.localRotation * Quaternion.AngleAxis(-stepRot, rTrSnap.transform.InverseTransformDirection(rTrSnap.transform.forward));
    }

    public void RotatenBit(float nbit)
    {
        rTrSnap.transform.localRotation = rTrSnap.transform.localRotation * Quaternion.AngleAxis(nbit, rTrSnap.transform.InverseTransformDirection(rTrSnap.transform.forward));
    }

        float CC1Distance()
    {
        return (this.cTr.position - this.c1Tr.position).magnitude;
    }

    [ContextMenu("Stop")]
    public void Stop()
    {
        StopAllCoroutines();
        boolUpdate = false;
        boolSnaping = false;        
    }
}
