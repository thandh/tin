﻿using System.Collections;
using System.Collections.Generic;
using tMeshIO;
using UnityEngine;

public class X_DefinePicaMovementX : MonoBehaviour
{
    [Header("Input")]
    public MeshFilter meshFilter = null;
    public Snap picaUpSnap = null;
        
    public Transform trXOrigin = null;
    public RotationFollow2Dir originSnap = null;

    public BoundingBox boundingBox = null;
    public Transform picaMovementPeek = null;
    public Snap picaMovementPeekSnap = null;

    [Header("Output")]
    [SerializeField] float maxDistance = 0f;
    public bool hasResult = false;

    [Header("useCerebellum")]
    public Transform trCerebellumOrigin = null;
    /// <summary>
    /// Sử dụng dot0 vector với cerebellum
    /// </summary>
    [ContextMenu("DefinePicaMovementX_UseDot0")]
    public void DefinePicaMovementX_UseDot0()
    {
        maxDistance = 0f;
        boundingBox.DefinePeek();
        trXOrigin.position = boundingBox.meshCenterBound;

        hasResult = false;

        Transform yPeek = null;        

        if (isDot0AndBetterDistance(boundingBox.peekX.position)) { picaMovementPeek.position = boundingBox.peekX.position; yPeek = boundingBox.peekY; if (!hasResult) hasResult = true; }
        if (isDot0AndBetterDistance(boundingBox.peekNX.position)) { picaMovementPeek.position = boundingBox.peekNX.position; yPeek = boundingBox.peekY; if (!hasResult) hasResult = true; }
        if (isDot0AndBetterDistance(boundingBox.peekY.position)) { picaMovementPeek.position = boundingBox.peekY.position; yPeek = boundingBox.peekX; if (!hasResult) hasResult = true; }
        if (isDot0AndBetterDistance(boundingBox.peekNY.position)) { picaMovementPeek.position = boundingBox.peekNY.position; yPeek = boundingBox.peekX; if (!hasResult) hasResult = true; }
        if (isDot0AndBetterDistance(boundingBox.peekZ.position)) { picaMovementPeek.position = boundingBox.peekZ.position; yPeek = boundingBox.peekX; if (!hasResult) hasResult = true; }
        if (isDot0AndBetterDistance(boundingBox.peekNZ.position)) { picaMovementPeek.position = boundingBox.peekNZ.position; yPeek = boundingBox.peekX; if (!hasResult) hasResult = true; }

        if (hasResult) picaSnap(yPeek);
        else { Debug.Log("can't find"); }
    }

    bool isDot0AndBetterDistance(Vector3 peek)
    {
        Vector3 dirToPeek = peek - boundingBox.meshCenterBound;
        Vector3 dirToCerebellum = trCerebellumOrigin.position - boundingBox.meshCenterBound;
        bool result = (Vector3.Dot(dirToPeek, dirToCerebellum) < 0 && dirToPeek.magnitude > maxDistance);
        if (result) maxDistance = dirToPeek.magnitude;
        return result;
    }

    [Header("useBS")]
    public MeshCollider bS = null;
    /// <summary>
    /// Sử dụng ray
    /// </summary>
    
    [Header("castInfo")]
    public CastInfoARay castInfoX = null;
    public CastInfoARay castInfoNX = null;
    public CastInfoARay castInfoY = null;
    public CastInfoARay castInfoNY = null;
    public CastInfoARay castInfoZ = null;
    public CastInfoARay castInfoNZ = null;

    [ContextMenu("DefinePicaMovementX_UseRay")]
    public void DefinePicaMovementX_UseRay()
    {
        OffsetAll();

        maxDistance = 0f;
        boundingBox.DefinePeek();
        trXOrigin.position = boundingBox.meshCenterBound;

        hasResult = false;

        Transform yPeek = null;        

        if (isNotHitBSAndBetterDistance(boundingBox.peekX, castInfoX)) { picaMovementPeek.position = boundingBox.peekX.position; Debug.Log("to X"); yPeek = boundingBox.peekY; if (!hasResult) hasResult = true; }
        if (isNotHitBSAndBetterDistance(boundingBox.peekNX, castInfoNX)) { picaMovementPeek.position = boundingBox.peekNX.position; Debug.Log("to NX"); yPeek = boundingBox.peekY; if (!hasResult) hasResult = true; }
        if (isNotHitBSAndBetterDistance(boundingBox.peekY, castInfoY)) { picaMovementPeek.position = boundingBox.peekY.position; Debug.Log("to Y"); yPeek = boundingBox.peekX; if (!hasResult) hasResult = true; }
        if (isNotHitBSAndBetterDistance(boundingBox.peekNY, castInfoNX)) { picaMovementPeek.position = boundingBox.peekNY.position; Debug.Log("to NY"); yPeek = boundingBox.peekX; if (!hasResult) hasResult = true; }
        if (isNotHitBSAndBetterDistance(boundingBox.peekZ, castInfoZ)) { picaMovementPeek.position = boundingBox.peekZ.position; Debug.Log("to Z"); yPeek = boundingBox.peekX; if (!hasResult) hasResult = true; }
        if (isNotHitBSAndBetterDistance(boundingBox.peekNZ, castInfoNX)) { picaMovementPeek.position = boundingBox.peekNZ.position; Debug.Log("to NZ"); yPeek = boundingBox.peekX; if (!hasResult) hasResult = true; }

        if (hasResult) { Debug.Log("chon " + yPeek.name + " lam picaUp"); picaSnap(yPeek); }
        else { Debug.Log("can't find"); }
    }

    [ContextMenu("DefinePicaMovementX_UseBSOri")]
    public void DefinePicaMovementX_UseBSOri()
    {
        maxDistance = 0f;
        boundingBox.DefinePeek();        

        hasResult = false;

        Transform yPeek = null;

        if (isClosestToBS(boundingBox.peekX)) { picaMovementPeek.position = boundingBox.peekX.position; Debug.Log("to X"); yPeek = boundingBox.peekY; if (!hasResult) hasResult = true; }
        if (isClosestToBS(boundingBox.peekNX)) { picaMovementPeek.position = boundingBox.peekNX.position; Debug.Log("to NX"); yPeek = boundingBox.peekY; if (!hasResult) hasResult = true; }
        if (isClosestToBS(boundingBox.peekY)) { picaMovementPeek.position = boundingBox.peekY.position; Debug.Log("to Y"); yPeek = boundingBox.peekX; if (!hasResult) hasResult = true; }
        if (isClosestToBS(boundingBox.peekNY)) { picaMovementPeek.position = boundingBox.peekNY.position; Debug.Log("to NY"); yPeek = boundingBox.peekX; if (!hasResult) hasResult = true; }
        if (isClosestToBS(boundingBox.peekZ)) { picaMovementPeek.position = boundingBox.peekZ.position; Debug.Log("to Z"); yPeek = boundingBox.peekX; if (!hasResult) hasResult = true; }
        if (isClosestToBS(boundingBox.peekNZ)) { picaMovementPeek.position = boundingBox.peekNZ.position; Debug.Log("to NZ"); yPeek = boundingBox.peekX; if (!hasResult) hasResult = true; }

        if (hasResult) { Debug.Log("chon " + yPeek.name + " lam picaUp"); picaSnap(yPeek); }
        else { Debug.Log("can't find"); }
    }

    void picaSnap(Transform yPeek)
    {        
        Debug.Log("picaMovementSnap.SnapZAxis();");
        originSnap.from1 = trXOrigin;
        originSnap.to1 = picaMovementPeek;
        originSnap.from2 = trXOrigin;
        originSnap.to2 = yPeek;
        originSnap.Snap_Forward_Upward();

        Debug.Log("SnapToTarget picaUpFollow");
        picaUpSnap.SnapToTarget();
        picaMovementPeekSnap.SnapRotation();
    }

    [Header("Layermask")]
    public LayerMask lm;

    bool isNotHitBSAndBetterDistance(Transform peek, CastInfoARay castInfo)
    {
        if (castInfo.boolHit) return false;

        lm = LayerMask.NameToLayer("BS");

        //RaycastHit hit;
        //bool isHit = Physics.Raycast(new Ray(trXOrigin.position, peek.position - trXOrigin.position), out hit, 9999f, lm);
        //if (isHit) { Debug.Log("from " + "trXOrigin " + trXOrigin.position + " and "  + peek.name + " " + peek.position + " will hit + " + hit.transform.parent.name); return false; }
        //else
        //{
        //    Debug.Log("from " + "trXOrigin " + trXOrigin.position + " and " + peek.name + " " + peek.position + " will NOT hit BS ");

        Vector3 dirToPeek = peek.position - trXOrigin.position;

        //    Debug.Log("last distance " + maxDistance);
        //    Debug.Log("dirToPeek.magnitude  " + dirToPeek.magnitude);

        if (dirToPeek.magnitude > maxDistance)
        {
            maxDistance = dirToPeek.magnitude;
            return true;
        }
        else return false;
            
        //}
    }

    Transform _trBSori = null;
    Transform trBSori { get { if (_trBSori == null) _trBSori = BS.Instance.boundOri; return _trBSori; } }

    bool isClosestToBS(Transform peek)
    {
        Vector3 dirBSToPeek = peek.position - trBSori.position;

        if (dirBSToPeek.magnitude > maxDistance)
        {
            maxDistance = dirBSToPeek.magnitude;
            return true;
        }
        else return false;
    }

    [Header("offset by Dir")]
    public List<OffsetByDirection> offsetList = new List<OffsetByDirection>();

    [ContextMenu("OffsetAll")]
    public void OffsetAll()
    {
        foreach (OffsetByDirection offset in offsetList) offset.Offset();
    }
}