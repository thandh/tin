﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Truncate : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    Mesh _mesh = null;
    Mesh mesh { get { if (_mesh == null) _mesh = GetComponent<MeshFilter>().sharedMesh; return _mesh; } }

    [Header("Params")]
    public bool boolTruncate = false;
    public bool hasX, hasNX = false;
    public bool hasY, hasNY = false;
    public bool hasZ, hasNZ = false;

    //void OnDrawGizmosSelected()
    //{
    //    Gizmos.color = Color.yellow;
    //    Gizmos.DrawWireCube(transform.position, new Vector3(1, 1, 1));
    //}

    private void Update()
    {
        if (!boolTruncate) return;

        if (hasX) hasX = TryRotate(new Vector3(1, 0, 0));
        if (hasNX) hasNX = TryRotate(new Vector3(-1, 0, 0));
        if (hasY) hasY = TryRotate(new Vector3(0, 1, 0));
        if (hasNY) hasNY = TryRotate(new Vector3(0, -1, 0));
        if (hasZ) hasZ = TryRotate(new Vector3(0, 0, 1));
        if (hasNZ) hasNZ = TryRotate(new Vector3(0, 0, -1));

        if (!hasX && !hasNX && !hasY && !hasNY && !hasZ && !hasNZ)
        {
            boolTruncate = false;
            Debug.Log("Done truncate");
        }
    }

    bool TryRotate(Vector3 eulerTruncate)
    {
        float lastVolume = Volume();
        UpdatePivotRotation(Vector3.zero, eulerTruncate);
        float currentVolume = Volume();
        return currentVolume < lastVolume;
    }

    [Header("Truncate")]
    public float stepTruncate = 1f;    

    [ContextMenu("DoTruncate")]
    public void DoTruncate()
    {
        if (isDebug) Debug.Log("Start DoTruncate!");
        boolTruncate = true;
        hasX = true; hasNX = true;
        hasY = true; hasNY = true;
        hasZ = true; hasNZ = true;
    }    

    //Calculates the renderer bounds of the gameobject with its children
    public Bounds CalculateRendererBounds(GameObject obj)
    {
        Quaternion prevRot = obj.transform.localRotation;

        obj.transform.localRotation = Quaternion.identity;

        Bounds bounds = new Bounds(obj.transform.position, Vector3.one);

        obj.GetComponent<Renderer>().bounds.Encapsulate(obj.GetComponent<Renderer>().bounds);        

        obj.transform.localRotation = prevRot;

        return bounds;
    }

    IEnumerator C_Truncate()
    {   
        Debug.Log("Done");

        yield break;
    }

    float Volume()
    {
        return mesh.bounds.size.x * mesh.bounds.size.y * mesh.bounds.size.z;
    }

    void UpdatePivotRotation(Vector3 pivot, Vector3 euler)
    {
        Vector3 center = pivot;
        Quaternion newRotation = new Quaternion();
        newRotation.eulerAngles = euler;

        Vector3[] verts = mesh.vertices;
        for (int i = 0; i < verts.Length; i++)
        {
            verts[i] = newRotation * (verts[i] - center) + center;
        }

        transform.position += new Vector3(0, 1, 0);
        transform.position -= new Vector3(0, 1, 0);

        mesh.vertices = verts; 
        mesh.RecalculateBounds(); 
    }
}