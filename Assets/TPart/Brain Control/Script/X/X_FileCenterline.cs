﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class X_FileCenterline : MonoBehaviour
{
    public bool isDebug = false;

    [Header("Params")]
    public string xfilename = "xCenterline.txt";

    [Header("Params View Only")]
    [SerializeField]
    string _currentFolder = string.Empty;
    string currentFolder
    {
        get { if (_currentFolder != Head.Instance.caseInfo.GetCurrentCaseFolder()) _currentFolder = Head.Instance.caseInfo.GetCurrentCaseFolder(); return _currentFolder; }
        set { _currentFolder = value; }
    }

    [Header("Get Params")]
    public bool doneOutFile = true;
    public centerFileJson fileJson = new centerFileJson();

    IEnumerator Start()
    {
        yield return new WaitUntil(() => Head.Instance != null);

        ResetParams();
        DoInput();

        yield break;
    }

    [ContextMenu("ResetParams")]
    public void ResetParams()
    {
        fileJson = new centerFileJson();
        fileJson.oriPos = Vector3.zero;
        fileJson.oriRot = Quaternion.identity;
        _currentFolder = string.Empty;
        Debug.LogWarning("Reseting : " + currentFolder);
    }

    [Header("Empty")]
    public bool doneEmpty = true;

    [ContextMenu("Empty")]
    public void Empty()
    {
        StartCoroutine(C_Empty());
    }

    IEnumerator C_Empty()
    {
        if (isDebug) Debug.Log("Start Empty !!");

        if (isDebug && string.IsNullOrEmpty(xfilename)) { Debug.Log("filename empty"); yield break; }

        if (!File.Exists(currentFolder + xfilename))
        {
            File.Create(currentFolder + xfilename);
            yield return new WaitForSeconds(2f);
        }

        yield return new WaitUntil(() => Existed() == true);

        doneEmpty = false;

        File.WriteAllText(currentFolder + xfilename, string.Empty);
        yield return new WaitUntil(() => IsFileLocked(new FileInfo(currentFolder + xfilename)) == false);

        doneEmpty = true;
        if (isDebug) Debug.Log("Done Empty");

        yield break;
    }

    [Header("Input process")]
    public bool doneInput = false;

    [ContextMenu("DoInput")]
    public void DoInput()
    {
        StartCoroutine(C_Input());
    }

    IEnumerator C_Input()
    {
        doneInput = false;

        CheckExisted();

        if (isDebug) Debug.Log("start yield return new WaitUntil(() => Existed() == true)");
        yield return new WaitUntil(() => Existed() == true);

        if (isDebug) Debug.Log("start yield return new WaitUntil(() => doneCheckExisted == true)");
        yield return new WaitUntil(() => doneCheckExisted == true);

        ReadExisted();
        if (isDebug) Debug.Log("start yield return new WaitUntil(() => doneReadExisted == true)");
        yield return new WaitUntil(() => doneReadExisted == true);

        doneInput = true;

        yield break;
    }

    bool doneCheckExisted = false;

    /// <summary>
    /// Check. If not existed, create.
    /// </summary>
    [ContextMenu("CheckExisted")]
    public void CheckExisted()
    {
        StartCoroutine(C_CheckExisted());
    }

    IEnumerator C_CheckExisted()
    {
        doneCheckExisted = false;

        if (isDebug) Debug.Log("Start X_FileCenterline CheckExisted ");
        if (isDebug) Debug.Log("Checking " + currentFolder + xfilename);

        if (!File.Exists(currentFolder + xfilename))
        {
            StreamWriter sw = new StreamWriter(currentFolder + xfilename);
            sw.WriteLine(new centerFileJson());
            sw.Close();

            if (isDebug) Debug.Log("start yield return new WaitForSeconds(1f)");
            yield return new WaitForSeconds(1f);

            outFile(fileJson.toJson());
            if (isDebug) Debug.Log("yield return new WaitUntil(() => doneOutFile == true)");
            yield return new WaitUntil(() => doneOutFile == true);
        }

        yield return new WaitForSeconds(1f);

        doneCheckExisted = true;
        if (isDebug) Debug.Log("Done X_FileCenterline CheckExisted ");

        yield break;
    }

    bool doneReadExisted = false;

    void ReadExisted()
    {
        StartCoroutine(C_ReadExisted());
    }

    IEnumerator C_ReadExisted()
    {
        doneReadExisted = false;

        if (isDebug) Debug.Log("start yield return new WaitUntil(() => Existed() == true)");
        yield return new WaitUntil(() => Existed() == true);

        StreamReader sr = new StreamReader(currentFolder + xfilename);
        try
        {
            fileJson = JsonUtility.FromJson<centerFileJson>(sr.ReadToEnd());
        }
        catch
        {
            fileJson = new centerFileJson();
        }

        sr.Close();

        doneReadExisted = true;
    }

    bool Existed()
    {
        return File.Exists(currentFolder + xfilename);
    }

    [ContextMenu("outCurreltFile")]
    public void outCurreltFile()
    {
        StartCoroutine(C_outFile(this.fileJson.toJson()));
    }

    public void outFile(string output)
    {
        StartCoroutine(C_outFile(output));
    }

    IEnumerator C_outFile(string output)
    {
        if (isDebug) Debug.Log("start X_FileCenterline C_outFile : " + currentFolder + xfilename);
        if (isDebug) Debug.Log("start yield return new WaitUntil(() => Existed() == true)");
        yield return new WaitUntil(() => Existed() == true);
        doneOutFile = false;

        //StreamWriter sw = new StreamWriter(currentFolder + xfilename);
        //sw.WriteLine(output);
        //sw.Close();

        File.WriteAllText(currentFolder + xfilename, output);

        if (isDebug) Debug.Log("start yield return new WaitUntil(() => IsFileLocked()");
        yield return new WaitUntil(() => IsFileLocked(new FileInfo(currentFolder + xfilename)) == false);

        doneOutFile = true;
        if (isDebug) Debug.Log("done X_FileCenterline C_outFile");

        yield break;
    }

    bool IsFileLocked(FileInfo file)
    {
        FileStream stream = null;

        try
        {
            stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
        }
        catch (IOException)
        {
            return true;
        }
        finally
        {
            if (stream != null)
                stream.Close();
        }

        return false;
    }

    [Header("Out put")]
    public Transform picaMovement = null;
    public Transform xOrigin = null;

    [ContextMenu("SetOutput")]
    public void SetOutput()
    {
        picaMovement.position = fileJson.oriPos;
        picaMovement.rotation = fileJson.oriRot;
        xOrigin.rotation = fileJson.oriRot;
    }

    [Header("Get From Scene")]
    public Transform picaMovementPeek = null;

    [ContextMenu("GetFromCurrentScene")]
    public void GetFromCurrentScene()
    {
        fileJson = new centerFileJson();
        fileJson.oriPos = xOrigin.position;
        fileJson.oriRot = picaMovementPeek.rotation;
    }

    [Header("Delete")]
    public bool doneDelete = true;

    [ContextMenu("DeleteCurrentFile")]
    public void DeleteCurrentFile()
    {
        StartCoroutine(C_DeleteCurrentFile());
    }

    IEnumerator C_DeleteCurrentFile()
    {
        doneDelete = false;

        if (File.Exists(currentFolder + xfilename)) File.Delete(currentFolder + xfilename);
        else Debug.Log(currentFolder + xfilename + " not existed");

        yield return new WaitUntil(() => File.Exists(currentFolder + xfilename) == false);

        doneDelete = true;

        yield break;
    }
}