﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAxis : MonoBehaviour
{
    public bool ClickHere = false;

    public Transform ori1 = null;
    public Transform ori2 = null;

    [ContextMenu("Rotate")]
    public void Rotate()
    {
        StartCoroutine(RotateWorld(1, ori1.position - ori2.position));
    }

    float rotateSpeed = 1f;
    public float delay = 0.4f;

    private IEnumerator RotateWorld(int dir, Vector3 _axis)
    {
        var startOrientation = transform.localRotation;
        var axis = transform.InverseTransformDirection(_axis);
        var start = 0;
        var end = 90.0f * dir;
        var t = 0.0f;

        while (t < 1.0f)
        {
            t += Time.deltaTime * rotateSpeed;
            transform.localRotation = startOrientation * Quaternion.AngleAxis(Mathf.Lerp(start, end, t), axis);
            yield return new WaitForSeconds(delay);
        }
        transform.localRotation = startOrientation * Quaternion.AngleAxis(end, axis);
    }

    [ContextMenu("Stop")]
    public void Stop()
    {
        StopAllCoroutines();
    }
}
