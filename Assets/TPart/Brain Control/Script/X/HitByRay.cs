﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class HitByRay : MonoBehaviour
{
    public bool isDebug = false;

    [Header("Input params")]
    public bool useWorld = false;
    public Vector3 useDir = Vector3.forward;
    public Transform trFrom = null;
    public Transform trTo = null;
    public LayerMask lmHit;
    public float fromStepBack = 0f;

    [Header("ray")]
    public bool showRay = false;
    public bool showFromTo = false;
    public bool showFromToFinal = false;
    public bool showluseDir = false;
    public bool showFinalToFinal = false;

    private void Update()
    {
        if (!showRay) return;
        if (trFrom == null || trTo == null) return;
        if (showFromTo) Debug.DrawRay(lfromPosFromTo, trTo.position - lfromPosFromTo, Color.blue);
        if (showFromToFinal) Debug.DrawRay(lfromPosFromToFinal, transform.position - lfromPosFromToFinal, Color.green);
        if (showluseDir) Debug.DrawRay(lfromPosluseDir, luseDir, Color.red);
        if (showFinalToFinal) Debug.DrawRay(lfromPosFinalToFinal, transform.position - lfromPosFinalToFinal, Color.magenta);
    }

    [SerializeField]
    Vector3 luseDir = Vector3.zero;
    [SerializeField]
    Vector3 lfromPosFromTo = Vector3.zero;
    [SerializeField]
    Vector3 lfromPosFromToFinal = Vector3.zero;
    [SerializeField]
    Vector3 lfromPosFinalToFinal = Vector3.zero;
    [SerializeField]
    Vector3 lfromPosluseDir = Vector3.zero;

    [ContextMenu("CalclfromPos")]
    public void CalclfromPos()
    {
        lfromPosFromTo = trFrom.position + (trTo.position - trFrom.position).normalized * fromStepBack;
        lfromPosFromToFinal = trFrom.position + (transform.position - trFrom.position).normalized * fromStepBack;
        lfromPosluseDir = trFrom.position + luseDir.normalized * fromStepBack;
        lfromPosFinalToFinal = transform.position + (trTo.position - trFrom.position).normalized * fromStepBack;
    }

    [ContextMenu("HitUseDir")]
    public void HitUseDir()
    {
        RaycastHit raycastHit;
        luseDir = useDir;
        if (!useWorld) luseDir = trFrom.TransformDirection(useDir);
        CalclfromPos();
        bool hit = Physics.Raycast(new Ray(lfromPosluseDir, luseDir), out raycastHit, luseDir.magnitude, lmHit.value);
        if (hit)
        {
            transform.position = raycastHit.point;
        }
        else
        {
            if (isDebug) Debug.Log("HitUseDir Khong hit!");
        }
    }

    [ContextMenu("HitUseCurrentToFinal")]
    public void HitUseCurrentToFinal()
    {
        RaycastHit raycastHit;
        luseDir = transform.position - trFrom.position;
        if (!useWorld) luseDir = trFrom.TransformDirection(useDir);
        CalclfromPos();
        bool hit = Physics.Raycast(new Ray(lfromPosFromToFinal, luseDir), out raycastHit, luseDir.magnitude, lmHit.value);
        if (hit)
        {
            transform.position = raycastHit.point;
        }
        else
        {
            if (isDebug) Debug.Log("HitUseCurrentToFinal Khong hit!");
        }
    }

    public Vector3 PosHitUseCurrentToFinal()
    {
        RaycastHit raycastHit;
        luseDir = transform.position - trFrom.position;
        if (!useWorld) luseDir = trFrom.TransformDirection(useDir);
        CalclfromPos();
        bool hit = Physics.Raycast(new Ray(lfromPosFromToFinal, luseDir), out raycastHit, luseDir.magnitude, lmHit.value);
        if (hit) return raycastHit.point;
        else
        {
            if (isDebug) Debug.Log("PosHitUseCurrentToFinal Khong hit! : " + trFrom.name + ", " + trTo.name);
            return -Vector3.one;
        }
    }

    public void HitInfoUseCurrentToFinal(out Vector3 posHit, out Transform trHit)
    {
        RaycastHit raycastHit;
        luseDir = transform.position - trFrom.position;
        if (!useWorld) luseDir = trFrom.TransformDirection(useDir);
        CalclfromPos();
        bool hit = Physics.Raycast(new Ray(lfromPosFromToFinal, luseDir), out raycastHit, luseDir.magnitude, lmHit.value);
        if (hit)
        {
            posHit = raycastHit.point;
            trHit = raycastHit.transform;
        }
        else
        {
            if (isDebug) Debug.Log("HitInfoUseCurrentToFinal Khong hit!");
            posHit = -Vector3.one;
            trHit = null;
        }
    }

    [ContextMenu("HitUseFinalToFinal")]
    public void HitUseFinalToFinal()
    {
        RaycastHit raycastHit;
        luseDir = trTo.position - trFrom.position;
        if (!useWorld) luseDir = trFrom.TransformDirection(useDir);
        CalclfromPos();
        bool hit = Physics.Raycast(new Ray(lfromPosFinalToFinal, luseDir), out raycastHit, luseDir.magnitude, lmHit.value);
        if (hit)
        {
            transform.position = raycastHit.point;
        }
        else
        {
            if (isDebug) Debug.Log("HitUseFinalToFinal Khong hit!");
        }
    }

    public Vector3 PosHitUseFinalToFinal()
    {
        RaycastHit raycastHit;
        luseDir = trTo.position - trFrom.position;
        if (!useWorld) luseDir = trFrom.TransformDirection(useDir);
        CalclfromPos();
        bool hit = Physics.Raycast(new Ray(lfromPosFinalToFinal, luseDir), out raycastHit, luseDir.magnitude, lmHit.value);
        if (hit) return raycastHit.point;
        else
        {
            if (isDebug) Debug.Log(trFrom.name + " khong hit " + trTo.name);
            return -Vector3.one;
        }
    }

    [ContextMenu("CheckHitUseFinalToFinal")]
    public bool CheckHitUseFinalToFinal()
    {
        RaycastHit raycastHit;
        luseDir = trTo.position - trFrom.position;
        if (!useWorld) luseDir = trFrom.TransformDirection(useDir);
        CalclfromPos();
        bool hit = Physics.Raycast(new Ray(lfromPosFinalToFinal, luseDir), out raycastHit, Mathf.Infinity, lmHit.value);
        if (hit)
        {
            transform.position = raycastHit.point;
        }
        else
        {
            if (isDebug) Debug.Log("CheckHitUseFinalToFinal Khong hit!");
        }
        return hit;
    }

    public void HitInfoUseFinalToFinal(out Vector3 posHit, out Transform trHit)
    {
        RaycastHit raycastHit;
        luseDir = trTo.position - trFrom.position;
        if (!useWorld) luseDir = trFrom.TransformDirection(useDir);
        CalclfromPos();
        bool hit = Physics.Raycast(new Ray(lfromPosFromToFinal, luseDir), out raycastHit, luseDir.magnitude, lmHit.value);
        if (hit)
        {
            posHit = raycastHit.point;
            trHit = raycastHit.transform;
        }
        else
        {
            if (isDebug) Debug.Log("HitInfoUseCurrentToFinal Khong hit!");
            posHit = -Vector3.one;
            trHit = null;
        }
    }
}
