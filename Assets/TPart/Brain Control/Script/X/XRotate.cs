﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XRotate : MonoBehaviour
{
    public bool isUpdate = false;

    private void Update()
    {
        if (!isUpdate) return;

        transform.Rotate(new Vector3(1, 0, 0));
    }
}
