﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XMethod : MonoBehaviour
{
    [ContextMenu("ShowBound")]
    public void toggleShowBound()
    {
        Debug.Log("toggleShowBound");
        X.Instance.auto.showMeshBounds_Runtime.draw = !X.Instance.auto.showMeshBounds_Runtime.draw;
    }

    [ContextMenu("auto")]
    public void auto()
    {
        X.Instance.auto.FullAuto();
    }
}
