﻿using UnityEngine;
using System.Collections;

public class ShowMeshBounds_Runtime : MonoBehaviour
{
    [SerializeField] bool _draw = true;
    public bool draw
    {
        get
        {
            return _draw;
        }

        set
        {
            _draw = value;
            AfterSetDraw();
        }
    }

    public Color color = Color.green;

    private Vector3 v3FrontTopLeft;
    private Vector3 v3FrontTopRight;
    private Vector3 v3FrontBottomLeft;
    private Vector3 v3FrontBottomRight;
    private Vector3 v3BackTopLeft;
    private Vector3 v3BackTopRight;
    private Vector3 v3BackBottomLeft;
    private Vector3 v3BackBottomRight;

    public Transform lineHolder = null;
    public LineRenderer l0;
    public LineRenderer l1;
    public LineRenderer l2;
    public LineRenderer l3;
    public LineRenderer l4;
    public LineRenderer l5;
    public LineRenderer l6;
    public LineRenderer l7;
    public LineRenderer l8;
    public LineRenderer l9;
    public LineRenderer l10;
    public LineRenderer l11;    

    void AfterSetDraw()
    {
        lineHolder.gameObject.SetActive(draw);
    }

    void Update()
    {
        if (!Application.isPlaying) return;

        if (!_draw) return;

        CalcPositons();
        DrawBox();
    }

    void CalcPositons()
    {
        Bounds bounds = GetComponent<MeshFilter>().sharedMesh.bounds;

        //Bounds bounds;
        //BoxCollider bc = GetComponent<BoxCollider>();
        //if (bc != null)
        //    bounds = bc.bounds;
        //else
        //return;

        Vector3 v3Center = bounds.center;
        Vector3 v3Extents = bounds.extents;

        v3FrontTopLeft = new Vector3(v3Center.x - v3Extents.x, v3Center.y + v3Extents.y, v3Center.z - v3Extents.z);  // Front top left corner
        v3FrontTopRight = new Vector3(v3Center.x + v3Extents.x, v3Center.y + v3Extents.y, v3Center.z - v3Extents.z);  // Front top right corner
        v3FrontBottomLeft = new Vector3(v3Center.x - v3Extents.x, v3Center.y - v3Extents.y, v3Center.z - v3Extents.z);  // Front bottom left corner
        v3FrontBottomRight = new Vector3(v3Center.x + v3Extents.x, v3Center.y - v3Extents.y, v3Center.z - v3Extents.z);  // Front bottom right corner
        v3BackTopLeft = new Vector3(v3Center.x - v3Extents.x, v3Center.y + v3Extents.y, v3Center.z + v3Extents.z);  // Back top left corner
        v3BackTopRight = new Vector3(v3Center.x + v3Extents.x, v3Center.y + v3Extents.y, v3Center.z + v3Extents.z);  // Back top right corner
        v3BackBottomLeft = new Vector3(v3Center.x - v3Extents.x, v3Center.y - v3Extents.y, v3Center.z + v3Extents.z);  // Back bottom left corner
        v3BackBottomRight = new Vector3(v3Center.x + v3Extents.x, v3Center.y - v3Extents.y, v3Center.z + v3Extents.z);  // Back bottom right corner

        v3FrontTopLeft = transform.TransformPoint(v3FrontTopLeft);
        v3FrontTopRight = transform.TransformPoint(v3FrontTopRight);
        v3FrontBottomLeft = transform.TransformPoint(v3FrontBottomLeft);
        v3FrontBottomRight = transform.TransformPoint(v3FrontBottomRight);
        v3BackTopLeft = transform.TransformPoint(v3BackTopLeft);
        v3BackTopRight = transform.TransformPoint(v3BackTopRight);
        v3BackBottomLeft = transform.TransformPoint(v3BackBottomLeft);
        v3BackBottomRight = transform.TransformPoint(v3BackBottomRight);
    }

    void DrawBox()
    {
        //if (Input.GetKey (KeyCode.S)) {
        l0.SetPositions(new Vector3[2] { v3FrontTopLeft, v3FrontTopRight });
        l1.SetPositions(new Vector3[2] { v3FrontTopRight, v3FrontBottomRight });
        l2.SetPositions(new Vector3[2] { v3FrontBottomRight, v3FrontBottomLeft });
        l3.SetPositions(new Vector3[2] { v3FrontBottomLeft, v3FrontTopLeft });

        l4.SetPositions(new Vector3[2] { v3BackTopLeft, v3BackTopRight });
        l5.SetPositions(new Vector3[2] { v3BackTopRight, v3BackBottomRight });
        l6.SetPositions(new Vector3[2] { v3BackBottomRight, v3BackBottomLeft });
        l7.SetPositions(new Vector3[2] { v3BackBottomLeft, v3BackTopLeft });

        l8.SetPositions(new Vector3[2] { v3FrontTopLeft, v3BackTopLeft });
        l9.SetPositions(new Vector3[2] { v3FrontTopRight, v3BackTopRight });
        l10.SetPositions(new Vector3[2] { v3FrontBottomRight, v3BackBottomRight });
        l11.SetPositions(new Vector3[2] { v3FrontBottomLeft, v3BackBottomLeft });        
        
        //Debug.DrawLine(v3FrontTopLeft, v3FrontTopRight, color);
        //Debug.DrawLine(v3FrontTopRight, v3FrontBottomRight, color);
        //Debug.DrawLine(v3FrontBottomRight, v3FrontBottomLeft, color);
        //Debug.DrawLine(v3FrontBottomLeft, v3FrontTopLeft, color);

        //Debug.DrawLine(v3BackTopLeft, v3BackTopRight, color);
        //Debug.DrawLine(v3BackTopRight, v3BackBottomRight, color);
        //Debug.DrawLine(v3BackBottomRight, v3BackBottomLeft, color);
        //Debug.DrawLine(v3BackBottomLeft, v3BackTopLeft, color);

        //Debug.DrawLine(v3FrontTopLeft, v3BackTopLeft, color);
        //Debug.DrawLine(v3FrontTopRight, v3BackTopRight, color);
        //Debug.DrawLine(v3FrontBottomRight, v3BackBottomRight, color);
        //Debug.DrawLine(v3FrontBottomLeft, v3BackBottomLeft, color);
        //}
    }

}