﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DistributeInLine : MonoBehaviour
{
    [Header("Params")]
    public HitByRay surfaceHitByRay = null;
    public MeshVertCollector vertCollector = null;

    public List<CereImpactPoint> listForce = null;

    [ContextMenu("Get List Force")]
    public void GetListForce()
    {
        listForce = GetComponentsInChildren<CereImpactPoint>().ToList();
    }

    [Header("Distribute")]
    public float dDistribute = 10f;
    public float dToCere = 10f;

    [ContextMenu("Distribute")]
    public void Distribute()
    {
        for (int i = 0; i < listForce.Count; i++)
        {
            if (i < vertCollector.pointList().Count)
            {
                listForce[i].gameObject.SetActive(true);
                listForce[i].transform.position = vertCollector.pointList()[i];                
            }
            else
            {
                listForce[i].gameObject.SetActive(false);
            }
        }

        transform.forward = surfaceHitByRay.trTo.position - surfaceHitByRay.trFrom.position;

        for (int i = 0; i < listForce.Count; i++)
        {
            listForce[i].StepFromCere(dToCere);
        }
    }
}
