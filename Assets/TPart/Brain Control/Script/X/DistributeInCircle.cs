﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DistributeInCircle : MonoBehaviour
{
    [Header("Params")]
    public HitByRay surfaceHitByRay = null;

    public List<CereImpactPoint> listForce = null;

    [ContextMenu("Get List Force")]
    public void GetListForce()
    {
        listForce = GetComponentsInChildren<CereImpactPoint>().ToList();
    }

    [Header("Distribute")]
    public float dDistribute = 10f;
    public float dToCere = 10f;

    [ContextMenu("Distribute")]
    public void Distribute()
    {
        for (int i = 0; i < listForce.Count; i++)
        {
            listForce[i].transform.position = transform.position;
            listForce[i].transform.rotation = transform.rotation;
            listForce[i].transform.Rotate(new Vector3(0, 0, i * (360 / listForce.Count)));
            listForce[i].transform.Translate(new Vector3(dDistribute, 0, 0), Space.Self);
        }

        transform.forward = surfaceHitByRay.trTo.position - surfaceHitByRay.trFrom.position;

        for (int i = 0; i < listForce.Count; i++)
        {            
            listForce[i].StepFromCere(dToCere);
        }
    }
}
