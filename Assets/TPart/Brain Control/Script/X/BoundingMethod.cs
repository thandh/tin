﻿using UnityEngine;
using System.Collections;

public class BoundingMethod : MonoBehaviour
{
    public BoundingBox boundingBox = null;    

    [Header("Origin")]
    public Transform origin = null;

    public bool sameDir = true;
    public Transform trTarget = null;
    
    /// <summary>
    /// Ngược lại hoặc cùng hướng với Target
    /// </summary>
    [ContextMenu("SnapOriginToMaxX")]
    public void SnapOriginToMaxX()
    {
        if (origin == null) return;

        float maxDistance = 0f;

        boundingBox.DefinePeek();

        //if (isBetter(boundingBox.peekX.position)) { origin.position = boundingBox.peekX.position; }
        //if (isBetter(boundingBox.peekNX.position)) { origin.position = boundingBox.peekNX.position; }
        //if (isBetter(boundingBox.peekY.position)) { origin.position = boundingBox.peekY.position; }
        //if (isBetter(boundingBox.peekNY.position)) { origin.position = boundingBox.peekNY.position; }
        //if (isBetter(boundingBox.peekZ.position)) { origin.position = boundingBox.peekZ.position; }
        //if (isBetter(boundingBox.peekNZ.position)) { origin.position = boundingBox.peekNZ.position; }

        //Debug.Log("SnapPosition picaUpFollow");
        //picaUpFollow.SnapPosition();

        //Debug.Log("picaMovementSnap.SnapXAxis();");
        //picaMovementSnap.from = trXOrigin;
        //picaMovementSnap.to = picaMovementPeek;
        //picaMovementSnap.SnapXAxis();
    }

    //bool isBetter(Vector3 peek)
    //{
    //    Vector3 dirToPeek = peek - boundingBox.meshCenterBound;
    //    Vector3 dirToCerebellum = trTarget.position - boundingBox.meshCenterBound;
    //    bool result = (Vector3.Dot(dirToPeek, dirToCerebellum) < 0 && dirToPeek.magnitude > maxDistance);
    //    if (result) maxDistance = dirToPeek.magnitude;
    //    return result;
    //}
}