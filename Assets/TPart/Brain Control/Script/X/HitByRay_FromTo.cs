﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitByRay_FromTo : MonoBehaviour
{
    public bool isDebug = false;

    [Header("Input params")]    
    public Transform trFrom = null;
    public Transform trTo = null;
    public LayerMask lmHit;

    public bool IsHit()
    {
        RaycastHit raycastHit;
        Vector3 dir = trTo.position - trFrom.position;
        bool hit = Physics.Raycast(new Ray(trFrom.position, dir), out raycastHit, dir.magnitude, lmHit.value);        
        if (isDebug) Debug.Log("Hit : " + hit);
        return hit;
    }

    public bool IsHit(out Vector3 posHit)
    {
        RaycastHit raycastHit;
        Vector3 dir = trTo.position - trFrom.position;
        bool hit = Physics.Raycast(new Ray(trFrom.position, dir), out raycastHit, dir.magnitude, lmHit.value);
        posHit = hit ? raycastHit.point : - Vector3.one;
        if (isDebug) Debug.Log("Hit : " + hit);
        return hit;
    }

    [ContextMenu("CheckHitFromTo")]
    public void CheckHitFromTo()
    {   
        Vector3 posHit = -Vector3.one;
        IsHit(out posHit);
    }

    [ContextMenu("GoHitFromTo")]
    public void GoHitFromTo()
    {
        Vector3 posHit = -Vector3.one;
        bool hit = IsHit(out posHit);
        if (hit) transform.position = posHit;
    }
}
