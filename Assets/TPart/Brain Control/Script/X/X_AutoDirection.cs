﻿using System.Collections;
using System.Collections.Generic;
using tMeshIO;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class X_AutoDirection : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Clickable")]
    public bool ClickHere = false;

    [Header("Start")]
    public bool autoAtStart = true;

    [Header("X")]
    public MeshFilter mf = null;
    public autoXMeshIO autoxMeshIO = null;
    public MeshOut meshOut = null;
    public Truncate truncate = null;
    public ShowMeshBounds_EditMode showMeshBounds_EditMode = null;
    public ShowMeshBounds_Runtime showMeshBounds_Runtime = null;    
    public XPointControl xPointControl = null;
    public Transform origin = null;        
    public MeshRenderer meshRenderer = null;
    public ApplybyWorldTransform applyOrigin = null;
    public SetPivotMethod setPivotMethod = null;
    public AutoXPanelMethod autoXPanelMethod = null;
    public BoundingBox boundingBox = null;
    public X_Definepeek x_definepeek = null;
    public X_DefinePicaMovementX x_definePicaMovementX = null;
    public OriginOfMesh originOfMesh = null;
    public OriginOfMeshBound originOfMeshBound = null;
    public X_FileCenterline x_fileCenterline = null;
    public HitByRay xHitSkull = null;

    private void Start()
    {
        FullAuto();
    }

    [Header("On Off")]
    public bool isOn = true;

    [ContextMenu("ToggleOnOff")]
    public void ToggleOnOff()
    {
        isOn = !isOn;
        Color c = meshRenderer.sharedMaterials[0].color;
        meshRenderer.sharedMaterials[0].color = isOn ? new Color(c.r, c.g, c.b, 1f) : new Color(c.r, c.g, c.b, .5f);
        //if (Application) showMeshBounds_EditMode.draw = !isOn;
    }

    [Header("Cerebellum")]
    public autoXMeshIO meshIO_cerebellum = null;

    [Header("Cerebellum")]
    public float timeAuto = 0;

    [ContextMenu("Full Auto")]
    public void FullAuto()
    {
        StartCoroutine(C_FullAuto());
    }

    IEnumerator C_FullAuto()
    {
#if UNITY_EDITOR
        if (!Application.isPlaying)
        {
            Selection.activeGameObject = this.gameObject;
        }
#endif

        if (Application.isPlaying)
        {
            if (!autoAtStart)
            {
                if (isDebug) Debug.Log("Case !autoAtStart, just AfterFullAuto()");
                AfterFullAuto();
                yield break;
            }
        }

        if (isDebug) Debug.Log("start x_fileCenterline.DoInput();");
        x_fileCenterline.DoInput();
        if (isDebug) Debug.Log("start yield return new WaitUntil(() => x_fileCenterline.doneInput == true)");
        yield return new WaitUntil(() => x_fileCenterline.doneInput == true);

        if (isDebug) Debug.Log("start CheckDoneAutob4()");
        CheckDoneAutob4();

        if (isDebug) Debug.Log("start yield return new WaitUntil(() => doneCheckAutob4 == true)");
        yield return new WaitUntil(() => doneCheckAutob4 == true);
        if (autob4) { x_fileCenterline.SetOutput(); AfterFullAuto(); yield break; }

        timeAuto = 0;
        float time = Time.time;

        if (isDebug) Debug.Log("start Full Auto");
        autoXPanelMethod.SayAndClear("start Automatic find X's direction");

        autoXPanelMethod.overlayOn();

        Init();
        if (isDebug) Debug.Log("yielding Init()");
        yield return new WaitUntil(() => doneInit == true);

        AutoDirection();
        if (isDebug) Debug.Log("yielding AutoDirection()");
        yield return new WaitUntil(() => doneAutoDirection == true);

        timeAuto = Time.time - time;
        AfterFullAuto(timeAuto);

        yield break;
    }

    [Header("Check done b4")]
    public bool autob4 = false;
    public bool doneCheckAutob4 = false;

    [ContextMenu("CheckDoneAutob4")]
    public void CheckDoneAutob4()
    {
        StartCoroutine(C_CheckDoneAutob4());
    }

    IEnumerator C_CheckDoneAutob4()
    {
        if (isDebug) Debug.Log("Start C_CheckDoneAutob4()");
        doneCheckAutob4 = false;
        autob4 = false;

        yield return new WaitUntil(() => x_fileCenterline.doneInput == true);

        try
        {
            autob4 = (x_fileCenterline.fileJson.oriPos != Vector3.zero) || (x_fileCenterline.fileJson.oriRot != Quaternion.identity);
            if (isDebug) Debug.Log("Done before with result : " + autob4);
        } catch { }

        doneCheckAutob4 = true;
        if (isDebug) Debug.Log("Done C_CheckDoneAutob4()");
        yield break;
    }

    public delegate void voidAfterFullAuto();
    public voidAfterFullAuto voidAfterAuto = null;

    void AfterFullAuto(float timeAuto = 0)
    {
        Color c = meshRenderer.sharedMaterials[0].color;
        meshRenderer.sharedMaterials[0].color = new Color(c.r, c.g, c.b, 1f);

        xHitSkull.HitUseDir();
        if (isDebug) Debug.Log("xHitSkull.Hit " + xHitSkull.transform.localPosition);

        if (isDebug) Debug.Log("done auto in " + timeAuto);
        autoXPanelMethod.overlayOff();
        autoXPanelMethod.SayAndClear("done auto in " + timeAuto);

        if (voidAfterAuto != null) voidAfterAuto.Invoke();
    }

    [Header("Init")]
    public bool doneInit = false;

    [ContextMenu("Init")]
    public void Init()
    {        
        StartCoroutine(C_Init());
    }

    IEnumerator C_Init()
    {
        if (isDebug) Debug.Log("start Init");
        doneInit = false;

        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;

        xPointControl.Reset0();
        autoxMeshIO.Reset0();
        setPivotMethod.Reset0();

        if (isDebug) Debug.Log("start meshOut.Empty();");
        meshOut.Empty();        
        yield return new WaitUntil(() => meshOut.doneEmpty == true);
        if (isDebug) Debug.Log("done meshOut.Empty();");

        if (isDebug) Debug.Log("start x_fileCenterline.Empty();");
        x_fileCenterline.Empty();
        yield return new WaitUntil(() => x_fileCenterline.doneEmpty == true);
        if (isDebug) Debug.Log("done x_fileCenterline.Empty();");

        xPointControl.oriOnOff(false);
        xPointControl.truncOnOff(false);

        if (!Application.isPlaying) showMeshBounds_EditMode.draw = true;
        else showMeshBounds_Runtime.draw = true;

        Color c = meshRenderer.sharedMaterials[0].color;
        meshRenderer.sharedMaterials[0].color = new Color(c.r, c.g, c.b, .5f);

        if (isDebug) Debug.Log("start autoxMeshIO.GetParams()");
        autoxMeshIO.GetParams();
        if (isDebug) Debug.Log("yielding autoxMeshIO.doneGetParams == true");
        yield return new WaitUntil(() => autoxMeshIO.doneGetParams == true);
        if (isDebug) Debug.Log("done meshIO_X.MeshOut");

        ///Chỗ này chỉ lấy được origin of mesh
        autoxMeshIO.PasteToOrigin();

        xPointControl.SetIndex(autoxMeshIO.iPos0, autoxMeshIO.iPos1, autoxMeshIO.iPos2);

        if (isDebug) Debug.Log("start xPointControl.GetInfoOrigin");
        xPointControl.GetInfoOrigin_byVert();
        if (isDebug) Debug.Log("done xPointControl.GetInfoOrigin");

        setPivotMethod.ZeroPosition();

        xPointControl.oriOnOff(true);

        doneInit = true;
        if (isDebug) Debug.Log("done Init");

        yield break;
    }

    [Header("Auto Direction")]
    public bool doneAutoDirection = true;

    [ContextMenu("AutoDirection")]
    public void AutoDirection()
    {    
        StartCoroutine(C_AutoDirection());
    }        
    
    IEnumerator C_AutoDirection()
    {
        if (isDebug) Debug.Log("Starting AutoDirection");
        doneAutoDirection = false;

        if (isDebug) Debug.Log("Reset Transform to zero");
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;

        if (isDebug) Debug.Log("start boundingBox.Truncate");
        truncate.DoTruncate();
        if (isDebug) Debug.Log("yielding boundingBox.Truncate");
        yield return new WaitUntil(() => truncate.boolTruncate == false && truncate.hasX == false && truncate.hasNX == false && truncate.hasY == false && truncate.hasNY == false && truncate.hasZ == false && truncate.hasNZ == false);
        if (isDebug) Debug.Log("done boundingBox.Truncate");

        if (isDebug) Debug.Log("start xPointControl.GetInfoTruncate");
        xPointControl.GetInfoTruncate_byVert();
        if (isDebug) Debug.Log("done boundingBox.Truncate");

        if (isDebug) Debug.Log("xPointControl.truncOnOff(true);");
        xPointControl.truncOnOff(true);
        
        //if (!Application.isPlaying)
        //{
            if (isDebug) Debug.Log("start xPointControl.GetInfoTruncate");
            xPointControl.SnapAngle_Update();

            if (isDebug) Debug.Log("yielding xPointControl.doneSnap == true");
            yield return new WaitUntil(() => xPointControl.doneSnap == true);
            if (isDebug) Debug.Log("done yielding xPointControl.doneSnap == true");
        //}
        //else
        //{
        //    if (isDebug) Debug.Log("start xPointControl.SnapAngle_Courotine");
        //    xPointControl.SnapAngle_Coroutine();

        //    if (isDebug) Debug.Log("yielding xPointControl.SnapAngle Coroutine");
        //    yield return new WaitUntil(() => xPointControl.doneAngleCoroutine == true);
        //}

        if (isDebug) Debug.Log("done xPointControl.SnapAngle");

        if (isDebug) Debug.Log("Local positon = 0");
        setPivotMethod.SetPivotTo(transform.parent.position);

        if (isDebug) Debug.Log("originOfMeshBound.Center();");
        originOfMeshBound.Center();

        if (isDebug) Debug.Log("boundingBox.DefinePeek()");
        boundingBox.DefinePeek();

        if (isDebug) Debug.Log("x_definePicaMovementX.DefinePicaMovementX_UseBSOri();");
        x_definePicaMovementX.DefinePicaMovementX_UseBSOri();

        xPointControl.oriOnOff(false);
        xPointControl.truncOnOff(false);

        if (!Application.isPlaying) showMeshBounds_EditMode.draw = false;
        else showMeshBounds_Runtime.draw = false;

        if (isDebug) Debug.Log("start x_fileCenterline.outFile");

        centerFileJson centerlineFile = new centerFileJson();
        centerlineFile.oriPos = x_definePicaMovementX.picaMovementPeek.position;
        centerlineFile.oriRot = x_definePicaMovementX.picaMovementPeek.rotation;

        x_fileCenterline.outFile(centerlineFile.toJson());
        yield return new WaitUntil(() => x_fileCenterline.doneOutFile == true);

        //if (isDebug) Debug.Log("if done, output and then input data back!!");
        //x_fileCenterline.DoInput();
        //yield return new WaitUntil(() => x_fileCenterline.doneInput == true);

        if (isDebug) Debug.Log("done x_fileCenterline.outFile");

        doneAutoDirection = true;
        if (isDebug) Debug.Log("Done AutoDirection");

        yield break;
    }

    [ContextMenu("Stop")]
    public void Stop()
    {
        StopAllCoroutines();
        autoxMeshIO.StopAllCoroutines();
        truncate.StopAllCoroutines();
        truncate.boolTruncate = false;
        showMeshBounds_EditMode.StopAllCoroutines();
        showMeshBounds_Runtime.StopAllCoroutines();        
        xPointControl.StopAllCoroutines();
        xPointControl.boolUpdate = false;
        meshOut.Stop();

        AfterFullAuto();
    }

    [ContextMenu("ResetParams")]
    public void ResetParams()
    {
        x_fileCenterline.ResetParams();
        meshOut.ResetParams();
    }

    [ContextMenu("ResetMesh")]
    public void ResetMesh()
    {
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        transform.localScale = Vector3.one;
        autoxMeshIO.MeshIn();
    }

    [ContextMenu("MeshOutCurrent")]
    public void MeshOutCurrent()
    {
        autoxMeshIO.OutputToMO();
        meshOut.meshOut_Current();
    }
}
