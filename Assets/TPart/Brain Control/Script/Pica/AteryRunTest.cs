﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AteryRunTest : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Params")]
    public AteryRun ateryRun = null;
    public int childID = 0;
    public float distance = 0;
    public float xPos = 0;
    public float yPos = 0;
    public float zPos = 0;

    [ContextMenu("RunAtCurrent")]
    public void RunAtCurrent()
    {
        ateryRun.RunAt(childID, distance, xPos, yPos, zPos);
    }
}
