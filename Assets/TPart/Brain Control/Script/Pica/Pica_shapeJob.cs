﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Pica_shapeJob : MonoBehaviour
{
    [Header("shapeControls")]
    public List<ShapeJob> shapeControls = new List<ShapeJob>();

    [ContextMenu("Get_sjControls")]
    public void Get_sjControls()
    {
        shapeControls = GetComponentsInChildren<ShapeJob>().ToList();
    }
}
