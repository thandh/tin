﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class PicaChild : MonoBehaviour, IPointerDownHandler
{
    [Header("Debug")]
    public bool isDebug = false;
    
    public enum etypePica { pica, atery }
    [Header("Self")]
    public etypePica typePica = etypePica.pica;

    SphereCollider _sc = null;
    public SphereCollider sc { get { if (_sc == null) _sc = GetComponent<SphereCollider>(); return _sc; } set { _sc = value; } }

    public void OnOffThru(bool On)
    {
        sc.isTrigger = On;
    }

    Rigidbody _rb = null;
    public Rigidbody rb { get { if (_rb == null) _rb = GetComponent<Rigidbody>(); return _rb; } set { _rb = value; } }

    [Header("Controls")]
    public HingeJointControl hjControl = null;
    public FixJointControl fjControl = null;
    public SpringJointControl sjControl = null;
    public ConfiJointControl cjControl = null;

    [ContextMenu("GetControls")]
    public void GetControls()
    {
        hjControl = GetComponent<HingeJointControl>();
        fjControl = GetComponent<FixJointControl>();
        sjControl = GetComponent<SpringJointControl>();
        cjControl = GetComponent<ConfiJointControl>();
    }

    [Header("Params Movement")]
    public bool boolAllowMove = true;
    public bool boolGoVerticalFar = false;
    public bool boolGoVerticalNear = false;
    public float verStep = 0.1f;

    [Header("Original")]
    public Vector3 _pos0 = Vector3.zero;
    public Quaternion _rot0 = Quaternion.identity;

    public Vector3 Position { get { return transform.position; } set { transform.position = value; } }
    public Quaternion Rotation { get { return transform.rotation; } set { transform.rotation = value; } }

    [Header("Params Stop")]
    public bool forcePicaStop = true;

    private void Start()
    {

    }

    private void OnDestroy()
    {

    }

    [ContextMenu("CopyTransition")]
    public void CopyTransition()
    {
        _pos0 = Position;
        _rot0 = Rotation;
    }

    [ContextMenu("ResetPosition")]
    public void ResetPosition()
    {
        Position = _pos0;
        Rotation = _rot0;
    }

    public void OnOff_Collider(bool On)
    {
        if (sc == null) sc = GetComponent<SphereCollider>();
        if (rb == null) rb = GetComponent<Rigidbody>();
        sc.enabled = On;
        if (sc.enabled == false) rb.constraints = RigidbodyConstraints.FreezeAll;
        else if (sc.enabled == true) rb.constraints = RigidbodyConstraints.None;
    }

    public void Toggle_Collider()
    {
        if (sc == null) sc = GetComponent<SphereCollider>();
        if (rb == null) rb = GetComponent<Rigidbody>();
        sc.enabled = !sc.enabled;
        if (sc.enabled == false) rb.constraints = RigidbodyConstraints.FreezeAll;
        else if (sc.enabled == true) rb.constraints = RigidbodyConstraints.None;
    }

    private void OnMouseDrag()
    {
        if (!boolAllowMove) return;

        if (boolGoVerticalFar || boolGoVerticalNear) return;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftControl) && boolGoVerticalFar) return;
        if (Input.GetKeyUp(KeyCode.LeftControl) && !boolGoVerticalFar) return;
        if (Input.GetKeyDown(KeyCode.LeftControl) && !boolGoVerticalFar) boolGoVerticalFar = true;
        if (Input.GetKeyUp(KeyCode.LeftControl) && boolGoVerticalFar) boolGoVerticalFar = false;

        if (Input.GetKeyDown(KeyCode.LeftShift) && boolGoVerticalNear) return;
        if (Input.GetKeyUp(KeyCode.LeftShift) && !boolGoVerticalNear) return;
        if (Input.GetKeyDown(KeyCode.LeftShift) && !boolGoVerticalNear) boolGoVerticalNear = true;
        if (Input.GetKeyUp(KeyCode.LeftShift) && boolGoVerticalNear) boolGoVerticalNear = false;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (!boolAllowMove) return;

        if (boolGoVerticalFar)
        {
            if (isDebug) Debug.Log("GoVerticalFar");
            Vector3 direction = Camera.main.transform.position - transform.position;
            transform.position += direction.normalized * verStep;
        }

        if (boolGoVerticalNear)
        {
            if (isDebug) Debug.Log("GoVerticalNear");
            Vector3 direction = Camera.main.transform.position - transform.position;
            transform.position -= direction.normalized * verStep;
        }
    }

    [Header("Params Sync")]

    PicaUp _picaUp = null;
    PicaUp picaUp { get { if (_picaUp == null) _picaUp = PICA.Instance.picaUp; return _picaUp; } }

    public void SyncTo(Vector3 pos, Quaternion rot)
    {
        StartCoroutine(C_SyncPos(pos));
        StartCoroutine(C_SyncRot(rot));
    }

    public void SyncPos(Vector3 pos)
    {
        StartCoroutine(C_SyncPos(pos));
    }

    IEnumerator C_SyncPos(Vector3 pos)
    {
        picaUp.Increase_stepPos();

        while ((Position - pos).magnitude >= picaUp.deltaCheckPos)
        {
            float step = Time.deltaTime * picaUp.stepPos;
            Position = new Vector3(Mathf.Lerp(Position.x, pos.x, step), Mathf.Lerp(Position.y, pos.y, step), Mathf.Lerp(Position.z, pos.z, step));
            yield return new WaitForEndOfFrame();
        }

        Position = pos;
        if (isDebug) Debug.Log("Done C_SyncPos");
        yield break;
    }

    public void SyncRot(Quaternion rot)
    {
        StartCoroutine(C_SyncRot(rot));
    }

    IEnumerator C_SyncRot(Quaternion rot)
    {
        while (Rotation != rot)
        {
            float step = Time.deltaTime * picaUp.stepRot;
            Rotation = new Quaternion(Mathf.Lerp(Rotation.x, rot.x, step), Mathf.Lerp(Rotation.y, rot.y, step), Mathf.Lerp(Rotation.z, rot.z, step), Mathf.Lerp(Rotation.w, rot.w, step));
            if (transform.name == "1") { if (isDebug) Debug.Log((Rotation.eulerAngles - rot.eulerAngles).magnitude); }
            yield return new WaitForEndOfFrame();
        }

        Rotation = rot;
        if (isDebug) Debug.Log("Done C_SyncRot");
        yield break;
    }

    [Header("Run To Skull")]
    public bool hitSkull = false;
    public float forceMultiply = 9f;
    public float forceMultiplyFinal = 1f;
    public float timeStep_forceMultiply = 0.05f;
    public float stepIncrease_forceMultiply = 0.1f;

    public void Increase_forceMultiply()
    {
        StartCoroutine(C_Increase_forceMultiply());
    }

    IEnumerator C_Increase_forceMultiply()
    {
        forceMultiply = 0f;
        while (forceMultiply != forceMultiplyFinal)
        {
            forceMultiply = Mathf.Lerp(forceMultiply, forceMultiplyFinal, Time.deltaTime * stepIncrease_forceMultiply);
            yield return new WaitForSeconds(timeStep_forceMultiply);
        }
        yield break;
    }

    [Header("RunToSkull")]
    public PicaMuzzlee hitSkullAnchor = null;
    public float currentDistanceToMove = 0f;
    public Vector3 currentDirToMove = Vector3.zero;
    public bool doneRunToSkull = true;

    [ContextMenu("RunToSkull")]
    public void RunToSkull()
    {
        currentDistanceToMove = picaUp.disTravel;

        X_AutoDirection Xauto = X.Instance.auto;
        currentDirToMove = Xauto.xHitSkull.transform.position - transform.position;

        forceRun = picaUp.forceRun;
        forceRunFinal = picaUp.forceRunFinal;
        timeStep_forceRun = picaUp.timeStep_forceRun;
        stepIncrease_forceRun = picaUp.stepIncrease_forceRun;

        StartCoroutine(C_RunToSkull());
    }    

    public IEnumerator C_RunToSkull()
    {
        if (isDebug) Debug.Log("Start C_RunToSkull() " + transform.name);

        doneRunToSkull = false;

        hitSkull = false;        
        Increase_forceRunDirection();

        PICA.Instance.picaDrag.SetPicaKinematic(false);

        while (
            ///Remove for allowing run thru Skull Collider
            ///!picaUp.CheckStopGoSkull() && 
            (transform.position - _pos0).magnitude < currentDistanceToMove)
        {
            if (isDebug) Debug.Log("Adding " + rb.transform.name + " force : " + currentDirToMove.normalized * forceRun + " distance travel : " + (transform.position - _pos0).magnitude);            
            rb.position += currentDirToMove.normalized * forceRun * Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }        

        if (isDebug) Debug.Log("Done C_RunToSkull()");

        if (picaUp.boolPauseAfterGoSkull)
        {
            Debug.Log("Scale0");
            TimeScale.Scale0();
        }

        doneRunToSkull = true;

        yield break;
    }

    [Header("forceRun params")]
    public float forceRun = 0.01f;
    public float forceRunFinal = 20f;
    public float timeStep_forceRun = 0.05f;
    public float stepIncrease_forceRun = 0.1f;

    public void Increase_forceRunDirection()
    {
        StartCoroutine(C_Increase_forceRunDirection());
    }

    IEnumerator C_Increase_forceRunDirection()
    {
        forceRun = 0f;
        while (forceRun != forceRunFinal)
        {
            forceRun = Mathf.Lerp(forceRun, forceRunFinal, Time.deltaTime * stepIncrease_forceRun);
            yield return new WaitForSeconds(timeStep_forceRun);
        }
        yield break;
    }

    [ContextMenu("Run0")]
    public void Run0()
    {
        StartCoroutine(C_Run0());
    }

    IEnumerator C_Run0()
    {
        while ((Position - _pos0).magnitude > picaUp.deltaCheckPos)
        {
            Vector3 dir = _pos0 - Position;

            rb.AddForce(dir.normalized * picaUp.forceRun);
            yield return new WaitForEndOfFrame();
        }
    }

    [ContextMenu("SelfRemove")]
    public void SelfRemove()
    {
        Pica pica = GetComponentInParent<Pica>();
        int index = pica.picachildList.FindIndex((x) => x == this);
        if (index == -1) { Debug.Log("Kong co!!"); return; }
        pica.picachildList.RemoveAt(index);

        if (Application.isPlaying) Destroy(gameObject); else DestroyImmediate(gameObject);
    }

    [ContextMenu("DebugAll")]
    public void DebugAll()
    {
        Pica pica = GetComponentInParent<Pica>();
        Debug.Log("ID in list : " + pica.picachildList.FindIndex((x) => x == this));
    }

    public int IDInList()
    {
        Pica pica = GetComponentInParent<Pica>();
        return pica.picachildList.FindIndex((x) => x == this);
    }

    [ContextMenu("Stop")]
    public void Stop()
    {
        StopAllCoroutines();
    }

    //[Header("Stop By Anchor")]
    //public Transform ancStop = null;

    //[ContextMenu("RemoveAncStop")]
    //public void RemoveAncStop()
    //{

    //}
}
