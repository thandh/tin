﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Pica_AutoSetup : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;
    public bool clickHere = false;

    [Header("PicaTr on scene")]
    public Transform picaTr = null;

    [Header("Params")]
    public List<Transform> childList = new List<Transform>();
    public List<SkinnedMeshRenderer> smrList = new List<SkinnedMeshRenderer>();

    [Header("ForceRefresh")]
    public bool doneForceRefresh = true;

    [ContextMenu("ForceRefresh")]
    public void ForceRefresh()
    {
        StartCoroutine(C_ForceRefresh());
    }

    IEnumerator C_ForceRefresh()
    {
        if (isDebug) Debug.Log("Start C_ForceRefresh");
        doneForceRefresh = false;

        Pica pica = picaTr.GetComponentInChildren<Pica>();
        if (pica != null) if (Application.isPlaying) Destroy(pica); else DestroyImmediate(pica);

        Pica_PickNDrag pick = picaTr.GetComponentInChildren<Pica_PickNDrag>();
        if (pick != null) if (Application.isPlaying) Destroy(pick); else DestroyImmediate(pick);

        PicaUp up = picaTr.GetComponentInChildren<PicaUp>();
        if (up != null) if (Application.isPlaying) Destroy(up); else DestroyImmediate(up);

        yield return new WaitUntil(() => pica == null && pick == null && up == null);
        yield return new WaitForEndOfFrame();

        if (isDebug) Debug.Log("Done C_ForceRefresh");
        doneForceRefresh = true;

        yield break;
    }

    [Header("DefineChild")]
    public bool doneDefineChild = true;

    [ContextMenu("DefineChild")]
    public void DefineChild()
    {
        StartCoroutine(C_DefineChild());
    }

    IEnumerator C_DefineChild()
    {
        if (isDebug) Debug.Log("Start C_DefineChild");
        doneDefineChild = false;

        if (picaTr == null) { if (isDebug) Debug.Log("picaGO null!! Return!!"); yield break; }

        Pica pica = picaTr.GetComponentInChildren<Pica>();
        if (pica != null) { if (isDebug) Debug.Log("Pica existed!! Return!!"); yield break; }

        smrList = picaTr.GetComponentsInChildren<SkinnedMeshRenderer>().ToList();

        childList = new List<Transform>();
        foreach (Transform tr in picaTr.GetComponentsInChildren<Transform>())
            if (tr != picaTr && tr.GetComponent<SkinnedMeshRenderer>() == null) childList.Add(tr);

        yield return new WaitForEndOfFrame();

        doneDefineChild = true;
        if (isDebug) Debug.Log("Done C_DefineChild");
        yield break;
    }

    [ContextMenu("LoadChild")]
    public void LoadChild()
    {
        List<PicaChild> picaList = GetComponentsInChildren<PicaChild>().ToList();
        childList = new List<Transform>();
        for (int i = 0; i < picaList.Count; i++) childList.Add(picaList[i].transform);
    }

    [ContextMenu("LoadSmr")]
    public void LoadSmr()
    {
        smrList = GetComponentsInChildren<SkinnedMeshRenderer>().ToList();
    }

    [Header("SetupChilds")]
    public bool currentSyncSortList = false;
    public bool doneSetupChilds = true;

    [ContextMenu("SetupChildsSyncOrNot")]
    public void SetupChildsSyncOrNot()
    {
        SetupChilds(currentSyncSortList, currentSetupChildsCollider);
    }

    public void SetupChilds(bool boolSyncSortList = true, bool boolSetupCollider = true)
    {
        StartCoroutine(C_SetupChilds(boolSyncSortList, boolSetupCollider));
    }

    IEnumerator C_SetupChilds(bool boolSyncSortList = true, bool boolSetupCollider = true)
    {
        if (isDebug) Debug.Log("Start C_SetupChilds");
        doneSetupChilds = false;

        if (boolSyncSortList) SyncSortList();

        foreach (Transform child in childList)
            SetupPicaChild(child, boolSetupCollider);

        yield return new WaitForEndOfFrame();
        if (isDebug) Debug.Log("Done C_SetupChilds");
        doneSetupChilds = true;

        yield break;
    }

    [Header("Current SetupChildsCollider")]
    public bool currentSetupChildsCollider = false;

    [ContextMenu("SetupChildsCollider")]
    public void SetupChildsCollider()
    {
        foreach (Transform child in childList) SetupCollider(child, currentSetupChildsCollider);
    }

    [Header("PicaChild")]
    public PicaMuzzlee muzzle = null;

    void SetupPicaChild(Transform child, bool boolSetupCollider = true)
    {
        PicaChild picaChild = child.GetComponent<PicaChild>() == null ? child.gameObject.AddComponent<PicaChild>() : child.GetComponent<PicaChild>();
        picaChild.hitSkullAnchor = muzzle;

        SetupHJControl(child);
        SetupFJControl(child);
        SetupSJControl(child);
        SetupCJControl(child);
        picaChild.GetControls();

        SetupRigidBodyControl(child);
        SetupRBTemplate(child);
        if (boolSetupCollider) SetupSCTemplate(child);

        SetupCollisionDetect(child);
        if (boolSetupCollider) SetupCollider(child);
    }

    void SetupHJControl(Transform child)
    {
        HingeJointControl hjControl = child.GetComponent<HingeJointControl>() == null ? child.gameObject.AddComponent<HingeJointControl>() : child.GetComponent<HingeJointControl>();
        HJsList hjList = child.GetComponent<HJsList>() == null ? child.gameObject.AddComponent<HJsList>() : child.GetComponent<HJsList>();
    }

    void SetupFJControl(Transform child)
    {
        FixJointControl fjControl = child.GetComponent<FixJointControl>() == null ? child.gameObject.AddComponent<FixJointControl>() : child.GetComponent<FixJointControl>();
    }

    void SetupSJControl(Transform child)
    {
        SpringJointControl sjControl = child.GetComponent<SpringJointControl>() == null ? child.gameObject.AddComponent<SpringJointControl>() : child.GetComponent<SpringJointControl>();
    }

    [Header("ConfiTemplate")]
    public ConfigurableJoint sampleConfi = null;

    void SetupCJControl(Transform child)
    {
        ConfiJointControl cjControl = child.GetComponent<ConfiJointControl>() == null ? child.gameObject.AddComponent<ConfiJointControl>() : child.GetComponent<ConfiJointControl>();
        cjControl.cjTemplate = sampleConfi;
    }

    void SetupRigidBodyControl(Transform child)
    {
        RigidBodyControl rbControl = child.GetComponent<RigidBodyControl>() == null ? child.gameObject.AddComponent<RigidBodyControl>() : child.GetComponent<RigidBodyControl>();
        RigidbodyInfo rbInfo = child.GetComponent<RigidbodyInfo>() == null ? child.gameObject.AddComponent<RigidbodyInfo>() : child.GetComponent<RigidbodyInfo>();
    }

    void SetupCollisionDetect(Transform child)
    {
        CollisionDetect colDetect = child.GetComponent<CollisionDetect>() == null ? child.gameObject.AddComponent<CollisionDetect>() : child.GetComponent<CollisionDetect>();
    }

    [Header("Setup Collider")]
    List<List<Vector3>> sortList = new List<List<Vector3>>();

    [ContextMenu("DebugSortList")]
    public void DebugSortList()
    {
        Debug.Log("Count : " + sortList.Count);
    }

    [ContextMenu("SyncSortList")]
    public void SyncSortList()
    {
        sortList = new List<List<Vector3>>();
        foreach (SkinnedMeshRenderer smr in smrList)
        {
            Mesh m = new Mesh();
            m.vertices = smr.sharedMesh.vertices;
            Vector3[] verts = m.vertices;
            sortList.Add(verts.ToList());
        }
    }

    void SetupCollider(Transform child, bool boolSetupCollider = true)
    {
        if (boolSetupCollider == false) return;

        float minDis = Mathf.Infinity;
        Vector3 posChild = child.position;

        foreach (List<Vector3> verts in sortList)
        {
            verts.Sort((a, b) => (a - posChild).magnitude.CompareTo((b - posChild).magnitude));
            if ((verts[0] - posChild).magnitude < minDis) minDis = (verts[0] - posChild).magnitude;
        }

        SphereCollider sc = child.GetComponent<SphereCollider>();
        if (sc != null) { sc.radius = minDis; if (isDebug) Debug.Log(child.name + " radius : " + minDis); }
    }

    [Header("RBTemplate")]
    public RigidbodyTemplate sampleRB = null;

    void SetupRBTemplate(Transform child)
    {
        RigidbodyTemplate rbTemplate = child.GetComponent<RigidbodyTemplate>() == null ? child.gameObject.AddComponent<RigidbodyTemplate>() : child.GetComponent<RigidbodyTemplate>();
        Rigidbody rb = child.GetComponent<Rigidbody>() == null ? child.gameObject.AddComponent<Rigidbody>() : child.GetComponent<Rigidbody>();
        rbTemplate.CopyFrom(sampleRB);
        rbTemplate.Paste();
    }

    [Header("SCTemplate")]
    public SphereColliderTemplate sampleSC = null;

    void SetupSCTemplate(Transform child)
    {
        SphereColliderTemplate scTemplate = child.GetComponent<SphereColliderTemplate>() == null ? child.gameObject.AddComponent<SphereColliderTemplate>() : child.GetComponent<SphereColliderTemplate>();
        SphereCollider sc = child.GetComponent<SphereCollider>() == null ? child.gameObject.AddComponent<SphereCollider>() : child.GetComponent<SphereCollider>();
        scTemplate.CopyFrom(sampleSC);
        scTemplate.Paste();
    }

    [Header("PICA")]
    public PICA PICA = null;
    public bool doneSetupPicaComponent = true;

    [ContextMenu("SetupPicaComponent")]
    public void SetupPicaComponent()
    {
        StartCoroutine(C_SetupPicaComponent());
    }

    IEnumerator C_SetupPicaComponent()
    {
        if (isDebug) Debug.Log("Start C_SetupPicaComponent");
        doneSetupPicaComponent = false;

        Pica pica = picaTr.GetComponent<Pica>() == null ? picaTr.gameObject.AddComponent<Pica>() : picaTr.GetComponent<Pica>();
        pica.LoadChild();
        this.pica = pica;

        Pica_PickNDrag picaPick = picaTr.GetComponent<Pica_PickNDrag>() == null ? picaTr.gameObject.AddComponent<Pica_PickNDrag>() : picaTr.GetComponent<Pica_PickNDrag>();
        picaPick.pica = pica;

        PicaUp picaUp = picaTr.GetComponent<PicaUp>() == null ? picaTr.gameObject.AddComponent<PicaUp>() : picaTr.GetComponent<PicaUp>();
        picaUp.pica = pica;
        picaUp._REZ = Rez.Instance.gameObject;

        Pica_hjJob hjJob = picaTr.GetComponent<Pica_hjJob>() == null ? picaTr.gameObject.AddComponent<Pica_hjJob>() : picaTr.GetComponent<Pica_hjJob>();
        Pica_sjJob sjJob = picaTr.GetComponent<Pica_sjJob>() == null ? picaTr.gameObject.AddComponent<Pica_sjJob>() : picaTr.GetComponent<Pica_sjJob>();
        Pica_rbJob rbJob = picaTr.GetComponent<Pica_rbJob>() == null ? picaTr.gameObject.AddComponent<Pica_rbJob>() : picaTr.GetComponent<Pica_rbJob>();
        Pica_cjJob cjJob = picaTr.GetComponent<Pica_cjJob>() == null ? picaTr.gameObject.AddComponent<Pica_cjJob>() : picaTr.GetComponent<Pica_cjJob>();

        ConfigurableJoint cjTemplate = picaTr.GetComponent<ConfigurableJoint>() == null ? picaTr.gameObject.AddComponent<ConfigurableJoint>() : picaTr.GetComponent<ConfigurableJoint>();

        PICA.pica = pica;
        PICA.picaUp = picaUp;
        PICA.picaDrag = picaPick;

        yield return new WaitForEndOfFrame();
        if (isDebug) Debug.Log("Done C_SetupPicaComponent");
        doneSetupPicaComponent = true;

        yield break;
    }

    [Header("FullAuto")]
    public bool boolForceFresh = true;
    public bool boolDefineChild = true;
    public bool boolSetupChilds = true;
    public bool boolSetupPicaComponent = true;

    [Header("FullAuto")]
    public bool doneFullAuto = true;

    [ContextMenu("FullAuto")]
    public void FullAuto()
    {
        StartCoroutine(C_FullAuto());
    }

    IEnumerator C_FullAuto()
    {
        if (isDebug) Debug.Log("Start C_FullAuto");
        doneFullAuto = false;

        if (boolForceFresh)
        {
            ForceRefresh();
            yield return new WaitUntil(() => doneForceRefresh == true);
        }

        if (boolDefineChild)
        {
            DefineChild();
            yield return new WaitUntil(() => doneDefineChild == true);
        }

        if (boolSetupChilds)
        {
            SetupChilds(currentSyncSortList, currentSetupChildsCollider);
            yield return new WaitUntil(() => doneSetupChilds == true);
        }

        if (boolSetupPicaComponent)
        {
            SetupPicaComponent();
            yield return new WaitUntil(() => doneSetupPicaComponent == true);
        }

        if (isDebug) Debug.Log("Done C_FullAuto");
        doneFullAuto = true;

        yield break;
    }

    [ContextMenu("DeleteShape")]
    public void DeleteShape()
    {
        foreach (Transform tr in childList)
        {
            ShapeJob sj = tr.GetComponentInChildren<ShapeJob>();
            if (sj != null)
                if (Application.isPlaying) Destroy(sj.gameObject); else DestroyImmediate(sj.gameObject);
        }
    }

    [ContextMenu("UpdateShape")]
    public void UpdateShape()
    {
        foreach (Transform tr in childList)
        {
            ShapeJob sj = tr.GetComponentInChildren<ShapeJob>();
            SphereCollider sc = tr.GetComponent<SphereCollider>();
            if (sc != null) { sj.lscale = new Vector3(sc.radius, sc.radius, sc.radius); sj.transform.localPosition = Vector3.zero; }
        }
    }

    [ContextMenu("ShowOnShape")]
    public void ShowOnShape()
    {
        foreach (Transform tr in childList)
        {
            ShapeJob sj = tr.GetComponentInChildren<ShapeJob>();
            sj.showShape = true; sj.ShowShape();
        }
    }

    [ContextMenu("ShowOffShape")]
    public void ShowOffShape()
    {
        foreach (Transform tr in childList)
        {
            ShapeJob sj = tr.GetComponentInChildren<ShapeJob>();
            sj.showShape = false; sj.ShowShape();
        }
    }

    [Header("Setup Joint")]
    public Pica_AutoSetup_CreateAnchor anchorCreate = null;
    public SeriPica seriPica = null;
    public Pica pica = null;

    [ContextMenu("ResetSeriPica")]
    public void ResetSeriPica()
    {
        if (this.pica == null) return;

        foreach (PicaChild child in this.pica.picachildList)
        {
            child.cjControl.cjdataList = new List<aCJData>();
            child.cjControl.PasteCJ();
        }
    }

    [ContextMenu("SetupSeriPica")]
    public void SetupSeriPica()
    {
        if (this.pica == null) return;

        foreach (SeriPica.picaSeri seri in seriPica.picaSeriList)
        {
            foreach (Transform tr in seri.trList)
            {
                if (tr != seri.trList[0])
                {
                    ConfiJointControl cjControl = tr.GetComponent<ConfiJointControl>();
                    cjControl.use = true;
                    cjControl.TargetPrevious();
                    cjControl.PasteCJ();
                }
            }
        }

        foreach (SeriPica.seriFromTo connect in seriPica.connectSeri)
        {
            ConfiJointControl cjControl = connect.trFrom.GetComponent<ConfiJointControl>();
            cjControl.use = true;
            cjControl.cjdataList.Add(new aCJData(connect.trTo));
            cjControl.PasteCJ();
        }
    }

    [Header("Setting CJ")]
    public float linearLimitSpring = 100f;
    public float damper = 100f;
    public bool doneSetAllConfigJointLimited = true;

    [ContextMenu("SetAllConfigJointLimited")]
    public void SetAllConfigJointLimited()
    {
        StartCoroutine(C_SetAllConfigJointLimited());
    }

    IEnumerator C_SetAllConfigJointLimited()
    {
        if (isDebug) Debug.Log("Start C_SetAllConfigJointLimited");
        doneSetAllConfigJointLimited = false;

        List<ConfigurableJoint> cjList = GetComponentsInChildren<ConfigurableJoint>().ToList();
        foreach (ConfigurableJoint cj in cjList)
        {
            cj.xMotion = ConfigurableJointMotion.Limited;
            cj.yMotion = ConfigurableJointMotion.Limited;
            cj.zMotion = ConfigurableJointMotion.Limited;

            cj.angularXMotion = ConfigurableJointMotion.Limited;
            cj.angularYMotion = ConfigurableJointMotion.Limited;
            cj.angularZMotion = ConfigurableJointMotion.Limited;

            SoftJointLimitSpring sjls = new SoftJointLimitSpring();
            sjls.spring = this.linearLimitSpring;
            sjls.damper = this.damper;

            cj.linearLimitSpring = sjls;
        }

        yield return new WaitForEndOfFrame();

        if (isDebug) Debug.Log("Done C_SetAllConfigJointLimited");
        doneSetAllConfigJointLimited = true;

        yield break;
    }
}
