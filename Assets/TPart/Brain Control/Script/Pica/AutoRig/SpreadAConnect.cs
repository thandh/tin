﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpreadAConnect : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;
    public bool clickHere = false;
    public bool boolWaitEndOfFrame = false;

    [Serializable]
    public class Spread
    {
        public Vector3 pos = Vector3.zero;
        public int minReach = 9999;
        public int minReachIndex = -1;
        public Spread() { }
        public Spread(Vector3 pos) { this.pos = pos; }
    }

    [Header("Spread")]
    public List<Spread> spreadList = new List<Spread>();
    public int spreadListCount = 0;

    [Header("Input")]
    public Transform fromBone = null;
    public Transform toBone = null;

    [Header("Define")]
    public DistributeBySattelite distribute = null;
    PicaRigScip.tBoneInfo fromBoneInfo = new PicaRigScip.tBoneInfo();
    PicaRigScip.tBoneInfo toBoneInfo = new PicaRigScip.tBoneInfo();
    List<PicaRigScip.tBoneInfo> avoidBoneInfo = new List<PicaRigScip.tBoneInfo>();

    [ContextMenu("DefineBoneInfo")]
    public void DefineBoneInfo()
    {
        avoidBoneInfo = new List<PicaRigScip.tBoneInfo>();
        for (int i = 0; i < distribute.boneInfoList.Count; i++) avoidBoneInfo.Add(new PicaRigScip.tBoneInfo(distribute.boneInfoList[i].boneTr.position, distribute.boneInfoList[i].radius));

        fromBoneInfo = avoidBoneInfo.Find((x) => x.bonePos == fromBone.transform.position);
        avoidBoneInfo.Remove(fromBoneInfo);

        toBoneInfo = avoidBoneInfo.Find((x) => x.bonePos == toBone.transform.position);
        avoidBoneInfo.Remove(toBoneInfo);
    }

    [Header("Setting")]
    public ConnectBySpreading connectBySpreading = null;

    [Header("Satellite")]
    SattelliteShape _sattelite = null;
    SattelliteShape sattelite { get { if (_sattelite == null) _sattelite = (Instantiate(connectBySpreading.sattelitePrefab.gameObject as GameObject).gameObject).GetComponent<SattelliteShape>(); return _sattelite; } }

    [Header("FullSpread")]
    public bool doneFullSpread = true;

    [ContextMenu("FullSpread")]
    public void FullSpread(bool boolRestart = true)
    {
        StartCoroutine(C_FullSpread(boolRestart));
    }

    public IEnumerator C_FullSpread(bool boolRestart = true)
    {
        if (isDebug) Debug.Log("Start C_FullSpread!");

        doneFullSpread = false;

        DefineBoneInfo();

        if (boolRestart)
        {
            RestartSpreadList();
            SpreadIt();
        }
        else
        {
            CheckCurrentSpreadList();
            if (resultCheckCurrent == false) SpreadIt();
        }
        yield return new WaitUntil(() => doneCheckSpreadAt == true);

        DebugHittoSpread();
        CheckDirect();

        if (resultDirect == true) CheckAddToStorage();

        if (Application.isPlaying) Destroy(sattelite.gameObject); else DestroyImmediate(sattelite.gameObject);

        doneFullSpread = true;
        if (isDebug) Debug.Log("Done C_FullSpread!");
        yield break;
    }

    void CheckAddToStorage()
    {
        tBoneConnectStorage storage = fromBone.GetComponent<tBoneConnectStorage>();
        if (storage == null) storage = fromBone.gameObject.AddComponent<tBoneConnectStorage>();
        if (storage.connectBones.FindIndex((x) => x.transform == toBone) == -1)
            storage.connectBones.Add(toBone.GetComponent<tBone>());
    }

    [ContextMenu("RestartSpreadList")]
    public void RestartSpreadList()
    {
        spreadList = new List<Spread>();

        Spread spread0 = new Spread(fromBone.transform.position);
        spread0.minReach = 0;
        spread0.minReachIndex = 0;
        spreadList.Add(spread0);
        spreadListCount = 1;
        currentCheckAt = 0;
    }

    [Header("Check Current")]
    public bool resultCheckCurrent = false;

    [ContextMenu("CheckCurrentSpreadList")]
    public void CheckCurrentSpreadList()
    {
        resultCheckCurrent = false;
        for (int i = 0; i < spreadList.Count; i++)
        {
            resultCheckCurrent = CheckHitTo(spreadList[i].pos);
            if (resultCheckCurrent == true)
            {
                if (isDebug) Debug.Log("Check current list " + resultCheckCurrent + " at " + i);
                hittoSpread = spreadList[i];
                break;
            }
        }

        if (isDebug) Debug.Log("Check current with count : " + spreadListCount + " : " + resultCheckCurrent);
    }

    [Header("Process")]
    public int currentCheckAt = -1;
    public bool hitTo = false;

    [ContextMenu("SpreadIt")]
    public void SpreadIt()
    {
        StartCoroutine(C_SpreadIt());
    }

    IEnumerator C_SpreadIt()
    {
        if (isDebug) Debug.Log("Start C_SpreadIt");

        hitTo = false;

        while (currentCheckAt < spreadList.Count && hitTo == false)
        {
            CheckSpreadAt(currentCheckAt);
            yield return new WaitUntil(() => doneCheckSpreadAt == true);

            if (!hitTo) currentCheckAt++;
            if (boolWaitEndOfFrame) yield return new WaitForEndOfFrame();
        }

        if (isDebug) Debug.Log("done C_SpreadIt " + hitTo + " count spread : " + spreadListCount);

        yield break;
    }

    [Header("Process")]
    public bool doneCheckSpreadAt = true;

    void CheckSpreadAt(int atIndex)
    {
        StartCoroutine(C_CheckSpreadAt(atIndex));
    }

    IEnumerator C_CheckSpreadAt(int spreadIndex)
    {
        doneCheckSpreadAt = false;
        Spread spread = spreadList[spreadIndex];

        for (int i = -1; i <= 1; i++)
            for (int j = -1; j <= 1; j++)
                for (int k = -1; k <= 1; k++)
                    if (i == 0 && j == 0 && k == 0) continue;
                    else
                    {
                        Vector3 newPos = spread.pos + new Vector3(this.connectBySpreading.vExtend.x * i, this.connectBySpreading.vExtend.y * j, this.connectBySpreading.vExtend.z * k);
                        if (spreadList.FindIndex((x) => x.pos == newPos) == -1)
                        {
                            StartCoroutine(CheckSattelite(newPos));
                            yield return new WaitUntil(() => sattelite.doneCloseIn == true);
                            if (currentAllClose == true)
                            {
                                Spread newSpread = new Spread(newPos);
                                if (spread.minReach + 1 < newSpread.minReach)
                                {
                                    newSpread.minReach = spread.minReach + 1;
                                    newSpread.minReachIndex = spreadIndex;
                                }

                                spreadList.Add(newSpread);
                                spreadListCount = spreadList.Count;
                                if (CheckHitTo(newPos))
                                {
                                    hitTo = true;
                                    doneCheckSpreadAt = true;
                                    this.hittoSpread = newSpread;
                                    yield break;
                                }
                            }
                        }
                    }

        doneCheckSpreadAt = true;
        yield break;
    }

    bool CheckHitTo(Vector3 pos)
    {
        return ((toBoneInfo.bonePos - pos).magnitude) <= toBoneInfo.radius;
    }

    [Header("HitTo Output")]
    public Spread hittoSpread = new Spread();

    [ContextMenu("DebugHittoSpread")]
    public void DebugHittoSpread()
    {
        StartCoroutine(C_DebugHittoSpread());
    }

    IEnumerator C_DebugHittoSpread()
    {
        if (hittoSpread.minReach == 9999) yield break; ;
        Spread traceSpread = hittoSpread;

        traceList = new List<Spread>();
        traceList.Add(traceSpread);

        while (traceSpread.minReach > 0)
        {
            traceSpread = spreadList[traceSpread.minReachIndex];
            traceList.Add(traceSpread);
            if (boolWaitEndOfFrame) yield return new WaitForEndOfFrame();
        }

        yield break;
    }

    [Header("Check Direct")]
    public HitByRay rayHit = null;
    public List<Spread> traceList = new List<Spread>();
    public bool resultDirect = false;

    [ContextMenu("CheckDirect")]
    public void CheckDirect()
    {
        resultDirect = true;

        for (int i = 0; i < traceList.Count - 1; i++)
        {
            CheckARay(traceList[i].pos, traceList[i + 1].pos);
            if (resultDirect == false) break;

            CheckARay(traceList[i + 1].pos, traceList[i].pos);
            if (resultDirect == false) break;
        }

        if (isDebug) Debug.Log("Direct " + resultDirect);
    }

    void CheckARay(Vector3 fromPos, Vector3 toPos)
    {
        rayHit.trFrom.position = fromPos;
        rayHit.trTo.position = toPos;
        Vector3 pos = -Vector3.one; Transform hitTr = null;
        rayHit.HitInfoUseFinalToFinal(out pos, out hitTr);
        if (pos != -Vector3.one && hitTr != null && hitTr != fromBone && hitTr != toBone)
        {
            resultDirect = false;
            if (isDebug) Debug.Log("Hit " + hitTr.name);
        }
    }

    bool currentAllClose = true;

    IEnumerator CheckSattelite(Vector3 pos)
    {
        currentAllClose = false;
        sattelite.CoreAt(pos);
        yield return new WaitUntil(() => sattelite.doneCloseIn == true);
        currentAllClose = sattelite.allCloseIn;
        yield break;
    }

    [ContextMenu("Stop")]
    public void Stop()
    {
        StopAllCoroutines();
    }
}
