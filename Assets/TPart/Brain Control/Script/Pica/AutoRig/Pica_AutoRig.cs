﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Pica_AutoRig : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;
    public bool clickHere = false;

    [Header("Smr")]
    public MeshMethods meshMethods = null;
    public SkinnedMeshRenderer smr = null;

    Transform ClosetBone(Vector3 pos)
    {
        if (bonesList.Count == 0 || bonesList == null) return null;
        Transform closetBone = bonesList[0];
        float currentClosest = (closetBone.position - pos).magnitude;
        for (int i = 1; i < bonesList.Count; i++)
            if ((bonesList[i].position - pos).magnitude < currentClosest)
            {
                closetBone = bonesList[i];
                currentClosest = (bonesList[i].position - pos).magnitude;
            }
        return closetBone;
    }

    int IDClosetBone(Vector3 pos)
    {
        if (bonesList.Count == 0 || bonesList == null) return -1;
        int closetIDBone = 0;
        float currentClosest = (bonesList[closetIDBone].position - pos).magnitude;
        for (int i = 1; i < bonesList.Count; i++)
            if ((bonesList[i].position - pos).magnitude < currentClosest)
            {
                closetIDBone = i;
                currentClosest = (bonesList[i].position - pos).magnitude;
            }
        return closetIDBone;
    }

    [Header("Process")]
    public bool boolBeforFullAuto = true;
    public bool boolRefreshMeshMethod = true;
    public bool boolDistribute = true;
    public bool boolRig = true;
    public bool boolConnectSpread = true;
    public bool boolSaveSrip = true;
    public bool boolSetupConnection = true;
    public bool boolSpreadConfi = true;
    public bool boolAfterFullAuto = true;

    [Header("Params")]
    public DistributeBySattelite distribute = null;
    public ConnectBySpreading connectSpreading = null;
    public ConnectBySpreadVert connectSpreadVert = null;
    public SpreadConfi spreadConfi = null;
    public Pica_AutoSetup autoSetup = null;

    [ContextMenu("FullAuto")]
    public void FullAuto()
    {
        StartCoroutine(C_FullAuto());
    }

    IEnumerator C_FullAuto()
    {
        if (isDebug) Debug.Log("Start C_FullAuto");

        if (boolBeforFullAuto)
        {
            BeforFullAuto();
        }

        if (boolRefreshMeshMethod)
        {
            meshMethods.ResetLocalMesh();
            meshMethods.Standardize();
            yield return new WaitForEndOfFrame();
        }

        if (boolDistribute)
        {
            distribute.doneDistribute = false;
            distribute.Distribute();
            yield return new WaitUntil(() => distribute.doneDistribute == true);
        }

        if (boolRig)
        {
            SyncDataFromDistribute();

            doneRig = false;
            StartCoroutine(C_Rig());
            yield return new WaitUntil(() => doneRig == true);
        }

        if (boolConnectSpread)
        {
            //connectSpreading.SpreadAll();
            //yield return new WaitUntil(() => connectSpreading.doneSpreadAll == true);

            connectSpreadVert.doneSpreadAll = false;
            connectSpreadVert.SpreadAll();
            yield return new WaitUntil(() => connectSpreadVert.doneSpreadAll == true);
        }

        if (boolSaveSrip)
        {
            if (isDebug) Debug.Log("Start SaveScrip");

            doneSaveScrip = false;
            SaveScrip();
            yield return new WaitUntil(() => doneSaveScrip == true);
        }

        if (boolSetupConnection)
        {
            autoSetup.doneFullAuto = false;
            autoSetup.FullAuto();
            yield return new WaitUntil(() => autoSetup.doneFullAuto == true);
        }

        if (boolSpreadConfi)
        {
            spreadConfi.doneSpreadConfi = false;
            spreadConfi.DoSpreadConfi();
            yield return new WaitUntil(() => spreadConfi.doneSpreadConfi == true);

            autoSetup.doneSetAllConfigJointLimited = false;
            autoSetup.SetAllConfigJointLimited();
            yield return new WaitUntil(() => autoSetup.doneSetAllConfigJointLimited == true);
        }

        if (boolAfterFullAuto)
        {
            AfterFullAuto();
        }

        if (isDebug) Debug.Log("Done C_FullAuto");
        yield break;
    }

    void BeforFullAuto()
    {
        MeshRenderer mr = meshMethods.GetComponent<MeshRenderer>();
        if (mr != null) mr.enabled = true;
        smr.enabled = false;
    }

    void AfterFullAuto()
    {
        MeshRenderer mr = meshMethods.GetComponent<MeshRenderer>();
        if (mr != null) mr.enabled = false;
        smr.enabled = true;
    }

    [Header("Bones")]
    public List<Transform> bonesList = new List<Transform>();

    [Header("Rig")]
    public bool doneRig = true;

    void SyncDataFromDistribute()
    {
        bonesList = new List<Transform>();
        tBone[] tbones = GetComponentsInChildren<tBone>();
        for (int i = 0; i < tbones.Length; i++) bonesList.Add(tbones[i].transform);
    }

    [ContextMenu("Rig")]
    public void Rig()
    {
        StartCoroutine(C_Rig());
    }

    IEnumerator C_Rig()
    {
        if (isDebug) Debug.Log("Start Rig");
        doneRig = false;

        Mesh mesh = new Mesh();
        mesh.name = "justRig";
        mesh.vertices = meshMethods.m.vertices;
        mesh.triangles = meshMethods.m.triangles;
        mesh.uv = meshMethods.m.uv;
        mesh.RecalculateNormals();

        Vector3[] vertices = mesh.vertices;
        BoneWeight[] weights = new BoneWeight[mesh.vertexCount];

        for (int i = 0; i < mesh.vertexCount; i++)
        {
            int closestBone = IDClosetBone(transform.TransformPoint(vertices[i]));
            if (closestBone == -1) { if (isDebug) Debug.Log("Khong tim thay tai : " + i); continue; }
            weights[i].boneIndex0 = closestBone;
            weights[i].weight0 = 1;
        }

        mesh.boneWeights = weights;

        Transform[] bonesAr = new Transform[bonesList.Count];
        for (int i = 0; i < bonesAr.Length; i++) bonesAr[i] = bonesList[i];

        Matrix4x4[] bindPoses = new Matrix4x4[bonesList.Count];
        for (int i = 0; i < bonesList.Count; i++)
            bindPoses[i] = bonesList[i].worldToLocalMatrix * transform.localToWorldMatrix;

        mesh.bindposes = bindPoses;

        smr.bones = bonesAr;
        smr.sharedMesh = mesh;

        doneRig = true;
        if (isDebug) Debug.Log("Done Rig");
        yield break;
    }

    [ContextMenu("Stop")]
    public void Stop()
    {
        StopAllCoroutines();
    }

    [Header("Save")]
    public PicaRigScip picaScrip = null;
    public bool doneSaveScrip = true;

    [ContextMenu("SaveScrip")]
    public void SaveScrip()
    {
        StartCoroutine(C_SaveScrip());
    }

    IEnumerator C_SaveScrip()
    {
        if (isDebug) Debug.Log("Start C_SaveScrip");
        doneSaveScrip = false;

        picaScrip.bones = new List<PicaRigScip.tBoneInfo>();
        for (int i = 0; i < distribute.boneInfoList.Count; i++)
        {
            PicaRigScip.tBoneInfo boneInfo = new PicaRigScip.tBoneInfo(distribute.boneInfoList[i]);
            picaScrip.bones.Add(boneInfo);

            tBoneConnectStorage storage = distribute.boneInfoList[i].boneTr.GetComponent<tBoneConnectStorage>();
            if (storage != null)
            {
                boneInfo.connectBones = new List<int>();
                for (int j = 0; j < storage.connectBones.Count; j++)
                    boneInfo.connectBones.Add(distribute.boneInfoList.FindIndex((x) => x.boneTr == storage.connectBones[j].transform));
            }
        }

        yield return new WaitForEndOfFrame();

        if (isDebug) Debug.Log("Start C_SaveScrip");
        doneSaveScrip = true;

        yield break;
    }

    [ContextMenu("LoadScrip")]
    public void LoadScrip()
    {
        StartCoroutine(C_LoadScrip());
    }

    IEnumerator C_LoadScrip()
    {
        if (isDebug) Debug.Log("Start LoadScrip");

        distribute.DeleteAll();
        yield return new WaitUntil(() => distribute.doneDeleteAll == true);

        for (int i = 0; i < picaScrip.bones.Count; i++)
        {
            distribute.CreateATBone(picaScrip.bones[i].bonePos, picaScrip.bones[i].radius);
            yield return new WaitUntil(() => distribute.doneCreateTbone == true);
        }

        bonesList = new List<Transform>();
        for (int i = 0; i < distribute.boneInfoList.Count; i++) bonesList.Add(distribute.boneInfoList[i].boneTr);

        for (int i = 0; i < picaScrip.bones.Count; i++)
            distribute.DistributeConnectAt(i, picaScrip.bones[i].connectBones);

        if (isDebug) Debug.Log("Done LoadScrip");

        yield break;
    }
}
