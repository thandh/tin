﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tBoneConnectStorage : MonoBehaviour
{
    [Header("Close")]
    public List<tBone> closeBones = new List<tBone>();

    public void DefineClose(List<DistributeBySattelite.tBoneInfo> boneList, int count)
    {
        closeBones = new List<tBone>();
        for (int i = 0; i < boneList.Count; i++)
            if (boneList[i].boneTr != this.transform)
                closeBones.Add(boneList[i].boneTr.GetComponent<tBone>());

        closeBones.Sort((a, b) => (a.transform.position - this.transform.position).magnitude.CompareTo((b.transform.position - this.transform.position).magnitude));
        if (count < closeBones.Count) closeBones.RemoveRange(count, closeBones.Count - count);        
    }

    [Header("Connectb")]
    public List<tBone> connectBones = new List<tBone>();
}
