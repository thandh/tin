﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpreadConfi : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;
    public bool ClickHere = false;

    [Header("SpreadConfi")]
    public DistributeBySattelite distribute = null;
    public bool doneSpreadConfi = true;

    [ContextMenu("DoSpreadConfi")]
    public void DoSpreadConfi()
    {
        StartCoroutine(C_SpreadConfi());
    }

    IEnumerator C_SpreadConfi()
    {
        if (isDebug) Debug.Log("Start C_SpreadConfi");
        doneSpreadConfi = false;

        foreach (DistributeBySattelite.tBoneInfo bone in distribute.boneInfoList)
        {
            ConfiJointControl cjControl = bone.boneTr.GetComponent<ConfiJointControl>();
            cjControl.cjdataList = new List<aCJData>();
        }

        yield return new WaitForEndOfFrame();

        foreach (DistributeBySattelite.tBoneInfo bone in distribute.boneInfoList)
        {
            tBoneConnectStorage storage = bone.boneTr.GetComponent<tBoneConnectStorage>();
            ConfiJointControl cjControl = bone.boneTr.GetComponent<ConfiJointControl>();
            cjControl.use = true;
            cjControl.isDebug = true;
            cjControl.PasteCJ();

            foreach (tBone tr in storage.connectBones)
            {
                if (tr.transform != bone.boneTr)
                {
                    ConfiJointControl cjControlStorage = tr.GetComponent<ConfiJointControl>();
                    if (cjControlStorage.cjdataList.FindIndex((x) => x.rb == bone.boneTr) != -1) continue;
                    cjControl.TargetThis(tr.transform);
                }
            }
        }

        yield return new WaitForEndOfFrame();

        foreach (DistributeBySattelite.tBoneInfo bone in distribute.boneInfoList)
        {
            ConfiJointControl cjControl = bone.boneTr.GetComponent<ConfiJointControl>();
            cjControl.Getlist();
        }

        yield return new WaitForEndOfFrame();

        if (isDebug) Debug.Log("Done C_SpreadConfi");
        doneSpreadConfi = true;

        yield break;
    }

    [ContextMenu("Stop")]
    public void Stop()
    {
        StopAllCoroutines();
    }
}
