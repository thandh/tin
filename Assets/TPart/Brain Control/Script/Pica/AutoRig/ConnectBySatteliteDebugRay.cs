﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ConnectBySatteliteDebugRay : MonoBehaviour
{
    [Header("Ray")]
    public List<DrawARay> rayList = new List<DrawARay>();

    [ContextMenu("GetRay")]
    public void GetRay()
    {
        rayList = GetComponentsInChildren<DrawARay>().ToList();
    }

    [ContextMenu("DeleteAll")]
    public void DeleteAll()
    {
        GetRay();
        for (int i = 0; i < rayList.Count; i++)
            if (Application.isPlaying) Destroy(rayList[i].gameObject); else DestroyImmediate(rayList[i].gameObject);

        rayList = new List<DrawARay>();
    }

    [Header("Connect")]
    public ConnectBySattelite connect = null;
    public GameObject rayPrefab = null;

    [ContextMenu("DebugRay")]
    public void DebugRay()
    {
        DeleteAll();
        List<DistributeBySattelite.tBoneInfo> boneInfoList = connect.distribute.boneInfoList;
        for (int i = 0; i < boneInfoList.Count; i++)
        {
            tBoneConnectStorage storage = boneInfoList[i].boneTr.GetComponent<tBoneConnectStorage>();
            if (storage == null) continue;

            for (int j = 0; j < storage.connectBones.Count; j++)
                CreateARay(boneInfoList[i].boneTr, storage.connectBones[j].transform);
        }
    }

    void CreateARay(Transform fromTr, Transform toTr)
    {
        DrawARay ray = Instantiate(rayPrefab as GameObject, fromTr.position, Quaternion.identity, transform).gameObject.GetComponent<DrawARay>();
        ray.transform.name = fromTr.name + "_" + toTr.name;
        ray.from.position = fromTr.position;
        ray.to.position = toTr.position;
        ray.useUpdate = true;
        rayList.Add(ray);
    }

    [ContextMenu("DebugAllNull")]
    public void DebugAllNull()
    {
        List<DistributeBySattelite.tBoneInfo> boneInfoList = connect.distribute.boneInfoList;
        for (int i = 0; i < boneInfoList.Count; i++)
        {
            tBoneConnectStorage storage = boneInfoList[i].boneTr.GetComponent<tBoneConnectStorage>();
            if (storage == null) continue;
            if (storage.connectBones.Count == 0) Debug.Log(boneInfoList[i].boneTr.name + " null at " + i);
        }
    }
}
