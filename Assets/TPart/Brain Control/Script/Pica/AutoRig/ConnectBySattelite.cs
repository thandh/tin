﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConnectBySattelite : MonoBehaviour
{
    [Header("Debug")]
    /// <summary>
    /// Manuavers in editor
    /// </summary>
    public bool isDebug = false;
    public bool ClickHere = false;

    [Header("Params")]
    public GameObject satteConnectPrefab = null;

    [Header("Inputs")]
    public DistributeBySattelite distribute = null;

    [Header("Close")]
    public int maxClose = 4;    

    [ContextMenu("FullAuto")]
    public void FullAuto()
    {
        StartCoroutine(C_Connect());
    }

    IEnumerator C_Connect()
    {
        InitBone();
        DefineClose();

        for (int i = 0; i < distribute.boneInfoList.Count; i++)
        {
            moveIndex = i;
            MoveConnectAt();
        }

        yield break;
    }

    [Header("Move At")]
    public int moveIndex = 0;
    public int moveToIndex = -1;

    [ContextMenu("MoveConnectAt")]
    public void MoveConnectAt()
    {
        DistributeBySattelite.tBoneInfo bone = distribute.boneInfoList[moveIndex];
        tBoneConnectStorage storage = bone.boneTr.gameObject.GetComponent<tBoneConnectStorage>();

        if (moveToIndex == -1)
            foreach (tBone closeBone in storage.closeBones) CreateConnectSatteAt(bone, closeBone);
        else
            CreateConnectSatteAt(bone, distribute.boneInfoList[moveToIndex].boneTr.GetComponent<tBone>());
    }

    void CreateConnectSatteAt(DistributeBySattelite.tBoneInfo bone, tBone closeBone)
    {
        MoveConnectSattelite satteConnect = (Instantiate(satteConnectPrefab as GameObject).gameObject).GetComponent<MoveConnectSattelite>();
        satteConnect.PrepareNMove(bone.boneTr.gameObject.GetComponent<tBoneConnectStorage>(),
            AfterSatteConnect, bone.boneTr, closeBone.transform, 1f);

        MoveConnectSattelite nSatteConnect = (Instantiate(satteConnectPrefab as GameObject).gameObject).GetComponent<MoveConnectSattelite>();
        nSatteConnect.PrepareNMove(bone.boneTr.gameObject.GetComponent<tBoneConnectStorage>(),
            AfterSatteConnect, bone.boneTr, closeBone.transform, -1f);
    }

    [ContextMenu("MoveNextConnect")]
    public void MoveNextConnect()
    {
        moveIndex = moveIndex <= distribute.boneInfoList.Count - 2 ? moveIndex + 1 : 0;
        MoveConnectAt();
    }

    [ContextMenu("DefineClose")]
    public void DefineClose()
    {
        foreach (DistributeBySattelite.tBoneInfo bone in distribute.boneInfoList)
            bone.boneTr.GetComponent<tBoneConnectStorage>().DefineClose(distribute.boneInfoList, maxClose);
    }

    [ContextMenu("InitBone")]
    public void InitBone()
    {
        foreach (DistributeBySattelite.tBoneInfo bone in distribute.boneInfoList)
        {
            tBoneConnectStorage storage = bone.boneTr.GetComponent<tBoneConnectStorage>();
            if (storage == null) storage = bone.boneTr.gameObject.AddComponent<tBoneConnectStorage>();
        }
    }

    public delegate void delAfterSatteConnect(MoveConnectSattelite moveSatte, tBoneConnectStorage storage);

    void AfterSatteConnect(MoveConnectSattelite moveSatte, tBoneConnectStorage storage)
    {
        if (moveSatte.resultConnect == MoveConnectSattelite.eConnectSattelite.hitToTr)
        {
            int index = storage.connectBones.FindIndex((x) => x.transform == moveSatte.toTr);
            if (index == -1)
            {
                storage.connectBones.Add(moveSatte.toTr.GetComponent<tBone>());
                if (isDebug) Debug.Log(storage.transform.name + " found new bone " + moveSatte.toTr.name);
            }
        }

        if (Application.isPlaying) Destroy(moveSatte.gameObject); else DestroyImmediate(moveSatte.gameObject);
    }
}
