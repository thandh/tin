﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveConnectSattelite : MonoBehaviour
{
    [Header("Debug")]
    /// <summary>
    /// Manuavers in editor
    /// </summary>
    public bool isDebug = false;
    public bool ClickHere = false;

    [Header("Process")]
    public Transform fromTr = null;
    public Transform toTr = null;
    public float multiplyForward = 1f;
    public float stepMove = 1f;
    public float currentExpand = 1f;

    SattelliteShape _sattelite = null;
    SattelliteShape sattelite { get { if (_sattelite == null) _sattelite = GetComponent<SattelliteShape>(); return _sattelite; } }

    public enum eConnectSattelite { No, hitToTr, hitNotToTr, hitBound }
    [Header("Ray")]
    public eConnectSattelite resultConnect = eConnectSattelite.No;
    public bool doneMove = true;

    public void PrepareNMove(tBoneConnectStorage currentStorage, ConnectBySattelite.delAfterSatteConnect afterConnect,
        Transform boneTr, Transform closeBone, float multiplyForward)
    {
        this.currentStorage = currentStorage;
        this.afterConnect += afterConnect;
        this.fromTr = boneTr;
        this.toTr = closeBone;
        this.multiplyForward = multiplyForward;
        Move();
    }

    [Header("Move process")]
    public int maxHitBoundCount = 10;
    public int currentHitBoundCount = 0;

    [ContextMenu("Move")]
    public void Move()
    {
        StartCoroutine(C_Move());
    }

    IEnumerator C_Move()
    {
        if (isDebug) Debug.Log("Start C_Move " + fromTr.name + " connect " + toTr.name);

        doneMove = false;
        resultConnect = eConnectSattelite.No;

        sattelite.CoreAt(fromTr.position);
        yield return new WaitUntil(() => sattelite.doneCore == true);

        if (sattelite.currentMinRound == null || sattelite.allCloseIn == false)
        {
            if (isDebug) Debug.Log("Khong co min round tai diem bat dau! " + fromTr.position);
            resultConnect = eConnectSattelite.No;
            doneMove = true;

            if (afterConnect != null && currentStorage != null) afterConnect.Invoke(this, currentStorage);
            if (isDebug) Debug.Log("Done C_Move");

            yield break;
        }
        else
        {
            resultConnect = eConnectSattelite.hitBound;
        }

        currentExpand = stepMove;
        bool breakWhile = false;
        currentHitBoundCount = 0;

        while (resultConnect == eConnectSattelite.hitBound && currentHitBoundCount <= maxHitBoundCount && breakWhile == false)
        {
            CheckHitDirect();

            if (resultConnect == eConnectSattelite.hitToTr || resultConnect == eConnectSattelite.hitNotToTr)
            {
                if (isDebug) Debug.Log("Done : " + resultConnect.ToString());
                breakWhile = true;
            }
            else
            {
                Vector3 posHit = -Vector3.one; Transform trHit = null;
                if (isDebug) Debug.Log("Check hitChild By Sattelite " + currentExpand);
                CheckHitPicaChild(out posHit, out trHit, sattelite.currentForward.normalized);
                if (resultConnect == eConnectSattelite.hitToTr)
                {
                    if (isDebug) Debug.Log("Done : " + eConnectSattelite.hitToTr.ToString());
                    breakWhile = true;
                }
                else if (resultConnect == eConnectSattelite.hitNotToTr)
                {
                    if (isDebug) Debug.Log("Done : " + eConnectSattelite.hitNotToTr.ToString());
                    breakWhile = true;
                }
                else
                {
                    CheckHitPicaBound(out posHit, out trHit, 1f);
                    if (resultConnect == eConnectSattelite.hitBound)
                    {
                        currentHitBoundCount++;

                        if (currentHitBoundCount >= maxHitBoundCount)
                        {
                            if (isDebug) Debug.LogWarning("Reach maxHitBound while checking " + fromTr.name + " and " + toTr.name + " mulDir : " + multiplyForward);
                            resultConnect = eConnectSattelite.No;
                            breakWhile = true;
                        }
                        else
                        {
                            sattelite.CoreAt(posHit);
                            yield return new WaitUntil(() => sattelite.doneCore == true);

                            if (sattelite.currentMinRound == null || sattelite.allCloseIn == false)
                            {
                                if (isDebug) Debug.Log("Khong co min round tai diem " + posHit);
                                resultConnect = eConnectSattelite.No;
                                breakWhile = true;
                            }
                        }
                    }
                    else
                    {
                        currentExpand += stepMove;
                        resultConnect = eConnectSattelite.hitBound;
                        breakWhile = false;
                    }
                }
            }

            yield return new WaitForEndOfFrame();
        }

        doneMove = true;
        if (afterConnect != null && currentStorage != null) afterConnect.Invoke(this, currentStorage);

        if (isDebug) Debug.Log("Done C_Move : " + resultConnect.ToString());

        yield break;
    }

    public tBoneConnectStorage currentStorage = null;
    public ConnectBySattelite.delAfterSatteConnect afterConnect = null;

    [Header("Check Move")]
    public HitByRay hitForward = null;
    public DrawARayAt_PosDir rayForward = null;

    [ContextMenu("CheckCoreAtFromTr")]
    public void CheckCoreAtFromTr()
    {
        StartCoroutine(C_CheckCoreAtFromTr());
    }

    IEnumerator C_CheckCoreAtFromTr()
    {
        sattelite.currentHitLM = LayerMask.GetMask("picaBound");
        sattelite.CoreAt(fromTr.position);
        yield return new WaitUntil(() => sattelite.doneCore == true);
        Debug.Log("sattelite tryHasResult : " + sattelite.tryHasResult);
        if (sattelite.tryHasResult)
        {
            rayForward.pos = sattelite.transform.position;
            rayForward.dir = sattelite.currentForward.normalized;
            rayForward.length = currentExpand;
        }

        yield break;
    }

    [ContextMenu("CheckHitDirect")]
    public void CheckHitDirect()
    {
        Vector3 posHit = -Vector3.one; Transform trHit = null;

        hitForward.lmHit = LayerMask.GetMask("picaBound");
        hitForward.trFrom.position = this.fromTr.position;
        hitForward.trTo.position = this.toTr.position;
        hitForward.HitInfoUseFinalToFinal(out posHit, out trHit);
        if (posHit != -Vector3.one && trHit != null)
        {
            resultConnect = eConnectSattelite.hitBound;
            if (isDebug) Debug.Log("Direct hit bound! Next step!");
        }
        else
        {            
            currentExpand = (hitForward.trTo.position - hitForward.trFrom.position).magnitude;
            if (isDebug) Debug.Log("Check hitChild Direct " + currentExpand);
            CheckHitPicaChild(out posHit, out trHit, (hitForward.trTo.position - hitForward.trFrom.position).normalized);
        }
    }

    /// <summary>
    /// Use sattelite
    /// </summary>
    [ContextMenu("CheckHitPicaChild")]
    public void CheckHitPicaChild()
    {
        Vector3 posHit = -Vector3.one; Transform trHit = null;
        if (isDebug) Debug.Log("Check hitChild By Sattelite " + currentExpand);
        CheckHitPicaChild(out posHit, out trHit, sattelite.currentForward.normalized);
    }

    void CheckHitPicaChild(out Vector3 posHit, out Transform trHit, Vector3 dir)
    {
        hitForward.lmHit = LayerMask.GetMask("picaChild");
        hitForward.trFrom.position = sattelite.transform.position;
        hitForward.trTo.position = hitForward.trFrom.position + dir * currentExpand * multiplyForward;
        hitForward.HitInfoUseFinalToFinal(out posHit, out trHit);
        if (posHit != -Vector3.one && trHit != null)
        {
            resultConnect = trHit == toTr ? eConnectSattelite.hitToTr : eConnectSattelite.hitNotToTr;
            if (isDebug) Debug.Log("hit " + trHit.name + " at " + posHit);
        }
        else
        {
            if (isDebug) Debug.Log("Current Expand : " + currentExpand + " khong hit picaChild!");
        }
    }

    [Header("currentOffset")]
    public float currentOff = 0.1f;

    [ContextMenu("CheckHitPicaBound")]
    public void CheckHitPicaBound()
    {
        Vector3 posHit = -Vector3.one; Transform trHit = null;
        CheckHitPicaBound(out posHit, out trHit, currentOff);
    }

    void CheckHitPicaBound(out Vector3 posHit, out Transform trHit, float offsetLength = 0f)
    {
        hitForward.lmHit = LayerMask.GetMask("picaBound");
        hitForward.trTo.position = sattelite.transform.position;
        hitForward.trFrom.position = hitForward.trTo.position + sattelite.currentForward.normalized * currentExpand * multiplyForward;
        hitForward.HitInfoUseFinalToFinal(out posHit, out trHit);
        if (posHit != -Vector3.one && trHit != null)
        {
            resultConnect = eConnectSattelite.hitBound;
            posHit -= sattelite.currentForward.normalized * offsetLength * multiplyForward;
            if (isDebug) Debug.Log("hit " + trHit.name + " at " + posHit);
        }
        else
        {
            resultConnect = eConnectSattelite.No;
            if (isDebug) Debug.Log("Check hit bound and get false");
        }
    }

    [ContextMenu("JumpToPicaBound")]
    public void JumpToPicaBound()
    {
        Vector3 posHit = -Vector3.one; Transform trHit = null;
        CheckHitPicaBound(out posHit, out trHit, currentOff);
        if (posHit != -Vector3.one && trHit != null) sattelite.transform.position = posHit;
    }

    [ContextMenu("Stop")]
    public void Stop()
    {
        StopAllCoroutines();
    }
}
