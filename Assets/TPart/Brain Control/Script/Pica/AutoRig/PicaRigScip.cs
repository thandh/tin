﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class PicaRigScip : ScriptableObject
{
    [Serializable]
    public class tBoneInfo
    {
        public Vector3 bonePos = Vector3.zero;
        public float radius = 0f;
        public tBoneInfo() { }
        public tBoneInfo(Vector3 pos, float radius) { this.bonePos = pos; this.radius = radius; }
        public tBoneInfo(DistributeBySattelite.tBoneInfo bone) { this.bonePos = bone.boneTr.position; this.radius = bone.radius; }
        public bool InRange(Vector3 pos, float secondRadius = 0f) { return (pos - bonePos).magnitude <= radius + secondRadius; }
        public List<int> connectBones = new List<int>();
    }
    
    public List<tBoneInfo> bones = new List<tBoneInfo>();
}