﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConnectBySpreadVert : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;
    public bool ClickHere = false;

    [Header("Inputs")]
    public DistributeBySattelite distribute = null;

    [Header("Params")]
    public GameObject connectPrefab = null;
    public int countConnectPrefab = 0;
    public int maxCountConnectPrefab = 20;

    [Header("Close")]
    public int maxClose = 4;

    [Header("Spread process")]
    public int currentAt = -1;
    float timeSpend = 0f;
    public bool doneSpreadAll = true;

    [ContextMenu("SpreadAll")]
    public void SpreadAll()
    {
        StartCoroutine(C_SpreadAll());
    }

    IEnumerator C_SpreadAll()
    {
        if (isDebug) Debug.Log("Start C_SpreadAll");

        doneSpreadAll = false;
        timeSpend = Time.time;
        InitBone();
        DefineClose();

        for (currentAt = 0; currentAt < distribute.boneInfoList.Count; currentAt++)
        {
            yield return new WaitUntil(() => countConnectPrefab < maxCountConnectPrefab == true);
            TryConnectAt(currentAt);
        }

        yield return new WaitUntil(() => countConnectPrefab == 0);
        doneSpreadAll = true;
        if (isDebug) Debug.Log("Done C_SpreadAll in " + (Time.time - timeSpend));

        yield break;
    }

    bool doneTryConnectAt = true;

    void TryConnectAt(int indexAt)
    {
        StartCoroutine(C_TryConnectAt(indexAt));
    }

    IEnumerator C_TryConnectAt(int indexAt)
    {
        doneTryConnectAt = false;

        tBoneConnectStorage storage = distribute.boneInfoList[indexAt].boneTr.GetComponent<tBoneConnectStorage>();
        if (storage == null || storage.closeBones.Count == 0) yield break;

        SpreadVertAConnect spreadConnect = Instantiate(connectPrefab as GameObject).gameObject.GetComponent<SpreadVertAConnect>();
        countConnectPrefab++;

        spreadConnect.fromBone = distribute.boneInfoList[indexAt].boneTr;
        spreadConnect.distribute = this.distribute;
        spreadConnect.toBone = new List<Transform>();
        for (int i = 0; i < storage.closeBones.Count; i++) spreadConnect.toBone.Add(storage.closeBones[i].transform);
        spreadConnect.FullSpread();
        yield return new WaitUntil(() => spreadConnect.doneFullSpread == true);

        if (Application.isPlaying) Destroy(spreadConnect.gameObject); else DestroyImmediate(spreadConnect.gameObject);
        countConnectPrefab--;

        doneTryConnectAt = true;
        yield break;
    }

    [ContextMenu("InitBone")]
    public void InitBone()
    {
        foreach (DistributeBySattelite.tBoneInfo bone in distribute.boneInfoList)
        {
            tBoneConnectStorage storage = bone.boneTr.GetComponent<tBoneConnectStorage>();
            if (storage == null) storage = bone.boneTr.gameObject.AddComponent<tBoneConnectStorage>();
        }
    }

    [ContextMenu("DefineClose")]
    public void DefineClose()
    {
        foreach (DistributeBySattelite.tBoneInfo bone in distribute.boneInfoList)
            bone.boneTr.GetComponent<tBoneConnectStorage>().DefineClose(distribute.boneInfoList, maxClose);
    }
}
