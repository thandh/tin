﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpreadVertAConnect : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;
    public bool clickHere = false;
    public bool boolWaitEndOfFrame = false;

    [Serializable]
    public class SpreadVert
    {
        public int vertID = -1;
        public Vector3 pos = Vector3.zero;
        public int minReach = 9999;
        public int minReachIndex = -1;
        public SpreadVert() { }
        public SpreadVert(int vertID) { this.vertID = vertID; }
    }

    [Serializable]
    public class TrisAtVert
    {
        public int vertID = -1;
        public List<Vector3> trisList = new List<Vector3>();
        public int minReach = 9999;
        public int minReachIndex = -1;
        public TrisAtVert() { this.vertID = -1; this.trisList = new List<Vector3>(); }
    }

    [Header("Vert Connection")]
    List<TrisAtVert> trisAtVertList = new List<TrisAtVert>();
    public int trisAtVertListCount = 0;

    [ContextMenu("DefineTrisAtVertList")]
    public void DefineTrisAtVertList()
    {
        int[] tris = distribute.meshMethods.m.triangles;
        Vector3[] vertices = distribute.meshMethods.m.vertices;

        trisAtVertList = new List<TrisAtVert>();
        for (int i = 0; i < vertices.Length; i++)
        {
            TrisAtVert newTrisAtVert = new TrisAtVert();
            newTrisAtVert.vertID = i;
            newTrisAtVert.trisList = new List<Vector3>();
            trisAtVertList.Add(newTrisAtVert);
        }
        trisAtVertListCount = trisAtVertList.Count;

        for (int i = 0; i < tris.Length / 3; i++)
        {
            Vector3 aTris = new Vector3(tris[i * 3], tris[i * 3 + 1], tris[i * 3 + 2]);
            trisAtVertList[tris[i * 3]].trisList.Add(aTris);
            trisAtVertList[tris[i * 3 + 1]].trisList.Add(aTris);
            trisAtVertList[tris[i * 3 + 2]].trisList.Add(aTris);
        }
    }

    [Header("Input fromBone")]
    public Transform fromBone = null;
    PicaRigScip.tBoneInfo fromBoneInfo = new PicaRigScip.tBoneInfo();

    [Header("Define")]
    public DistributeBySattelite distribute = null;

    [Serializable]
    public class VertBoneHitInfo
    {
        public Transform toBone = null;
        public PicaRigScip.tBoneInfo boneInfo = new PicaRigScip.tBoneInfo();
        public int vertID = -1;
        public VertBoneHitInfo() { }
    }

    [Header("Input toBone")]
    public List<Transform> toBone = new List<Transform>();
    List<VertBoneHitInfo> toBoneHitInfo_ToCheck = new List<VertBoneHitInfo>();
    public int toBoneHitInfo_ToCheckCount = 0;
    List<VertBoneHitInfo> toBoneHitInfo_Result = new List<VertBoneHitInfo>();
    public int toBoneHitInfo_ResultCount = 0;

    [ContextMenu("Refresh")]
    public void Refresh()
    {
        this.spreadList = new List<SpreadVert>();
        spreadListCount = 0;

        this.trisAtVertList = new List<TrisAtVert>();
        trisAtVertListCount = 0;
    }

    [Header("Spread")]
    List<SpreadVert> spreadList = new List<SpreadVert>();
    public int spreadListCount = 0;

    [ContextMenu("DefineBoneInfo")]
    public void DefineBoneInfo()
    {
        if (isDebug) Debug.Log("Start DefineBoneInfo");

        fromBoneInfo.bonePos = fromBone.position;
        fromBoneInfo.radius = fromBone.GetComponent<SphereCollider>().radius;

        toBoneHitInfo_ToCheck = new List<VertBoneHitInfo>();
        toBoneHitInfo_ToCheckCount = 0;

        toBoneHitInfo_Result = new List<VertBoneHitInfo>();
        toBoneHitInfo_ResultCount = 0;

        for (int i = 0; i < toBone.Count; i++)
        {
            VertBoneHitInfo vbHitInfo = new VertBoneHitInfo();
            vbHitInfo.toBone = toBone[i];
            vbHitInfo.vertID = -1;
            vbHitInfo.boneInfo.bonePos = toBone[i].position;
            vbHitInfo.boneInfo.radius = toBone[i].GetComponent<SphereCollider>().radius;

            toBoneHitInfo_ToCheck.Add(vbHitInfo);
        }
        toBoneHitInfo_ToCheckCount = toBoneHitInfo_ToCheck.Count;

        if (isDebug) Debug.Log("Done DefineBoneInfo");
    }

    [Header("FullSpread")]
    public bool doneFullSpread = true;

    [ContextMenu("FullSpread")]
    public void FullSpread()
    {
        StartCoroutine(C_FullSpread());
    }

    public IEnumerator C_FullSpread()
    {
        if (isDebug) Debug.Log("Start C_FullSpread!");

        doneFullSpread = false;

        Refresh();
        DefineTrisAtVertList();
        DefineBoneInfo();
        RestartSpreadList();
        SpreadIt();
        yield return new WaitUntil(() => doneSpreadIt == true);

        TraceAll();
        yield return new WaitUntil(() => doneTraceAll == true);

        doneFullSpread = true;
        if (isDebug) Debug.Log("Done C_FullSpread!");
        yield break;
    }

    [Header("Restart")]
    Vector3[] vertices = new Vector3[0];

    [ContextMenu("RestartSpreadList")]
    public void RestartSpreadList()
    {
        if (isDebug) Debug.Log("Start RestartSpreadList");

        spreadList = new List<SpreadVert>();

        Mesh m = distribute.meshMethods.m;
        this.vertices = m.vertices;

        for (int i = 0; i < this.vertices.Length; i++)
            if (CheckInRange(this.vertices[i], fromBoneInfo))
            {
                SpreadVert spread0 = new SpreadVert();
                spread0.vertID = i;
                spread0.pos = this.vertices[i];
                spread0.minReach = 0;
                spread0.minReachIndex = 0;
                spreadList.Add(spread0);
                spreadListCount = 1;
                currentCheckAt = 0;

                this.trisAtVertList[spread0.vertID].minReach = 0;
                this.trisAtVertList[spread0.vertID].minReachIndex = -1;
                trisAtVertListCount = this.trisAtVertList.Count;

                i = this.vertices.Length;
            }

        if (isDebug) Debug.Log("Done RestartSpreadList");
    }

    [Header("Process")]
    public int currentCheckAt = -1;
    public bool doneSpreadIt = true;

    [ContextMenu("SpreadIt")]
    public void SpreadIt()
    {
        StartCoroutine(C_SpreadIt());
    }

    IEnumerator C_SpreadIt()
    {
        if (isDebug) Debug.Log("Start C_SpreadIt");
        doneSpreadIt = false;

        while (currentCheckAt < spreadList.Count && toBoneHitInfo_ToCheck.Count > 0)
        {
            CheckSpreadAt(currentCheckAt);
            yield return new WaitUntil(() => doneCheckSpreadAt == true);

            currentCheckAt++;
            if (boolWaitEndOfFrame) yield return new WaitForEndOfFrame();
        }

        doneSpreadIt = true;
        if (isDebug) Debug.Log("Done C_SpreadIt");

        yield break;
    }

    [Header("Process")]
    public bool doneCheckSpreadAt = true;

    void CheckSpreadAt(int atIndex)
    {
        StartCoroutine(C_CheckSpreadAt(atIndex));
    }

    IEnumerator C_CheckSpreadAt(int spreadIndex)
    {
        if (isDebug) Debug.Log("Start C_CheckSpreadAt " + spreadIndex);
        doneCheckSpreadAt = false;

        SpreadVert spread = spreadList[spreadIndex];
        for (int i = 0; i < this.trisAtVertList[spread.vertID].trisList.Count; i++)
        {
            Vector3 aTris = this.trisAtVertList[spread.vertID].trisList[i];
            CheckAddASpreadVert(spreadIndex, (int)aTris.x, this.spreadList[spreadIndex].minReach);
            CheckAddASpreadVert(spreadIndex, (int)aTris.y, this.spreadList[spreadIndex].minReach);
            CheckAddASpreadVert(spreadIndex, (int)aTris.z, this.spreadList[spreadIndex].minReach);
        }

        doneCheckSpreadAt = true;
        if (isDebug) Debug.Log("Done C_CheckSpreadAt " + spreadIndex);
        yield break;
    }

    bool doneCheckAddASpreadVert = true;

    void CheckAddASpreadVert(int atIndex, int vertID, int currentMinReach)
    {
        doneCheckAddASpreadVert = false;

        if (spreadList[atIndex].vertID == vertID) { doneCheckAddASpreadVert = true; return; }
        if (currentMinReach + 1 >= this.trisAtVertList[vertID].minReach) { doneCheckAddASpreadVert = true; return; }
        if (CheckInToBoneHitInfo(vertID, this.vertices[vertID])) return;

        SpreadVert newspread = new SpreadVert();
        newspread.vertID = vertID;
        newspread.pos = this.vertices[vertID];
        newspread.minReach = currentMinReach + 1;
        newspread.minReachIndex = spreadList[atIndex].vertID;

        this.trisAtVertList[vertID].minReach = currentMinReach + 1;
        this.trisAtVertList[vertID].minReachIndex = spreadList[atIndex].vertID;

        spreadList.Add(newspread);
        spreadListCount++;

        doneCheckAddASpreadVert = true;
    }

    bool CheckInRange(Vector3 pos, PicaRigScip.tBoneInfo tboneInfo)
    {
        return ((tboneInfo.bonePos - pos).magnitude) <= tboneInfo.radius;
    }

    bool CheckInRange(Vector3 pos, DistributeBySattelite.tBoneInfo tboneInfo)
    {
        return ((tboneInfo.boneTr.position - pos).magnitude) <= tboneInfo.radius;
    }

    bool CheckInToBoneHitInfo(int vertID, Vector3 pos)
    {
        for (int i = 0; i < toBoneHitInfo_ToCheck.Count; i++)
            if (CheckInRange(pos, toBoneHitInfo_ToCheck[i].boneInfo))
            {
                VertBoneHitInfo vbHitInfo = toBoneHitInfo_ToCheck[i];
                vbHitInfo.vertID = vertID;

                toBoneHitInfo_Result.Add(toBoneHitInfo_ToCheck[i]);
                toBoneHitInfo_ResultCount = toBoneHitInfo_Result.Count;

                toBoneHitInfo_ToCheck.RemoveAt(i);
                toBoneHitInfo_ToCheckCount = toBoneHitInfo_ToCheck.Count;

                return true;
            }

        return false;
    }

    [Header("Trace")]
    int boneTraceIndex = -1;
    List<TrisAtVert> traceList = new List<TrisAtVert>();
    bool doneTrace = true;

    [ContextMenu("TraceBone")]
    public void TraceBone()
    {
        StartCoroutine(C_TraceBone());
    }

    IEnumerator C_TraceBone()
    {
        if (isDebug) Debug.Log("Start C_TraceBone");

        if (boneTraceIndex == -1) yield break;

        TrisAtVert traceSpread = trisAtVertList[toBoneHitInfo_Result[boneTraceIndex].vertID];

        traceList = new List<TrisAtVert>();
        traceList.Add(traceSpread);

        while (traceSpread.minReach > 0)
        {
            traceSpread = trisAtVertList[traceSpread.minReachIndex];
            traceList.Add(traceSpread);
            if (isDebug) Debug.Log("Trace " + traceSpread.vertID + " with " + traceSpread.minReach);
            if (boolWaitEndOfFrame) yield return new WaitForEndOfFrame();
        }

        if (isDebug) Debug.Log("Done C_TraceBone");

        yield break;
    }

    [Header("Trace")]
    public bool doneTraceAll = true;

    [ContextMenu("TraceAll")]
    public void TraceAll()
    {
        StartCoroutine(C_TraceAll());
    }

    IEnumerator C_TraceAll()
    {
        if (isDebug) Debug.Log("Done C_TraceAll");
        doneTraceAll = false;

        tBoneConnectStorage storage = fromBone.GetComponent<tBoneConnectStorage>();
        storage.connectBones = new List<tBone>();

        for (int i = 0; i < toBoneHitInfo_Result.Count; i++)
        {
            boneTraceIndex = i;
            TraceBone();
            yield return new WaitUntil(() => doneTrace == true);

            if (CheckCurrentTrace(toBoneHitInfo_Result[i].toBone))
                storage.connectBones.Add(toBoneHitInfo_Result[i].toBone.GetComponent<tBone>());
        }

        doneTraceAll = true;
        if (isDebug) Debug.Log("Done C_TraceAll");

        yield break;
    }

    bool CheckCurrentTrace(Transform toBone)
    {
        for (int i = 0; i < traceList.Count; i++)
            if (CheckATraceAt(this.vertices[this.traceList[i].vertID], toBone) == false)
                return false;
        return true;
    }

    bool CheckATraceAt(Vector3 pos, Transform toBone)
    {
        DistributeBySattelite.tBoneInfo tboneInfo = distribute.boneInfoList.Find((x) => CheckInRange(pos, x) == true);
        if (tboneInfo == null || tboneInfo.boneTr == fromBone || tboneInfo.boneTr == toBone) return true;
        else return false;
    }

    [ContextMenu("Stop")]
    public void Stop()
    {
        StopAllCoroutines();
    }
}
