﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class DistributeBySattelite : MonoBehaviour
{
    [Header("Debug")]
    /// <summary>
    /// Manuavers in editor
    /// </summary>
    public bool isDebug = false;
    public bool ClickHere = false;

    [Header("Params")]
    /// <summary>
    /// Sattelite
    /// </summary>
    public GameObject sattelitePrefab = null;
    SattelliteShape _sattelite = null;
    public SattelliteShape sattelite { get { if (_sattelite == null) _sattelite = (Instantiate(sattelitePrefab as GameObject).gameObject).GetComponent<SattelliteShape>(); return _sattelite; } }

    /// <summary>
    /// Mesh method to hold mesh
    /// </summary>
    public MeshMethods meshMethods = null;

    [Serializable]
    public class tBoneInfo
    {
        public Transform boneTr = null;
        public float radius = 0f;
        public tBoneInfo() { }
        public tBoneInfo(PicaRigScip.tBoneInfo bone) { this.boneTr.position = bone.bonePos; this.radius = bone.radius; }
        public bool InRange(Vector3 pos, float secondRadius = 0f) { return (pos - boneTr.position).magnitude <= radius + secondRadius; }
    }

    [Header("Output")]
    public List<tBoneInfo> boneInfoList = new List<tBoneInfo>();

    [Header("Output")]
    public float rangeThreshhold = 1f;
    public float minRadius = 1f;
    public float maxRadius = 4f;

    bool InRange(Vector3 pos, float secondRadius = 0f)
    {
        foreach (tBoneInfo boneInfo in this.boneInfoList)
            if (boneInfo.InRange(pos, secondRadius)) return true;
        return false;
    }

    [Header("Delete")]
    public bool doneDeleteAll = true;

    [ContextMenu("DeleteAll")]
    public void DeleteAll()
    {
        StartCoroutine(C_DeleteAll());
    }

    IEnumerator C_DeleteAll()
    {
        if (isDebug) Debug.Log("Start DeleteAll");
        doneDeleteAll = false;

        for (int i = 0; i < boneInfoList.Count; i++)
            if (boneInfoList[i] != null && boneInfoList[i].boneTr != null)
                if (Application.isPlaying) Destroy(boneInfoList[i].boneTr.gameObject);
                else DestroyImmediate(boneInfoList[i].boneTr.gameObject);

        boneInfoList = new List<tBoneInfo>();

        tBone[] tbones = GetComponentsInChildren<tBone>();
        for (int i = 0; i < tbones.Length; i++)
            if (Application.isPlaying) Destroy(tbones[i].gameObject);
            else DestroyImmediate(tbones[i].gameObject);

        doneDeleteAll = true;
        if (isDebug) Debug.Log("Done DeleteAll");

        yield break;
    }

    [Header("Distribute")]
    public bool doneDistribute = true;

    [ContextMenu("Distribute")]
    public void Distribute()
    {
        StartCoroutine(C_Distribute());
    }

    IEnumerator C_Distribute()
    {
        if (isDebug) Debug.Log("Start C_Distribute");

        doneDistribute = false;
        DeleteAll();

        Transform meshMethodsTr = meshMethods.transform;
        Mesh m = meshMethods.m;
        Vector3[] vertices = m.vertices;

        sattelite.gameObject.SetActive(true);

        for (int i = 0; i < vertices.Length; i++)
        {
            if (isDebug) Debug.Log("Check at vert : " + i);

            if (InRange(meshMethods.transform.TransformPoint(vertices[i]), minRadius + rangeThreshhold)) continue;

            Vector3 pos = meshMethodsTr.TransformPoint(vertices[i]);
            sattelite.CoreAt(pos);
            yield return new WaitUntil(() => sattelite.doneCore == true);

            if (sattelite.tryHasResult == false || sattelite.disExpand >= sattelite.maxExpand) continue;
            if (InRange(sattelite.transform.position, sattelite.currentMinRound.currentMaxDis + rangeThreshhold)) continue;
            
            CreateATBone(sattelite.transform.position, sattelite.currentMinRound.currentMaxDis);
        }

        if (Application.isPlaying) Destroy(sattelite.gameObject); else DestroyImmediate(sattelite.gameObject);

        doneDistribute = true;
        if (isDebug) Debug.Log("Done C_Distribute");

        yield break;
    }

    [Header("CreateTbone")]
    public bool doneCreateTbone = true;

    public void CreateATBone(Vector3 pos, float radius)
    {
        StartCoroutine(C_CreateATBone(pos, radius));
    }

    IEnumerator C_CreateATBone(Vector3 pos, float radius)
    {
        if (isDebug) Debug.Log("Start C_CreateATBone");
        doneCreateTbone = false;

        tBoneInfo bone = new tBoneInfo();
        bone.boneTr = new GameObject().transform;
        bone.boneTr.name = "tBone" + this.boneInfoList.Count;
        bone.boneTr.gameObject.layer = LayerMask.NameToLayer("picaChild");
        bone.boneTr.position = pos;
        bone.boneTr.parent = transform;

        bone.radius = Mathf.Clamp(radius, minRadius, maxRadius);
        tBone tbone = bone.boneTr.gameObject.AddComponent<tBone>();
        SphereCollider sc = bone.boneTr.gameObject.AddComponent<SphereCollider>();
        sc.isTrigger = true;
        Rigidbody rb = bone.boneTr.gameObject.AddComponent<Rigidbody>();
        rb.useGravity = false;
        rb.isKinematic = true;
        sc.radius = bone.radius;
        this.boneInfoList.Add(bone);

        doneCreateTbone = true;
        if (isDebug) Debug.Log("Done C_CreateATBone");

        yield break;
    }

    public void DistributeConnectAt(int atIndex, List<int> connectbones = null)
    {
        tBoneInfo bone = boneInfoList[atIndex];
        tBoneConnectStorage storage = bone.boneTr.gameObject.GetComponent<tBoneConnectStorage>();
        if (storage == null) storage = bone.boneTr.gameObject.AddComponent<tBoneConnectStorage>();
        for (int i = 0; i < connectbones.Count; i++)
            storage.connectBones.Add(boneInfoList[connectbones[i]].boneTr.GetComponent<tBone>());
    }

    [Header("Update")]
    public bool useUpdate = false;
    public bool boolUDistribute = false;

    [ContextMenu("UDistribute")]
    public void UDistribute()
    {
        boolUDistribute = true;
    }

    private void Update()
    {
        if (!useUpdate) return;


    }

    [ContextMenu("Stop")]
    public void Stop()
    {
        StopAllCoroutines();
        boolUDistribute = false;
    }
}
