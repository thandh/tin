﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tPlane_inPoint_inNormal : tPlane, itPlane
{
    [Header("params")]
    public Vector3 _inPoint = Vector3.zero;
    public Vector3 _inNormal = Vector3.zero;

    private Plane thisPlane = new Plane();

    public Vector3 inNormal()
    {
        return _inNormal;
    }

    public Vector3 inPoint()
    {
        return _inPoint;
    }

    public Plane GetPlane()
    {
        thisPlane = new Plane(inNormal(), inPoint());
        return thisPlane;
    }

    public float DistanceTo(Vector3 point)
    {
        return thisPlane.GetDistanceToPoint(point);
    }
}
