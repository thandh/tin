﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class DrawTPlane_Editor : MonoBehaviour
{
    public bool isDraw = false;

    [Header("Color")]
    public Color edgeColor = Color.green;
    public Color normalColor = Color.red;

    [Header("Params")]
    public MonoBehaviour tplane = null;
    itPlane _itPlane = null;
    itPlane itPlane { get { if (_itPlane == null) _itPlane = tplane.GetComponent<itPlane>(); return _itPlane; } }

    private void Update()
    {
        if (!isDraw) return;
        if (tplane == null) return;

        Vector3 v3 = Vector3.zero;

        if (itPlane.inNormal().normalized != Vector3.forward)
            v3 = Vector3.Cross(itPlane.inNormal(), Vector3.forward).normalized * itPlane.inNormal().magnitude;
        else
            v3 = Vector3.Cross(itPlane.inNormal(), Vector3.up).normalized * itPlane.inNormal().magnitude; ;

        var corner0 = itPlane.inPoint() + v3;
        var corner2 = itPlane.inPoint() - v3;
        var q = Quaternion.AngleAxis(90.0f, itPlane.inNormal());
        v3 = q * v3;
        var corner1 = itPlane.inPoint() + v3;
        var corner3 = itPlane.inPoint() - v3;

        Debug.DrawLine(corner0, corner2, edgeColor);
        Debug.DrawLine(corner1, corner3, edgeColor);
        Debug.DrawLine(corner0, corner1, edgeColor);
        Debug.DrawLine(corner1, corner2, edgeColor);
        Debug.DrawLine(corner2, corner3, edgeColor);
        Debug.DrawLine(corner3, corner0, edgeColor);
        Debug.DrawRay(itPlane.inPoint(), itPlane.inNormal(), normalColor);
    }
}
