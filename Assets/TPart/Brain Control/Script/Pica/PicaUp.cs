﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public delegate void BeforePicaGo();
public delegate void AfterPicaGo();

public class PicaUp : MonoBehaviour
{
    [Header("Process")]
    public bool isDebug = false;    

    public TextAsset pudHardCode = null;
    PicaUpData pud = new PicaUpData();
    
    [Header("Params In")]    
    public Pica pica = null;
    
    public BeforePicaGo beforePicaGo = null;
    public AfterPicaGo afterPicaGo = null;

    [Header("ResetAtStart")]
    public bool boolResetAtStart = false;

    private void Start()
    {
        if (pudHardCode != null) pud = JsonUtility.FromJson<PicaUpData>(pudHardCode.text);
        if (boolResetAtStart) ResetAtStart();
    }

    public void ResetAtStart()
    {
        StartCoroutine(C_ResetAtStart());
    }

    IEnumerator C_ResetAtStart()
    {
        GoDown();
        yield return new WaitUntil(() => IsGoDown == false);

        yield break;
    }

    [Header("HJChild")]
    private List<PicaChild> _childList = null;
    public List<PicaChild> childList { get { if (_childList == null || _childList.Count == 0) _childList = pica.picachildList; return _childList; } }

    [ContextMenu("Save PrefPicaUpData")]
    public void Save_PrefPicaUpData()
    {
        pud = new PicaUpData();
        for (int i = 0; i < childList.Count; i++)
        {
            pud.posList.Add(childList[i].Position);
            pud.rotList.Add(childList[i].Rotation);
        }
        PlayerPrefs.SetString("PicaUpData", pud.ToJson());
        PlayerPrefs.Save();
    }

    [ContextMenu("Save Text")]
    public void Save_Text()
    {
        pud = new PicaUpData();
        for (int i = 0; i < childList.Count; i++)
        {
            pud.posList.Add(childList[i].Position);
            pud.rotList.Add(childList[i].Rotation);
        }
        string path = Application.persistentDataPath + "/" + "test.txt";
        Debug.Log(path);
        File.WriteAllText(path, pud.ToJson());
    }

    [ContextMenu("Load PrefPicaUpData")]
    public void Load_PrefPicaUpData()
    {
        pud = JsonUtility.FromJson<PicaUpData>(PlayerPrefs.GetString("PicaUpData"));
        for (int i = 0; i < childList.Count; i++)
        {
            childList[i].Position = pud.posList[i];
            childList[i].Rotation = pud.rotList[i];
        }
    }

    [ContextMenu("Load PicaUp TextAsset")]
    public void Load_TextAssetPicaUpData()
    {
        pud = JsonUtility.FromJson<PicaUpData>(pudHardCode.text);
        for (int i = 0; i < childList.Count; i++)
        {
            childList[i].Position = pud.posList[i];
            childList[i].Rotation = pud.rotList[i];
        }
    }

    [Header("GoUp")]
    public bool IsGoUp = false;
    public int UpPosNotDone = -1;
    public int UpRotNotDone = -1;

    [ContextMenu("Go Up")]
    public void GoUp()
    {
        if (isDebug) Debug.Log("start Go Up");

        StopAllCoroutines();
        pica.StopAllCoroutine();        

        if (beforePicaGo != null) beforePicaGo.Invoke();

        if (IsGoUp || IsGoDown)
        {
            Debug.Log("Chua chay xong cai cu");
            Load_TextAssetPicaUpData();
            ResetRBnHJFJSJ();

            IsGoDown = false;
            IsGoUp = false;

            if (afterPicaGo != null) afterPicaGo.Invoke();

            picaUp_StopAllCoroutines();

            if (isDebug) Debug.Log("done Go Up");

            return;
        }

        StartCoroutine(C_GoUp());
    }

    IEnumerator C_GoUp()
    {
        IsGoUp = true;
        UpPosNotDone = -1;
        UpRotNotDone = -1;

        for (int i = 0; i < childList.Count; i++)
        {
            childList[i].SyncTo(pud.posList[i], pud.rotList[i]);
        }

        yield return new WaitUntil(() => CheckIsPosAllUp() == true && CheckIsRotAllUp() == true);

        ResetRBnHJFJSJ();

        if (afterPicaGo != null) afterPicaGo.Invoke();

        IsGoUp = false;

        if (isDebug) Debug.Log("done Go Up");

        yield break;
    }

    bool CheckIsPosAllUp()
    {
        for (int i = 0; i < childList.Count; i++)
        {
            if ((childList[i].Position - pud.posList[i]).magnitude >= deltaCheckPos)
            {
                UpPosNotDone = i;
                return false;
            }
        }

        return true;
    }

    bool CheckIsRotAllUp()
    {
        for (int i = 0; i < childList.Count; i++)
        {
            if ((childList[i].Rotation.eulerAngles - pud.rotList[i].eulerAngles).magnitude >= deltaCheckRot)
            {
                UpRotNotDone = i;
                return false;
            }
        }

        return true;
    }

    [Header("GoDown")]
    public bool IsGoDown = false;
    public int DownNotDone = -1;
    public int DownPosNotDone = -1;
    public int DownRotNotDone = -1;

    public float stepPos = 0.01f;    
    public float stepPosFinal = 1f;
    public float timeStep_stepPos = 0.05f;
    public float stepIncrease_stepPos = 0.1f;

    public void Increase_stepPos()
    {
        StartCoroutine(C_Increase_stepPos());
    }

    IEnumerator C_Increase_stepPos()
    {
        stepPos = 0f;
        while (stepPos != stepPosFinal)
        {
            stepPos = Mathf.Lerp(stepPos, stepPosFinal, Time.deltaTime * stepIncrease_stepPos);
            yield return new WaitForSeconds(timeStep_stepPos);
        }
        yield break;
    }

    public float stepRot = 30f;

    public float deltaCheckPos = 0.01f;
    public float deltaCheckRot = 1f;

    [ContextMenu("DeleteRBnJoints")]
    public void DeleteRBnJoints()
    {
        StartCoroutine(C_DeleteRBnJoints());
    }

    IEnumerator C_DeleteRBnJoints()
    {
        if (isDebug) Debug.Log("Delete RB n Joints");

        DeleteJoints();
        yield return new WaitUntil(() => IsAllJointsDeleted() == true);

        for (int i = 0; i < childList.Count; i++)
        {
            childList[i].GetComponent<RigidbodyTemplate>().DestroyRB();
            childList[i].GetComponent<SphereColliderTemplate>().DestroySC();
        }

        yield break;
    }

    bool IsAllJointsDeleted()
    {
        bool isDeleted = true;
        for (int i = 0; i < childList.Count; i++)
        {
            if (!childList[i].cjControl.IsRemoveAll()) return false;
            if (!childList[i].fjControl.IsRemoveAll()) return false;
            if (!childList[i].sjControl.IsRemoveAll()) return false;
            if (!childList[i].hjControl.IsRemoveAll()) return false;
        }
        return isDeleted;
    }

    public void DeleteJoints()
    {    
        if (isDebug) Debug.Log("Delete Joints");

        for (int i = 0; i < childList.Count; i++)
        {
            childList[i].cjControl.RemoveCJInList();
            childList[i].cjControl.ForceRemoveAllCJ();

            childList[i].hjControl.RemoveOnlyAllHJ();
            childList[i].sjControl.RemoveOnlyAllSJ();

            childList[i].fjControl.RemoveFJInlist();
            childList[i].fjControl.ForceRemoveAllFJ();
        }     
    }

    [ContextMenu("ResetRBnHJFJSJ")]
    public void ResetRBnHJFJSJ()
    {
        for (int i = 0; i < childList.Count; i++)
        {
            childList[i].GetComponent<RigidbodyTemplate>().AddRB();
            childList[i].GetComponent<SphereColliderTemplate>().AddSC();

            childList[i].GetComponent<HingeJointControl>().PasteHJ();
            childList[i].GetComponent<SpringJointControl>().PasteSJ();
            childList[i].GetComponent<ConfiJointControl>().PasteCJ();
            childList[i].GetComponent<FixJointControl>().PasteFJ();

            childList[i].sc.enabled = true;
        }
    }

    void ResetAllboolRunning()
    {
        IsGoDown = false;
        IsGoUp = false;
        IsGoREZ = false;
        IsGoSkull = false;
        IsGoSkull_Onehit = false;
        StopImmediate();
    }

    bool isOneBoolRunning()
    {
        return (IsGoDown || IsGoUp || IsGoSkull || IsGoREZ || IsGoSkull_Onehit);
    }

    [ContextMenu("Go Down")]
    public void GoDown()
    {
        if (isDebug) Debug.Log("start Go Down");

        StopAllCoroutines();
        pica.StopAllCoroutine();        

        if (beforePicaGo != null) beforePicaGo.Invoke();

        DeleteRBnJoints();

        if (isOneBoolRunning())
        {
            Debug.Log("Chua chay xong cai cu");
            pica.ResetPosition();

            ResetRBnHJFJSJ();

            ResetAllboolRunning();

            if (afterPicaGo != null) afterPicaGo.Invoke();

            picaUp_StopAllCoroutines();

            if (isDebug) Debug.Log("stop Go Down");

            return;
        }

        StartCoroutine(C_GoDown());
    }

    IEnumerator C_GoDown()
    {
        if (isDebug) Debug.Log("Start Go Down");

        IsGoDown = true;
        DownNotDone = -1;
        DownPosNotDone = -1;
        DownRotNotDone = -1;

        childList[0].hitSkullAnchor.ResetMuzzle();

        REZ.GetComponent<SphereCollider>().enabled = false;

        int indexFar = -1;
        float distanceFar = -1f;

        for (int i = 0; i < childList.Count; i++)
        {
            if (distanceFar < (childList[i].Position - childList[i]._pos0).magnitude)
            {
                indexFar = i;
                distanceFar = (childList[i].Position - childList[i]._pos0).magnitude;
            }

            childList[i].SyncTo(childList[i]._pos0, childList[i]._rot0);            
        }
                
        yield return new WaitUntil(() => CheckIsAllDown() == true);

        ResetRBnHJFJSJ();

        StopImmediate();

        if (afterPicaGo != null) afterPicaGo.Invoke();

        REZ.GetComponent<SphereCollider>().enabled = false;

        IsGoDown = false;

        if (isDebug) Debug.Log("Done Go Down");

        yield break;
    }

    bool CheckIsAllDown()
    {
        int index = childList.FindIndex((x) => (x.Position - x._pos0).magnitude >= deltaCheckPos || (x.Rotation.eulerAngles - x._rot0.eulerAngles).magnitude >= deltaCheckRot);
        if (index != -1) { DownNotDone = index; return false; }
        return true;
    }

    bool CheckIsPosAllDown()
    {
        int index = childList.FindIndex((x) => (x.Position - x._pos0).magnitude >= deltaCheckPos);
        if (index != -1) { DownPosNotDone = index; return false; }
        return true;
    }

    bool CheckIsRotAllDown()
    {
        int index = childList.FindIndex((x) => (x.Rotation.eulerAngles - x._rot0.eulerAngles).magnitude >= deltaCheckRot);
        if (index != -1) { DownRotNotDone = index; return false; }
        return true;
    }

    bool CheckIsIndexDown(int index)
    {   
        return (childList[index].Position - childList[index]._pos0).magnitude <= deltaCheckPos 
            && (childList[index].Rotation.eulerAngles - childList[index]._rot0.eulerAngles).magnitude <= deltaCheckRot;
    }    

    [ContextMenu("TestRotation0")]
    public void TestRotation0()
    {
        for (int i = 0; i < childList.Count; i++)
        {
            childList[i].Rotation = Quaternion.identity;
        }
    }

    [Header("Automove out of REZ by X")]
    public GameObject _REZ = null;
    GameObject REZ { get { if (_REZ == null) _REZ = GameObject.Find("REZ"); return _REZ; } }

    public bool IsGoREZ = false;
    public int REZPosNotDone = -1;
    public float stepPosREZ = 1f;
    public float deltaCheckPosREZ = 0.01f;

    public float forceMultiply = 9f;

    [ContextMenu("GoOutREZ")]
    public void GoOutREZ()
    {
        if (isDebug) Debug.Log("start Go REZ");

        StopAllCoroutines();
        pica.StopAllCoroutine();        

        if (beforePicaGo != null) beforePicaGo.Invoke();

        if (isOneBoolRunning())
        {
            Debug.Log("Chua chay xong cai cu");

            ResetRBnHJFJSJ();

            ResetAllboolRunning();

            if (afterPicaGo != null) afterPicaGo.Invoke();

            picaUp_StopAllCoroutines();

            if (isDebug) Debug.Log("stop Go REZ");

            return;
        }

        StartCoroutine(C_GoOutREZ());
    }

    IEnumerator C_GoOutREZ()
    {
        IsGoREZ = true;
        REZPosNotDone = -1;

        REZ.GetComponent<SphereCollider>().enabled = false;

        while (!CheckIsPosAllREZ())
        {
            for (int i = 0; i < childList.Count; i++)
            {
                if ((childList[i].Position - REZ.transform.position).magnitude <= REZ.GetComponent<SphereCollider>().radius * REZ.transform.localScale.x + childList[i].GetComponent<SphereCollider>().radius * childList[i].transform.localScale.x)
                {
                    Debug.Log("Add force " + childList[i].name + " dir " + X.Instance.picaUpDir.forward + " force " + X.Instance.picaUpDir.forward * forceMultiply);
                                        
                    childList[i].GetComponent<Rigidbody>().AddForce((X.Instance.picaUpDir.forward.normalized).normalized * forceMultiply);
                    break;
                }
            }

            yield return new WaitForEndOfFrame();
        }

        ResetRBnHJFJSJ();

        if (afterPicaGo != null) afterPicaGo.Invoke();

        REZ.GetComponent<SphereCollider>().enabled = true;

        IsGoREZ = false;

        if (isDebug) Debug.Log("done Go REZ");

        yield break;
    }

    bool CheckIsPosAllREZ()
    {
        int index = childList.FindIndex((x) => (x.Position - REZ.transform.position).magnitude <= REZ.GetComponent<SphereCollider>().radius * REZ.transform.localScale.x + x.GetComponent<SphereCollider>().radius * x.transform.localScale.x);
        if (index != -1) { REZPosNotDone = index; return false; }
        return true;
    }

    [Header("forceRun params")]
    public float forceRun = 0.01f;
    public float forceRunFinal = 20f;
    public float timeStep_forceRun = 0.05f;
    public float stepIncrease_forceRun = 0.1f;

    [Header("Automove go to skull by X UNTIL ANY OTHER BONE HIT")]    

    public bool IsGoSkull_Onehit = false;
    public int GoSkull_Onehit_PosNotDone = -1;
    public bool goSkull_Onehit = false;

    public void Increase_forceRunDirection()
    {
        StartCoroutine(C_Increase_forceRunDirection());
    }

    IEnumerator C_Increase_forceRunDirection()
    {
        forceRun = 0f;
        while (forceRun != forceRunFinal)
        {
            forceRun = Mathf.Lerp(forceRun, forceRunFinal, Time.deltaTime * stepIncrease_forceRun);
            yield return new WaitForSeconds(timeStep_forceRun);
        }
        yield break;
    }

    [ContextMenu("GoSkullOneHit")]
    public void GoSkull_StopWhenOneHit()
    {
        if (isDebug) Debug.Log("start Go One Hit");

        StopAllCoroutines();
        pica.StopAllCoroutine();                

        if (beforePicaGo != null) beforePicaGo.Invoke();

        if (isOneBoolRunning())
        {
            Debug.Log("Chua chay xong cai cu");

            ResetRBnHJFJSJ();

            ResetAllboolRunning();

            if (afterPicaGo != null) afterPicaGo.Invoke();

            picaUp_StopAllCoroutines();

            if (isDebug) Debug.Log("done Go One Hit");

            return;
        }

        StartCoroutine(C_GoSkullOneHit());
    }

    IEnumerator C_GoSkullOneHit()
    {
        if (isDebug) Debug.Log("Start C_GoSkullOneHit()");

        IsGoSkull_Onehit = true;
        GoSkull_Onehit_PosNotDone = -1;
        goSkull_Onehit = false;

        pica.OnOffThru(true);

        float minDistance = 999999f;

        for (int i = 0; i < childList.Count; i++)
        {
            if ((childList[i].Position - REZ.transform.position).magnitude <= REZ.GetComponent<SphereCollider>().radius * REZ.transform.localScale.x + childList[i].GetComponent<SphereCollider>().radius * childList[i].transform.localScale.x && (childList[i].Position - REZ.transform.position).magnitude < minDistance)
            {
                minDistance = (childList[i].Position - REZ.transform.position).magnitude;
                GoSkull_Onehit_PosNotDone = i;                
            }
        }

        if (GoSkull_Onehit_PosNotDone != -1)
        {

            if (isDebug) Debug.Log("Running follow " + X.Instance.picaUpDir.forward);

            if (isDebug) Debug.Log(childList[GoSkull_Onehit_PosNotDone].gameObject.name + "needs to run!!");

            ///In-if : compare to AteryRun, else-if : always set false
            if (ManngerSystem.Instance.boolUsePauseAfterGoSkull)
                boolPauseAfterGoSkull = true;
            else            
                boolPauseAfterGoSkull = false;

            childList[GoSkull_Onehit_PosNotDone].RunToSkull();

            if (isDebug) Debug.Log("start yield CheckStop_GoSkull_OneHit() == true");
            yield return new WaitUntil(() => CheckStop_GoSkull_OneHit() == true);
            if (isDebug) Debug.Log("done yield CheckStop_GoSkull_OneHit() == true");

            childList[GoSkull_Onehit_PosNotDone].StopAllCoroutines();
        }

        if (afterPicaGo != null) afterPicaGo.Invoke();

        ResetRBnHJFJSJ();

        IsGoSkull_Onehit = false;

        pica.OnOffThru(false);

        if (isDebug) Debug.Log("done Go One Hit");

        yield break;
    }

    bool CheckStop_GoSkull_OneHit()
    {
        bool value = false;

        foreach (SkullCollider sc in skull.Instance.skullCollis)
        {
            int iL = sc.detect.collis.FindIndex((x) => x.layerName == "picaChild");
            if (iL != -1)
            {
                if (sc.detect.collis.Count > 0)
                {
                    if (isDebug) Debug.Log(sc.transform.name + " contains picaChild > 0");
                    if (isDebug)
                    {
                        for (int i = 0; i < sc.detect.collis.Count; i++)
                        {
                            for (int j = 0; j < sc.detect.collis[i].trList.Count;j++)
                                Debug.Log("Hit : " + sc.detect.collis[i].trList[j].name);
                        }
                    }

                    value = true;
                    break;
                }
            }
        }

        return value;
    }

    [Header("Automove go to skull by X UNTIL SELF HIT")]

    public bool IsGoSkull = false;
    public int GoSkullPosNotDone = -1;
    public bool hitSkull = false;

    [ContextMenu("GoSkull")]
    public void GoSkull_StopWhenSelfHit()
    {
        if (isDebug) Debug.Log("start Go Skull");

        StopAllCoroutines();
        pica.StopAllCoroutine();        

        if (beforePicaGo != null) beforePicaGo.Invoke();

        if (isOneBoolRunning())
        {
            Debug.Log("Chua chay xong cai cu");

            ResetRBnHJFJSJ();

            ResetAllboolRunning();

            if (afterPicaGo != null) afterPicaGo.Invoke();

            picaUp_StopAllCoroutines();

            if (isDebug) Debug.Log("done Go Skull");

            return;
        }

        StartCoroutine(C_GoSkull());
    }

    /// <summary>
    /// Use to stop recording Video by PicaUp
    /// </summary>
    public bool boolPauseAfterGoSkull = false;

    IEnumerator C_GoSkull()
    {
        IsGoSkull = true;        

        GoSkullPosNotDone = -1;
        hitSkull = false;

        float minDistance = 999999f;

        for (int i = 0; i < childList.Count; i++)
        {
            if ( childList[i].typePica == PicaChild.etypePica.pica && (childList[i].Position - REZ.transform.position).magnitude <= REZ.GetComponent<SphereCollider>().radius * REZ.transform.localScale.x + childList[i].GetComponent<SphereCollider>().radius * childList[i].transform.localScale.x && (childList[i].Position - REZ.transform.position).magnitude < minDistance)
            {
                minDistance = (childList[i].Position - REZ.transform.position).magnitude;
                GoSkullPosNotDone = i;
            }
        }

        if (GoSkullPosNotDone == -1)
        {
            if (isDebug) Debug.Log("GoSkullPosNotDone == -1");
            yield break;
        }

        ///In-if : compare to AteryRun, else-if : always set false
        if (ManngerSystem.Instance.boolUsePauseAfterGoSkull)
            boolPauseAfterGoSkull = true;
        else
            boolPauseAfterGoSkull = false;        

        childList[GoSkullPosNotDone].hitSkullAnchor.ResetMuzzle();
        childList[GoSkullPosNotDone].RunToSkull();

        yield return new WaitUntil(() => CheckStopGoSkull() == true);
                
        childList[GoSkullPosNotDone].hitSkullAnchor.SetAnchor(childList[GoSkullPosNotDone]);

        childList[GoSkullPosNotDone].StopAllCoroutines();        

        if (afterPicaGo != null) afterPicaGo.Invoke();

        StopAndWait();
        yield return new WaitUntil(() => isStopAndWait == false);

        IsGoSkull = false;

        if (isDebug) Debug.Log("done Go Skull");

        if (boolPauseAfterGoSkull)
        {
            Debug.Log("Scale0");
            TimeScale.Scale0();
        }

        yield break;
    }

    [ContextMenu("DebugInRez")]
    public void DebugInRez()
    {
        for (int i = 0; i < childList.Count; i++)
        {
            float distance = (childList[i].Position - REZ.transform.position).magnitude;
            if (distance <= REZ.GetComponent<SphereCollider>().radius * REZ.transform.localScale.x + childList[i].GetComponent<SphereCollider>().radius * childList[i].transform.localScale.x)
            {
                Debug.Log(childList[i].transform.name + ", " + childList[i].typePica + " distance to REZ : " + distance);
            }
        }
    }

    public bool CheckStopGoSkull()
    {
        bool value = false;

        if (GoSkullPosNotDone == -1)
        {
            if (isDebug) Debug.Log("GoSkullPosNotDone == -1");
            return false;
        }

        foreach (SkullCollider sc in skull.Instance.skullCollis)
        {
            int iL = sc.detect.collis.FindIndex((x) => x.layerName == "picaChild");
            if (iL != -1)
            {
                int iTr = sc.detect.collis[iL].trList.IndexOf(childList[GoSkullPosNotDone].gameObject);                    
                if (iTr != -1)
                {
                    value = true;
                    break;
                }
            }
        }

        return value;
    }    

    [ContextMenu("picaUp_StopAllCoroutines")]
    public void picaUp_StopAllCoroutines()
    {
        if (isDebug) Debug.Log("picaUp_StopAllCoroutines");
        StopAllCoroutines();
        pica.StopAllCoroutine();
    }
        
    public enum movementType
    {
        StopWhenAnyHit,
        StopWhenSelfHit
    }

    [Header("Movement Type")]
    public movementType mType = movementType.StopWhenAnyHit;    

    [ContextMenu("StopPicaDepend")]
    public void StopPicaDepend()
    {
        foreach (SkullCollider sc in skull.Instance.skullCollis)
        {
            if (sc.detect.collis.Count == 0) return;

            switch (mType)
            {
                case PicaUp.movementType.StopWhenAnyHit:
                    if (isDebug) Debug.Log("stop because " + sc.detect.collis);
                    picaUp_StopAllCoroutines();
                    break;

                case PicaUp.movementType.StopWhenSelfHit:
                    if (GoSkullPosNotDone == -1) return;
                    int indexSelfLayer = sc.detect.collis.FindIndex((x) => x.layerName == LayerMask.LayerToName(childList[GoSkullPosNotDone].gameObject.layer));
                    if (indexSelfLayer == -1) return;
                    int indexSelfTr = sc.detect.collis[indexSelfLayer].trList.IndexOf(childList[GoSkullPosNotDone].gameObject);
                    if (indexSelfTr != -1)
                    {
                        if (isDebug) Debug.Log("StopPicaDepend StopWhenSelfHit " + childList[GoSkullPosNotDone].transform.name);
                        picaUp_StopAllCoroutines();
                    }
                    break;
            }
        }
    }

    void StopImmediate()
    {
        //foreach (PicalHJChild child in childList)
        //{
        //    child.rb.Sleep();
        //    child.rb.velocity = Vector3.zero;
        //}
    }

    [Header("StopAndWait")]
    public bool isStopAndWait = false;
    public int veloNotDone = -1;
    public float deltaVelocity = 0.001f;
    public float infDrag = 999999f;    

    [ContextMenu("StopAndWait")]
    public void StopAndWait()
    {
        StartCoroutine(C_StopAndWait());
    }

    IEnumerator C_StopAndWait()
    {
        isStopAndWait = true;
        veloNotDone = -1;        

        foreach (PicaChild child in childList)
        {
            child.rb.drag = infDrag;
            child.rb.velocity = Vector3.zero;
        }

        while (!CheckIsAllMoving()) yield return new WaitForEndOfFrame();

        yield return new WaitForSeconds(1f);

        foreach (PicaChild child in childList)
        {
            child.GetComponent<RigidbodyTemplate>().Paste();
        }

        isStopAndWait = false;

        yield break;
    }

    bool CheckIsAllMoving()
    {
        int index = childList.FindIndex((x) => x.rb.velocity.magnitude > deltaVelocity);
        if (index != -1) { veloNotDone = index; return false; }
        return true;
    }

    [Header("Travel")]
    public float disTravel = 6f;

    [ContextMenu("AutoGo")]
    public void AutoGo()
    {
        if (isDebug) Debug.Log("Start AutoGo");

        TimeScale.Scale1();

        switch (mType)
        {
            case movementType.StopWhenAnyHit:
                skull.Instance.ResetskullCollis();
                GoSkull_StopWhenOneHit();
                break;

            case movementType.StopWhenSelfHit:
                skull.Instance.ResetskullCollis();
                GoSkull_StopWhenSelfHit();
                break;
        }        
    }
}
