﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HingeJointTarget : MonoBehaviour
{
    public Rigidbody tgTarget = null;
    public List<HingeJoint> hjList = new List<HingeJoint>();        

    [ContextMenu("GetHJ")]
    public void GetHJ()
    {
        List<HingeJoint> hjList = GetComponents<HingeJoint>().ToList();
        int index = hjList.FindIndex((x) => x.connectedBody != tgTarget);
        while (index != -1)
        {
            hjList.RemoveAt(index);
            index = hjList.FindIndex((x) => x.connectedBody != tgTarget);
        }

        this.hjList = hjList;
    }
}
