﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Pica_hjJob : MonoBehaviour
{
    [Header("hjControls")]
    public List<HingeJointControl> hjControls = new List<HingeJointControl>();

    [ContextMenu("Get_hjControls")]
    public void Get_hjControls()
    {
        hjControls = GetComponentsInChildren<HingeJointControl>().ToList();
    }

    [Header("Template")]
    public aHJData hjTemplate = new aHJData();

    [ContextMenu("SyncPartially")]
    public void SyncPartially()
    {
        foreach (HingeJointControl hj in hjControls)
        {
            List<aHJData> hjdataList = hj.hjdataList;
            for (int i = 0; i < hjdataList.Count; i++)
            {
                hjdataList[i].SyncPartially(this.hjTemplate);
            }
        }
    }

    [ContextMenu("SyncFully")]
    public void SyncFully()
    {
        foreach (HingeJointControl hjc in hjControls)
        {
            List<HingeJoint> hjList = hjc.hjList;
            for (int i = 0; i < hjList.Count; i++)
            {
                HingeJoint hj = hjList[i];
                hjTemplate.PastePartiallyToHJoint(hj, out hj);
            }
        }
    }
}
