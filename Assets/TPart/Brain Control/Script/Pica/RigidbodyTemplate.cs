﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class RigidbodyTemplate : MonoBehaviour
{
    [Header("Component")]
    public Rigidbody _rb = null;
    public Rigidbody rb { get { if (_rb == null) _rb = GetComponent<Rigidbody>(); return _rb; } set { _rb = value; } }    

    [Header("Params")]
    public float mass = 999f;
    public float drag = 99999f;
    public float angulardrag = 99999f;
    public bool useGravity = false;
    public bool isKinematic = false;
    public RigidbodyConstraints constrain = RigidbodyConstraints.None;

    [ContextMenu("Copy")]
    public void Copy()
    {
        mass = rb.mass;
        drag = rb.drag;
        angulardrag = rb.angularDrag;
        useGravity = rb.useGravity;
        isKinematic = rb.isKinematic;
        constrain = rb.constraints;
    }

    public void CopyFrom(RigidbodyTemplate rb)
    {
        this.mass = rb.mass;
        this.drag = rb.drag;
        this.angulardrag = rb.angulardrag;
        this.useGravity = rb.useGravity;
        this.isKinematic = rb.isKinematic;
        this.constrain = rb.constrain;
    }

    [ContextMenu("Paste")]
    public void Paste()
    {
        rb.mass = mass;
        rb.drag = drag;
        rb.angularDrag = angulardrag;
        rb.useGravity = useGravity;
        rb.isKinematic = isKinematic;
        rb.constraints = constrain;
    }

    public void DestroyRB()
    {
        //DestroyImmediate(rb);
        if (Application.isPlaying) Destroy(rb); else DestroyImmediate(rb);
    }    

    public void AddRB()
    {
        if (rb == null)
            rb = gameObject.AddComponent<Rigidbody>();
        Paste();
    }

    public Rigidbody AddReturnRB()
    {
        if (rb == null)
            rb = gameObject.AddComponent<Rigidbody>();
        Paste();
        return rb;
    }
}
