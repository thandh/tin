﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Pica_rbJob : MonoBehaviour
{
    [Header("rbControls")]
    public List<RigidBodyControl> rbControls = new List<RigidBodyControl>();

    [ContextMenu("Get_rbControls")]
    public void Get_rbControls()
    {
        rbControls = GetComponentsInChildren<RigidBodyControl>().ToList();
    }

    [Header("Template")]
    public aRBData rbTemplate = new aRBData();

    [ContextMenu("SyncFully")]
    public void SyncFully()
    {
        foreach (RigidBodyControl rbc in rbControls)
        {
            if (rbc._rb == null) rbc._rb = rbc.GetComponent<Rigidbody>();
            if (rbc._rb == null) return;
            rbTemplate.PasteToRigidBody(rbc._rb, out rbc._rb);
        }
    }
}
