﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PicaPreb_RevertPos : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Inputs")]
    public Transform currentPica = null;
    public Transform prebPica = null;

    [ContextMenu("RevertPos")]
    public void RevertPos()
    {
        Pica pica = currentPica.GetComponent<Pica>();
        for (int i = 0; i < pica.picachildList.Count; i++)
        {
            Transform prebChild = prebPica.Find(pica.picachildList[i].transform.name);
            if (prebChild != null)
            {
                if (isDebug) Debug.Log("Reverting " + pica.picachildList[i].transform.name);
                pica.picachildList[i].Position = prebChild.position;
                pica.picachildList[i].Rotation = prebChild.rotation;
            }
        }
    }
}
