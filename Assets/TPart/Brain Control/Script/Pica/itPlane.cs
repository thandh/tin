﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface itPlane
{
    Vector3 inNormal();
    Vector3 inPoint();
    Plane GetPlane();
    float DistanceTo(Vector3 point);
}
