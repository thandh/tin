﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PicaMuzzlee : MonoBehaviour
{
    public Rigidbody rb = null;
    public FixedJoint fj = null;    

    [ContextMenu("ResetMuzzle")]
    public void ResetMuzzle()
    {
        fj.connectedBody = null;
    }

    public void SetAnchor(PicaChild child)
    {
        transform.position = child.Position;
        fj.connectedBody = child.rb;
    }
}
