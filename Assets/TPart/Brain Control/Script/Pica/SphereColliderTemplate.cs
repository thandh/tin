﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SphereColliderTemplate : MonoBehaviour
{
    public SphereCollider _sc = null;
    public SphereCollider sc { get { if (_sc == null) _sc = GetComponent<SphereCollider>(); return _sc; } set { _sc = value; } }

    [Header("Params")]
    public float radius = 1f;
    public bool isTrigger = false;

    [ContextMenu("Copy")]
    public void Copy()
    {
        this.radius = sc.radius;
        this.isTrigger = sc.isTrigger;
    }

    public void CopyFrom(SphereColliderTemplate scTemplate)
    {
        this.radius = scTemplate.radius;
        this.isTrigger = scTemplate.isTrigger;
    }

    [ContextMenu("Paste")]
    public void Paste()
    {
        sc.radius = this.radius;
        sc.isTrigger = this.isTrigger;
    }

    public void DestroySC()
    {
        //DestroyImmediate(rb);
        if (Application.isPlaying) Destroy(sc); else DestroyImmediate(sc);
    }    

    public void AddSC()
    {
        if (sc == null)
            sc = gameObject.AddComponent<SphereCollider>();
        Paste();
    }

    public SphereCollider AddReturnSC()
    {
        if (sc == null)
            sc = gameObject.AddComponent<SphereCollider>();
        Paste();
        return sc;
    }
}
