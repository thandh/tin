﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AHingeJointSetting : MonoBehaviour
{
    public HingeJoint hj = null;

    [ContextMenu("AnchorCenter")]
    public void AnchorCenter()
    {
        Vector3 posCenter = (transform.position + hj.connectedBody.position) / 2f;
        Vector3 dirCenter = posCenter - transform.position;
        float angle = Vector3.Angle(dirCenter, transform.right);
        float xAnchor = Mathf.Sin(angle) * Mathf.Abs((transform.position.x - hj.connectedBody.position.x) / 2f);
        hj.anchor = new Vector3(xAnchor, 0, 0);
    }
}
