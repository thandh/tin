﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class aRBData
{
    public float mass = 1;
    public float drag = 1;
    public float angularDrag = 1;
    public bool useGravity = false;
    public bool isKinematic = false;

    public aRBData() { }

    public void CopyFromRigidBody(Rigidbody rb)
    {
        this.mass = rb.mass;
        this.drag = rb.drag;
        this.angularDrag = rb.angularDrag;
        this.useGravity = rb.useGravity;
        this.isKinematic = rb.isKinematic;
    }

    public void PasteToRigidBody(Rigidbody rb, out Rigidbody rbOut)
    {
        rb.mass = this.mass;
        rb.drag = this.drag;
        rb.angularDrag = this.angularDrag;
        rb.useGravity = this.useGravity;
        rb.isKinematic = this.isKinematic;

        rbOut = rb;
    }
}

public class RigidBodyControl : MonoBehaviour
{
    [Header("data")]
    public aRBData rbdata = new aRBData();

    public Rigidbody _rb = null;
    public Rigidbody rb
    {
        get { if (_rb == null) _rb = GetComponent<Rigidbody>(); return _rb; }        
    }

    [ContextMenu("Get rb")]
    public void GetRB()
    {
        _rb = GetComponent<Rigidbody>();
    }

    [ContextMenu("CopyRB")]
    public void CopyRB()
    {
        rbdata = new aRBData();
        rbdata.CopyFromRigidBody(rb);
    }

    [ContextMenu("PasteRB")]
    public void PasteRB()
    {
        if (_rb == null) _rb = gameObject.AddComponent<Rigidbody>();
        rbdata.PasteToRigidBody(_rb, out _rb);
    }
}
