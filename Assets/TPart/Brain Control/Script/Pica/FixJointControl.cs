﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class aFJData
{
    public Transform rb;    

    public aFJData() { }
    public aFJData(Transform rb) { this.rb = rb; }
}

public class FixJointControl : MonoBehaviour
{
    [Header("Process")]
    public bool use = false;

    public List<aFJData> fjListData = new List<aFJData>();

    PicaChild _picaChild = null;
    PicaChild picaChild { get { if (_picaChild == null) _picaChild = GetComponent<PicaChild>(); return _picaChild; } }

    [ContextMenu("CopyFJ")]
    public void CopyFJ()
    {
        if (!use) return;

        fjListData = new List<aFJData>();
        List<FixedJoint> listFJ = GetComponents<FixedJoint>().ToList();
        for (int i = 0; i < listFJ.Count; i++)
        {
            aFJData data = new aFJData();
            data.rb = listFJ[i].connectedBody.transform;
            fjListData.Add(data);
        }
    }

    [ContextMenu("PasteFJ")]
    public void PasteFJ()
    {
        if (!use) return;

        RemoveFJInlist();
        ForceRemoveAllFJ();
         
        for (int i = 0; i < fjListData.Count; i ++)
        {
            aFJData data = fjListData[i];
            Rigidbody rb = data.rb.GetComponent<Rigidbody>();
            if (rb == null) rb = data.rb.GetComponent<RigidbodyTemplate>().AddReturnRB();
            FixedJoint fj = gameObject.AddComponent<FixedJoint>();
            fj.connectedBody = rb;
            fjList.Add(fj);
        }
    }

    [Header("fj list")]
    public List<FixedJoint> fjList = new List<FixedJoint>();

    [ContextMenu("Getlist")]
    public void Getlist()
    {
        if (!use) return;

        fjList = GetComponentsInChildren<FixedJoint>().ToList();
    }

    public bool IsRemoveAll()
    {
        return GetComponentsInChildren<FixedJoint>().Length == 0;
    }

    [ContextMenu("CopyRemoveAllFJ")]
    public void CopyRemoveAllFJ()
    {
        if (!use) return;

        CopyFJ();
        RemoveFJInlist();
        ForceRemoveAllFJ();
    }

    [ContextMenu("RemoveFJInlist")]
    public void RemoveFJInlist()
    {
        if (!use) return;
        
        foreach (FixedJoint fj in fjList)
        {
            //DestroyImmediate(fj);
            if (Application.isPlaying) Destroy(fj); else DestroyImmediate(fj);
        }
        this.fjList = new List<FixedJoint>();
    }

    [ContextMenu("ForceRemoveAllFJ")]
    public void ForceRemoveAllFJ()
    {
        if (!use) return;

        fjList = GetComponents<FixedJoint>().ToList();

        foreach (FixedJoint fj in fjList)
        {
            //DestroyImmediate(fj);
            if (Application.isPlaying) Destroy(fj); else DestroyImmediate(fj);
        }
        this.fjList = new List<FixedJoint>();
    }
}
