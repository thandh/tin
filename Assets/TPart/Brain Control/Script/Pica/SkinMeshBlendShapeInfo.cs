﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinMeshBlendShapeInfo : MonoBehaviour
{
    [Header("Params")]
    public SkinnedMeshRenderer smr = null;

    [Header("Get")]
    public int indexGet = 0;

    [ContextMenu("DebugAll")]
    public void DebugAll()
    {
        Debug.Log("Weight at " + indexGet + " : " + smr.GetBlendShapeWeight(indexGet));
        Debug.Log("Bone[] length : " + smr.bones.Length);
        for (int i = 0; i < smr.bones.Length; i++)
        {
            Debug.Log(smr.bones[i].name);
        }        
    }
}
