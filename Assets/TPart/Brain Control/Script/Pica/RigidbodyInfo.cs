﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class RigidbodyInfo : MonoBehaviour
{
    Rigidbody _rb = null;
    public Rigidbody rb { get { if (_rb == null) _rb = GetComponent<Rigidbody>(); return _rb; } set { _rb = value; } }

    [Header("Show Info")]
    public bool globalShow = false;
    
    public bool showVelocity = false;
    public Vector3 velocity = Vector3.zero;

    private void Update()
    {
        if (!globalShow) return;
        if (rb == null) return;
        if (showVelocity) velocity = rb.velocity;
    }
}
