﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class aSJData
{
    public Transform rb = null;
    //public Vector3 anchor = Vector3.zero;
    //public bool autoConfigureConnected = true;
    public float spring = 10f;
    public float damper = 0.2f;
    public float minDis = 0f;
    public float maxDis = 0f;
    public float tolerance = 0.025f;
    public float breakForce = Mathf.Infinity;
    public float breakTorque = Mathf.Infinity;
    public bool enableCollision = false;
    public bool enablePreprocessing = false;
    public float massScale = 1f;
    public float connectedMassScale = 1f;

    public aSJData() { }

    public void CopyFromSJoint(SpringJoint sj)
    {
        this.rb = sj.connectedBody == null ? null : sj.connectedBody.transform;
        //this.anchor = sj.anchor;
        //this.autoConfigureConnected = sj.autoConfigureConnectedAnchor;
        this.spring = sj.spring;
        this.damper = sj.damper;
        this.minDis = sj.minDistance;
        this.maxDis = sj.maxDistance;
        this.tolerance = sj.tolerance;
        this.breakForce = sj.breakForce;
        this.breakTorque = sj.breakTorque;
        this.enableCollision = sj.enableCollision;
        this.enablePreprocessing = sj.enablePreprocessing;
        this.massScale = sj.massScale;
        this.connectedMassScale = sj.connectedMassScale;
    }

    public void PasteToSJoint(SpringJoint sj, out SpringJoint sjOut)
    {
        sj.connectedBody = this.rb == null ? null : this.rb.GetComponent<Rigidbody>();
        //sj.anchor = this.anchor;
        //sj.autoConfigureConnectedAnchor = this.autoConfigureConnected;        
        sj.spring = this.spring;
        sj.damper = this.damper;
        sj.minDistance = this.minDis;
        sj.maxDistance = this.maxDis;
        sj.tolerance = this.tolerance;
        sj.breakForce = this.breakForce;
        sj.breakTorque = this.breakTorque;
        sj.enableCollision = this.enableCollision;
        sj.enablePreprocessing = this.enablePreprocessing;
        sj.massScale = this.massScale;
        sj.connectedMassScale = this.connectedMassScale;

        sjOut = sj;
    }

    public void PastePartiallyToSJoint(SpringJoint sj, out SpringJoint sjOut)
    {        
        sj.spring = this.spring;
        sj.damper = this.damper;
        sj.minDistance = this.minDis;
        sj.maxDistance = this.maxDis;
        sj.tolerance = this.tolerance;
        sj.breakForce = this.breakForce;
        sj.breakTorque = this.breakTorque;
        sj.enableCollision = this.enableCollision;
        sj.enablePreprocessing = this.enablePreprocessing;
        sj.massScale = this.massScale;
        sj.connectedMassScale = this.connectedMassScale;

        sjOut = sj;
    }

    public void SyncPartially(aSJData data)
    {        
        //this.anchor = sj.anchor;
        //this.autoConfigureConnected = sj.autoConfigureConnectedAnchor;
        this.spring = data.spring;
        this.damper = data.damper;
        this.minDis = data.minDis;
        this.maxDis = data.maxDis;
        this.tolerance = data.tolerance;
        this.breakForce = data.breakForce;
        this.breakTorque = data.breakTorque;
        this.enableCollision = data.enableCollision;
        this.enablePreprocessing = data.enablePreprocessing;
        this.massScale = data.massScale;
        this.connectedMassScale = data.connectedMassScale;
    }
}

public class SpringJointControl : MonoBehaviour
{
    public bool isDebug = false;

    [Header("Process")]
    public bool use = false;

    [Header("data list")]
    public List<aSJData> sjdataList = new List<aSJData>();

    [ContextMenu("CopySJ")]
    public void CopySJ()
    {
        if (!use) return;

        sjdataList = new List<aSJData>();
        List<SpringJoint> listSJ = GetComponents<SpringJoint>().ToList();
        for (int i = 0; i < listSJ.Count; i++)
        {
            aSJData data = new aSJData();
            data.CopyFromSJoint(listSJ[i]);
            sjdataList.Add(data);
        }
    }

    [ContextMenu("PasteSJ")]
    public void PasteSJ()
    {
        if (!use) return;

        RemoveOnlyAllSJ();

        for (int i = 0; i < sjdataList.Count; i++)
        {
            aSJData data = sjdataList[i];
            Rigidbody rb = data.rb.GetComponent<Rigidbody>();
            if (rb == null) rb = data.rb.GetComponent<RigidbodyTemplate>().AddReturnRB();
            SpringJoint sj = gameObject.AddComponent<SpringJoint>();
            data.PasteToSJoint(sj, out sj);
            sjList.Add(sj);
        }
    }

    [Header("sj list")]
    public List<SpringJoint> sjList = new List<SpringJoint>();

    [ContextMenu("Getlist")]
    public void Getlist()
    {
        if (!use) return;

        sjList = GetComponentsInChildren<SpringJoint>().ToList();
    }

    public bool IsRemoveAll()
    {
        return GetComponentsInChildren<SpringJoint>().Length == 0;
    }

    [ContextMenu("CopyRemoveAllSJ")]
    public void CopyRemoveAllSJ()
    {
        if (!use) return;

        CopySJ();
        RemoveOnlyAllSJ();
    }

    [ContextMenu("RemoveOnlyAllSJ")]
    public void RemoveOnlyAllSJ()
    {
        if (!use) return;

        foreach (SpringJoint sj in sjList)
        {
            //DestroyImmediate(sj);
            if (Application.isPlaying) Destroy(sj); else DestroyImmediate(sj);            
        }
        sjList = new List<SpringJoint>();
    }

    [ContextMenu("MiddleBone_GetInput")]
    public void MiddleBone_GetInput()
    {
        if (sjdataList.Count != 2) { Debug.Log("sjdataList.Count != 2"); return; }
        int currentIndex = transform.GetSiblingIndex();
        Transform parent = transform.parent;
        sjdataList[0].rb = parent.GetChild(currentIndex - 1);
        sjdataList[1].rb = parent.GetChild(currentIndex + 1);
    }
}
