﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class aHJData
{
    public Transform rb = null;
    public Vector3 anchor = Vector3.zero;    

    [Serializable]
    public class springData
    {
        public float springForce = 0f;
        public float damper = 0f;
        public float targetposition = 0f;

        public springData() { }

        public void CopyFromJointSpring(HingeJoint hj)
        {
            JointSpring spring = hj.spring;
            this.springForce = spring.spring;
            this.damper = spring.damper;
            this.targetposition = spring.targetPosition;
        }

        public JointSpring PasteToJointSpring()
        {
            JointSpring sj = new JointSpring();
            sj.spring = this.springForce;
            sj.damper = this.damper;
            sj.targetPosition = this.targetposition;
            return sj;
        }
    }

    [Serializable]
    public class motorData
    {
        public float targetVelo = 0f;
        public float force = 0f;
        public bool freeSpin = false;

        public motorData() { }

        public void CopyFromJointMotor(HingeJoint hj)
        {
            JointMotor motor = hj.motor;
            this.targetVelo = motor.targetVelocity;
            this.force = motor.force;
            this.freeSpin = motor.freeSpin;
        }

        public JointMotor PasteToJointMotor()
        {
            JointMotor motor = new JointMotor();
            motor.targetVelocity = this.targetVelo;
            motor.force = this.force;
            motor.freeSpin = this.freeSpin;
            return motor;
        }
    }

    [Serializable]
    public class limitData
    {
        public float min = 0f;
        public float max = 0f;
        public float bounciness = 0f;
        public float bounceMinVelo = 0f;
        public float contactDistance = 0f;

        public limitData() { }

        public void CopyFromJointLimit(HingeJoint hj)
        {
            JointLimits limits = hj.limits;
            this.min = limits.min;
            this.max = limits.max;
            this.bounciness = limits.bounciness;
            this.bounceMinVelo = limits.bounceMinVelocity;
            this.contactDistance = limits.contactDistance;
        }

        public JointLimits PasteToJointLimit()
        {
            JointLimits jl = new JointLimits();
            jl.min = this.min;
            jl.max = this.max;
            jl.bounciness = this.bounciness;
            jl.bounceMinVelocity = this.bounceMinVelo;
            jl.contactDistance = this.contactDistance;            
            return jl;
        }
    }

    public aHJData() { }

    [Header("Spring Info")]
    public bool useSpring = false;    
    [SerializeField] private springData _springdata = null;
    public springData springdata { get { return _springdata; } set { _springdata = value; useSpring = _springdata != null; } }

    [Header("Spring Motor")]
    public bool useMotor = false;
    [SerializeField] private motorData _motordata = null;
    public motorData motordata { get { return _motordata; } set { _motordata = value; useMotor = _motordata != null; } }

    [Header("Spring Limit")]
    public bool useLimit = false;
    [SerializeField] private limitData _limdata = null;
    public limitData limdata { get { return _limdata; } set { _limdata = value; useLimit = _limdata != null; } }

    [Header("Break Force - Connected Mass Scale")]
    public float breakForce = Mathf.Infinity;
    public float breakTorque = Mathf.Infinity;
    public bool enableCollision = false;
    public bool enablePreprocessing = true;
    public float massScale = 1f;
    public float connectedMassScale = 1f;

    public void CopyFromHJoint(HingeJoint hj)
    {
        this.rb = hj.connectedBody == null ? null : hj.connectedBody.transform;
        //this.anchor = sj.anchor;
        //this.autoConfigureConnected = sj.autoConfigureConnectedAnchor;
        this.springdata = new springData();
        this.springdata.CopyFromJointSpring(hj);

        this.motordata = new motorData();
        this.motordata.CopyFromJointMotor(hj);

        this.limdata = new limitData();
        this.limdata.CopyFromJointLimit(hj);

        this.breakForce = hj.breakForce;
        this.breakTorque = hj.breakTorque;
        this.enableCollision = hj.enableCollision;
        this.enablePreprocessing = hj.enablePreprocessing;
        this.massScale = hj.massScale;
        this.connectedMassScale = hj.connectedMassScale;
    }

    public void PasteToHJoint(HingeJoint hj, out HingeJoint hjOut)
    {
        hj.connectedBody = this.rb == null ? null : this.rb.GetComponent<Rigidbody>();
        //sj.anchor = this.anchor;
        //sj.autoConfigureConnectedAnchor = this.autoConfigureConnected;        

        hj.useSpring = this.useSpring;
        hj.spring = this.springdata.PasteToJointSpring();

        hj.useMotor = this.useMotor;
        hj.motor = this.motordata.PasteToJointMotor();

        hj.useLimits = this.useLimit;
        hj.limits = this.limdata.PasteToJointLimit();

        hj.breakForce = this.breakForce;
        hj.breakTorque = this.breakTorque;
        hj.enableCollision = this.enableCollision;
        hj.enablePreprocessing = this.enablePreprocessing;
        hj.massScale = this.massScale;
        hj.connectedMassScale = this.connectedMassScale;

        hjOut = hj;
    }

    public void SyncPartially(aHJData data)
    {
        this.springdata = data.springdata;
        this.limdata = data.limdata;
        this.motordata = data.motordata;
        this.breakForce = data.breakForce;
        this.breakTorque = data.breakTorque;
        this.enableCollision = data.enableCollision;
        this.enablePreprocessing = data.enablePreprocessing;
        this.massScale = data.massScale;
        this.connectedMassScale = data.connectedMassScale;
    }

    public void PastePartiallyToHJoint(HingeJoint hj, out HingeJoint hjOut)
    {
        hj.useSpring = this.useSpring;
        hj.spring = this.springdata.PasteToJointSpring();

        hj.useMotor = this.useMotor;
        hj.motor = this.motordata.PasteToJointMotor();

        hj.useLimits = this.useLimit;
        hj.limits = this.limdata.PasteToJointLimit();

        hj.breakForce = this.breakForce;
        hj.breakTorque = this.breakTorque;
        hj.enableCollision = this.enableCollision;
        hj.enablePreprocessing = this.enablePreprocessing;
        hj.massScale = this.massScale;
        hj.connectedMassScale = this.connectedMassScale;

        hjOut = hj;
    }
}

public class HingeJointControl : MonoBehaviour
{
    [Header("Process")]
    public bool use = false;

    [Header("data list")]
    public List<aHJData> hjdataList = new List<aHJData>();    

    [ContextMenu("CopyHJ")]
    public void CopyHJ()
    {
        if (!use) return;

        hjdataList = new List<aHJData>();
        List<HingeJoint> listHJ = GetComponents<HingeJoint>().ToList();
        for (int i = 0; i < listHJ.Count; i++)
        {
            aHJData data = new aHJData();
            data.CopyFromHJoint(listHJ[i]);
            hjdataList.Add(data);
        }
    }

    [ContextMenu("PasteHJ")]
    public void PasteHJ()
    {
        if (!use) return;

        RemoveOnlyAllHJ();
         
        for (int i = 0; i < hjdataList.Count; i ++)
        {
            aHJData data = hjdataList[i];
            Rigidbody rb = data.rb.GetComponent<Rigidbody>();
            if (rb == null) rb = data.rb.GetComponent<RigidbodyTemplate>().AddReturnRB();
            HingeJoint hj = gameObject.AddComponent<HingeJoint>();
            data.PasteToHJoint(hj, out hj);
            hjList.Add(hj);
        }
    }

    [Header("hj list")]
    public List<HingeJoint> hjList = new List<HingeJoint>();

    [ContextMenu("Getlist")]
    public void Getlist()
    {
        if (!use) return;

        hjList = GetComponentsInChildren<HingeJoint>().ToList();
    }

    public bool IsRemoveAll()
    {
        return GetComponentsInChildren<HingeJoint>().Length == 0;
    }

    [ContextMenu("CopyRemoveAllHJ")]
    public void CopyRemoveAllHJ()
    {
        if (!use) return;

        CopyHJ();
        RemoveOnlyAllHJ();
    }

    [ContextMenu("RemoveOnlyAllHJ")]
    public void RemoveOnlyAllHJ()
    {
        if (!use) return;

        foreach (HingeJoint hj in hjList)
        {
            //DestroyImmediate(hj);
            if (Application.isPlaying) Destroy(hj); else DestroyImmediate(hj);
        }
        hjList = new List<HingeJoint>();
    }

    [ContextMenu("TargetPrevious")]
    public void TargetPrevious()
    {
        int childIndex = transform.GetSiblingIndex();
        HingeJoint hj = gameObject.AddComponent<HingeJoint>();
        hj.connectedBody = transform.parent.GetChild(childIndex - 1).GetComponent<Rigidbody>();
    }

    [ContextMenu("TargetNext")]
    public void TargetNext()
    {
        int childIndex = transform.GetSiblingIndex();
        HingeJoint hj = gameObject.AddComponent<HingeJoint>();
        hj.connectedBody = transform.parent.GetChild(childIndex + 1).GetComponent<Rigidbody>();
    }

    [ContextMenu("MiddleBone_GetInput")]
    public void MiddleBone_GetInput()
    {
        if (hjdataList.Count != 2) { Debug.Log("hjdataList.Count != 2"); return; }
        int currentIndex = transform.GetSiblingIndex();
        Transform parent = transform.parent;
        hjdataList[0].rb = parent.GetChild(currentIndex - 1);
        hjdataList[1].rb = parent.GetChild(currentIndex + 1);
    }
}
