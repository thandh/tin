﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AteryRun : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    //[Header("Params")]
    //public Pica pica = null;
    //public PicaUp picaUp = null;
    //public 

    public void RunAt(int childID, float distance, float xPos, float yPos, float zPos)
    {
        StartCoroutine(C_RunAt(childID, distance, xPos, yPos, zPos));
    }

    IEnumerator C_RunAt(int childID, float distance, float xPos, float yPos, float zPos)
    {
        if (isDebug) Debug.Log("Atery RunAt : " + childID);

        TimeScale.Scale1();

        ///In-if : compare to Pica Up Godown, else-if : always set false
        if (ManngerSystem.Instance.boolUsePauseAfterGoSkull)
            PICA.Instance.picaUp.boolPauseAfterGoSkull = false;
        else
            PICA.Instance.picaUp.boolPauseAfterGoSkull = false;

        PicaChild picaChild = PICA.Instance.pica.picachildList[childID];

        if (xPos == -1 && yPos == -1 && zPos == -1)
        {
            picaChild.RunToSkull();
        } else
        {
            picaChild.currentDistanceToMove = distance;

            ///picaChild.currentDirToMove = new Vector3(xPos, yPos, zPos) - picaChild.Position;
            
            ///Follow world -OZ (BS direction)
            picaChild.currentDirToMove = new Vector3(0, 0, -1f);

            picaChild.forceRun = PICA.Instance.picaUp.forceRun;
            picaChild.forceRunFinal = PICA.Instance.picaUp.forceRunFinal;
            picaChild.timeStep_forceRun = PICA.Instance.picaUp.timeStep_forceRun;
            picaChild.stepIncrease_forceRun = PICA.Instance.picaUp.stepIncrease_forceRun;

            PICA.Instance.picaUp.GoSkullPosNotDone = childID;
            picaChild.StartCoroutine(picaChild.C_RunToSkull());
        }

        yield return new WaitUntil(() => picaChild.doneRunToSkull == true);

        //PICA.Instance.picaDrag.SetPicaKinematic(true);
        picaChild.rb.isKinematic = true;

        yield break;
    }
}