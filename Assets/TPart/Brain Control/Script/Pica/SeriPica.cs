﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SeriPica : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Params")]
    public Pica pica = null;

    [Serializable]
    public class picaSeri
    {
        public List<Transform> trList = new List<Transform>();
        public picaSeri() { }
    }

    [Serializable]
    public class seriFromTo
    {
        public Transform trFrom = null;
        public Transform trTo = null;
    }

    [Header("TrySeries")]
    public List<picaSeri> picaSeriList = new List<picaSeri>();
    public List<seriFromTo> seriFromToList = new List<seriFromTo>();

    [ContextMenu("DeleteAllSeri")]
    public void DeleteAllSeri()
    {
        picaSeriList = new List<picaSeri>();
    }

    [ContextMenu("TryTheseSeri")]
    public void TryTheseSeri()
    {
        for (int i = 0; i < seriFromToList.Count; i++)
        {
            TrySeri(seriFromToList[i]);
        }
    }

    void TrySeri(seriFromTo seriFromTo)
    {
        if (seriFromTo.trFrom == null || seriFromTo.trTo == null)
        {
            if (isDebug) Debug.Log("Null!!");
            return;
        }

        int iFrom = pica.picachildList.FindIndex((x) => x.transform == seriFromTo.trFrom);
        int iTo = pica.picachildList.FindIndex((x) => x.transform == seriFromTo.trTo);

        if (iFrom == -1 || iTo == -1) { if (isDebug) Debug.Log("-1!!"); return; }

        if (picaSeriList.FindIndex((x) => x.trList[0] == seriFromTo.trFrom && x.trList[x.trList.Count - 1] == seriFromTo.trTo) != -1)
        {
            if (isDebug) Debug.Log("Da ton tai " + seriFromTo.trFrom + ", " + seriFromTo.trTo);
            return;
        }

        if (isDebug) Debug.Log("Adding (" + iFrom + "," + iTo + ")" + seriFromTo.trFrom.name + ", " + seriFromTo.trTo.name);

        picaSeri seri = new picaSeri();
        seri.trList = new List<Transform>();
        for (int i = iFrom; i < iTo + 1; i++) { seri.trList.Add(pica.picachildList[i].transform); }
        picaSeriList.Add(seri);
    }

    [Header("ConnectSeri")]
    public List<seriFromTo> connectSeri = new List<seriFromTo>();
}
