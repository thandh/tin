﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Pica_cjJob : MonoBehaviour
{
    [Header("cjControls")]
    public List<ConfiJointControl> cjControls = new List<ConfiJointControl>();

    [ContextMenu("Get_cjControls")]
    public void Get_cjControls()
    {
        cjControls = GetComponentsInChildren<ConfiJointControl>().ToList();
    }

    [Header("Template")]
    public ConfigurableJoint cjTemplate = null;

    [ContextMenu("SyncFully")]
    public void SyncFully()
    {
        if (cjTemplate != null)
        {
            foreach (ConfiJointControl cjc in cjControls)
            {
                cjc.cjTemplate = cjTemplate;
                List<ConfigurableJoint> cjList = cjc.cjList;
                for (int i = 0; i < cjList.Count; i++)
                {
                    ConfigurableJoint cj = cjList[i];
                    cjc.cjdataList[i].PastePartiallyToCJoint(this.cjTemplate, cj, out cj);
                }
            }
        }
    }
}
