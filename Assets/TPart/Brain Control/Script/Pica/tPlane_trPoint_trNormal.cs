﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tPlane_trPoint_trNormal : tPlane, itPlane
{
    [Header("params")]
    public Transform _trPoint = null;
    public Transform _trStart = null;
    public Transform _trEnd = null;

    private Plane thisPlane = new Plane();

    public Vector3 inNormal()
    {
        return _trEnd.position - _trStart.position;
    }

    public Vector3 inPoint()
    {
        return _trPoint.position;
    }

    public Plane GetPlane()
    {
        thisPlane = new Plane(inNormal(), inPoint());
        return thisPlane;
    }

    public float DistanceTo(Vector3 point)
    {
        return thisPlane.GetDistanceToPoint(point);
    }
}
