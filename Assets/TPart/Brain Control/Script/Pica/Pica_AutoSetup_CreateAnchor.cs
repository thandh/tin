﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Pica_AutoSetup_CreateAnchor : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Params")]
    public Transform anchorPreb = null;
    public Pica_AutoSetup pica_AutoSetup = null;

    [Header("Childs need anchor")]
    public List<Transform> childList = new List<Transform>();

    [ContextMenu("ResetAnchor")]
    public void ResetAnchor()
    {

    }

    [ContextMenu("AnchorThem")]
    public void AnchorThem()
    {
        foreach(Transform tr in childList)
        {
            FixJointControl fjControl = tr.GetComponent<FixJointControl>();
            if (fjControl.fjListData.Count == 0)
            {
                GameObject anchorGO = Instantiate((GameObject)anchorPreb.gameObject, pica_AutoSetup.picaTr);
                anchorGO.SetActive(true);
                anchorGO.transform.SetSiblingIndex(tr.GetSiblingIndex() + 1);
                Snap snap = anchorGO.GetComponent<Snap>();
                snap.target = tr;
                snap.SnapPosition();
                fjControl.fjListData.Add(new aFJData(anchorGO.transform));
            }
            fjControl.use = true;
            fjControl.PasteFJ();
        }
    }

    //[Header("Stop Use Anchor")]
    //public int stopAncID = -1;

    //[ContextMenu("StopAtID")]
    //public void StopAtID()
    //{
    //    StopByAnc(stopAncID);
    //}

    //public void StopByAnc(int childID)
    //{
    //    Pica pica = PICA.Instance.pica;
    //    PicaChild childPica = pica.picachildList.Find((x) => x)

    //    if (ancStop != null)
    //    {
    //        RemoveAncStop();
    //    }

    //    Pica_AutoSetup_CreateAnchor creatAnchor

    //    GameObject anchorGO = Instantiate((GameObject)anchorPreb.gameObject, pica_AutoSetup.picaTr);
    //    anchorGO.SetActive(true);
    //    anchorGO.transform.SetSiblingIndex(tr.GetSiblingIndex() + 1);
    //    Snap snap = anchorGO.GetComponent<Snap>();
    //    snap.target = tr;
    //    snap.SnapPosition();
    //    fjControl.fjListData.Add(new aFJData(anchorGO.transform));
    //}
}
