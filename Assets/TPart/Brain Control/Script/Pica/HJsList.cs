﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HJsList : MonoBehaviour
{
    public List<HingeJoint> hjlist = new List<HingeJoint>();

    [ContextMenu("GetList")]
    public void GetList()
    {
        hjlist = GetComponents<HingeJoint>().ToList();
    }

    [Header("Enable Collider")]
    public bool setEnableCollider = false;

    [ContextMenu("SetEnableCollider")]
    public void SetEnableCollider()
    {
        foreach (HingeJoint hj in hjlist) hj.enableCollision = setEnableCollider;
    }

    [ContextMenu("DeleteAll")]
    public void DeleteAll()
    {
        foreach (HingeJoint hj in hjlist)
            if (Application.isPlaying) Destroy(hj); else DestroyImmediate(hj);

        hjlist = new List<HingeJoint>();
    }
}
