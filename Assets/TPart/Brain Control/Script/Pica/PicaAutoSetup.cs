﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PicaAutoSetup : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Input")]
    public SkinnedMeshRenderer skin = null;

    [ContextMenu("Setup")]
    public void Setup()
    {
        //if (isDebug) Debug.Log(skin.);
    }
}
