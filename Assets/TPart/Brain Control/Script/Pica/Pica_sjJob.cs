﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Pica_sjJob : MonoBehaviour
{
    [Header("sjControls")]
    public List<SpringJointControl> sjControls = new List<SpringJointControl>();

    [ContextMenu("Get_sjControls")]
    public void Get_sjControls()
    {
        sjControls = GetComponentsInChildren<SpringJointControl>().ToList();
    }

    [Header("Template")]
    public aSJData sjTemplate = new aSJData();

    [ContextMenu("SyncPartially")]
    public void SyncPartially()
    {
        foreach (SpringJointControl sj in sjControls)
        {
            List<aSJData> sjdataList = sj.sjdataList;
            for (int i = 0; i < sjdataList.Count; i++)
            {
                sjdataList[i].SyncPartially(this.sjTemplate);
            }
        }
    }

    [ContextMenu("SyncFully")]
    public void SyncFully()
    {
        foreach (SpringJointControl sjc in sjControls)
        {
            List<SpringJoint> sjList = sjc.sjList;
            for (int i = 0; i < sjList.Count; i ++)
            {
                SpringJoint sj = sjList[i];
                sjTemplate.PastePartiallyToSJoint(sj, out sj);
            }
        }
    }
}
