﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Pica_ConfigurationJoint : MonoBehaviour
{
    public List<PicaBone> boneList = new List<PicaBone>();

    [ContextMenu("Getlist")]
    public void Getlist()
    {
        boneList = GetComponentsInChildren<PicaBone>().ToList();
    }

    [ContextMenu("DeleteAllConfiJoint")]
    public void DeleteAllConfiJoint()
    {
        for (int i = 1; i < boneList.Count; i++)
        {
            ConfigurableJoint[] confiJoints = boneList[i].GetComponents<ConfigurableJoint>();
            for (int j = 0; j < confiJoints.Length; j++)
                if (Application.isPlaying) Destroy(confiJoints[j]); else DestroyImmediate(confiJoints[j]);
        }
    }

    [ContextMenu("AddConfiJoint")]
    public void AddConfiJoint()
    {
        for (int i = 1; i < boneList.Count; i++)
        {
            ConfigurableJoint cj = boneList[i].gameObject.AddComponent<ConfigurableJoint>();
            cj.connectedBody = boneList[i - 1].GetComponent<Rigidbody>();
        }
    }
}
