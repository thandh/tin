﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PicaUpData
{
    public List<Vector3> posList = new List<Vector3>();
    public List<Quaternion> rotList = new List<Quaternion>();

    public PicaUpData()
    {
        posList = new List<Vector3>();
        rotList = new List<Quaternion>();
    }

    public string ToJson() { return JsonUtility.ToJson(this); }
}
