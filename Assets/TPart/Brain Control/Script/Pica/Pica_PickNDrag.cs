﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class Pica_PickNDrag : MonoBehaviour
{
    [Header("Process")]
    public bool isDebug = false;

    [Header("Lock")]
    public bool isLock = true;

    CamPivot_Movement _cmc = null;
    CamPivot_Movement cmc { get { if (_cmc == null) _cmc = FindObjectOfType<CamPivot_Movement>(); return _cmc; } }

    public Pica pica = null;

    [Header("Pick and drag")]
    public LayerMask maskChild = -99;
    public LayerMask maskBound = -99;

    public Transform lastHit = null;
    public PicaChild picaChild = null;
    public Vector3 lastPos = -Vector3.one;
    public Vector3 lastMousePos = -Vector3.one;

    [HideInInspector]
    public float stepMove = 1f;

    private void Start()
    {
        maskChild = LayerMask.GetMask("picaChild");
        maskBound = LayerMask.GetMask("picaBound");
    }

    [ContextMenu("CopyTransition")]
    public void CopyTransition()
    {
        foreach (PicaChild child in pica.picachildList)
            child.CopyTransition();
    }

    [ContextMenu("ResetPosition")]
    public void ResetPosition()
    {
        foreach (PicaChild child in pica.picachildList)
            child.ResetPosition();
    }

    [ContextMenu("StopAllCoroutine")]
    public void StopAllCoroutine()
    {
        StopAllCoroutines();
        for (int i = 0; i < pica.picachildList.Count; i++)
        {
            pica.picachildList[i].StopAllCoroutines();
        }
    }

    [ContextMenu("Toggle_Collider")]
    public void Toggle_Collider()
    {
        for (int i = 0; i < pica.picachildList.Count; i++)
        {
            pica.picachildList[i].Toggle_Collider();
        }
    }

    GameObject picaGuidance = null;
    public float stepPos = 0.5f;

    private void Update()
    {
        if (isLock) return;

        if (Input.GetMouseButton(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Debug.DrawRay(ray.origin, ray.direction * Mathf.Infinity);

            if (lastHit == null && lastPos == -Vector3.one && lastMousePos == -Vector3.one)
            {
                if (Physics.Raycast(ray, out hit, Mathf.Infinity, maskChild.value))
                {
                    if (lastHit == null)
                    {
                        cmc.IsOn = false;

                        lastHit = hit.transform;

                        picaChild = lastHit.GetComponent<PicaChild>();
                        if (picaChild != null && picaChild.boolAllowMove)
                        {
                            SetPicaKinematic(false);
                        }

                        lastPos = hit.transform.position;
                        lastMousePos = ray.origin;

                        picaGuidance = new GameObject();
                        picaGuidance.transform.position = lastPos;
                        return;
                    }
                }
            }
            else if (lastHit != null && lastPos != -Vector3.one && lastMousePos != -Vector3.one)
            {
                if (!lastHit.GetComponent<PicaChild>().boolAllowMove) return;

                Vector3 reflect = (ray.origin - lastMousePos) * (lastPos - Camera.main.transform.position).magnitude / (lastMousePos - Camera.main.transform.position).magnitude;
                picaGuidance.transform.position = lastPos + reflect;

                float step = Time.deltaTime * stepPos;
                lastHit.transform.position = new Vector3(Mathf.Lerp(lastHit.transform.position.x, picaGuidance.transform.position.x, step),
                    Mathf.Lerp(lastHit.transform.position.y, picaGuidance.transform.position.y, step),
                    Mathf.Lerp(lastHit.transform.position.z, picaGuidance.transform.position.z, step));
            }
        }
        else
        {
            if (lastHit == null) return;

            cmc.IsOn = true;

            lastHit = null;
            if (picaChild != null) picaChild.rb.isKinematic = false;
            lastPos = -Vector3.one;
            lastMousePos = -Vector3.one;
            DestroyImmediate(picaGuidance);
            SetPicaKinematic(true);
        }
    }

    public void SetPicaKinematic(bool On)
    {
        foreach (PicaChild child in pica.picachildList)
        {
            child.rb.isKinematic = On;
        }
    }

    public void OnOffLock(bool On)
    {
        isLock = On;
    }

    public void OnOffLock(Toggle tg)
    {
        isLock = !tg.isOn;
    }
}
