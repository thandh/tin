﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HingeJointInfo : MonoBehaviour
{
    public HingeJoint hj = null;    

    Rigidbody _rb = null;
    Rigidbody rb { get { if (_rb == null) _rb = GetComponent<Rigidbody>(); return _rb; } }

    [ContextMenu("DebugPosition")]
    public void DebugPosition()
    {
        Debug.Log("rb position " + rb.position);
        Debug.Log("connect rb position " + hj.connectedBody.position);
        Debug.Log("distance " + (rb.position - hj.connectedBody.position).magnitude);
        Debug.Log("distanceX " + Mathf.Abs(rb.position.x - hj.connectedBody.position.x));
        Debug.Log("distanceY " + Mathf.Abs(rb.position.y - hj.connectedBody.position.y));
        Debug.Log("distanceZ " + Mathf.Abs(rb.position.z - hj.connectedBody.position.z));
        Debug.Log("anchor " + hj.anchor);
    }
}
