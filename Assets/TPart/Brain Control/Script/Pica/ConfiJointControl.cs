﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class aCJData
{
    public Transform rb = null;

    public aCJData() { }
    public aCJData(Transform rb) { this.rb = rb; }

    public void CopyFromCJoint(ConfigurableJoint cj)
    {
        this.rb = cj.connectedBody == null ? null : cj.connectedBody.transform;        
    }

    public void PasteToCJoint(ConfigurableJoint cj, out ConfigurableJoint cjOut)
    {
        cj.connectedBody = this.rb == null ? null : this.rb.GetComponent<Rigidbody>();
        cjOut = cj;
    }

    public void PastePartiallyToCJoint(ConfigurableJoint cjTemplate, ConfigurableJoint cj, out ConfigurableJoint cjOut)
    {
        if (cjTemplate != null)
        {
            cj.xMotion = cjTemplate.xMotion;
            cj.yMotion = cjTemplate.yMotion;
            cj.zMotion = cjTemplate.zMotion;
            cj.angularXMotion = cjTemplate.angularXMotion;
            cj.angularYMotion = cjTemplate.angularYMotion;
            cj.angularZMotion = cjTemplate.angularZMotion;
            cj.linearLimitSpring = cjTemplate.linearLimitSpring;
            cj.linearLimit = cjTemplate.linearLimit;
            cj.angularXLimitSpring = cjTemplate.angularXLimitSpring;
            cj.lowAngularXLimit = cjTemplate.lowAngularXLimit;
            cj.highAngularXLimit = cjTemplate.highAngularXLimit;
            cj.angularYZLimitSpring = cjTemplate.angularYZLimitSpring;
            cj.angularYLimit = cjTemplate.angularYLimit;
            cj.angularZLimit = cjTemplate.angularZLimit;
            cj.targetPosition = cjTemplate.targetPosition;
            cj.targetVelocity  = cjTemplate.targetVelocity;
            cj.xDrive = cjTemplate.xDrive;
            cj.yDrive = cjTemplate.yDrive;
            cj.zDrive = cjTemplate.zDrive;
            cj.targetRotation = cjTemplate.targetRotation;
            cj.targetAngularVelocity = cjTemplate.targetAngularVelocity;
            cj.rotationDriveMode = cjTemplate.rotationDriveMode;
            cj.angularXDrive = cjTemplate.angularXDrive;
            cj.angularYZDrive = cjTemplate.angularYZDrive;
            cj.slerpDrive = cjTemplate.slerpDrive;
            cj.projectionMode = cjTemplate.projectionMode;
            cj.projectionDistance = cjTemplate.projectionDistance;
            cj.projectionAngle = cjTemplate.projectionAngle;
            cj.configuredInWorldSpace = cjTemplate.configuredInWorldSpace;
            cj.swapBodies = cjTemplate.swapBodies;
            cj.breakForce = cjTemplate.breakForce;
            cj.breakTorque = cjTemplate.breakTorque;
            cj.enableCollision = cjTemplate.enableCollision;
            cj.enablePreprocessing = cjTemplate.enablePreprocessing;
            cj.massScale = cjTemplate.massScale;
            cj.connectedMassScale = cjTemplate.connectedMassScale;
        }

        cjOut = cj;
    }
}

public class ConfiJointControl : MonoBehaviour
{
    [Header("Process")]
    public bool isDebug = false;
    public bool use = false;

    [Header("data list")]
    public List<aCJData> cjdataList = new List<aCJData>();

    public ConfigurableJoint cjTemplate = null;

    [ContextMenu("CopyCJ")]
    public void CopyCJ()
    {
        if (!use) return;

        cjdataList = new List<aCJData>();
        List<ConfigurableJoint> listCJ = GetComponents<ConfigurableJoint>().ToList();
        for (int i = 0; i < listCJ.Count; i++)
        {
            aCJData data = new aCJData();
            data.CopyFromCJoint(listCJ[i]);
            cjdataList.Add(data);
        }
    }

    [ContextMenu("PasteCJ")]
    public void PasteCJ()
    {
        if (!use) return;

        RemoveCJInList();
        ForceRemoveAllCJ();

        for (int i = 0; i < cjdataList.Count; i++)
        {
            aCJData data = cjdataList[i];
            Rigidbody rb = data.rb.GetComponent<Rigidbody>();
            if (rb == null) rb = data.rb.GetComponent<RigidbodyTemplate>().AddReturnRB();
            ConfigurableJoint cj = gameObject.AddComponent<ConfigurableJoint>();
            data.PasteToCJoint(cj, out cj);
            if (this.cjTemplate != null) data.PastePartiallyToCJoint(this.cjTemplate, cj, out cj);
            cjList.Add(cj);
        }
    }

    [Header("cj list")]
    public List<ConfigurableJoint> cjList = new List<ConfigurableJoint>();

    [ContextMenu("Getlist")]
    public void Getlist()
    {
        if (!use) return;

        cjList = GetComponentsInChildren<ConfigurableJoint>().ToList();
    }

    public bool IsRemoveAll()
    {
        return GetComponentsInChildren<ConfigurableJoint>().Length == 0;
    }

    [ContextMenu("DebugDistance")]
    public void DebugDistance()
    {
        for (int i = 0; i < cjList.Count; i++)
        {
            float distance = (cjList[i].connectedBody.transform.position - transform.position).magnitude;
            Debug.Log("to " + cjList[i].connectedBody.gameObject.name + " " + distance);
        }
    }

    [ContextMenu("CopyRemoveAllCJ")]
    public void CopyRemoveAllCJ()
    {
        if (!use) return;

        CopyCJ();
        RemoveCJInList();
        ForceRemoveAllCJ();
    }

    [ContextMenu("RemoveCJInList")]
    public void RemoveCJInList()
    {
        if (!use) return;

        foreach (ConfigurableJoint cj in cjList)
        {
            //DestroyImmediate(cj);
            if (Application.isPlaying) Destroy(cj); else DestroyImmediate(cj);
        }
        cjList = new List<ConfigurableJoint>();
    }

    [ContextMenu("ForceRemoveAllCJ")]
    public void ForceRemoveAllCJ()
    {
        if (!use) return;

        cjList = GetComponents<ConfigurableJoint>().ToList();

        foreach (ConfigurableJoint cj in cjList)
        {
            //DestroyImmediate(cj);
            if (Application.isPlaying) Destroy(cj); else DestroyImmediate(cj);
        }
        cjList = new List<ConfigurableJoint>();
    }

    [ContextMenu("TargetPrevious")]
    public void TargetPrevious()
    {
        int childIndex = transform.GetSiblingIndex();
        if (childIndex == 0) { if (isDebug) Debug.Log("Invalid!"); return; }

        int previousIndex = childIndex - 1;

        ConfigurableJoint cj = gameObject.GetComponents<ConfigurableJoint>().ToList().Find((x) => x.connectedBody.transform == transform.parent.GetChild(previousIndex));
        if (cj == null)
        {
            if (isDebug) Debug.Log("CJ null! Create one at " + this.transform.name);
            cj = gameObject.AddComponent<ConfigurableJoint>();
        }

        cj.connectedBody = transform.parent.GetChild(previousIndex).GetComponent<Rigidbody>();
        if (isDebug) Debug.Log("Done " + this.transform.name + " target previous");
    }
        
    public void TargetThis(Transform tr)
    {
        if (tr == transform) { if (isDebug) Debug.Log("Self transform!"); return; }
        ConfigurableJoint cj = gameObject.GetComponents<ConfigurableJoint>().ToList().Find((x) => x.connectedBody.transform == tr);
        if (cj == null)
        {
            if (isDebug) Debug.Log("CJ null! Create one at " + this.transform.name);
            cj = gameObject.AddComponent<ConfigurableJoint>();
        }
        cj.connectedBody = tr.GetComponent<Rigidbody>();
        cjdataList.Add(new aCJData(tr));
        if (isDebug) Debug.Log("Connect " + transform.name + " and " + tr.name);
    }

    [ContextMenu("TargetParent")]
    public void TargetParent()
    {   
        ConfigurableJoint cj = gameObject.AddComponent<ConfigurableJoint>();
        cj.connectedBody = transform.parent.GetComponent<Rigidbody>();
    }

    [ContextMenu("MiddleBone_GetInput")]
    public void MiddleBone_GetInput()
    {
        if (cjdataList.Count != 2) { Debug.Log("cjdataList.Count != 2"); return; }
        int currentIndex = transform.GetSiblingIndex();
        Transform parent = transform.parent;
        cjdataList[0].rb = parent.GetChild(currentIndex - 1);
        cjdataList[1].rb = parent.GetChild(currentIndex + 1);
    }
}
