﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class Pica : MonoBehaviour
{
    [Header("Process")]
    public bool isDebug = false;

    public bool isLock = true;

    public List<PicaChild> picachildList = new List<PicaChild>();    

    [ContextMenu("CopyTransition")]
    public void CopyTransition()
    {
        foreach (PicaChild child in picachildList)
            child.CopyTransition();
    }

    [ContextMenu("ResetPosition")]
    public void ResetPosition()
    {
        foreach (PicaChild child in picachildList)
            child.ResetPosition();
    }

    [ContextMenu("LoadChild")]
    public void LoadChild()
    {
        picachildList = new List<PicaChild>();
        for (int i = 0; i < transform.childCount; i++)
        {
            PicaChild hj = transform.GetChild(i).GetComponent<PicaChild>();
            if (hj != null)
            {
                picachildList.Add(hj);
            }
        }
    }

    [ContextMenu("StopAllCoroutine")]
    public void StopAllCoroutine()
    {
        StopAllCoroutines();
        for (int i = 0; i < picachildList.Count; i++)
        {
            picachildList[i].StopAllCoroutines();
        }
    }    

    [ContextMenu("Toggle_Collider")]
    public void Toggle_Collider()
    {
        for (int i = 0; i < picachildList.Count; i++)
        {
            picachildList[i].Toggle_Collider();
        }        
    }
    
    public void OnOffThru(bool On)
    {
        foreach (PicaChild child in picachildList) child.OnOffThru(On);
    }
}
