﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class PicaUpMethod : MonoBehaviour
{
    PicaUp _picaUp = null;
    PicaUp picaUp { get { if (_picaUp == null) _picaUp = FindObjectOfType<PicaUp>(); return _picaUp; } }

    [ContextMenu("picaUp_StopAllCoroutines")]
    public void picaUp_StopAllCoroutines()
    {
        picaUp.StopAllCoroutines();
        picaUp.pica.StopAllCoroutine();
    }
}
