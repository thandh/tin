﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkullCollider : MonoBehaviour
{
    public bool isDebug = false;

    [Header("Params Stop Pica")]
    public bool forcePicaStop = true;
    public CollisionDetect detect = null;

    private void Start()
    {
        //skull.Instance.Add_onChange(ForcePicaStop, transform);
    }

    private void OnDestroy()
    {
        //skull.Instance.Remove_onChange(ForcePicaStop, transform);
    }

    void ForcePicaStop()
    {
        if (forcePicaStop)
        {
            picaUp.StopPicaDepend();
        }
    }

    [Header("Params")]
    PicaUp _picaUp = null;
    PicaUp picaUp { get { if (_picaUp == null) _picaUp = FindObjectOfType<PicaUp>(); return _picaUp; } }
        
    //private void OnCollisionEnter(Collision collision)
    //{
    //    Debug.Log("OnCollisionEnter " + collision.gameObject.name);

    //    if (forcePicaStop)
    //    {
    //        PicalHJChild pChild = collision.gameObject.GetComponent<PicalHJChild>();
    //        if (pChild != null)
    //        {
    //            GetComponent<PicaUpMethod>().picaUp_StopAllCoroutines();
    //        }
    //    }
    //}

    //private void OnTriggerEnter(Collider other)
    //{
    //    Debug.Log("OnTriggerEnter " + other.gameObject.name);

    //    if (forcePicaStop)
    //    {
    //        PicalHJChild pChild = other.GetComponent<PicalHJChild>();
    //        if (pChild != null)
    //        {
    //            GetComponent<PicaUpMethod>().picaUp_StopAllCoroutines();
    //        }
    //    }
    //}
}
