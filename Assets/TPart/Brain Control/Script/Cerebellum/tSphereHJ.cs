﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class tSphereHJ : MonoBehaviour
{
    public List<Rigidbody> rgList = new List<Rigidbody>();

    [ContextMenu("Connect")]
    public void Connect()
    {
        List<HingeJoint> hjList = GetComponents<HingeJoint>().ToList();
        foreach (Rigidbody rg in rgList)
        {
            if (hjList.FindIndex((x) => x.connectedBody == rg) != -1) continue;
            HingeJoint hj = gameObject.AddComponent<HingeJoint>();
            hj.connectedBody = rg;
        }
    }

    [ContextMenu("RemoveAll")]
    public void RemoveAll()
    {
        List<HingeJoint> hjList = GetComponents<HingeJoint>().ToList();
        foreach (HingeJoint hj in hjList)
        {
            DestroyImmediate(hj);
        }
    }
}
