﻿using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

#if UNITY_EDITOR
[ExecuteInEditMode]
#endif
public class tCerebellumSphereManual : MonoBehaviour
{
    bool doneOff = false;
    bool doneOn = false;

    [SerializeField] bool _isSelected = false;
    bool isSelected
    {
        get { return _isSelected; }
        set
        {
            _isSelected = value;
            spheres.gameObject.SetActive(_isSelected);
        }
    }

    Transform _spheres = null;
    public Transform spheres
    {
        get
        {
            if (_spheres == null) _spheres = transform.Find("spheres");
            if (_spheres == null) { _spheres = Instantiate(new GameObject("spheres"), transform).transform; _spheres.name = "spheres"; }
            return _spheres;
        }
    }

    private void Update()
    {
#if UNITY_EDITOR
        if (Application.isPlaying) return;

        if (Selection.activeGameObject != this.gameObject)
        {
            if (!doneOff)
            {
                doOff();
                doneOn = false;
            }
            return;
        }
        else
        {
            if (!doneOn)
            {
                doOn();
                doneOff = false;
            }
            return;
        }
#endif
    }

    void doOff()
    {
        isSelected = false;
        doneOff = true;
    }

    void doOn()
    {
        isSelected = true;
        doneOn = true;
    }
}
