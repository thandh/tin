﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class sphereDataHJ : MonoBehaviour
{
    public HingeJoint hj = null;

    [ContextMenu("Remove")]
    public void Remove()
    {
        tCerebellumSphereConnect sphereConnect = transform.parent.parent.GetComponent<tCerebellumSphereConnect>();        
        int index = sphereConnect.listHJ.FindIndex((x) => x == hj);
        if (index == -1) { Debug.Log("index -1"); return; }

        sphereConnect.listHJ.RemoveAt(index);
        DestroyImmediate(hj);
        Invoke("Destroy", 0.3f);
    }

    void Destroy()
    {
        DestroyImmediate(this.gameObject);
    }

    [ContextMenu("CenterYAnchor")]
    public void CenterYAnchor()
    {
        hj.anchor = new Vector3(0, (hj.transform.position - hj.connectedBody.transform.position).magnitude / 2f, 0);
    }
}