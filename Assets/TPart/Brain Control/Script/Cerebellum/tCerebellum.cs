﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tCerebellum : Singleton<tCerebellum>
{
    public enum useWhat
    {
        bone100,
        bone3k,
        deform
    }

    public tCerebellum.useWhat _useWhat = useWhat.deform;

    MeshDeformer _md = null;
    public MeshDeformer md { get { if (_md == null) _md = currentMesh.GetComponent<MeshDeformer>(); return _md; } }

    MeshRenderer _mr = null;
    public MeshRenderer mr { get { if (_mr == null) _mr = currentMesh.GetComponent<MeshRenderer>(); return _mr; } }

    public Transform origin = null;

    public Transform useBrain = null;    
    public Transform currentMesh = null;

    private void Start()
    {
        SetBrain();
    }

    public void SetBrain()
    {
        switch (_useWhat)
        {
            case useWhat.bone100: break;
            case useWhat.bone3k: break;
            case useWhat.deform: break;
        }
    }
}
