﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FJTemplate : MonoBehaviour
{
    public FixedJoint fj = null;
    public Rigidbody connectedBody = null;

    [ContextMenu("Copy")]
    public void Copy()
    {
        this.connectedBody = fj.connectedBody;
    }

    [ContextMenu("Paste")]
    public void Paste()
    {
        fj.connectedBody = this.connectedBody;
    }
}
