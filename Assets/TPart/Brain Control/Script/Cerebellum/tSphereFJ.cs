﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class tSphereFJ : MonoBehaviour
{
    public List<Rigidbody> rgList = new List<Rigidbody>();

    public Vector3 originPosition = -Vector3.one;
    public Quaternion originRotation = Quaternion.identity;

    public Vector3 Position { get { return transform.position; } set { transform.position = value; } }
    public Quaternion Rotation { get { return transform.rotation; } set { transform.rotation = value; } }

    tCerebellumUp _tcerebellumUp = null;
    tCerebellumUp tcerebellumUp { get { if (_tcerebellumUp == null) _tcerebellumUp = FindObjectOfType<tCerebellumUp>(); return _tcerebellumUp; } }

    [ContextMenu("GetOrigin")]
    public void GetOrigin()
    {
        originPosition = Position;
        originRotation = Rotation;
    }

    [ContextMenu("Connect")]
    public void Connect()
    {
        List<FixedJoint> fjList = GetComponents<FixedJoint>().ToList();
        foreach (Rigidbody rg in rgList)
        {
            if (fjList.FindIndex((x) => x.connectedBody == rg) != -1) continue;
            FixedJoint fj = gameObject.AddComponent<FixedJoint>();
            fj.connectedBody = rg;
        }
    }

    [ContextMenu("RemoveAllFJ")]
    public void RemoveAllFJ()
    {
        List<FixedJoint> fjList = GetComponents<FixedJoint>().ToList();
        foreach (FixedJoint fj in fjList)
        {
            DestroyImmediate(fj);
        }
    }

    [ContextMenu("RemoveRB")]
    public void RemoveRB()
    {
        DestroyImmediate(GetComponent<Rigidbody>());
    }

    [ContextMenu("FreezeRB")]
    public void FreezeRB()
    {
        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
    }

    [ContextMenu("RemoveAllFJTemplate")]
    public void RemoveAllFJTemplate()
    {
        List<FJTemplate> fjtemplateList = GetComponents<FJTemplate>().ToList();
        for (int i = 0; i < fjtemplateList.Count; i++)
        {
            DestroyImmediate(fjtemplateList[i]);
        }
    }

    [ContextMenu("CreateFJTemplate")]
    public void CreateFJTemplate()
    {
        List<FJTemplate> fjtemplateList = GetComponents<FJTemplate>().ToList();
        List<FixedJoint> fjList = GetComponents<FixedJoint>().ToList();

        foreach (FixedJoint fj in fjList)
        {
            int index = fjtemplateList.FindIndex((x) => x == fj);
            FJTemplate fjtemplate = null;
            if (index != -1)
            {
                fjtemplate = fjtemplateList[index];
            }
            else
            {
                fjtemplate = gameObject.AddComponent<FJTemplate>();
                fjtemplateList.Add(fjtemplate);
            }
            fjtemplate.fj = fj;
            fjtemplate.Copy();
        }
    }

    [ContextMenu("ResurrectFJ")]
    public void ResurrectFJ()
    {
        List<FJTemplate> fjtemplateList = GetComponents<FJTemplate>().ToList();
        foreach (FJTemplate fjt in fjtemplateList)
        {
            if (fjt.fj == null)
            {
                fjt.fj = gameObject.AddComponent<FixedJoint>();
                fjt.Paste();
            }
        }
    }

    public void SyncTo(Vector3 pos, Quaternion rot)
    {
        StartCoroutine(C_SyncPos(pos));
        StartCoroutine(C_SyncRot(rot));
    }

    IEnumerator C_SyncPos(Vector3 pos)
    {
        while ((Position - pos).magnitude >= tcerebellumUp.deltaCheckPos)
        {
            float step = Time.deltaTime * tcerebellumUp.stepPos;
            Position = new Vector3(Mathf.Lerp(Position.x, pos.x, step), Mathf.Lerp(Position.y, pos.y, step), Mathf.Lerp(Position.z, pos.z, step));
            yield return new WaitForEndOfFrame();
        }

        Position = pos;
        Debug.Log("Done C_SyncPos");
        yield break;
    }

    IEnumerator C_SyncRot(Quaternion rot)
    {
        //while ((Rotation.eulerAngles - rot.eulerAngles).magnitude >= picaUp.deltaCheckRot)
        //{
        //    float step = picaUp.stepRot;
        //    Rotation.SetEulerAngles(new Vector3(Mathf.Lerp(Rotation.eulerAngles.x, rot.eulerAngles.x, step), Mathf.Lerp(Rotation.eulerAngles.y, rot.eulerAngles.y, step), Mathf.Lerp(Rotation.eulerAngles.z, rot.eulerAngles.z, step)));
        //    yield return new WaitForEndOfFrame();
        //}

        while (Rotation != rot)
        {
            float step = Time.deltaTime * tcerebellumUp.stepRot;
            Rotation = new Quaternion(Mathf.Lerp(Rotation.x, rot.x, step), Mathf.Lerp(Rotation.y, rot.y, step), Mathf.Lerp(Rotation.z, rot.z, step), Mathf.Lerp(Rotation.w, rot.w, step));
            if (transform.name == "1") Debug.Log((Rotation.eulerAngles - rot.eulerAngles).magnitude);
            yield return new WaitForEndOfFrame();
        }

        Rotation = rot;
        Debug.Log("Done C_SyncRot");
        yield break;
    }
}
