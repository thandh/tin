﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tCerebellumSphereConnect : MonoBehaviour
{
    tCerebellumSphereManual _manual = null;
    tCerebellumSphereManual manual { get { if (_manual == null) _manual = GetComponent<tCerebellumSphereManual>(); return _manual; } }

    public List<HingeJoint> listHJ = new List<HingeJoint>();
    public Transform connectTo = null;
    public int removeAt = -1;

    [SerializeField] GameObject spherePreb = null;

    [ContextMenu("Connect2Way")]
    public void Connect2Way()
    {
        if (connectTo == null) return;
        HingeJoint hj1 = gameObject.AddComponent<HingeJoint>();
        hj1.connectedBody = connectTo.GetComponent<Rigidbody>();

        HingeJoint hj2 = connectTo.gameObject.AddComponent<HingeJoint>();
        hj2.connectedBody = GetComponent<Rigidbody>();

        listHJ.Add(hj2);
        sphereDataHJ sData2 = Instantiate(spherePreb, manual.spheres).GetComponent<sphereDataHJ>();
        sData2.hj = hj2;
        sData2.transform.position = connectTo.transform.position;            

        connectTo.GetComponent<tCerebellumSphereConnect>().listHJ.Add(hj1);
        sphereDataHJ sData1 = Instantiate(spherePreb, connectTo.GetComponent<tCerebellumSphereManual>().spheres).GetComponent<sphereDataHJ>();
        sData1.hj = hj1;
        sData1.transform.position = transform.position;
    }

    [ContextMenu("Connect1Way")]
    public void Connect1Way()
    {
        if (connectTo == null) return;
        HingeJoint hj = gameObject.AddComponent<HingeJoint>();
        hj.connectedBody = connectTo.GetComponent<Rigidbody>();

        listHJ.Add(hj);
        sphereDataHJ sData = Instantiate(spherePreb, manual.spheres).GetComponent<sphereDataHJ>();
        sData.hj = hj;
        sData.transform.position = connectTo.transform.position;
    }
}
