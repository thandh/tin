﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public delegate void AfterCerebellumGo();

public class tCerebellumUp : Singleton<tCerebellumUp>
{
    public bool IsGoOrigin = false;
    public int OriginPosNotDone = -1;
    public int OriginRotNotDone = -1;

    public float stepPos = 1f;
    public float stepRot = 30f;

    public float deltaCheckPos = 0.01f;
    public float deltaCheckRot = 1f;

    public List<tSphereFJ> tsphereFJList = new List<tSphereFJ>();

    public AfterCerebellumGo afterCerebellumGo = null;

    [ContextMenu("LoadtSphereFJList")]
    public void LoadtSphereFJList()
    {
        tsphereFJList = GetComponentsInChildren<tSphereFJ>().ToList();
    }

    public void DeleteAllFJ()
    {        
        for (int i = 0; i < tsphereFJList.Count; i++)
        {
            tsphereFJList[i].RemoveAllFJ();            
        }
    }

    public void FreezeAllRB()
    {
        for (int i = 0; i < tsphereFJList.Count; i++)
        {
            tsphereFJList[i].FreezeRB();
        }
    }

    public void ResetAllHJnRB()
    {        
        for (int i = 0; i < tsphereFJList.Count; i++)
        {
            tsphereFJList[i].GetComponent<RigidbodyTemplate>().Paste();
            tsphereFJList[i].ResurrectFJ();
        }
    }

    [ContextMenu("Go Origin")]
    public void GoOrigin()
    {
        StopAllCoroutines();        

        DeleteAllFJ();
        FreezeAllRB();

        //if (beforePicaGo != null) beforePicaGo.Invoke();

        if (IsGoOrigin)
        {
            Debug.Log("Chua chay xong cai cu");            
            ResetAllHJnRB();
                        
            IsGoOrigin = false;

            if (afterCerebellumGo != null) afterCerebellumGo.Invoke();

            return;
        }

        StartCoroutine(C_GoOrigin());
    }

    IEnumerator C_GoOrigin()
    {
        IsGoOrigin = true;
        OriginPosNotDone = -1;
        OriginRotNotDone = -1;        

        for (int i = 0; i < tsphereFJList.Count; i++)
        {
            tsphereFJList[i].SyncTo(tsphereFJList[i].originPosition, tsphereFJList[i].originRotation);
        }

        yield return new WaitUntil(() => CheckIsPosAllUp() == true && CheckIsRotAllUp() == true);

        ResetAllHJnRB();

        if (afterCerebellumGo != null) afterCerebellumGo.Invoke();

        IsGoOrigin = false;
        yield break;
    }

    bool CheckIsPosAllUp()
    {
        for (int i = 0; i < tsphereFJList.Count; i++)
        {
            if ((tsphereFJList[i].Position - tsphereFJList[i].originPosition).magnitude >= deltaCheckPos)
            {
                OriginPosNotDone = i;
                return false;
            }
        }

        return true;
    }

    bool CheckIsRotAllUp()
    {
        for (int i = 0; i < tsphereFJList.Count; i++)
        {
            if ((tsphereFJList[i].Rotation.eulerAngles - tsphereFJList[i].originRotation.eulerAngles).magnitude >= deltaCheckRot)
            {
                OriginRotNotDone = i;
                return false;
            }
        }

        return true;
    }
}
