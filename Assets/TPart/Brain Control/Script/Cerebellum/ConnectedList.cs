﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ConnectedList : MonoBehaviour
{
    public List<Rigidbody> rgList = new List<Rigidbody>();

    public float radius = 1f;
    public string tagFind = "tSphereCerebellum";

    [ContextMenu("Find")]
    public void Find()
    {
        this.rgList = new List<Rigidbody>();

        List<Rigidbody> rgList = FindObjectsOfType<Rigidbody>().ToList();
        int index = rgList.FindIndex((x) => x.tag != tagFind);
        while (index != -1)
        {
            rgList.RemoveAt(index);
            index = rgList.FindIndex((x) => x.tag != tagFind);
        }

        index = 0;
        while (index < rgList.Count)
        {
            index++;
            if ((rgList[index - 1].transform.position - transform.position).magnitude > radius
                || (rgList[index - 1].transform.position - transform.position).magnitude == 0)
            {                
                rgList.RemoveAt(index - 1);
                index--;
            }            
        }

        this.rgList = rgList;
    }

    [ContextMenu("SyncToAll")]
    public void SyncToAll()
    {
        tSphereHJ tsphereHJ = GetComponent<tSphereHJ>();
        if (tsphereHJ != null) tsphereHJ.rgList = this.rgList;

        tSphereFJ tsphereFJ = GetComponent<tSphereFJ>();
        if (tsphereFJ != null) tsphereFJ.rgList = this.rgList;
    }
}
