﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindVertCloseInfo : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;    

    [Header("Inputs")]
    public FindVertClose findVert = null;

    [Header("Jump")]
    public int indexJump = 0;

    [ContextMenu("JumpToIndex")]
    public void JumpToIndex()
    {
        transform.position = findVert.transform.TransformPoint(findVert.m.vertices[indexJump]);
    }
}
