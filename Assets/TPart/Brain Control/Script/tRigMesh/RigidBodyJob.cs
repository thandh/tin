﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RigidBodyJob : MonoBehaviour
{
    [Header("Rigid")]
    [SerializeField] Rigidbody _rb = null;
    public Rigidbody rb {
        get
        {
            if (_rb == null) _rb = GetComponent<Rigidbody>();
            if (_rb == null) _rb = gameObject.AddComponent<Rigidbody>();
            return _rb;
        }
        set { _rb = value; }
    }

    public void SyncRBTemplate(RigidbodyTemplate tbTemplate)
    {
        this.rb.mass = tbTemplate.mass;
        this.rb.drag = tbTemplate.drag;
        this.rb.angularDrag = tbTemplate.angulardrag;
        this.rb.useGravity = tbTemplate.useGravity;
        this.rb.isKinematic = tbTemplate.isKinematic;
        this.rb.constraints = tbTemplate.constrain;
    }

    [ContextMenu("DeleteRB")]
    public void DeleteRB()
    {
        if (rb != null)
            if (Application.isPlaying) Destroy(rb); else DestroyImmediate(rb);
    }

    [ContextMenu("RestoreRB")]
    public void RestoreRB()
    {        
        if (rb == null) rb = gameObject.AddComponent<Rigidbody>();        
    }
}
