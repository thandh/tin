﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class tRig_autoCreate : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = true;

    [Header("Clickable")]
    public bool ClickHere = true;

    [ContextMenu("SetisDebug")]
    public void SetisDebug()
    {
        dknodes.isDebug = this.isDebug;
        combineNodes.isDebug = this.isDebug;
        distribute.isDebug = this.isDebug;
        findConnected.isDebug = this.isDebug;
        refresh.isDebug = this.isDebug;
        findClose.isDebug = this.isDebug;
        foreach (dkNode node in dknodes.nodeList)
        {
            node.sprintJob.isDebug = this.isDebug;
        }
    }

    [Header("tRig")]
    public tRigMesh tRig = null;
    public dkNodes dknodes = null;

    public FindVertClose findClose = null;
    public TINDefineTrClose defineClose = null;
    public dkNodes_CombineNodesClose combineNodes = null;
    public dkNodes_CleanSJJ cleanSJJ = null;
    public dkNodes_FindConnected findConnected = null;
    public dkNodes_DistributeByFindVertClose distribute = null;
    public dkNodes_Refresh refresh = null;

    [Header("FullAuto")]

    public bool autoAtStart = false;
    public bool doneAuto = true;
    public bool waitCouroutine = false;

    public AutoXPanelMethod autoXPanelMethod = null;

    [ContextMenu("SetWaitCouroutine")]
    public void SetWaitCouroutine()
    {
        dknodes.waitCouroutine = this.waitCouroutine;
        combineNodes.waitCouroutine = this.waitCouroutine;
        distribute.waitCouroutine = this.waitCouroutine;
        findConnected.waitCouroutine = this.waitCouroutine;
        refresh.waitCouroutine = this.waitCouroutine;
        findClose.waitCouroutine = this.waitCouroutine;
        foreach (dkNode node in dknodes.nodeList)
        {
            node.sprintJob.waitCouroutine = this.waitCouroutine;
        }
    }

    [Header("FullAuto Jobs")]
    public bool bool_DeleteAll = false;
    public bool bool_findVert = false;
    public bool bool_Split = false;
    public bool bool_ReCreateNodes = true;
    public bool bool_Distribute = true;
    public bool bool_GetOriginal = true;
    public bool bool_FindConnected = true;
    public bool bool_ConnectNodes = true;
    public bool bool_SyncRBTemplate = true;
    public bool bool_SyncSJTemplate = true;
    public bool bool_ShowShape = true;
    public bool bool_GroupClose = true;
    public bool bool_StoreAllSJJ = true;

    private void Start()
    {
        if (autoAtStart) FullAuto();
    }

    public bool doneOnceRuntime = false;
    public string sceneDoneRuntime = string.Empty;

    [ContextMenu("RuntimeAuto")]
    public void RuntimeAuto()
    {
        StartCoroutine(C_RuntimeAuto());
    }

    IEnumerator C_RuntimeAuto()
    {
        float time = Time.time;

        autoXPanelMethod.SayAndClear("Start Full Auto Rig Brain in 1 second");
        autoXPanelMethod.overlayOn();

        yield return new WaitForSeconds(1f);
        if (isDebug) Debug.Log("Done wait 1 second!");

        FullAuto();
        yield return new WaitUntil(() => doneAuto == true);

        AfterFullAuto(Time.time - time);

        doneOnceRuntime = true;
        sceneDoneRuntime = SceneManager.GetActiveScene().name;

        yield break;
    }

    [ContextMenu("FullAuto")]
    public void FullAuto()
    {
        StartCoroutine(C_FullAuto());
    }

    IEnumerator C_FullAuto()
    {
        tRig.updateFromNodes = false;

        if (isDebug) if (Application.isPlaying) Debug.Log("Start Full Auto Rig Brain in 1 second");

        float time = Time.time;

        doneAuto = false;        

        findClose.ResetLocalMesh();        
        tRig.meshMethods.ResetLocalMesh();

        if (bool_DeleteAll)
        {
            if (isDebug) Debug.Log("Start dknodes.DeleteAll()");
            dknodes.DeleteAll();
            if (isDebug) Debug.Log("Done dknodes.DeleteAll()");
        }

        if (bool_findVert)
        {
            if (isDebug) Debug.Log("Start findClose.Find()");            
            findClose.Find();
            if (waitCouroutine) if (Application.isPlaying) yield return new WaitUntil(() => findClose.doneCFind == true);
            if (isDebug) Debug.Log("Done findClose.Find()");
        }

        if (bool_ReCreateNodes)
        {
            if (isDebug) Debug.Log("Start ReCreateNodes()");
            ReCreateNodes();
            if (waitCouroutine) if (Application.isPlaying) yield return new WaitUntil(() => doneCCreateNodes == true);
            if (isDebug) Debug.Log("Done ReCreateNodes()");
        }

        if (bool_Distribute)
        {
            if (isDebug) Debug.Log("Start Distribute!!");
            Distribute();
            if (waitCouroutine) if (Application.isPlaying) yield return new WaitUntil(() => doneCDistribute == true);
            if (isDebug) Debug.Log("Done Distribute!!");
        }

        if (bool_GetOriginal)
        {
            if (isDebug) Debug.Log("Start RefreshJobGetOriginal()");
            RefreshJobGetOriginal();
            if (waitCouroutine) if (Application.isPlaying) yield return new WaitUntil(() => doneCRefreshJobGetOriginal == true);
            if (isDebug) Debug.Log("RefreshJobGetOriginal()");
        }

        if (bool_FindConnected)
        {
            if (isDebug) Debug.Log("Start findConnected.FindConnected()");
            findConnected.FindConnected();
            if (isDebug) Debug.Log("Done findConnected.FindConnected()");
        }

        if (bool_SyncRBTemplate)
        {
            time = Time.time;
            SyncRBTemplate();
            if (isDebug) Debug.Log("Done Sync RB Template in : " + (Time.time - time));
        }

        if (bool_SyncSJTemplate)
        {
            time = Time.time;
            SyncSJTemplate();
            if (isDebug) Debug.Log("Done Sync SJ Template in : " + (Time.time - time));
        }

        if (bool_ShowShape)
        {
            time = Time.time;
            ShowShape();
            if (isDebug) Debug.Log("Done ShowShap in : " + (Time.time - time));
        }

        if (bool_ConnectNodes)
        {
            if (isDebug) Debug.Log("Start Connnect Nodes!!");
            ConnectNodes();
            if (waitCouroutine) if (Application.isPlaying) yield return new WaitUntil(() => doneCConnectNodes == true);
            if (isDebug) Debug.Log("Done Start Connnect Nodes!!");
        }

        if (bool_GroupClose)
        {
            if (isDebug) Debug.Log("Start Group At!!");
            combineNodes.GroupClose();
            if (waitCouroutine) if (Application.isPlaying) yield return new WaitUntil(() => combineNodes.doneGroupClose == true);
            if (isDebug) Debug.Log("Done Group At!!");
        }

        if (bool_StoreAllSJJ)
        {
            time = Time.time;
            dknodes.StoreAllSJJ();
            if (isDebug) Debug.Log("Done Store ALL SJJ in : " + (Time.time - time));
        }

        doneAuto = true;

        //tRig.updateFromNodes = true;

        yield break;
    }

    void AfterFullAuto(float timeAuto = 0)
    {
        if (isDebug) Debug.Log("done auto in " + timeAuto);
        autoXPanelMethod.overlayOff();
        autoXPanelMethod.SayAndClear("done Auto Brain in " + timeAuto);
    }    

    public enum dkCreateType { fromScratch, fromPrefab }

    [Header("ReCreateNodes")]
    public dkCreateType createType = dkCreateType.fromScratch;
    public bool doneCCreateNodes = true;
    public bool forceReCreate = true;
    public GameObject dknodesPrefab = null;

    [ContextMenu("ReCreateNodes")]
    public void ReCreateNodes()
    {
        StartCoroutine(C_CreateNodes());
    }

    IEnumerator C_CreateNodes()
    {
        float time = Time.time;

        doneCCreateNodes = false;

        switch (createType)
        {
            case dkCreateType.fromScratch:

                dknodes.DeleteAll();
                dknodes.Recreate();
                if (waitCouroutine) yield return new WaitUntil(() => dknodes.nodeList.Count == findClose.vertCollectCount);

                break;

            case dkCreateType.fromPrefab:
                if (dknodes == null)
                {
                    CreateDKnodes();
                }
                else
                {
                    if (forceReCreate)
                    {
                        if (Application.isPlaying) Destroy(dknodes.gameObject); else DestroyImmediate(dknodes.gameObject);
                        CreateDKnodes();
                    }
                    else
                    {
                        if (dknodes.nodeList.Count != this.findClose.vertCollectCount)
                        {
                            if (isDebug) Debug.Log("Khac number collect nen phai tao lai dknodes!!");
                            CreateDKnodes();
                        }
                    }
                }

                break;
        }

        doneCCreateNodes = true;

        if (isDebug) Debug.Log("Done recreate nodes in : " + (Time.time - time));

        yield break;
    }

    void CreateDKnodes()
    {
        GameObject dknodesGO = Instantiate(dknodesPrefab, transform);
        dknodesGO.name = "dkNodes";
        dknodesGO.transform.SetAsFirstSibling();

        this.dknodes = dknodesGO.GetComponent<dkNodes>();
        tRig.dknodes = dknodesGO.GetComponent<dkNodes>();
        this.dknodes.numberCreate = findClose.vertCollectCount;
        this.dknodes.nodePrefab = tRig.newNode;

        this.distribute = dknodesGO.GetComponent<dkNodes_DistributeByFindVertClose>();
        this.distribute.findVert = findClose;

        this.findConnected = dknodesGO.GetComponent<dkNodes_FindConnected>();
        this.findConnected.mm = tRig.meshMethods;
        this.findConnected.findVert = findClose;

        this.combineNodes = dknodesGO.GetComponent<dkNodes_CombineNodesClose>();
        this.cleanSJJ = dknodesGO.GetComponent<dkNodes_CleanSJJ>();

        this.refresh = dknodesGO.GetComponent<dkNodes_Refresh>();
        this.refresh.rbTemplate = tRig.rbTemplate;
        this.refresh.sjTemplate = tRig.sprintTemplate;
    }

    [Header("DistributeAndGroup")]
    public bool doneDistributeAndGroup = true;

    [ContextMenu("DistributeAndGroup")]
    public void DistributeAndGroup()
    {
        StartCoroutine(C_DistributeAndGroup());
    }

    IEnumerator C_DistributeAndGroup()
    {
        float time = Time.time;

        doneCDistribute = false;

        List<dkNode> nodeList = dknodes.nodeList;
        List<int> verticesCloseID = findClose.verticesCloseID;

        int i = 0;
        while (i < nodeList.Count)
        {
            nodeList[i].idJob.IDInMesh = verticesCloseID[i];
            nodeList[i].InitPosition();
            dknodes.AddAnReverseID(nodeList[i].idJob.IDInMesh, i);

            combineNodes.GroupAt(nodeList[i]);

            i++;
        }

        if (waitCouroutine) yield return new WaitUntil(() => i == nodeList.Count);

        doneCDistribute = true;
        if (isDebug) Debug.Log("Done distribute nodes in : " + (Time.time - time));

        yield break;
    }

    [Header("Distribute")]
    public bool doneCDistribute = true;

    [ContextMenu("Distribute")]
    public void Distribute()
    {
        StartCoroutine(C_Distribute());
    }

    IEnumerator C_Distribute()
    {
        float time = Time.time;

        doneCDistribute = false;

        dkNodes_DistributeByFindVertClose distripute = dknodes.GetComponent<dkNodes_DistributeByFindVertClose>();
        distripute.Distribute();
        if (waitCouroutine) yield return new WaitUntil(() => distribute.doneCDistribute == true);

        doneCDistribute = true;
        if (isDebug) Debug.Log("Done distribute nodes in : " + (Time.time - time));

        yield break;
    }

    [Header("RefreshJobGetOriginal")]
    public bool doneCRefreshJobGetOriginal = true;

    [ContextMenu("RefreshJobGetOriginal")]
    public void RefreshJobGetOriginal()
    {
        StartCoroutine(C_RefreshJobGetOriginal());
    }

    IEnumerator C_RefreshJobGetOriginal()
    {
        doneCDistribute = false;
        float time = Time.time;

        refresh.RefreshJobGetOriginal();

        doneCDistribute = true;
        if (isDebug) Debug.Log("Done distribute nodes in : " + (Time.time - time));

        yield break;
    }

    [Header("ConnectNodes")]
    public bool doneCConnectNodes = true;

    [ContextMenu("ConnectNodes")]
    public void ConnectNodes()
    {
        StartCoroutine(C_ConnectNodes());
    }

    IEnumerator C_ConnectNodes()
    {
        doneCConnectNodes = false;

        float time = Time.time;

        foreach (dkNode node in dknodes.nodeList)
        {
            node.deformJob.AddID(node.idJob.IDInMesh);
            //Bỏ vì đã refactor trong dknodes_FindConnected
            //node.sprintFindJOb.FindConnected();
            node.sprintJob.GenerateSJList();
        }

        doneCConnectNodes = true;
        if (isDebug) Debug.Log("Done connect nodes in : " + (Time.time - time));

        yield break;
    }

    [ContextMenu("SyncSJTemplate")]
    public void SyncSJTemplate()
    {
        SprintJointTemplate sjTemplate = tRig.sprintTemplate;

        foreach (dkNode node in dknodes.nodeList)
        {
            node.sprintJob.SyncSJTemplate(sjTemplate);
        }
    }

    [ContextMenu("SyncRBTemplate")]
    public void SyncRBTemplate()
    {
        RigidbodyTemplate rbJob = tRig.rbTemplate;

        foreach (dkNode node in dknodes.nodeList)
        {
            node.rbJob.SyncRBTemplate(rbJob);
        }
    }

    [Header("Show Shape")]
    public bool showShape = false;

    [ContextMenu("ShowShape")]
    public void ShowShape()
    {
        foreach (dkNode node in dknodes.nodeList)
        {
            node.shapeJob.showShape = showShape;
            node.shapeJob.ShowShape();
        }
    }
}
