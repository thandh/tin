﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ShapeJob : MonoBehaviour
{
    [Header("Scale")]
    [SerializeField] Vector3 _lscale = Vector3.one;
    public Vector3 lscale { get { return _lscale; } set { _lscale = value; HandleSetScale(); } }

    void HandleSetScale()
    {
        transform.localScale = _lscale;
    }

    [Header("Show Renderer")]
    MeshRenderer _mr = null;
    public MeshRenderer mr { get { if (_mr == null) _mr = GetComponent<MeshRenderer>(); return _mr; } }
        
    SkinnedMeshRenderer _smr = null;
    public SkinnedMeshRenderer smr { get { if (_smr == null) _smr = GetComponent<SkinnedMeshRenderer>(); return _smr; } }

    Material _matRuntime = null;
    Material _matEditor = null;
    public Material mat
    {
        get
        {
            if (Application.isPlaying && _matRuntime == null)
            {
                if (mr != null) _matRuntime = mr.material;
                else if (smr != null) _matRuntime = smr.material;
            }
            else if (!Application.isPlaying && _matEditor == null)
            {
                if (mr != null) _matEditor = mr.sharedMaterial;
                else if (smr != null) _matEditor = smr.sharedMaterial;
            }

            if (Application.isPlaying) return _matRuntime; else return _matEditor;
        }

        set
        {
            if (Application.isPlaying)
            {
                _matRuntime = value;
                if (mr != null) mr.material = _matRuntime;
                else if (smr != null) smr.material = _matRuntime;
            }
            else
            {
                _matEditor = value;
                if (mr != null) mr.sharedMaterial = _matEditor;
                else if (smr != null) smr.sharedMaterial = _matEditor;
            }
        }
    }

    public bool showAtStart = false;
    public bool showShape = true;

    [ContextMenu("ShowShape")]
    public void ShowShape()
    {
        if (mr != null) mr.enabled = showShape;
        else if (smr != null) smr.enabled = showShape;
    }

    [ContextMenu("ToggleShowShape")]
    public void ToggleShowShape()
    {
        showShape = !showShape;
        if (mr != null) mr.enabled = showShape; 
        else if (smr != null)  smr.enabled = showShape; 
    }

    public Material currentMat = null;

    [ContextMenu("UpdateCurrentMat")]
    public void UpdateCurrentMat()
    {
        mat = currentMat;
    }

    [ContextMenu("InitState")]
    public void InitState()
    {
        lscale = transform.localScale;
        if (mr != null) showShape = mr.enabled;
        else if (smr != null) showShape = smr.enabled;
    }

    private void Start()
    {
        InitState();

        if (showAtStart)
        {
            showShape = true;
            ShowShape();
        }
    }    
}
