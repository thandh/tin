﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SprintJointTemplate : MonoBehaviour
{
    public bool AutoConfigureConnectedAnchor = true;
    public float Spring = 1000f;
    public float Damper = 10;
    public float MinDistance = 0;
    public float MaxDistance = 0;
    public float Tolerance = 0.025f;
    public float BreakForce = Mathf.Infinity;
    public float BreakTorque = Mathf.Infinity;
    public bool EnableCollision = false;
    public bool EnablePreprocess = true;
    public float MassScale = 1f;
    public float ConnectedMassScale = 1f;

    public void Paste(SpringJoint sjIn, out SpringJoint sjOut)
    {        
        sjIn.autoConfigureConnectedAnchor = this.AutoConfigureConnectedAnchor;
        sjIn.spring = this.Spring;
        sjIn.damper = this.Damper;
        sjIn.minDistance = this.MinDistance;
        sjIn.maxDistance = this.MaxDistance;
        sjIn.tolerance = this.Tolerance;
        sjIn.breakForce = this.BreakForce;
        sjIn.breakTorque = this.BreakTorque;
        sjIn.enableCollision = this.EnableCollision;
        sjIn.enablePreprocessing = this.EnablePreprocess;
        sjIn.massScale = this.MassScale;
        sjIn.connectedMassScale = this.ConnectedMassScale;
        sjOut = sjIn;
    }
}
