﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SprintJointJob : MonoBehaviour
{
    public bool isDebug = false;
    public bool waitCouroutine = false;

    [Header("Spring Joint")]
    public List<SpringJoint> sjList = new List<SpringJoint>();

    [ContextMenu("Get SJList")]
    public void GetSJList()
    {
        sjList = GetComponents<SpringJoint>().ToList();
    }

    [Header("DeleteAllSJ")]
    public bool doneDeleteAllSJ = true;

    [ContextMenu("Delete All SJ")]
    public void DeleteAllSJ()
    {
        StartCoroutine(C_DeleteAllSJ());
    }

    IEnumerator C_DeleteAllSJ()
    {
        doneDeleteAllSJ = false;

        foreach (SpringJoint sj in sjList)
            if (sj != null)
                if (Application.isPlaying) Destroy(sj); else DestroyImmediate(sj);

        yield return new WaitUntil(() => sjList.FindIndex((x) => x != null) == -1);
        sjList = new List<SpringJoint>();

        doneDeleteAllSJ = true;

        yield break;
    }   

    [Header("Generate")]        
    public SprintJointJob_FindConnected IDConnectList = null;
    public SprintJointTemplate sjTemplate = null;    
    public dkNodes dknodes = null;

    public bool doneCGenerateSJList = true;

    [ContextMenu("GenerateSJList")]
    public void GenerateSJList()
    {
        StartCoroutine(C_GenerateSJList());
    }

    IEnumerator C_GenerateSJList()
    {
        doneCGenerateSJList = false;

        float time = Time.time;

        DeleteAllSJ();
        if (waitCouroutine) yield return new WaitUntil(() => sjList.Count == 0);

        int IDInMesh = IDConnectList.idJob.IDInMesh;
        List<int> connectedID = IDConnectList.connectedID;
        List<dkNode> nodeList = dknodes.nodeList;
        Dictionary<int, int> reverseID = dknodes.reverseID;

        int i = 0;

        for (i = 0; i < connectedID.Count; i++)
        {
            if (connectedID[i] == IDInMesh) continue;
            if (!reverseID.ContainsKey(connectedID[i])) continue;

            AddASJ(nodeList[reverseID[connectedID[i]]].GetComponent<RigidBodyJob>().rb);
        }

        if (waitCouroutine) yield return new WaitUntil(() => i == connectedID.Count);

        doneCGenerateSJList = true;
        if (isDebug) Debug.Log("Done generate SJJ in : " + (Time.time - time));

        yield break;
    }

    public void AddASJ(Rigidbody rb)
    {
        if (rb == null || rb == GetComponent<Rigidbody>()) return;        

        SpringJoint sj = gameObject.AddComponent<SpringJoint>();
        sjTemplate.Paste(sj, out sj);
        sj.connectedBody = rb;
        sjList.Add(sj);
    }

    public void AddSJFromSJJ(SprintJointJob sjj)
    {
        foreach (SpringJoint sj in sjj.sjList)
        {
            AddASJ(sj.connectedBody);
        }
    }

    public void AddSJFromStorage(SprintJointJob_Storage storage)
    {
        foreach (Transform tr in storage.trList)
        {
            AddASJ(tr.GetComponent<Rigidbody>());
        }
    }

    public void SyncSJTemplate(SprintJointTemplate sjTemplate)
    {
        this.sjTemplate = sjTemplate;

        foreach (SpringJoint sj in sjList)
        {
            sj.autoConfigureConnectedAnchor = sjTemplate.AutoConfigureConnectedAnchor;
            sj.spring = sjTemplate.Spring;
            sj.damper = sjTemplate.Damper;
            sj.minDistance = sjTemplate.MinDistance;
            sj.maxDistance = sjTemplate.MaxDistance;
            sj.tolerance = sjTemplate.Tolerance;
            sj.breakForce = sjTemplate.BreakForce;
            sj.breakTorque = sjTemplate.BreakTorque;
            sj.enableCollision = sjTemplate.EnableCollision;
            sj.enablePreprocessing = sjTemplate.EnablePreprocess;
            sj.massScale = sjTemplate.MassScale;
            sj.connectedMassScale = sjTemplate.ConnectedMassScale;            
        }
    }

    [ContextMenu("CleanSJList")]
    public void CleanSJList()
    {
        int i = 0;
        while (i < sjList.Count)
        {
            if (sjList[i].connectedBody == null)
            {
                SpringJoint sj = sjList[i];
                sjList.RemoveAt(i);
                if (Application.isPlaying) Destroy(sj); else DestroyImmediate(sj);
            } else
            {
                i++;
            }
        }
    }
}
