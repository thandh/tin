﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SprintJointJob_FindConnected : MonoBehaviour
{
    public bool isDebug = false;

    Mesh m = null;

    public void InitMesh(Mesh m)
    {
        this.m = m;
    }

    [Header("Self Input")]
    public IDJob idJob = null;
    SprintJointJob sjJob = null;

    [Header("Output")]
    public List<int> connectedID = new List<int>();

    public void ConnectedIDAddJustNew(int ID)
    {
        int index = connectedID.IndexOf(ID);
        if (index == -1) connectedID.Add(ID);
    }

    public Dictionary<int, int> connectedIDDic = new Dictionary<int, int>();

    [ContextMenu("StopCoroutines")]
    public void StopCoroutines()
    {
        StopAllCoroutines();
    }

    [ContextMenu("FindConnected")]
    public void FindConnected()
    {
        int IDInMesh = idJob.IDInMesh;
        int[] triangles = m.triangles;

        float time = Time.time;

        connectedID = new List<int>();

        if (isDebug) Debug.Log("tris Length " + triangles.Length);

        for (int i = 0; i < triangles.Length / 3; i++)
        {
            if (triangles[i * 3] == IDInMesh ||
                triangles[i * 3 + 1] == IDInMesh ||
                triangles[i * 3 + 2] == IDInMesh)
            {
                connectedID.Add(triangles[i * 3]);
                connectedID.Add(triangles[i * 3 + 1]);
                connectedID.Add(triangles[i * 3 + 2]);
            }
        }

        TrimConnectedID();

        if (isDebug) Debug.Log("Done in " + (Time.time - time));
    }

    [ContextMenu("TrimConnectedID")]
    public void TrimConnectedID()
    {
        connectedIDDic = new Dictionary<int, int>();
        foreach (int i in connectedID)        
            if (i != idJob.IDInMesh && !connectedIDDic.ContainsKey(i))
                connectedIDDic.Add(i, i);

        connectedID = new List<int>();
        foreach (KeyValuePair<int, int> pair in connectedIDDic)
            connectedID.Add(pair.Key);
    }
}
