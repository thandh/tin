﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SprintJointJob_GenerateConnected : MonoBehaviour
{
    [Header("Generate")]

    public SprintJointJob sprintJob = null;
    public SprintJointJob_FindConnected findConnected = null;
    public tRigMesh trigMesh = null;    

    [HideInInspector]
    public bool doneGenerateConnected = true;

    [ContextMenu("GenerateConnected")]
    public void GenerateConnected()
    {
        StartCoroutine(C_GenerateConnected());
    }

    IEnumerator C_GenerateConnected()
    {
        doneGenerateConnected = false;

        int IDInMesh = findConnected.idJob.IDInMesh;
        List<int> connectedID = findConnected.connectedID;
        List<dkNode> nodeList = trigMesh.dknodes.nodeList;
        Dictionary<int, int> reverseID = trigMesh.dknodes.reverseID;
        List<SpringJoint> sjList = sprintJob.sjList;

        for (int i = 0; i < connectedID.Count; i++)
        {
            if (connectedID[i] == IDInMesh) continue;
            if (reverseID.ContainsKey(connectedID[i])) continue;

            GameObject newNode = Instantiate(trigMesh.newNode, trigMesh.dknodes.transform);

        }

        doneGenerateConnected = true;

        yield break;
    }
}
