﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SprintJointJob_Storage : MonoBehaviour
{   
    public SprintJointJob sjj = null;

    public List<Transform> trList = new List<Transform>();

    [ContextMenu("Store")]
    public void Store()
    {
        trList = new List<Transform>();
        foreach (SpringJoint sj in sjj.sjList)
        {
            if (sj.connectedBody != null)
                trList.Add(sj.connectedBody.transform);
        }
    }

    [ContextMenu("Distribute")]
    public void Distribute()
    {
        sjj.AddSJFromStorage(this);
    }
}
