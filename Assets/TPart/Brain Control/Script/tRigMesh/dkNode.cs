﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dkNode : MonoBehaviour
{
    public bool isDebug = false;

    private dkNodes _dknodes = null;
    private dkNodes dknodes { get { if (_dknodes == null) _dknodes = GetComponentInParent<dkNodes>(); return _dknodes; } }

    public Vector3 Position { get { return transform.position; } set { transform.position = value; }  }
    public Quaternion Rotation { get { return transform.rotation; } set { transform.rotation = value; } }

    [Header("ID Job")]    
    public IDJob idJob = null;

    public void InitPosition()
    {
        if (idJob.IDInMesh == -1) return;
        Position = dknodes.tRigmesh.meshMethods.VertPosition(idJob.IDInMesh);
    }

    [Header("Sprint Job")]
    public SprintJointJob sprintJob = null;

    [Header("Sprint Job Storage")]
    public SprintJointJob_Storage sprintStore = null;

    [Header("Refresh Job")]
    public RefreshJob refreshJob = null;

    [ContextMenu("Store")]
    public void Store()
    {
        dknodes.RemoveAtNode(this);
    }

    [Header("Sprint Find Connected Job")]
    public SprintJointJob_FindConnected sprintFindJob = null;

    [Header("RigidBody Job")]
    public RigidBodyJob rbJob = null;

    [Header("Shape Job")]
    public ShapeJob shapeJob = null;

    [Header("tRig Deform Job")]
    public dkDeformJob deformJob = null;

    [ContextMenu("SelfRemove")]
    public void SelfRemove()
    {
        dknodes.RemoveAtNode(this);        
    }

    [ContextMenu("Destroy")]
    public void Destroy()
    {
        Destroy(gameObject);
    }

    [ContextMenu("DestroyImmediate")]
    public void DestroyImmediate()
    {
        DestroyImmediate(gameObject);
    }

    [ContextMenu("SelfGroup")]
    public void SelfGroup()
    {
        dknodes.GetComponent<dkNodes_CombineNodesClose>().GroupAt(this);
    }
}
