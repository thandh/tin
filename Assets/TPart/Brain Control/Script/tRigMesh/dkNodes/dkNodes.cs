﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class dkNodes : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;
    public bool isDebugTime = false;
    public bool isDebugNode = false;

    [Header("Wait Couroutine")]
    public bool waitCouroutine = true;

    private tRigMesh _trigmesh = null;
    public tRigMesh tRigmesh { get { if (_trigmesh == null) _trigmesh = GetComponentInParent<tRigMesh>(); return _trigmesh; } }

    [Header("List Control")]
    public List<dkNode> nodeList = new List<dkNode>();

    [ContextMenu("Get dkNode")]
    public void GetdkNode()
    {
        nodeList = GetComponentsInChildren<dkNode>().ToList();
    }

    [SerializeField]
    public IntIntDictionary reverseID = new IntIntDictionary();

    [ContextMenu("GetReverseID")]
    public void GetReverseID()
    {
        reverseID = new IntIntDictionary();
        for (int i = 0; i < nodeList.Count; i++)
        {
            AddAnReverseID(nodeList[i].idJob.IDInMesh, i);
        }
    }

    [ContextMenu("AddAnReverseID")]
    public void AddAnReverseID(int IDInMesh, int i)
    {
        if (reverseID.ContainsKey(nodeList[i].idJob.IDInMesh)) return;
        reverseID.Add(nodeList[i].idJob.IDInMesh, i);
    }

    [ContextMenu("Delete All")]
    public void DeleteAll()
    {
        foreach (dkNode node in nodeList)
            if (Application.isPlaying) Destroy(node.gameObject); else DestroyImmediate(node.gameObject);

        nodeList = new List<dkNode>();
        reverseID = new IntIntDictionary();

        List<Transform> trList = GetComponentsInChildren<Transform>().ToList();
        foreach (Transform tr in trList)
        {
            if (tr == this.transform) continue;
            if (Application.isPlaying) Destroy(tr.gameObject); else DestroyImmediate(tr.gameObject);
        }
    }

    [Header("Create")]
    public int numberCreate = 500;
    public GameObject nodePrefab = null;

    public bool doneCCreate = true;

    [ContextMenu("Create")]
    public void Recreate()
    {
        StartCoroutine(C_ReCreate());
    }

    IEnumerator C_ReCreate()
    {
        float time = Time.time;

        doneCCreate = false;

        nodeList = new List<dkNode>();

        int i = 0;
        for (i = 0; i < numberCreate; i++)
        {
            GameObject go = Instantiate(nodePrefab, this.transform);

            dkNode dknode = go.GetComponent<dkNode>();
            IDJob idJob = go.GetComponent<IDJob>();
            SprintJointJob sprintJob = go.GetComponent<SprintJointJob>();
            SprintJointJob_FindConnected connected = go.GetComponent<SprintJointJob_FindConnected>();
            SprintJointJob_Storage storage = go.GetComponent<SprintJointJob_Storage>();
            RefreshJob refreshJob = go.GetComponent<RefreshJob>();

            dknode.idJob = idJob;

            dknode.sprintJob = sprintJob;
            dknode.sprintJob.waitCouroutine = this.waitCouroutine;
            dknode.sprintJob.isDebug = this.isDebug;

            dknode.sprintFindJob = connected;
            dknode.sprintStore = storage;
            dknode.refreshJob = refreshJob;

            RigidBodyJob rbJob = go.GetComponent<RigidBodyJob>();

            RigidbodyTemplate rbTemplate = tRigmesh.rbTemplate;
            rbJob.SyncRBTemplate(rbTemplate);

            dkDeformJob deformJob = go.GetComponent<dkDeformJob>();
            dknode.deformJob = deformJob;

            sprintJob.sjTemplate = tRigmesh.sprintTemplate;
            sprintJob.dknodes = this;

            connected.InitMesh(tRigmesh.meshMethods.m);

            nodeList.Add(dknode);
        }

        if (waitCouroutine) yield return new WaitUntil(() => i == numberCreate);

        if (isDebug) if (isDebugTime) Debug.Log("Done create nodes in : " + (Time.time - time));
        doneCCreate = true;
        yield break;
    }

    [Header("RemoveAtNode")]
    public bool doneRemoveAtNode = true;

    public void RemoveAtNode(dkNode node)
    {
        if (isDebug) if (isDebugNode) Debug.Log("Removing " + node.gameObject.name + " id in list : " + reverseID[node.idJob.IDInMesh] + " idInMesh " + node.idJob.IDInMesh);

        doneRemoveAtNode = false;

        foreach (SpringJoint sj in node.sprintJob.sjList)
        {
            if (sj.connectedBody != null)
            {
                dkNode connectedNode = sj.connectedBody.GetComponent<dkNode>();

                int IsjDel = connectedNode.sprintJob.sjList.FindIndex((x) => x.connectedBody != null && x.connectedBody.GetComponent<dkNode>() == this);
                if (IsjDel != -1)
                {
                    SpringJoint sjDel = connectedNode.sprintJob.sjList[IsjDel];
                    if (Application.isPlaying) Destroy(sjDel); else DestroyImmediate(sjDel);
                    connectedNode.sprintJob.sjList.RemoveAt(IsjDel);
                }
            }
            else
            {
                if (Application.isPlaying) Destroy(sj); else DestroyImmediate(sj);
                node.sprintJob.GetSJList();
            }
        }

        nodeList.Remove(node);
        GetReverseID();
        if (Application.isPlaying) Destroy(node.gameObject); else DestroyImmediate(node.gameObject);

        doneRemoveAtNode = true;
    }

    [ContextMenu("StoreAllSJJ")]
    public void StoreAllSJJ()
    {
        foreach (dkNode node in nodeList)
        {
            node.sprintStore.Store();
        }
    }

    [ContextMenu("DistributeAllSJJ")]
    public void DistributeAllSJJ()
    {
        foreach (dkNode node in nodeList)
        {
            node.sprintStore.Distribute();
        }
    }
}
