﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class dkNodes_Refresh : MonoBehaviour
{
    public bool isDebug = false;
    public bool waitCouroutine = false;

    [Header("Refresh")]
    public dkNodes nodes = null;
    public RigidbodyTemplate rbTemplate = null;
    public SprintJointTemplate sjTemplate = null;

    [ContextMenu("RefreshJob GetOrinal")]
    public void RefreshJobGetOriginal()
    {
        List<dkNode> nodeList = nodes.nodeList;

        foreach (dkNode node in nodeList)
        {
            ((TransformComparor)node.refreshJob.original).GetCurrent();
        }
    }

    [Header("Refresh")]
    public bool doneRefreshTrans = true;
    public int countDoneRefresh = 0;

    [ContextMenu("RefreshTrans")]
    public void RefreshTrans()
    {
        StartCoroutine(C_RefreshTrans());
    }

    IEnumerator C_RefreshTrans()
    {
        doneRefreshTrans = false;
        countDoneRefresh = 0;

        List<dkNode> nodeList = nodes.nodeList;

        foreach (dkNode node in nodeList)
        {
            ((dkNode_RefreshJob)node.refreshJob).GhostRefresh();
        }

        yield return new WaitUntil(() => countDoneRefresh == nodeList.Count);

        foreach (dkNode node in nodeList)
        {
            node.rbJob.RestoreRB();
            node.rbJob.SyncRBTemplate(rbTemplate);
        }

        foreach (dkNode node in nodeList)
        {
            node.sprintStore.Distribute();
            node.sprintJob.SyncSJTemplate(sjTemplate);
        }

        doneRefreshTrans = true;

        yield break;
    }

    void AddCountDoneAfterRefresh()
    {
        countDoneRefresh++;
    }

    private void OnEnable()
    {
        List<dkNode> nodeList = nodes.nodeList;

        foreach (dkNode node in nodeList)
        {
            ((dkNode_RefreshJob)node.refreshJob).afterRefresh += AddCountDoneAfterRefresh;
        }
    }

    private void OnDisable()
    {
        List<dkNode> nodeList = nodes.nodeList;

        foreach (dkNode node in nodeList)
        {
            ((dkNode_RefreshJob)node.refreshJob).afterRefresh -= AddCountDoneAfterRefresh;
        }
    }
}
