﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class dkNodes_CleanSJJ : MonoBehaviour
{
    public bool isDebug = false;

    [Header("Inputs")]
    public dkNodes nodes = null;    

    [ContextMenu("CleanSJJ")]
    public void CleanSJJ()
    {
        foreach(dkNode node in nodes.nodeList)
        {
            node.sprintJob.CleanSJList();
        }
    }
}
