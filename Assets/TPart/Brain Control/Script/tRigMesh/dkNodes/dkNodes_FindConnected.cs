﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using UnityEngine;

public class dkNodes_FindConnected : MonoBehaviour
{
    public bool isDebug = false;
    public bool waitCouroutine = false;

    [Header("Params")]
    public MeshMethods mm = null;
    public FindVertClose findVert = null;
    public dkNodes dknodes = null;

    [Header("CDistribute")]
    public bool doneFindConnected = true;

    [ContextMenu("FindConnected")]
    public void FindConnected()
    {
        StartCoroutine(C_FindConnected());
    }
        
    IEnumerator C_FindConnected()
    {
        float time = Time.time;

        doneFindConnected = false;

        int[] triangles = mm.m.triangles;
        List<dkNode> nodeList = dknodes.nodeList;
        IntIntDictionary reverseID = dknodes.reverseID;

        foreach (dkNode node in nodeList)
        {
            node.sprintFindJob.connectedID = new List<int>();
            node.sprintFindJob.connectedIDDic = new Dictionary<int, int>();
        }

        for (int i = 0; i < triangles.Length / 3; i++)
        {
            if (reverseID.ContainsKey(triangles[i * 3]))
            {
                if (reverseID.ContainsKey(triangles[i * 3 + 1])) AddConnectedID(nodeList[reverseID[triangles[i * 3]]], triangles[i * 3 + 1]);
                if (reverseID.ContainsKey(triangles[i * 3 + 2])) AddConnectedID(nodeList[reverseID[triangles[i * 3]]], triangles[i * 3 + 2]);
            }

            if (reverseID.ContainsKey(triangles[i * 3 + 1]))
            {
                if (reverseID.ContainsKey(triangles[i * 3])) AddConnectedID(nodeList[reverseID[triangles[i * 3 + 1]]], triangles[i * 3]);
                if (reverseID.ContainsKey(triangles[i * 3 + 2])) AddConnectedID(nodeList[reverseID[triangles[i * 3 + 1]]], triangles[i * 3 + 2]);
            }

            if (reverseID.ContainsKey(triangles[i * 3 + 2]))
            {
                if (reverseID.ContainsKey(triangles[i * 3])) AddConnectedID(nodeList[reverseID[triangles[i * 3 + 2]]], triangles[i * 3]);
                if (reverseID.ContainsKey(triangles[i * 3 + 1])) AddConnectedID(nodeList[reverseID[triangles[i * 3 + 2]]], triangles[i * 3 + 1]);
            }            
        }

        foreach (dkNode node in nodeList)
        {
            node.sprintFindJob.TrimConnectedID();
        }

        if (isDebug) Debug.Log("Done find connected in : " + (Time.time - time));
        doneFindConnected = true;
        yield break;
    }

    void AddConnectedID(dkNode node, int ID)
    {
        node.sprintFindJob.ConnectedIDAddJustNew(ID);
    }
}
