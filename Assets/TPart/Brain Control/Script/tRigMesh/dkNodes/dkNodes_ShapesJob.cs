﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class dkNodes_ShapesJob : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Current")]
    public bool isShow = false;

    [Header("List")]
    public List<ShapeJob> shapeList = new List<ShapeJob>();

    [ContextMenu("GetList")]
    public void GetList()
    {
        shapeList = GetComponentsInChildren<ShapeJob>().ToList();
    }

    [ContextMenu("ShowShape")]
    public void ShowShape()
    {
        foreach (ShapeJob sj in shapeList) { sj.showShape = true; sj.ShowShape(); }
    }

    [ContextMenu("ToggleShowShape")]
    public void ToggleShowShape()
    {
        foreach (ShapeJob sj in shapeList) sj.ToggleShowShape();
    }
}
