﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using UnityEngine;

public class dkNodes_DistributeByFindVertClose : MonoBehaviour
{
    public bool isDebug = false;
    public bool waitCouroutine = false;

    [Header("Params")]
    public FindVertClose findVert = null;
    public dkNodes dknodes = null;

    [Header("CDistribute")]
    public bool doneCDistribute = true;

    [ContextMenu("Distribute")]
    public void Distribute()
    {
        StartCoroutine(C_Distribute());
    }
        
    IEnumerator C_Distribute()
    {
        float time = Time.time;

        doneCDistribute = false;

        if (dknodes.nodeList.Count != findVert.verticesCloseID.Count)
        {
            Debug.Log("Count not match!! Return!!");
            doneCDistribute = true;
            yield break;
        }

        List<dkNode> nodeList = dknodes.nodeList;
        List<int> verticesCloseID = findVert.verticesCloseID;

        int i = 0;
        for (i = 0; i < nodeList.Count; i++)
        {
            nodeList[i].idJob.IDInMesh = verticesCloseID[i];
            nodeList[i].InitPosition();
        }

        if (waitCouroutine) yield return new WaitUntil(() => i == nodeList.Count);

        dknodes.GetReverseID();        

        if (isDebug) Debug.Log("Done distribute nodes in : " + (Time.time - time));
        doneCDistribute = true;
        yield break;
    }
}
