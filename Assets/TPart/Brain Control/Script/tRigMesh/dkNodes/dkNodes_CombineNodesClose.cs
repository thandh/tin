﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class dkNodes_CombineNodesClose : MonoBehaviour
{
    public bool isDebug = false;
    public bool waitCouroutine = false;

    [Header("Inputs")]
    public dkNodes nodes = null;

    [SerializeField]
    IntIntDictionary trackGroup = new IntIntDictionary();

    [ContextMenu("ResetTrackGroup")]
    public void ResetTrackGroup()
    {
        trackGroup = new IntIntDictionary();
    }

    [Header("Outputs")]
    public Material colorGroupNode = null;

    [Header("GroupClose")]
    public bool doneGroupClose = true;

    [ContextMenu("GroupClose")]
    public void GroupClose()
    {
        StartCoroutine(C_GroupClose());
    }

    IEnumerator C_GroupClose()
    { 
        doneGroupClose = false;

        trackGroup = new IntIntDictionary();

        int i = 0;
        while (i < nodes.nodeList.Count)
        {            
            GroupAt(nodes.nodeList[i]);
            i++;            
        }

        doneGroupClose = true;

        yield break;
    }

    public float dClose = 0.1f;

    [Header("GroupAt")]
    public bool doneGroupAt = true;

    public void GroupAt(dkNode nodeClose)
    {        
        doneGroupAt = false;

        List<int> closeID = new List<int>();

        foreach (dkNode node in nodes.nodeList)
        {
            if (node == nodeClose) continue;
            if (trackGroup.ContainsKey(node.idJob.IDInMesh)) continue;
            if ((node.Position - nodeClose.Position).magnitude < dClose)
            {
                closeID.Add(node.idJob.IDInMesh);                
            }
        }

        if (closeID.Count != 0)
        {
            if (isDebug) Debug.Log("Grouping at " + nodeClose.gameObject.name + " id in list : " + nodes.reverseID[nodeClose.idJob.IDInMesh]);
            if (isDebug) Debug.Log("Other node ");
            if (isDebug) for (int i = 0; i < closeID.Count; i++) if (closeID[i] != nodeClose.idJob.IDInMesh) Debug.Log(closeID[i]);

            closeID.Add(nodeClose.idJob.IDInMesh);
            trackGroup.Add(nodeClose.idJob.IDInMesh, nodeClose.idJob.IDInMesh);            
            nodeClose.shapeJob.currentMat = colorGroupNode;
            nodeClose.shapeJob.UpdateCurrentMat();

            foreach (int i in closeID)
            {
                if (i == nodeClose.idJob.IDInMesh) continue;

                dkNode dkNode = nodes.nodeList[nodes.reverseID[i]];
                nodeClose.deformJob.AddID(i);
                trackGroup.Add(dkNode.idJob.IDInMesh, dkNode.idJob.IDInMesh);
                nodeClose.sprintJob.AddSJFromSJJ(dkNode.sprintJob);
                nodes.RemoveAtNode(dkNode);                
            }            
        }

        doneGroupAt = true;        
    }

    [ContextMenu("MarkClose")]
    public void MarkClose()
    {
        foreach(dkNode nodeClose in nodes.nodeList)
        {
            List<int> closeID = new List<int>();

            foreach (dkNode node in nodes.nodeList)
            {
                if (node == nodeClose) continue;                
                if ((node.Position - nodeClose.Position).magnitude < dClose)
                {
                    nodeClose.shapeJob.currentMat = colorGroupNode;
                    nodeClose.shapeJob.UpdateCurrentMat();
                }
            }
        }
    }
}
