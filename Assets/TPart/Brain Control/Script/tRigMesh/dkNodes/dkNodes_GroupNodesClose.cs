﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class dkNodes_GroupNodesClose : MonoBehaviour
{
    public List<dkCloseGroup> groupList = new List<dkCloseGroup>();

    [Header("Inputs")]
    public dkNodes nodes = null;
    
    [Header("Prefab")]
    public GameObject closePrefab = null;

    private Dictionary<int, int> trackGroup = new Dictionary<int, int>();

    [ContextMenu("GroupClose")]
    public void GroupClose()
    {
        trackGroup = new Dictionary<int, int>();
        groupList = new List<dkCloseGroup>();

        foreach (dkNode node in nodes.nodeList)
        {
            GroupAt(node);
        }
    }

    public float dClose = 0.1f;

    void GroupAt(dkNode nodeClose)
    {        
        List<int> closeID = new List<int>();

        Vector3 centerOfClose = Vector3.zero;        

        foreach (dkNode node in nodes.nodeList)
        {
            if (node == nodeClose) continue;
            if (trackGroup.ContainsKey(node.idJob.IDInMesh)) continue;
            if ((node.Position - nodeClose.Position).magnitude < dClose)
            {
                closeID.Add(node.idJob.IDInMesh);
                trackGroup.Add(node.idJob.IDInMesh, node.idJob.IDInMesh);
                centerOfClose += node.Position;
            }
        }

        if (closeID.Count != 0)
        {
            GameObject closeGO = Instantiate(closePrefab, (centerOfClose / (float)closeID.Count), Quaternion.identity, nodes.transform);
            dkCloseGroup dkCloseGroup = closeGO.GetComponent<dkCloseGroup>();
            groupList.Add(dkCloseGroup);
            dkCloseGroup.nodeList = new List<dkNode>();

            foreach (int i in closeID)
            {
                dkNode dkNode = nodes.nodeList[nodes.reverseID[i]];
                dkNode.transform.parent = dkCloseGroup.transform;
                dkCloseGroup.nodeList.Add(dkNode);
            }            
        }
    }

    [ContextMenu("Delete All")]
    public void DeleteAll()
    {
        foreach (dkCloseGroup group in groupList)
            if (Application.isPlaying) Destroy(group.gameObject); else DestroyImmediate(group.gameObject);

        groupList = new List<dkCloseGroup>();        
    }
}
