﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrainResetMesh : ResetMesh
{
    [Header("Debug")]
    public  bool brainDebug = false;

    [Header("Params")]
    public tRigMesh trigMesh = null;

    [ContextMenu("BrainReset")]
    public void BrainReset()
    {
        StartCoroutine(C_BrainReset());
    }

    IEnumerator C_BrainReset()
    {
        if (brainDebug) Debug.Log("Start C_BrainReset");

        bool lastupdateFromNodes = trigMesh.updateFromNodes;
        trigMesh.updateFromNodes = false;

        ToReset();
        yield return new WaitUntil(() => doneReset == true);

        trigMesh.UpdateNodeToMM();
        trigMesh.updateFromNodes = lastupdateFromNodes;

        if (brainDebug) Debug.Log("Done C_BrainReset");
        yield break;
    }
}
