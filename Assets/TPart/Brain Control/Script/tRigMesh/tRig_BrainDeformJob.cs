﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tRig_BrainDeformJob : MonoBehaviour
{
    [Header("Inputs")]
    public dkNodes nodes = null;

    [ContextMenu("ResetDeform")]
    public void ResetDeform()
    {

    }

    [ContextMenu("AutoDeform")]
    public void AutoDeform()
    {

    }
}
