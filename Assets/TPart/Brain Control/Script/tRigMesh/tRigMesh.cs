﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tRigMesh : MonoBehaviour
{
    #region Declaration

    public bool isDebug = false;

    [Header("Input")]
    public MeshMethods meshMethods = null;
    public dkNodes dknodes = null;
    public SprintJointTemplate sprintTemplate = null;
    public RigidbodyTemplate rbTemplate = null;

    [Header("Prefab")]
    public GameObject newNode = null;

    #endregion Declaration

    ///=================================================================================///

    #region Methods

    [ContextMenu("InitPosition")]
    public void InitPosition()
    {
        if (isDebug) Debug.Log("Initing Position " + dknodes.nodeList.Count + " dkNode(s)");        
        foreach (dkNode n in dknodes.nodeList) n.InitPosition();
    }

    [ContextMenu("FindConnectedByEachSelf")]
    public void FindConnectedByEachSelf()
    {
        if (isDebug) Debug.Log("Initing Find Connection " + dknodes.nodeList.Count + " dkNode(s)");
        foreach (dkNode n in dknodes.nodeList) n.InitPosition();
    }

    #endregion Methods

    [Header("Update")]
    public bool updateFromNodes = false;

    public bool wait = true;
    public float stepUpdateMM = 0.5f;

    private float lastTimeUpdate = 0f;    

    private void Update()
    {
        if (!updateFromNodes) return;

        if (wait)
        {
            if (Time.time - lastTimeUpdate > stepUpdateMM)
                UpdateMeshMethod();
        }
        else
        {
            UpdateMeshMethod();
        }
    }

    [ContextMenu("UpdateMeshMethod")]
    public void UpdateMeshMethod()
    {
        if (dknodes == null || meshMethods == null)
        {
            updateFromNodes = false;
            return;
        }

        List<dkNode> nodeList = dknodes.nodeList;
        Vector3[] vertices = meshMethods.m.vertices;

        foreach (dkNode node in nodeList)
        {
            vertices[node.idJob.IDInMesh] = transform.InverseTransformPoint(node.Position);
            dkDeformJob deformJob = node.deformJob;
            foreach (KeyValuePair<int, int> key in deformJob.IDDic)
            {
                if (key.Key != node.idJob.IDInMesh)
                    vertices[key.Key] = transform.InverseTransformPoint(node.Position);
            }
        }

        meshMethods.UpdateVertices(vertices);
        lastTimeUpdate = Time.time;
    }

    [ContextMenu("UpdateNodeToMM")]
    public void UpdateNodeToMM()
    {
        if (dknodes == null || meshMethods == null)
        {
            updateFromNodes = false;
            return;
        }

        List<dkNode> nodeList = dknodes.nodeList;
        Vector3[] vertices = meshMethods.m.vertices;

        foreach (dkNode node in nodeList)
        {
            node.transform.position = transform.TransformPoint(vertices[node.idJob.IDInMesh]);
        }
    }
}
