﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TINDefineTrClose : MonoBehaviour
{
    [Header("trClose")]
    public OriginOfMesh xOrigin = null;
    public VertClosestToTransform closeSinusX = null;

    [ContextMenu("DefineTrClose")]
    public void DefineTrClose()
    {
        xOrigin.Center();
        closeSinusX.GetClose();
    }
}
