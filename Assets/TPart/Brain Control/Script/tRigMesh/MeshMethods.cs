﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshMethods : MonoBehaviour
{
    [Header("Input")]
    public MeshFilter mf = null;

    Mesh _mRuntime = null;
    Mesh _mEditor = null;
    public Mesh m
    {
        get
        {
            if (Application.isPlaying && _mRuntime == null) _mRuntime = mf.mesh;
            else if (!Application.isPlaying && _mEditor == null) _mEditor = mf.sharedMesh;
            if (Application.isPlaying) return _mRuntime; else return _mEditor;
        }
    }

    [ContextMenu("ResetLocalMesh")]
    public void ResetLocalMesh()
    {
        _mRuntime = null;
        _mEditor = null;
    }

    private MeshCollider _m_collider = null;
    private MeshCollider m_collider { get { if (_m_collider == null) _m_collider = GetComponent<MeshCollider>(); return _m_collider; } }

    public Vector3 VertPosition(int IDInMesh)
    {
        if (IDInMesh == -1) return Vector3.zero;
        return transform.TransformPoint(m.vertices[IDInMesh]);
    }

    [ContextMenu("DebugMInfo")]
    public void DebugMInfo()
    {
        Debug.Log("Vert " + m.vertexCount);
        Debug.Log("Tris" + m.triangles.Length);
    }

    [ContextMenu("DebugVertWolrdPos")]
    public void DebugVertWolrdPos()
    {
        Vector3[] vertices = m.vertices;
        for (int i = 0; i < m.vertexCount; i++)
        {
            Debug.Log("Vert at " + i + " pos at : " + transform.TransformPoint(vertices[i]));
        }

    }

    public void UpdateVertices(Vector3[] verts)
    {
        m.vertices = verts;
        m.RecalculateNormals();
        m.RecalculateBounds();

        if (m_collider != null)
        {
            m_collider.sharedMesh = null;
            m_collider.sharedMesh = m;
        }
    }

    [ContextMenu("Standardize")]
    public void Standardize()
    {
        Vector3[] vertices = m.vertices;
        Vector3 localScale = transform.localScale;
        for (int i = 0; i < vertices.Length; i++)
            vertices[i] = new Vector3(vertices[i].x * localScale.x, vertices[i].y * localScale.y, vertices[i].z * localScale.z);
        transform.localScale = Vector3.one;
        UpdateVertices(vertices);
    }
}
