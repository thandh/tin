﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SplitMesh : MonoBehaviour
{
    [Header("Params")]
    public bool isDebug = false;

    [Header("Params")]
    public FindVertClose findVert = null;
    public TINDefineTrClose closeDefine = null;
    public CreateMesh createMesh = null;
    public CreateMesh removeMesh = null;
    public CreateMesh betweenMeshC = null;
    [HideInInspector]
    public CreateMesh betweenMeshD = null;

    [Header("Process")]
    public bool doneFullSplit = true;

    [ContextMenu("FullSplit")]
    public void FullSplit()
    {
        StartCoroutine(C_FullSplit());
    }
    
    IEnumerator C_FullSplit()
    {
        if (isDebug) Debug.Log("start C_FullSplit()");
        closeDefine.DefineTrClose();

        findVert.ResetLocalMesh();
        if (isDebug) Debug.Log("start yield findVert.doneCFind == true");
        findVert.Find();        
        yield return new WaitUntil(() => findVert.doneCFind == true);
        if (isDebug) Debug.Log("done yield findVert.doneCFind == true");

        CreateMesh();
        RemoveMesh();
        BetweenMeshC();

        SwapAfterSplit();

        if (isDebug) Debug.Log("done C_FullSplit()");

        yield break;
    }

    [ContextMenu("CreateMesh")]
    public void CreateMesh()
    {
        List<int> verticesCloseID = findVert.verticesCloseID;

        Vector3[] verts = findVert.m.vertices;
        List<Vector3> vertList = new List<Vector3>();
        Dictionary<int, int> reverseID = new Dictionary<int, int>();
        for (int i = 0; i < verticesCloseID.Count; i++)
        {
            vertList.Add(verts[findVert.verticesCloseID[i]]);                
            reverseID.Add(verticesCloseID[i], i);
        }

        int[] tris = findVert.m.triangles;
        List<int> trisList = new List<int>();
        for (int i = 0; i < tris.Length / 3; i++)
        {
            if (reverseID.ContainsKey(tris[i * 3]) && reverseID.ContainsKey(tris[i * 3 + 1]) && reverseID.ContainsKey(tris[i * 3 + 2]))
            {
                trisList.Add(reverseID[tris[i * 3]]);
                trisList.Add(reverseID[tris[i * 3 + 1]]);
                trisList.Add(reverseID[tris[i * 3 + 2]]);
            }
        }

        createMesh.meshName = "SplitNCreate";

        Debug.Log("Output verts count : " + vertList.Count);
        createMesh.vertList = vertList;

        Debug.Log("Output tris count : " + trisList.Count);
        createMesh.trisList = trisList;

        createMesh.CreateCurrentMesh();
    }

    [ContextMenu("RemoveMesh")]
    public void RemoveMesh()
    {
        List<int> verticesCloseID = findVert.verticesCloseID;
        Vector3[] verts = findVert.m.vertices;

        List<Vector3> vertList = new List<Vector3>();
        int[] reverseID_In = new int[verts.Length];
        for (int i = 0; i < reverseID_In.Length; i++) reverseID_In[i] = -1;

        List<int> inverseID_Out = new List<int>();
        int[] reverseID_Out = new int[verts.Length];

        for (int i = 0; i < reverseID_Out.Length; i++) reverseID_Out[i] = -1;

        for (int i = 0; i < verticesCloseID.Count; i++) reverseID_In[verticesCloseID[i]] = i;

        for (int i = 0; i < verts.Length; i++)
        {
            if (reverseID_In[i] == -1)
            {
                vertList.Add(verts[i]);                    
                inverseID_Out.Add(i);
                reverseID_Out[i] = inverseID_Out.Count - 1;
            }
        }

        int[] tris = findVert.m.triangles;
        List<int> trisList = new List<int>();

        int countDontGet = 0;
        for (int i = 0; i < tris.Length / 3; i++)
        {
            List<Vector3> vertToAdd = new List<Vector3>();

            if (reverseID_Out[tris[i * 3]] != -1 && reverseID_Out[tris[i * 3 + 1]] != -1 && reverseID_Out[tris[i * 3 + 2]] != -1)
            {
                trisList.Add(reverseID_Out[tris[i * 3]]);
                trisList.Add(reverseID_Out[tris[i * 3 + 1]]);
                trisList.Add(reverseID_Out[tris[i * 3 + 2]]);
            }
            else
            {
                countDontGet++;
            }
        }

        Debug.Log("Total " + countDontGet + " don't get tris!");

        removeMesh.meshName = "SplitNRemove";

        Debug.Log("Output verts count : " + vertList.Count);
        removeMesh.vertList = vertList;

        Debug.Log("Output tris count : " + trisList.Count);
        removeMesh.trisList = trisList;

        removeMesh.CreateCurrentMesh();
    }

    public class AnEdge
    {
        public Vector3 p1 = Vector3.zero;
        public Vector3 p2 = Vector3.zero;
        public AnEdge() { this.p1 = Vector3.zero; this.p2 = Vector3.zero; }
        public AnEdge(Vector3 p1, Vector3 p2) { this.p1 = p1; this.p2 = p2; }
        public Vector3 v3Center() { return (p1 + p2) / 2; }
        public string strCenter() { return v3Center().ToString(); }
    }

    public class EdgeInfo
    {
        public AnEdge edge = new AnEdge();
        public int count = 0;
        public EdgeInfo() { }
    }

    Dictionary<string, EdgeInfo> edgeCreateDic = new Dictionary<string, EdgeInfo>();
    Dictionary<string, EdgeInfo> edgeOnceDic = new Dictionary<string, EdgeInfo>();

    List<Vector3> vertListC = new List<Vector3>();
    List<int> trisListC = new List<int>();

    Dictionary<string, EdgeInfo> pointInEdgeOnceCreateDic = new Dictionary<string, EdgeInfo>();

    [ContextMenu("BetweenMeshC")]
    public void BetweenMeshC()
    {
        List<Vector3> vertListCreate = createMesh.vertList;
        List<int> trisListCreate = createMesh.trisList;

        edgeCreateDic = new Dictionary<string, EdgeInfo>();
        for (int i = 0; i < trisListCreate.Count / 3; i++)
        {
            CountAnEdgeInRemove(vertListCreate[trisListCreate[i * 3]], vertListCreate[trisListCreate[i * 3 + 1]]);
            CountAnEdgeInRemove(vertListCreate[trisListCreate[i * 3]], vertListCreate[trisListCreate[i * 3 + 2]]);
            CountAnEdgeInRemove(vertListCreate[trisListCreate[i * 3 + 1]], vertListCreate[trisListCreate[i * 3 + 2]]);
        }

        edgeOnceDic = new Dictionary<string, EdgeInfo>();

        foreach (KeyValuePair<string, EdgeInfo> e in edgeCreateDic)
            if (e.Value.count == 1)
            {
                edgeOnceDic.Add(e.Key, e.Value);
                if (!pointInEdgeOnceCreateDic.ContainsKey(e.Value.edge.p1.ToString())) pointInEdgeOnceCreateDic.Add(e.Value.edge.p1.ToString(), e.Value);
                if (!pointInEdgeOnceCreateDic.ContainsKey(e.Value.edge.p2.ToString())) pointInEdgeOnceCreateDic.Add(e.Value.edge.p2.ToString(), e.Value);
            }

        Debug.Log("Outer edge : " + edgeOnceDic.Count);

        Vector3[] verts = findVert.m.vertices;
        int[] tris = findVert.m.triangles;

        vertListC = new List<Vector3>();
        trisListC = new List<int>();

        for (int i = 0; i < tris.Length / 3; i++)                    
            TryDrawTris(verts[tris[i * 3]], verts[tris[i * 3 + 1]], verts[tris[i * 3 + 2]]);        

        betweenMeshC.meshName = "betweenMeshC";

        Debug.Log("Output verts count : " + vertListC.Count);
        betweenMeshC.vertList = vertListC;

        Debug.Log("Output tris count : " + trisListC.Count);
        betweenMeshC.trisList = trisListC;

        betweenMeshC.CreateCurrentMesh();
    }

    void TryDrawTris(Vector3 p1, Vector3 p2, Vector3 p3)
    {
        if (!pointInEdgeOnceCreateDic.ContainsKey(p1.ToString()) && !pointInEdgeOnceCreateDic.ContainsKey(p2.ToString()) && !pointInEdgeOnceCreateDic.ContainsKey(p3.ToString())) return;

        Vector3 p1Edge = Vector3.zero;
        Vector3 p2Edge = Vector3.zero;
        Vector3 p3NotEdge = Vector3.zero;

        if (!pointInEdgeOnceCreateDic.ContainsKey(p1.ToString())) { p3NotEdge = p1; p1Edge = p2; p2Edge = p3; }
        else if (!pointInEdgeOnceCreateDic.ContainsKey(p2.ToString())) { p3NotEdge = p2; p1Edge = p1; p2Edge = p3; }
        else if (!pointInEdgeOnceCreateDic.ContainsKey(p3.ToString())) { p3NotEdge = p3; p1Edge = p1; p2Edge = p2; }

        string centerInRemove1 = StrCenterOfEdge(p3NotEdge, p1Edge);
        string centerInRemove2 = StrCenterOfEdge(p3NotEdge, p2Edge);

        if (edgeCreateDic.ContainsKey(centerInRemove1) && edgeCreateDic.ContainsKey(centerInRemove2)) return;
        else
        {
            int iP1 = vertListC.IndexOf(p1);
            if (iP1 == -1) { vertListC.Add(p1); iP1 = vertListC.Count - 1; }

            int iP2 = vertListC.IndexOf(p2);
            if (iP2 == -1) { vertListC.Add(p2); iP2 = vertListC.Count - 1; }

            int iP3 = vertListC.IndexOf(p3);
            if (iP3 == -1) { vertListC.Add(p3); iP3 = vertListC.Count - 1; }

            trisListC.Add(iP1);
            trisListC.Add(iP2);
            trisListC.Add(iP3);
        }
    }

    void CountAnEdgeInRemove(Vector3 p1, Vector3 p2)
    {
        string strCenter = ((p1 + p2) / 2).ToString();

        if (!edgeCreateDic.ContainsKey(strCenter))
        {
            EdgeInfo edgeInfo = new EdgeInfo();
            edgeInfo.edge.p1 = p1;
            edgeInfo.edge.p2 = p2;
            edgeInfo.count = 1;

            edgeCreateDic.Add(edgeInfo.edge.strCenter(), edgeInfo);
        }
        else
        {
            edgeCreateDic[strCenter].count++;
        }
    }

    void CheckAddEdgeOnceN2InRemove(Vector3 p1, Vector3 p2, Vector3 p3)
    {
        string center12 = StrCenterOfEdge(p1, p2);
        string center13 = StrCenterOfEdge(p1, p3);
        string center23 = StrCenterOfEdge(p2, p3);

        if (!edgeOnceDic.ContainsKey(center12) && !edgeOnceDic.ContainsKey(center13) && !edgeOnceDic.ContainsKey(center23)) return;

        string centerOnce = string.Empty;
        string centerInRemove1 = string.Empty;
        string centerInRemove2 = string.Empty;

        if (edgeOnceDic.ContainsKey(center12)) { centerOnce = center12; centerInRemove1 = center13; centerInRemove2 = center23; }
        else if (edgeOnceDic.ContainsKey(center13)) { centerOnce = center13; centerInRemove1 = center12; centerInRemove2 = center23; }
        else if (edgeOnceDic.ContainsKey(center23)) { centerOnce = center23; centerInRemove1 = center12; centerInRemove2 = center13; }

        if (edgeCreateDic.ContainsKey(centerInRemove1) || edgeCreateDic.ContainsKey(centerInRemove2)) return;
        else
        {
            int iP1 = vertListC.IndexOf(p1);
            if (iP1 == -1) { vertListC.Add(p1); iP1 = vertListC.Count - 1; }

            int iP2 = vertListC.IndexOf(p2);
            if (iP2 == -1) { vertListC.Add(p2); iP2 = vertListC.Count - 1; }

            int iP3 = vertListC.IndexOf(p3);
            if (iP3 == -1) { vertListC.Add(p3); iP3 = vertListC.Count - 1; }

            trisListC.Add(iP1);
            trisListC.Add(iP2);
            trisListC.Add(iP3);
        }
    }

    string StrCenterOfEdge(Vector3 p1, Vector3 p2)
    {
        return ((p1 + p2) / 2).ToString();
    }

    Dictionary<string, EdgeInfo> edgeCDic = new Dictionary<string, EdgeInfo>();
    Dictionary<string, EdgeInfo> edgeCOnceDic = new Dictionary<string, EdgeInfo>();

    List<Vector3> vertListD = new List<Vector3>();
    List<int> trisListD = new List<int>();

    [ContextMenu("BetweenMeshD")]
    public void BetweenMeshD()
    {
        List<Vector3> vertListBetween = betweenMeshC.vertList;
        List<int> trisListBetween = betweenMeshC.trisList;

        edgeCDic = new Dictionary<string, EdgeInfo>();
        for (int i = 0; i < trisListBetween.Count / 3; i++)
        {
            CountAnEdgeInC(vertListBetween[trisListBetween[i * 3]], vertListBetween[trisListBetween[i * 3 + 1]]);
            CountAnEdgeInC(vertListBetween[trisListBetween[i * 3]], vertListBetween[trisListBetween[i * 3 + 2]]);
            CountAnEdgeInC(vertListBetween[trisListBetween[i * 3 + 1]], vertListBetween[trisListBetween[i * 3 + 2]]);
        }

        edgeCOnceDic = new Dictionary<string, EdgeInfo>();

        foreach (KeyValuePair<string, EdgeInfo> e in edgeCDic)
            if (e.Value.count == 1) edgeCOnceDic.Add(e.Key, e.Value);

        Debug.Log("Outer edge : " + edgeCDic.Count);

        Vector3[] verts = findVert.m.vertices;
        int[] tris = findVert.m.triangles;

        vertListD = new List<Vector3>();
        trisListD = new List<int>();

        for (int i = 0; i < tris.Length / 3; i++)
            CheckAddEdgeDOnceN2InRemove(verts[tris[i * 3]], verts[tris[i * 3 + 1]], verts[tris[i * 3 + 2]]);

        betweenMeshD.meshName = "betweenMeshD";

        Debug.Log("Output verts count : " + vertListC.Count);
        betweenMeshD.vertList = vertListD;

        Debug.Log("Output tris count : " + trisListC.Count);
        betweenMeshD.trisList = trisListD;
    }

    void CountAnEdgeInC(Vector3 p1, Vector3 p2)
    {
        string strCenter = ((p1 + p2) / 2).ToString();

        if (!edgeCDic.ContainsKey(strCenter))
        {
            EdgeInfo edgeInfo = new EdgeInfo();
            edgeInfo.edge.p1 = p1;
            edgeInfo.edge.p2 = p2;
            edgeInfo.count = 1;

            edgeCDic.Add(edgeInfo.edge.strCenter(), edgeInfo);
        }
        else
        {
            edgeCDic[strCenter].count++;
        }
    }

    void CheckAddEdgeDOnceN2InRemove(Vector3 p1, Vector3 p2, Vector3 p3)
    {
        string center12 = StrCenterOfEdge(p1, p2);
        string center13 = StrCenterOfEdge(p1, p3);
        string center23 = StrCenterOfEdge(p2, p3);

        if (!edgeCOnceDic.ContainsKey(center12) && !edgeCOnceDic.ContainsKey(center13) && !edgeCOnceDic.ContainsKey(center23)) return;

        string centerOnce = string.Empty;
        string centerInRemove1 = string.Empty;
        string centerInRemove2 = string.Empty;

        if (edgeCOnceDic.ContainsKey(center12)) { centerOnce = center12; centerInRemove1 = center13; centerInRemove2 = center23; }
        else if (edgeCOnceDic.ContainsKey(center13)) { centerOnce = center13; centerInRemove1 = center12; centerInRemove2 = center23; }
        else if (edgeCOnceDic.ContainsKey(center23)) { centerOnce = center23; centerInRemove1 = center12; centerInRemove2 = center13; }

        if (edgeCreateDic.ContainsKey(centerInRemove1) || edgeCreateDic.ContainsKey(centerInRemove2)) return;
        else if (edgeCDic.ContainsKey(centerInRemove1) || edgeCDic.ContainsKey(centerInRemove2)) return;
        else
        {
            int iP1 = vertListD.IndexOf(p1);
            if (iP1 == -1) { vertListD.Add(p1); iP1 = vertListD.Count - 1; }

            int iP2 = vertListD.IndexOf(p2);
            if (iP2 == -1) { vertListD.Add(p2); iP2 = vertListD.Count - 1; }

            int iP3 = vertListD.IndexOf(p3);
            if (iP3 == -1) { vertListD.Add(p3); iP3 = vertListD.Count - 1; }

            trisListD.Add(iP1);
            trisListD.Add(iP2);
            trisListD.Add(iP3);
        }
    }

    bool CheckBothSide(Vector3 vert1, Vector3 vert2, Vector3 vert3, List<Vector3> createAr, List<Vector3> removeAr, out List<Vector3> vertToAdd)
    {
        vertToAdd = new List<Vector3>();

        int belongCreate = 0; int belongRemove = 0;

        if (createAr.Contains(vert1)) belongCreate++;
        else if (removeAr.Contains(vert1)) belongRemove++;
        else { Debug.Log("This is wrong!"); return false; }

        if (createAr.Contains(vert2)) belongCreate++;
        else if (removeAr.Contains(vert2)) belongRemove++;
        else { Debug.Log("This is wrong!"); return false; }

        if (createAr.Contains(vert3)) belongCreate++;
        else if (removeAr.Contains(vert3)) belongRemove++;
        else { Debug.Log("This is wrong!"); return false; }

        if (belongCreate > 0 && belongRemove > 0)
        {
            if (!removeAr.Contains(vert1)) vertToAdd.Add(vert1);
            if (!removeAr.Contains(vert2)) vertToAdd.Add(vert2);
            if (!removeAr.Contains(vert3)) vertToAdd.Add(vert3);
        }

        return vertToAdd.Count > 0;
    }

    [Header("Output")]
    public CreateMesh brainCreate = null;
    public MeshCollider mcBrain = null;

    [ContextMenu("SwapAfterSplit")]
    public void SwapAfterSplit()
    {
        brainCreate.vertList = createMesh.vertList;
        brainCreate.trisList = createMesh.trisList;
        brainCreate.CreateCurrentMesh();

        mcBrain.sharedMesh = brainCreate._mesh;

        findVert.vertCollectCount = brainCreate.vertList.Count;
        GetComponentInChildren<dkNodes>().numberCreate = brainCreate.vertList.Count;        

        createMesh.gameObject.SetActive(false);
        removeMesh.gameObject.SetActive(true);
        betweenMeshC.gameObject.SetActive(true);
    }
}
