﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IDJob : MonoBehaviour
{
    [Header("Params")]

    [SerializeField] int _IDInMesh = -1;
    public int IDInMesh { get { return _IDInMesh; } set { _IDInMesh = value; HandleSetID(_IDInMesh); } }

    public string NamePrefix = string.Empty;

    void HandleSetID(int ID)
    {
        gameObject.name = NamePrefix + "_" + ID.ToString();
    }

    [ContextMenu("SetID")]
    public void SetID()
    {
        IDInMesh = _IDInMesh;
    }
}
