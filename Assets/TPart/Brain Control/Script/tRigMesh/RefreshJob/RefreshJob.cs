﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RefreshJob : MonoBehaviour
{
    public RefreshComparor original = null;
    public RefreshComparor current = null;

    [Header("Refresh params")]
    /// <summary>
    /// if true, move state step by step.
    /// if false, jump to state
    /// </summary>
    public bool useGhostState = true;
    
    [ContextMenu("Refresh")]
    public virtual void Refresh()
    {
        
    }
}
