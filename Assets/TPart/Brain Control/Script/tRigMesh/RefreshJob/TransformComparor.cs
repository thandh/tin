﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TransformComparor : RefreshComparor, iRefreshComparor
{
    public Vector3 pos = Vector3.zero;
    public Quaternion rot = Quaternion.identity;

    [Header("Equal")]
    public bool useEqualExact = false;
    public float deltaPos = 0.01f;
    public float deltaRot = 0.01f;

    public bool IsEqual(RefreshComparor refCompare)
    {        
        TransformComparor transCompare = (TransformComparor)refCompare;
        return useEqualExact == false ?            
            ((this.pos - transCompare.pos).magnitude < deltaPos && (this.rot.eulerAngles - transCompare.rot.eulerAngles).magnitude < deltaRot)
            : (this.pos == transCompare.pos && this.rot == transCompare.rot);
    }

    [ContextMenu("GetCurrent")]
    public void GetCurrent()
    {
        pos = transform.position;
        rot = transform.rotation;
    }

    [ContextMenu("Apply")]
    public void Apply()
    {
        transform.position = this.pos;
        transform.rotation = this.rot;
    }

    [Header("Step")]
    public float stepPos = 0.1f;
    public float stepRot = 0.1f;

    public void StepTo(TransformComparor transCom)
    {
        transform.position = Vector3.Lerp(transform.position, transCom.pos, stepPos);
        transform.rotation = Quaternion.Lerp(transform.rotation, transCom.rot, stepRot);
    }
}
