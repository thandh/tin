﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface iRefreshComparor
{
    bool IsEqual(RefreshComparor comparor);
}
