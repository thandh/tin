﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dkNode_RefreshJob : RefreshJob
{
    [Header("Inputs")]
    public dkNode node = null;

    [ContextMenu("Refresh")]
    public override void Refresh()
    {
        if (!Application.isPlaying)
        {
            JumRefresh();
        } else
        {
            if (!node.refreshJob.useGhostState) JumRefresh();
            else GhostRefresh();            
        }
    }

    private void JumRefresh()
    {
        TransformComparor transCom = (TransformComparor)node.refreshJob.original;
        transCom.Apply();
    }

    [Header("GhostRefresh")]
    public bool doneGhostRefresh = true;
    public delegate void delAfterRefreshTrans();
    public delAfterRefreshTrans afterRefresh = null;

    [ContextMenu("GhostRefresh")]
    public void GhostRefresh()
    {
        StartCoroutine(C_GhostRefresh());
    }

    IEnumerator C_GhostRefresh()
    {
        doneGhostRefresh = false;

        node.sprintJob.DeleteAllSJ();
        yield return new WaitUntil(() => node.sprintJob.doneDeleteAllSJ == true);

        node.rbJob.DeleteRB();
                
        TransformComparor oriCom = (TransformComparor) node.refreshJob.original;
        TransformComparor curCom = (TransformComparor) node.refreshJob.current;

        curCom.GetCurrent();

        while (!oriCom.IsEqual(curCom))
        {
            curCom.StepTo(oriCom);
            yield return new WaitForEndOfFrame();
            curCom.GetCurrent();
        }

        if (afterRefresh != null) afterRefresh.Invoke();

        doneGhostRefresh = true;

        yield break;
    }
}
