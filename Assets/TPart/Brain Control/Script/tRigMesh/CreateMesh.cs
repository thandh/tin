﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateMesh : MonoBehaviour
{
    [Header("Params")]
    public bool isDebug = false;

    [Header("Params")]
    public Mesh _mesh = null;
    public MeshFilter mf = null;

    [Header("Output")]
    public string meshName = string.Empty;    
    [HideInInspector] public List<Vector3> vertList = new List<Vector3>();    
    [HideInInspector] public List<int> trisList = new List<int>();

    [ContextMenu("CreateCurrentMesh")]
    public void CreateCurrentMesh()
    {
        _mesh = new Mesh();
        _mesh.name = meshName;
        _mesh.vertices = vertList.ToArray();
        _mesh.triangles = trisList.ToArray();
        _mesh.RecalculateNormals();
        _mesh.RecalculateBounds();
        mf.mesh = _mesh;
    }

    [ContextMenu("DebugCurrent")]
    public void DebugCurrent()
    {
        Debug.Log("Vert count : " + vertList.Count);
        Debug.Log("Tris count : " + trisList.Count);
    }

    [ContextMenu("CheckCurrent")]
    public void CheckCurrent()
    {
        int count = 0;
        for (int i = 0; i < trisList.Count; i++)
        {
             if (trisList[i] > vertList.Count - 1 || trisList[i] < 0)
            {
                Debug.Log(trisList[i] + ", " + i);
                count++;
            }
        }
        Debug.Log(count);        
    }

    //[ContextMenu("SpillMesh")]
    //public void SpillMesh()
    //{
    //    for (int i = 0; i < trisList.Count / 3; i++)
    //    {
    //        GameObject go = Instantiate(new GameObject(), transform);
    //        go.transform.localPosition = Vector3.zero;
    //        go.transform.localRotation = Quaternion.identity;
    //        go.transform.localScale = Vector3.one;
    //        MeshFilter mf = go.AddComponent<MeshFilter>();
    //        MeshRenderer mr = go.AddComponent<MeshRenderer>();
    //        Mesh m = new Mesh();
    //        m.name = 
    //    }
    //}
}
