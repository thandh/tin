﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TransparentJob : MonoBehaviour
{
    [Header("Renderer")]
    public Renderer r = null;        
    
    Material _matRuntime = null;
    Material _matEditor = null;
    public Material mat
    {
        get
        {
            if (Application.isPlaying && _matRuntime == null)
            {
                if (r != null) _matRuntime = r.material;
            }
            else if (!Application.isPlaying && _matEditor == null)
            {
                if (r != null) _matEditor = r.sharedMaterial;
            }

            if (Application.isPlaying) return _matRuntime; else return _matEditor;
        }

        set
        {
            if (Application.isPlaying)
            {
                _matRuntime = value;
                if (r != null) r.material = _matRuntime;                
            }
            else
            {
                _matEditor = value;
                if (r != null) r.sharedMaterial = _matEditor;                
            }
        }
    }
        
    void SetTransparent(float t)
    {
        Color currentColor = mat.color;
        mat.color = new Color(currentColor.r, currentColor.g, currentColor.b, t);
    }

    [ContextMenu("Set100")]
    public void Set100()
    {
        SetTransparent(1f);
    }

    [ContextMenu("Set50")]
    public void Set50()
    {
        SetTransparent(0.5f);
    }

    [ContextMenu("Set0")]
    public void Set0()
    {
        SetTransparent(0f);
    }
}
