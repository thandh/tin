﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dkDeformJob : MonoBehaviour
{
    [SerializeField]
    public IntIntDictionary IDDic = new IntIntDictionary();

    public void AddID(int ID)
    {
        if (IDDic.ContainsKey(ID)) return;
        IDDic.Add(ID, ID);
    }

    [ContextMenu("DebugAllID")]
    public void DebugAllID()
    {
        string str = string.Empty;
        foreach (KeyValuePair<int, int> key in IDDic)
        {
            str += key + " ";            
        }
        Debug.Log(str);
    }
}
