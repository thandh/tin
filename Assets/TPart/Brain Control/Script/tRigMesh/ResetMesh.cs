﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetMesh : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    Vector3[] vertsToReset = null;
    Vector3[] vertCurrent = null;

    [Header("Params")]
    public MeshFilter mf = null;

    private void Start()
    {
        GetParams();
    }

    [ContextMenu("GetParams")]
    public void GetParams()
    {
        vertsToReset = mf.sharedMesh.vertices;
        vertCurrent = vertsToReset;
        DebugCurrent();
    }

    [ContextMenu("ResetImmediate")]
    public void ResetImmediate()
    {
        mf.sharedMesh.vertices = vertsToReset;        
        mf.sharedMesh.RecalculateBounds();
        mf.sharedMesh.RecalculateNormals();
    }

    [ContextMenu("ToReset")]
    public void ToReset()
    {        
        StartCoroutine(C_ToReset());
    }

    [Header("Resetting")]
    public bool doneReset = true;
    public int countDoneReset = 0;

    IEnumerator C_ToReset()
    {
        if (isDebug) Debug.Log("Start C_ToReset");

        vertCurrent = mf.sharedMesh.vertices;
        countDoneReset = 0; doneReset = false;
        for (int i = 0; i < vertsToReset.Length; i++) StartCoroutine(C_MovingAVert(i));

        while (countDoneReset < vertsToReset.Length - 1)
        {
            mf.sharedMesh.vertices = vertCurrent;
            mf.sharedMesh.RecalculateNormals();
            mf.sharedMesh.RecalculateBounds();
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitUntil(() => (countDoneReset >= vertsToReset.Length - 1) == true);

        doneReset = true;
        if (isDebug) Debug.Log("Done C_ToReset : " + countDoneReset);
        yield break;
    }

    [Header("MovingVert")]
    public float deltaDis = 0.1f;
    public float speedVert = 1f;

    IEnumerator C_MovingAVert(int vID)
    {
        while ((vertCurrent[vID] - vertsToReset[vID]).magnitude > deltaDis)
        {
            vertCurrent[vID] = Vector3.Lerp(vertCurrent[vID], vertsToReset[vID], Time.time * speedVert);            
            yield return new WaitForEndOfFrame();             
        }

        //if (isDebug) Debug.Log("Dont at ID : " + vID + " at count : " + countDoneReset);
        vertCurrent[vID] = vertsToReset[vID];
        countDoneReset++;
        yield break;
    }

    [ContextMenu("Stop")]
    public void Stop()
    {
        StopAllCoroutines();
        doneReset = true;
    }

    [ContextMenu("DebugCurrent")]
    public void DebugCurrent()
    {
        Debug.Log("Verts : " + vertsToReset.Length);        
    }
}
