﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindVertClose : MonoBehaviour
{
    public bool isDebug = false;
    public bool waitCouroutine = false;

    [Header("Inputs")]
    public MeshFilter mf = null;
    [SerializeField]
    Mesh _m = null;
    public Mesh m { get { if (_m == null) { if (Application.isPlaying) _m = mf.mesh; else _m = mf.sharedMesh; } return _m; } }

    [ContextMenu("ResetLocalMesh")]
    public void ResetLocalMesh()
    {
        _m = null;
    }

    [ContextMenu("DebugInfo")]
    public void DebugInfo()
    {
        Debug.Log("Current vert count : " + m.vertexCount);
    }

    public Transform trClose = null;

    [Header("Params")]
    public int vertCollectCount = 100;

    [Header("Outputs")]
    public List<int> verticesCloseID = new List<int>();
    public Dictionary<int, int> reverseCloseID = new Dictionary<int, int>();

    [Header("Find")]
    public bool doneCFind = true;
    private Vector3[] currentVertices = new Vector3[0];

    [ContextMenu("Find")]
    public void Find()
    {
        StartCoroutine(C_Find());
    }

    IEnumerator C_Find()
    {
        if (isDebug) Debug.Log("start C_Find()");

        doneCFind = false;

        float time = Time.time;

        verticesCloseID = new List<int>();
        reverseCloseID = new Dictionary<int, int>();

        currentVertices = m.vertices;
        for (int i = 0; i < currentVertices.Length; i++)
            currentVertices[i] = transform.TransformPoint(currentVertices[i]);                

        vertCollectCount = Math.Min(currentVertices.Length, vertCollectCount);

        for (int i = 0; i < currentVertices.Length; i++)
            verticesCloseID.Add(i);

        verticesCloseID.Sort((v1, v2) => ((currentVertices[v1] - trClose.position).magnitude).CompareTo(((currentVertices[v2] - trClose.position).magnitude)));        

        if (verticesCloseID.Count > vertCollectCount)
        {
            verticesCloseID.RemoveRange(vertCollectCount, verticesCloseID.Count - vertCollectCount);
            if (waitCouroutine) yield return new WaitUntil(() => verticesCloseID.Count == vertCollectCount);
        }

        for (int i = 0; i < vertCollectCount; i++) reverseCloseID.Add(verticesCloseID[i], i);
        if (waitCouroutine) yield return new WaitUntil(() => reverseCloseID.Count == vertCollectCount);

        doneCFind = true;

        if (isDebug) Debug.Log("Done find close vert in : " + (Time.time - time) + " with vertCount : " + reverseCloseID.Count);

        if (isDebug) Debug.Log("done C_Find()");

        yield break;
    }

    public Vector3 CurrentCenter()
    {
        if (currentVertices.Length == 0) { Debug.Log("currentVertices.Length == 0"); return Vector3.zero; }
        Vector3 pos = Vector3.zero;
        foreach (Vector3 v in currentVertices) pos += v;
        pos /= currentVertices.Length;
        return pos;
    }
}
