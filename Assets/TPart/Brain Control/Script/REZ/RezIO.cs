﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class RezIO : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;
    public bool ClickHere = false;

    [Header("Runtime")]
    public string filename = string.Empty;

    [Header("GetParams")]
    public bool doneRezOut = true;

    [Header("Params View Only")]
    [SerializeField]
    string _currentFolder = string.Empty;
    string currentFolder
    {
        get
        {
            if (_currentFolder != Head.Instance.caseInfo.GetCurrentCaseFolder()) _currentFolder = Head.Instance.caseInfo.GetCurrentCaseFolder();
            return _currentFolder;
        }
        set { _currentFolder = value; }
    }

    [ContextMenu("ResetParams")]
    public void ResetParams()
    {
        _currentFolder = string.Empty;
        Debug.LogWarning("Reseting : " + currentFolder);
    }

    IEnumerator Start()
    {
        yield return new WaitUntil(() => Head.Instance != null);

        ResetParams();
        CheckExisted();
        yield break;
    }

    [Header("Force Create File")]
    public bool forceCheckExisted = true;

    [ContextMenu("CheckExisted")]
    public void CheckExisted()
    {
        if (string.IsNullOrEmpty(filename)) { Debug.Log("filename empty"); return; }

        if (!File.Exists(currentFolder + filename))
        {
            File.Create(currentFolder + filename);
        }
    }

    bool Existed()
    {
        if (isDebug && string.IsNullOrEmpty(filename)) { Debug.Log("filename empty"); return false; }
        bool existed = File.Exists(currentFolder + filename);
        return existed;
    }

    [Header("Empty")]
    public bool doneEmpty = true;

    [ContextMenu("Empty")]
    public void Empty()
    {
        StartCoroutine(C_Empty());
    }

    IEnumerator C_Empty()
    {
        if (isDebug) Debug.Log("Start Empty !!");

        if (isDebug && string.IsNullOrEmpty(filename)) { Debug.Log("filename empty"); yield break; }

        if (!File.Exists(currentFolder + filename))
        {
            File.Create(currentFolder + filename);
            yield return new WaitForSeconds(2f);
        }

        yield return new WaitUntil(() => Existed() == true);

        doneEmpty = false;

        yield return new WaitUntil(() => IsFileLocked(new FileInfo(currentFolder + filename)) == false);
        File.WriteAllText(currentFolder + filename, new RezData(Vector3.zero).toJson());
        yield return new WaitUntil(() => IsFileLocked(new FileInfo(currentFolder + filename)) == false);

        doneEmpty = true;
        if (isDebug) Debug.Log("Done Empty");

        yield break;
    }

    [Serializable]
    public class RezData
    {
        public Vector3 pos = Vector3.zero;
        public RezData() { }
        public RezData(Vector3 pos) { this.pos = pos; }
        public string toJson() { return JsonUtility.ToJson(this); }
    }

    [ContextMenu("RezIn")]
    public void RezIn()
    {
        if (File.Exists(currentFolder + filename))
            transform.position = JsonUtility.FromJson<RezData>(File.ReadAllText(currentFolder + filename)).pos;
    }

    public void RezIn(string path)
    {
        if (File.Exists(path))
            transform.position = JsonUtility.FromJson<RezData>(File.ReadAllText(path)).pos;
    }

    [ContextMenu("RezOut_Current")]
    public void RezOut_Current()
    {
        StartCoroutine(C_RezOut());
    }

    IEnumerator C_RezOut()
    {
        if (isDebug) Debug.Log("Start RezOut, position : " + transform.position);
        if (isDebug && string.IsNullOrEmpty(filename)) { Debug.Log("filename empty"); yield break; }

        doneRezOut = false;
        string output = new RezData(transform.position).toJson();

        if (File.Exists(currentFolder + filename)) yield return new WaitUntil(() => IsFileLocked(new FileInfo(currentFolder + filename)) == false);
        File.WriteAllText(currentFolder + filename, output);
        yield return new WaitUntil(() => IsFileLocked(new FileInfo(currentFolder + filename)) == false);

        doneRezOut = true;
        if (isDebug) Debug.Log("Done C_RezOut");

        yield break;
    }

    [ContextMenu("Debug_Current")]
    public void Debug_Current()
    {
        Debug.Log(new RezData(transform.position).toJson());
    }

    bool IsFileLocked(FileInfo file)
    {
        FileStream stream = null;

        try
        {
            stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
        }
        catch (IOException)
        {
            return true;
        }
        finally
        {
            if (stream != null)
                stream.Close();
        }

        return false;
    }

    [ContextMenu("Stop")]
    public void Stop()
    {
        StopAllCoroutines();
    }

    [Header("Delete")]
    public bool doneDelete = true;

    [ContextMenu("DeleteCurrentFile")]
    public void DeleteCurrentFile()
    {
        StartCoroutine(C_DeleteCurrentFile());
    }

    IEnumerator C_DeleteCurrentFile()
    {
        doneDelete = false;

        if (File.Exists(currentFolder + filename)) File.Delete(currentFolder + filename);
        else Debug.Log(currentFolder + filename + " not existed");

        yield return new WaitUntil(() => File.Exists(currentFolder + filename) == false);

        doneDelete = true;

        yield break;
    }
}
