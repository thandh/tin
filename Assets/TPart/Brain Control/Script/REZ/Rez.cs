﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rez : Singleton<Rez>
{
    [Header("Params")]
    public SphereCollider sc = null;
    public bool boolAllowMove = false;

    [Header("Outputs")]
    public float radius = 0f;

    [ContextMenu("CalculateLength")]
    public void CalculateLength()
    {
        radius = sc.radius * transform.localScale.x;
    }
}
