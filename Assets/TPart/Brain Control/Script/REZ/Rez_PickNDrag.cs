﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class Rez_PickNDrag : MonoBehaviour
{
    [Header("Process")]
    public bool isDebug = false;

    [Header("Lock")]
    public bool isLock = true;

    [Header("Pick and drag")]
    public LayerMask maskRez = -99;    

    public Transform lastHit = null;
    public Rez rezTr = null;
    public Vector3 lastPos = -Vector3.one;
    public Vector3 lastMousePos = -Vector3.one;

    [HideInInspector]
    public float stepMove = 1f;

    private void Start()
    {
        maskRez = LayerMask.GetMask("Rez");        
    }

    [ContextMenu("Stop")]
    public void Stop()
    {
        StopAllCoroutines();        
    }    

    GameObject rezGuidance = null;
    public float stepPos = 0.5f;

    private void Update()
    {
        if (isLock) return;

        if (Input.GetMouseButton(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);            

            if (lastHit == null && lastPos == -Vector3.one && lastMousePos == -Vector3.one)
            {
                if (Physics.Raycast(ray, out hit, Mathf.Infinity, maskRez.value))
                {
                    if (lastHit == null)
                    {
                        lastHit = hit.transform;
                        lastPos = hit.transform.position;
                        lastMousePos = ray.origin;

                        rezGuidance = new GameObject();
                        rezGuidance.transform.position = lastPos;
                        return;
                    }
                }
            }
            else if (lastHit != null && lastPos != -Vector3.one && lastMousePos != -Vector3.one)
            {
                if (!lastHit.GetComponent<Rez>().boolAllowMove) return;

                Vector3 reflect = (ray.origin - lastMousePos) * (lastPos - Camera.main.transform.position).magnitude / (lastMousePos - Camera.main.transform.position).magnitude;
                rezGuidance.transform.position = lastPos + reflect;

                float step = Time.deltaTime * stepPos;
                lastHit.transform.position = new Vector3(Mathf.Lerp(lastHit.transform.position.x, rezGuidance.transform.position.x, step),
                    Mathf.Lerp(lastHit.transform.position.y, rezGuidance.transform.position.y, step),
                    Mathf.Lerp(lastHit.transform.position.z, rezGuidance.transform.position.z, step));
            }
        }
        else
        {
            if (lastHit == null) return;            

            lastHit = null;            
            lastPos = -Vector3.one;
            lastMousePos = -Vector3.one;
            DestroyImmediate(rezGuidance);            
        }
    }    
}
