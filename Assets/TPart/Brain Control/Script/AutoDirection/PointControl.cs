﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class PointControl : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Index")]
    [SerializeField] MeshFilter mf = null;
    [SerializeField] SkinnedMeshRenderer smr = null;
    Mesh _m = null;
    Mesh m { get {
            if (_m == null)
            {
                if (mf != null) _m = mf.sharedMesh;
                else if (smr != null) _m = smr.sharedMesh;
            }
            return _m; } }

    [ContextMenu("ResetMesh")]
    public void ResetMesh()
    {
        _m = null;
    }

    public Vector3 oriIndex = Vector3.zero;
    public Vector3 truncIndex = Vector3.zero;

    [Header("Origin")]
    public Transform oriPos0 = null;
    public Transform oriPos1 = null;
    public Transform oriPos2 = null;
    public Transform oriNormalCube = null;
    public Transform oriNormalCube1 = null;
    public Vector3 oriNormalCubeLookDirection = Vector3.zero;
    Plane oriPlane;

    [Header("Truncate")]
    public Transform truncPos0 = null;
    public Transform truncPos1 = null;
    public Transform truncPos2 = null;
    public Transform truncNormalCube = null;
    public Transform truncNormalCube1 = null;
    public Vector3 truncNormalCubeLookDirection = Vector3.zero;
    Plane truncPlane;

    public void Reset0()
    {
        oriIndex = -Vector3.one;
        truncIndex = -Vector3.one;

        oriPos0.position = Vector3.zero;
        oriPos1.position = Vector3.zero;
        oriPos2.position = Vector3.zero;
        oriNormalCube.position = Vector3.zero;
        oriNormalCube1.position = Vector3.zero;
        oriNormalCubeLookDirection = Vector3.zero;

        truncPos0.position = Vector3.zero;
        truncPos1.position = Vector3.zero;
        truncPos2.position = Vector3.zero;
        truncNormalCube.position = Vector3.zero;
        truncNormalCube1.position = Vector3.zero;
        truncNormalCubeLookDirection = Vector3.zero;
    }

    public void SetIndex(int iPos0, int iPos1, int iPos2)
    {
        oriIndex = new Vector3(iPos0, iPos1, iPos2);
        truncIndex = new Vector3(iPos0, iPos1, iPos2);
    }

    [ContextMenu("GetInfoOrigin_byVert")]
    public void GetInfoOrigin_byVert()
    {
        oriPos0.position = transform.TransformPoint(m.vertices[(int)oriIndex.x]);
        oriPos1.position = transform.TransformPoint(m.vertices[(int)oriIndex.y]);
        oriPos2.position = transform.TransformPoint(m.vertices[(int)oriIndex.z]);

        oriPlane = new Plane(oriPos0.position, oriPos1.position, oriPos2.position);

        oriNormalCube.position = oriPos0.position;
        oriNormalCube1.position = oriPos0.position;
        oriNormalCubeLookDirection = Vector3.Cross((oriPos1.position - oriPos0.position), (oriPos2.position - oriPos0.position)).normalized;
        oriNormalCube.rotation = Quaternion.LookRotation(oriNormalCubeLookDirection);
        oriNormalCube1.rotation = oriNormalCube.rotation;
        oriNormalCube1.Rotate(new Vector3(0, 90, 0));

        Debug.Log("oriNormalCube rotation " + oriNormalCube.rotation);
    }

    [ContextMenu("GetInfoTruncate_byVert")]
    public void GetInfoTruncate_byVert()
    {
        truncPos0.position = transform.TransformPoint(m.vertices[(int)truncIndex.x]);
        truncPos1.position = transform.TransformPoint(m.vertices[(int)truncIndex.y]);
        truncPos2.position = transform.TransformPoint(m.vertices[(int)truncIndex.z]);

        truncPlane = new Plane(truncPos0.position, truncPos1.position, truncPos2.position);        
        truncNormalCube.position = truncPos0.position;
        truncNormalCube1.position = truncPos0.position;
        truncNormalCubeLookDirection = Vector3.Cross((truncPos1.position - truncPos0.position), (truncPos2.position - truncPos0.position)).normalized;
        truncNormalCube.rotation = Quaternion.LookRotation(truncNormalCubeLookDirection);
        truncNormalCube1.rotation = truncNormalCube.rotation;
        truncNormalCube1.Rotate(new Vector3(0, 90, 0));

        Debug.Log("truncNormalCube rotation " + truncNormalCube.rotation);
    }

    [ContextMenu("GetInfoOrigin_byTris")]
    public void GetInfoOrigin_byTris()
    {
        oriPos0.position = transform.TransformPoint(CenterTris(m.vertices, m.triangles, (int)oriIndex.x));
        oriPos1.position = transform.TransformPoint(m.vertices[(int)oriIndex.y]);
        oriPos2.position = transform.TransformPoint(m.vertices[(int)oriIndex.z]);

        oriPlane = new Plane(oriPos0.position, oriPos1.position, oriPos2.position);
        oriNormalCube.position = oriPos0.position;
        oriNormalCube1.position = oriPos0.position;
        oriNormalCubeLookDirection = Vector3.Cross((oriPos1.position - oriPos0.position), (oriPos2.position - oriPos0.position)).normalized;
        oriNormalCube.rotation = Quaternion.LookRotation(oriNormalCubeLookDirection);
        oriNormalCube1.rotation = oriNormalCube.rotation;
        oriNormalCube1.Rotate(new Vector3(0, 90, 0));

        Debug.Log("oriNormalCube rotation " + oriNormalCube.rotation);
    }

    [ContextMenu("GetInfoTruncate_byTris")]
    public void GetInfoTruncate_byTris()
    {
        truncPos0.position = transform.TransformPoint(CenterTris(m.vertices, m.triangles, (int)truncIndex.x));
        truncPos1.position = transform.TransformPoint(m.vertices[(int)truncIndex.y]);
        truncPos2.position = transform.TransformPoint(m.vertices[(int)truncIndex.z]);
                
        truncPlane = new Plane(truncPos0.position, truncPos1.position, truncPos2.position);
        truncNormalCube.position = truncPos0.position;
        truncNormalCube1.position = truncPos0.position;
        truncNormalCubeLookDirection = Vector3.Cross((truncPos1.position - truncPos0.position), (truncPos2.position - truncPos0.position)).normalized;
        truncNormalCube.rotation = Quaternion.LookRotation(truncNormalCubeLookDirection);
        truncNormalCube1.rotation = truncNormalCube.rotation;
        truncNormalCube1.Rotate(new Vector3(0, 90, 0));

        Debug.Log("truncNormalCube rotation " + truncNormalCube.rotation);
    }

    Vector3 CenterTris(Vector3[] verts, int[] tris, int index)
    {
        Vector3 center = Vector3.zero;
        Vector3 pos0 = verts[tris[index * 3]];
        Vector3 pos1 = verts[tris[index * 3 + 1]];
        Vector3 pos2 = verts[tris[index * 3 + 2]];
        center = (pos0 + pos1 + pos2) / 3f;
        return center;
    }

    public void oriOnOff(bool On)
    {
        oriPos0.GetComponent<MeshRenderer>().enabled = On;
        oriPos1.GetComponent<MeshRenderer>().enabled = On;
        oriPos2.GetComponent<MeshRenderer>().enabled = On;
        oriNormalCube.GetComponent<MeshRenderer>().enabled = On;
        oriNormalCube1.GetComponent<MeshRenderer>().enabled = On;        
    }

    public void truncOnOff(bool On)
    {
        truncPos0.GetComponent<MeshRenderer>().enabled = On;
        truncPos1.GetComponent<MeshRenderer>().enabled = On;
        truncPos2.GetComponent<MeshRenderer>().enabled = On;
        truncNormalCube.GetComponent<MeshRenderer>().enabled = On;
        truncNormalCube1.GetComponent<MeshRenderer>().enabled = On;
    }

    [ContextMenu("SnapAngle")]
    public void SnapAngle()
    {
        Quaternion dif = Quaternion.FromToRotation(oriNormalCube.rotation.eulerAngles, truncNormalCube.rotation.eulerAngles);
        transform.Rotate(dif.eulerAngles);

        //float difX = Quaternion.
        //Angle(oriNormalCube.rotation, truncNormalCube.rotation);
    }

    [ContextMenu("SnapAngle_Update")]
    public void SnapAngle_Update()
    {
        SnapPosition();

        lasteulerAngle = Vector3.zero;
        stepRot = stepRotBegin;
        deltaDis = deltaDisBegin;

        boolUpdate = true;
        boolSnaping = true;
    }

    [ContextMenu("SnapPosition")]
    public void SnapPosition()
    {   
        transform.position += oriPos0.position - truncPos0.position;
    }

    [Header("Update to snap Angle")]
    public bool boolUpdate = false;
    public bool boolSnaping = false;

    Vector3 lasteulerAngle = Vector3.zero;

    [Header("Snap angle params")]
    [SerializeField] float stepRot = 0.1f;
    [SerializeField] float deltaDis = 1f;

    [SerializeField] float stepRotBegin = 1f;
    [SerializeField] float deltaDisBegin = 1f;

    [SerializeField] float stepRotFinal = 0.01f;
    [SerializeField] float deltaDisFinal = 0.01f;

    [SerializeField] float deltaDelta = 0.01f;

    [SerializeField] float multiplyDeltaTime = 1f;    

    private void Update()
    {
        if (!boolUpdate) return;

        if (boolSnaping)
        {   
            if (Mathf.Abs(stepRot - stepRotFinal) < deltaDelta && Mathf.Abs(deltaDis - deltaDisFinal) < deltaDelta)
            {
                Debug.Log("Done update snaping " + TotalDistance());
                boolUpdate = false;
                boolSnaping = false;
            }

            bool has = false;
            if (TotalDistance() > deltaDis)
            {
                if (lasteulerAngle != new Vector3(0, 0, -stepRot))
                    has = TryRotate(new Vector3(stepRot, 0, 0));

                if (has)
                {
                    if (isDebug) Debug.Log("Rotate X");// + new Vector3(stepRot, 0, 0));
                    DoRotate(new Vector3(stepRot, 0, 0)); lasteulerAngle = new Vector3(stepRot, 0, 0); 
                }
                else
                {
                    if (lasteulerAngle != new Vector3(stepRot, 0, 0))
                        has = TryRotate(new Vector3(-stepRot, 0, 0));

                    if (has)
                    {
                        if (isDebug) Debug.Log("Rotate NX");// + new Vector3(-stepRot, 0, 0));
                        DoRotate(new Vector3(-stepRot, 0, 0)); lasteulerAngle = new Vector3(-stepRot, 0, 0); 
                    }
                    else
                    {
                        if (lasteulerAngle != new Vector3(-stepRot, 0, 0))
                            has = TryRotate(new Vector3(0, stepRot, 0));

                        if (has)
                        {
                            if (isDebug) Debug.Log("Rotate Y");// + new Vector3(0, stepRot, 0));
                            DoRotate(new Vector3(0, stepRot, 0)); lasteulerAngle = new Vector3(0, stepRot, 0); 
                        }
                        else
                        {
                            if (lasteulerAngle != new Vector3(0, stepRot, 0))
                                has = TryRotate(new Vector3(0, -stepRot, 0));

                            if (has)
                            {
                                if (isDebug) Debug.Log("Rotate NY");// + new Vector3(0, -stepRot, 0));
                                DoRotate(new Vector3(0, -stepRot, 0)); lasteulerAngle = new Vector3(0, -stepRot, 0); 
                            }
                            else
                            {
                                if (lasteulerAngle != new Vector3(0, -stepRot, 0))
                                    has = TryRotate(new Vector3(0, 0, stepRot));

                                if (has)
                                {
                                    if (isDebug) Debug.Log("Rotate Z");// + new Vector3(0, 0, stepRot));
                                    DoRotate(new Vector3(0, 0, stepRot)); lasteulerAngle = new Vector3(0, 0, stepRot); 
                                }
                                else
                                {
                                    if (lasteulerAngle != new Vector3(0, 0, stepRot))
                                        has = TryRotate(new Vector3(0, 0, -stepRot));

                                    if (has)
                                    {
                                        if (isDebug) Debug.Log("Rotate NZ");// + new Vector3(0, 0, -stepRot));
                                        DoRotate(new Vector3(0, 0, -stepRot)); lasteulerAngle = new Vector3(0, 0, -stepRot); 
                                    }
                                    else
                                    {
                                        if (stepRot != stepRotFinal || deltaDis != deltaDisFinal)
                                        {
                                            stepRot = Mathf.Lerp(stepRot, stepRotFinal, Time.deltaTime);
                                            deltaDis = Mathf.Lerp(deltaDis, deltaDisFinal, Time.deltaTime);
                                        }
                                        else
                                        {
                                            Debug.Log("Done update snaping " + TotalDistance());
                                            boolUpdate = false;
                                            boolSnaping = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                if (stepRot != stepRotFinal || deltaDis != deltaDisFinal)
                {
                    stepRot = Mathf.Lerp(stepRot, stepRotFinal, Time.deltaTime);
                    deltaDis = Mathf.Lerp(deltaDis, deltaDisFinal, Time.deltaTime);
                }
                else
                {
                    Debug.Log("Done update snaping " + TotalDistance());
                    boolUpdate = false;
                    boolSnaping = false;
                }
            }
        }
    }
    
    [Header("Coroutine to snap Angle")]
    public bool doneAngleCoroutine = true;
    public float deltaAngle = 0.05f;
    /// <summary>
    /// Hàm này thay thế cái update ở trên khi chạy runtime
    /// </summary>
    [ContextMenu("SnapAngle_Coroutine")]
    public void SnapAngle_Coroutine()
    {
        StartCoroutine(C_Snap1());
    }

    IEnumerator C_Snap()
    {
        doneAngleCoroutine = false;

        while (Mathf.Abs(stepRot - stepRotFinal) > deltaDelta && Mathf.Abs(deltaDis - deltaDisFinal) > deltaDelta)
        {
            bool has = false;
            while (TotalDistance() > deltaDis)
            {
                if (lasteulerAngle != new Vector3(0, 0, -stepRot))
                {
                    StartCoroutine(C_TryRotate(new Vector3(stepRot, 0, 0)));
                    yield return new WaitUntil(() => doneC_TryRotate == true);
                    has = boolC_TryRotate;
                }

                if (has)
                {
                    if (isDebug) Debug.Log("Rotate X");// + new Vector3(stepRot, 0, 0));
                    StartCoroutine(C_DoRotate(new Vector3(stepRot, 0, 0)));
                    yield return new WaitUntil(() => doneC_DoRotate == true);
                    lasteulerAngle = new Vector3(stepRot, 0, 0);
                }
                else
                {
                    if (lasteulerAngle != new Vector3(stepRot, 0, 0))
                    {
                        StartCoroutine(C_TryRotate(new Vector3(-stepRot, 0, 0)));
                        yield return new WaitUntil(() => doneC_TryRotate == true);
                        has = boolC_TryRotate;
                    }

                    if (has)
                    {
                        if (isDebug) Debug.Log("Rotate NX");// + new Vector3(-stepRot, 0, 0));
                        StartCoroutine(C_DoRotate(new Vector3(-stepRot, 0, 0)));
                        yield return new WaitUntil(() => doneC_DoRotate == true);
                        lasteulerAngle = new Vector3(-stepRot, 0, 0);
                    }
                    else
                    {
                        if (lasteulerAngle != new Vector3(-stepRot, 0, 0))
                        {
                            StartCoroutine(C_TryRotate(new Vector3(0, stepRot, 0)));
                            yield return new WaitUntil(() => doneC_TryRotate == true);
                            has = boolC_TryRotate;
                        }

                        if (has)
                        {
                            if (isDebug) Debug.Log("Rotate Y");// + new Vector3(0, stepRot, 0));
                            StartCoroutine(C_DoRotate(new Vector3(0, stepRot, 0)));
                            yield return new WaitUntil(() => doneC_DoRotate == true);
                            lasteulerAngle = new Vector3(0, stepRot, 0);
                        }
                        else
                        {
                            if (lasteulerAngle != new Vector3(0, stepRot, 0))
                            {
                                StartCoroutine(C_TryRotate(new Vector3(0, -stepRot, 0)));
                                yield return new WaitUntil(() => doneC_TryRotate == true);
                                has = boolC_TryRotate;
                            }

                            if (has)
                            {
                                if (isDebug) Debug.Log("Rotate NY");// + new Vector3(0, -stepRot, 0));
                                StartCoroutine(C_DoRotate(new Vector3(0, -stepRot, 0)));
                                yield return new WaitUntil(() => doneC_DoRotate == true);
                                lasteulerAngle = new Vector3(0, -stepRot, 0);
                            }
                            else
                            {
                                if (lasteulerAngle != new Vector3(0, -stepRot, 0))
                                {
                                    StartCoroutine(C_TryRotate(new Vector3(0, 0, stepRot)));
                                    yield return new WaitUntil(() => doneC_TryRotate == true);
                                    has = boolC_TryRotate;
                                }

                                if (has)
                                {
                                    if (isDebug) Debug.Log("Rotate Z");// + new Vector3(0, 0, stepRot));
                                    StartCoroutine(C_DoRotate(new Vector3(0, 0, stepRot)));
                                    yield return new WaitUntil(() => doneC_DoRotate == true);
                                    lasteulerAngle = new Vector3(0, 0, stepRot);
                                }
                                else
                                {
                                    if (lasteulerAngle != new Vector3(0, 0, stepRot))
                                    {
                                        StartCoroutine(C_TryRotate(new Vector3(0, 0, -stepRot)));
                                        yield return new WaitUntil(() => doneC_TryRotate == true);
                                        has = boolC_TryRotate;
                                    }

                                    if (has)
                                    {
                                        if (isDebug) Debug.Log("Rotate NZ");// + new Vector3(0, 0, -stepRot));
                                        StartCoroutine(C_DoRotate(new Vector3(0, 0, -stepRot)));
                                        yield return new WaitUntil(() => doneC_DoRotate == true);
                                        lasteulerAngle = new Vector3(0, 0, -stepRot);
                                    }                                    
                                }
                            }
                        }
                    }
                }
            }            
            
            if (stepRot != stepRotFinal || deltaDis != deltaDisFinal)
            {
                stepRot = Mathf.Lerp(stepRot, stepRotFinal, Time.deltaTime);
                deltaDis = Mathf.Lerp(deltaDis, deltaDisFinal, Time.deltaTime);
            }
        }

        doneAngleCoroutine = true;

        Debug.Log("Done update snaping " + TotalDistance());        

        yield break;
    }

    /// <summary>
    /// Use array
    /// </summary>
    /// <returns></returns>

    Vector3[] dirAr = new Vector3[6] { new Vector3(1, 0, 0), new Vector3(-1, 0, 0), new Vector3(0, 1, 0), new Vector3(0, -1, 0), new Vector3(0, 0, 1), new Vector3(0, 0, -1) };
    [SerializeField] bool[] hasAr = new bool[6] { true, true, true, true, true, true };
    [SerializeField] int iDirection = 0;

    IEnumerator C_Snap1()
    {
        doneAngleCoroutine = false;

        bool boolbreak = false;

        deltaDis = deltaDisBegin;
        stepRot = stepRotBegin;

        while (Mathf.Abs(stepRot - stepRotFinal) > deltaDelta && Mathf.Abs(deltaDis - deltaDisFinal) > deltaDelta)
        {
            bool has = false;

            while (TotalDistance() > deltaDis && !boolbreak)
            {
                StartCoroutine(C_TryRotate(dirAr[iDirection] * stepRot));
                yield return new WaitUntil(() => doneC_TryRotate == true);
                has = boolC_TryRotate;

                while (has)
                {
                    StartCoroutine(C_DoRotate(dirAr[iDirection] * stepRot));
                    yield return new WaitUntil(() => doneC_DoRotate == true);
                    
                    StartCoroutine(C_TryRotate(dirAr[iDirection] * stepRot));
                    yield return new WaitUntil(() => doneC_TryRotate == true);
                    has = boolC_TryRotate;
                    if (has) hasAr = new bool[6] { true, true, true, true, true, true };
                }

                hasAr[iDirection] = false;

                if (TotalDistance() > deltaDis)
                {
                    int count = 0;

                    do { iDirection = (iDirection + 1) % 6; count++; }
                    while (hasAr[iDirection] == false && count <= 6);

                    if (count > 6) boolbreak = true;
                }
            }

            if (stepRot != stepRotFinal || deltaDis != deltaDisFinal)
            {
                stepRot = Mathf.Lerp(stepRot, stepRotFinal, Time.deltaTime * multiplyDeltaTime);
                deltaDis = Mathf.Lerp(deltaDis, deltaDisFinal, Time.deltaTime * multiplyDeltaTime);

                boolbreak = false;
                hasAr = new bool[6] { true, true, true, true, true, true };
            }
        }

        doneAngleCoroutine = true;

        yield break;
    }

    bool TryRotate(Vector3 eulerAngle)
    {
        if (isDebug) Debug.Log("TryRotate " + eulerAngle);

        SnapPosition();

        Vector3 lastPos = transform.position;
        Quaternion lastQua = transform.rotation;

        float lastDistance = TotalDistance();

        DoRotate(eulerAngle);

        float currentDistance = TotalDistance();

        //Debug.Log("eulerAngle " + eulerAngle + " last dis " + lastDistance + " current dis " + currentDistance);

        if (!Application.isPlaying)
        {
            transform.position = lastPos;
            transform.rotation = lastQua;
        }
        
        return currentDistance < lastDistance;
    }

    private bool doneC_TryRotate = true;
    private bool boolC_TryRotate = false;

    IEnumerator C_TryRotate(Vector3 eulerAngle)
    {
        doneC_TryRotate = false;

        if (isDebug) Debug.Log("C_TryRotate " + eulerAngle);

        SnapPosition();

        Vector3 lastPos = transform.position;
        Quaternion lastQua = transform.rotation;

        float lastDistance = TotalDistance();

        StartCoroutine(C_DoRotate(eulerAngle));
        yield return new WaitUntil(() => doneC_DoRotate == true);

        float currentDistance = TotalDistance();

        //Debug.Log("eulerAngle " + eulerAngle + " last dis " + lastDistance + " current dis " + currentDistance);

        if (!Application.isPlaying)
        {
            transform.position = lastPos;
            transform.rotation = lastQua;
        }

        boolC_TryRotate = currentDistance < lastDistance;

        doneC_TryRotate = true;

        yield break;
    }

    [ContextMenu("CheckALLC_TryRotate")]
    public void CheckALLC_TryRotate()
    {
        StartCoroutine(C_CheckALLC_TryRotate());
    }

    IEnumerator C_CheckALLC_TryRotate()
    {
        StartCoroutine(C_TryRotate(new Vector3(stepRot, 0, 0)));
        yield return new WaitUntil(() => doneC_TryRotate == true);
        if (isDebug) Debug.Log("X " + doneC_TryRotate);

        StartCoroutine(C_TryRotate(new Vector3(-stepRot, 0, 0)));
        yield return new WaitUntil(() => doneC_TryRotate == true);
        if (isDebug) Debug.Log("NX " + doneC_TryRotate);

        StartCoroutine(C_TryRotate(new Vector3(0, stepRot, 0)));
        yield return new WaitUntil(() => doneC_TryRotate == true);
        if (isDebug) Debug.Log("Y " + doneC_TryRotate);

        StartCoroutine(C_TryRotate(new Vector3(0, -stepRot, 0)));
        yield return new WaitUntil(() => doneC_TryRotate == true);
        if (isDebug) Debug.Log("NY " + doneC_TryRotate);

        StartCoroutine(C_TryRotate(new Vector3(0, 0, stepRot)));
        yield return new WaitUntil(() => doneC_TryRotate == true);
        if (isDebug) Debug.Log("Z " + doneC_TryRotate);

        StartCoroutine(C_TryRotate(new Vector3(0, 0, -stepRot)));
        yield return new WaitUntil(() => doneC_TryRotate == true);
        if (isDebug) Debug.Log("NZ " + doneC_TryRotate);
    }

    void DoRotate(Vector3 eulerAngle)
    {   
        transform.eulerAngles += eulerAngle;

        SnapPosition();

        transform.localPosition += new Vector3(1f, 0, 0);
        transform.localPosition -= new Vector3(1f, 0, 0);
    }

    private bool doneC_DoRotate = true;

    IEnumerator C_DoRotate(Vector3 eulerAngle)
    {
        doneC_DoRotate = false;

        if (isDebug) Debug.Log("C_DoRotate " + eulerAngle);

        Vector3 lastEulerAngles = transform.eulerAngles;
        //transform.eulerAngles += eulerAngle;
        transform.Rotate(eulerAngle);

        yield return new WaitUntil(() => transform.eulerAngles != lasteulerAngle);
            
        if (isDebug) Debug.Log("done waiting angle " + eulerAngle + " " + transform.eulerAngles + " " + lasteulerAngle);    

        SnapPosition();

        doneC_DoRotate = true;

        yield break;
    }

    float TotalDistance()
    {
        return ((oriPos0.position - truncPos0.position).magnitude + (oriPos1.position - truncPos1.position).magnitude + (oriPos2.position - truncPos2.position).magnitude);
    }

    [Header("Rotate Test")]
    public Vector3 eulerAngle_Test = Vector3.zero;

    [ContextMenu("RotateTest")]
    public void RotateTest()
    {
        transform.Rotate(eulerAngle_Test);
    }

    [ContextMenu("Stop")]
    public void Stop()
    {
        StopAllCoroutines();
    }
}
