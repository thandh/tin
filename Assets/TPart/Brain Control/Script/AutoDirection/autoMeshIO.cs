﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace tMeshIO
{
    public class autoMeshIO : MonoBehaviour
    {   
        public MeshFilter mf = null;
        public MeshOut mo = null;

        [Header("Params")]
        public Vector3 origin = Vector3.zero;
        public Transform trOrigin = null;

        public int iPos0 = -1; 
        public int iPos1 = -1;
        public int iPos2 = -1;        

        [Header("GetParams")]
        public bool doneGetParams = true;

        [ContextMenu("GetParams")]
        public void GetParams()
        {            
            StartCoroutine(C_GetParams());
        }
        
        IEnumerator C_GetParams()
        {
            Debug.Log("start C_GetParams");
            
            doneGetParams = false;
            float time = Time.time;

            origin = Vector3.zero;
            iPos0 = -1; iPos1 = -1; iPos2 = -1;

            int iToPlane0 = -1; int iToPlane1 = -1; int iToPlane2 = -1;
            int iToPlane3 = -1; int iToPlane4 = -1; int iToPlane5 = -1;
            
            float disToPlane0 = float.MaxValue; float disToPlane1 = float.MaxValue; float disToPlane2 = float.MaxValue;
            float disToPlane3 = float.MaxValue; float disToPlane4 = float.MaxValue; float disToPlane5 = float.MaxValue;

            Vector3 worldPos0 = transform.TransformPoint(mf.sharedMesh.vertices[0]);
            Vector3 midPlane0 = worldPos0 + new Vector3(99999f, 0, 0);
            Vector3 midPlane1 = worldPos0 + new Vector3(099999f, 0, 0);
            Vector3 midPlane2 = worldPos0 + new Vector3(0, 99999f, 0);
            Vector3 midPlane3 = worldPos0 + new Vector3(0, -99999f, 0);
            Vector3 midPlane4 = worldPos0 + new Vector3(0, 0, 99999f);
            Vector3 midPlane5 = worldPos0 + new Vector3(0, 0, -99999f);

            Vector3[] vertsWorld = new Vector3[mf.sharedMesh.vertices.Length];

            for (int i = 0; i < vertsWorld.Length; i++)
            {
                vertsWorld[i] = transform.TransformPoint(mf.sharedMesh.vertices[i]);
                origin += vertsWorld[i];

                if (disToPlane0 > (vertsWorld[i] - midPlane0).magnitude) { iToPlane0 = i; disToPlane0 = (vertsWorld[i] - midPlane0).magnitude; }
                if (disToPlane1 > (vertsWorld[i] - midPlane1).magnitude) { iToPlane1 = i; disToPlane1 = (vertsWorld[i] - midPlane1).magnitude; }
                if (disToPlane2 > (vertsWorld[i] - midPlane2).magnitude) { iToPlane2 = i; disToPlane2 = (vertsWorld[i] - midPlane2).magnitude; }
                if (disToPlane3 > (vertsWorld[i] - midPlane3).magnitude) { iToPlane3 = i; disToPlane3 = (vertsWorld[i] - midPlane3).magnitude; }
                if (disToPlane4 > (vertsWorld[i] - midPlane4).magnitude) { iToPlane4 = i; disToPlane4 = (vertsWorld[i] - midPlane4).magnitude; }
                if (disToPlane5 > (vertsWorld[i] - midPlane5).magnitude) { iToPlane5 = i; disToPlane5 = (vertsWorld[i] - midPlane5).magnitude; }
            }

            Debug.Log(iToPlane0 + " " + iToPlane1 + " " + iToPlane2 + " " + iToPlane3 + " " + iToPlane4 + " " + iToPlane5);

            origin /= vertsWorld.Length;

            List<int> arrangedList = arrangedList_ItoPlane(iToPlane0, iToPlane1, iToPlane2, iToPlane3, iToPlane4, iToPlane5,
                disToPlane0, disToPlane1, disToPlane2, disToPlane3, disToPlane4, disToPlane5);

            iPos0 = arrangedList[0]; 
            iPos1 = arrangedList[1]; 
            iPos2 = arrangedList[2]; 

            Debug.Log("start C_MeshOut");
            mo.meshOut(vertsWorld, new tMesh(vertsWorld).toJson());
            Debug.Log("yielding C_MeshOut");
            yield return new WaitUntil(() => mo.donemeshOut == true);
            Debug.Log("done C_MeshOut");

            tMesh tMesh = JsonUtility.FromJson<tMesh>(mo.textAsset.text);
            Debug.Log("MeshOut from : " + vertsWorld.Length + " verts to " + tMesh.verts.list.Count + " verts");

            doneGetParams = true;
            Debug.Log("done C_GetParams in " + (Time.time - time));

            yield break;
        }

        List<int> arrangedList_ItoPlane(int i0, int i1, int i2, int i3, int i4, int i5, 
            float dis0, float dis1, float dis2, float dis3, float dis4, float dis5)
        {
            List<int> listIndex = new List<int> { i0, i1, i2, i3, i4, i5 };
            List<float> listDistance = new List<float> { dis0, dis1, dis2, dis3, dis4, dis5 };
            
            for (int i = 0; i < listIndex.Count - 1; i++)
                for (int j = i + 1; j < listIndex.Count; j++)
                {
                    if (listDistance[i] < listDistance[j])
                    {
                        int itemp = listIndex[i]; float dtemp = listDistance[i];
                        listIndex[i] = listIndex[j]; listDistance[i] = listDistance[j];
                        listIndex[j] = itemp; listDistance[j] = dtemp;
                    }
                }

            return listIndex;
        }

        [Header("Params View Only")]
        [SerializeField] string _currentFolder = string.Empty;
        string currentFolder { get { if (_currentFolder != Head.Instance.caseInfo.GetCurrentCaseFolder()) _currentFolder = Head.Instance.caseInfo.GetCurrentCaseFolder(); return _currentFolder; } }

        [ContextMenu("MeshIn")]
        public void MeshIn()
        {
            StreamReader sr = new StreamReader(currentFolder + mo.filename);
            //tMesh tMesh = JsonUtility.FromJson<tMesh>(mo.textAsset.text);
            tMesh tMesh = JsonUtility.FromJson<tMesh>(sr.ReadToEnd());
            sr.Close();

            Debug.Log("MeshIn : " + tMesh.verts.list.Count + " verts");

            Vector3[] vertsLocal = tMesh.GetVerts();
            for (int i = 0; i < vertsLocal.Length; i++)
            {
                vertsLocal[i] = transform.InverseTransformPoint(vertsLocal[i]);
            }

            mf.sharedMesh.vertices = vertsLocal;
            mf.sharedMesh.RecalculateBounds();
        }

        [ContextMenu("CalculateOrigin")]
        public void CalculateOrigin()
        {
            Vector3[] vertsWorld = new Vector3[mf.sharedMesh.vertices.Length];

            for (int i = 0; i < vertsWorld.Length; i++)
            {
                vertsWorld[i] = transform.TransformPoint(mf.sharedMesh.vertices[i]);
                origin += vertsWorld[i];                
            }

            origin /= vertsWorld.Length;
        }

        [ContextMenu("PasteToOrigin")]
        public void PasteToOrigin()
        {
            trOrigin.position = origin;
            //trOrigin.rotation = transform.TransformVector(mf.sharedMesh.bounds.)
        }
    }
}