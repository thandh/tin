﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class tMeshVert
{
    public float x = 0;
    public float y = 0;
    public float z = 0;
    public tMeshVert() { }
    public tMeshVert(Vector3 v) { this.x = v.x; this.y = v.y; this.z = v.z; }
    public Vector3 GetVector3() { return new Vector3(x, y, z); }
}

[Serializable]
public class tMeshVertList
{
    public List<tMeshVert> list = new List<tMeshVert>();
    public tMeshVertList() { }
    public tMeshVertList(Vector3[] verts)
    {
        this.list = new List<tMeshVert>();
        for (int i = 0; i < verts.Length; i++) this.list.Add(new tMeshVert(verts[i]));
    }
}

[Serializable]
public class tMesh
{
    public tMeshVertList verts = new tMeshVertList();
    public tMesh() { }
    public tMesh(Vector3[] verts) { this.verts = new tMeshVertList(verts); }
    public Vector3[] GetVerts()
    {
        Vector3[] verts = new Vector3[this.verts.list.Count];
        for (int i = 0; i < verts.Length; i++) verts[i] = this.verts.list[i].GetVector3();
        return verts;
    }

    public string toJson()
    {
        return JsonUtility.ToJson(this);
    }
}