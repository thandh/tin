﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

[Serializable]
public class centerFileJson
{
    public Vector3 oriPos = Vector3.zero;
    public Quaternion oriRot = Quaternion.identity;
    public centerFileJson() { }
    public centerFileJson(Vector3 oriPos, Quaternion oriRot) { this.oriPos = oriPos; this.oriRot = oriRot; }
    public string toJson() { return JsonUtility.ToJson(this); }
}

public class FileCenterline : MonoBehaviour
{
    public bool isDebug = false;

    [Header("Params")]
    public string filename = string.Empty;

    [Header("Get Params")]
    public bool doneOutFile = true;
    public centerFileJson xfileJson = new centerFileJson();

    [Header("Params View Only")]
    [SerializeField] string _currentFolder = string.Empty;
    string currentFolder { get { if (_currentFolder != Head.Instance.caseInfo.GetCurrentCaseFolder()) _currentFolder = Head.Instance.caseInfo.GetCurrentCaseFolder(); return _currentFolder; } }

    private void Start()
    {
        DoInput();
    }

    public bool doneInput = false;

    [ContextMenu("DoInput")]
    public void DoInput()
    {
        StartCoroutine(C_Input());
    }

    IEnumerator C_Input()
    {
        if (string.IsNullOrEmpty(filename)) { if (isDebug) Debug.Log("filename empty"); yield break; }

        doneInput = false;

        CheckExisted();

        yield return new WaitUntil(() => Existed() == true);
        yield return new WaitUntil(() => doneCheckExisted == true);

        ReadExisted();
        yield return new WaitUntil(() => doneReadExisted == true);

        doneInput = true;

        yield break;
    }

    bool doneCheckExisted = false;

    [ContextMenu("CheckExisted")]
    public void CheckExisted()
    {
        StartCoroutine(C_CheckExisted());
    }

    IEnumerator C_CheckExisted()
    {
        if (string.IsNullOrEmpty(filename)) { if (isDebug) Debug.Log("filename empty"); yield break; }

        doneCheckExisted = false;

        if (isDebug) Debug.Log("Checking " + currentFolder + filename);

        if (!File.Exists(currentFolder + filename))
        {
            StreamWriter sw = new StreamWriter(currentFolder + filename);
            sw.WriteLine(new centerFileJson());
            sw.Close();

            yield return new WaitForSeconds(1f);

            outFile(xfileJson.toJson());
            yield return new WaitUntil(() => doneOutFile == true);
        }

        yield return new WaitForSeconds(1f);

        doneCheckExisted = true;

        yield break;
    }

    bool doneReadExisted = false;

    void ReadExisted()
    {
        StartCoroutine(C_ReadExisted());
    }

    IEnumerator C_ReadExisted()
    {
        if (string.IsNullOrEmpty(filename)) { if (isDebug) Debug.Log("filename empty"); yield break; }

        doneReadExisted = false;

        yield return new WaitUntil(() => Existed() == true);

        StreamReader sr = new StreamReader(currentFolder + filename);
        xfileJson = JsonUtility.FromJson<centerFileJson>(sr.ReadToEnd());
        sr.Close();

        doneReadExisted = true;
    }

    bool Existed()
    {
        if (string.IsNullOrEmpty(filename)) { if (isDebug) Debug.Log("filename empty"); return false; }

        return File.Exists(currentFolder + filename);
    }

    public void outFile(string output)
    {
        StartCoroutine(C_outFile(output));
    }

    IEnumerator C_outFile(string output)
    {
        if (string.IsNullOrEmpty(filename)) { if (isDebug) Debug.Log("filename empty"); yield break; }

        yield return new WaitUntil(() => Existed() == true);

        doneOutFile = false;

        StreamWriter sw = new StreamWriter(currentFolder + filename);
        sw.WriteLine(output);
        sw.Close();

        yield return new WaitUntil(() => IsFileLocked(new FileInfo(currentFolder + filename)) == false);

        doneOutFile = true;

        yield break;
    }

    bool IsFileLocked(FileInfo file)
    {
        FileStream stream = null;

        try
        {
            stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
        }
        catch (IOException)
        {
            return true;
        }
        finally
        {
            if (stream != null)
                stream.Close();
        }

        return false;
    }

    [Header("Out put")]
    public Transform centerNdirection = null;

    [ContextMenu("SetOutput")]
    public void SetOutput()
    {
        centerNdirection.position = xfileJson.oriPos;
        centerNdirection.rotation = xfileJson.oriRot;
    }
}
