﻿using System.Collections;
using System.Collections.Generic;
using tMeshIO;
using UnityEngine;

public class MeshAutoDirection : MonoBehaviour
{
    public bool isDebug = false;
    
    [Header("Start")]
    public bool autoAtStart = true;

    [Header("Mesh")]
    public MeshFilter mf = null;
    public MeshRenderer meshRenderer = null;
    public autoMeshIO autoMeshIO = null;
    public MeshOut meshOut = null;
    public Truncate truncate = null;
    public ShowMeshBounds_EditMode showMeshBounds_EditMode = null;
    public ShowMeshBounds_Runtime showMeshBounds_Runtime = null;
    public Follow followOrigin = null;
    public PointControl pointControl = null;    
    public SetPivotMethod setPivotMethod = null;
    public AutoXPanelMethod autoXPanelMethod = null;
    public BoundingBox boundingBox = null;    
    public OriginOfMesh originOfMesh = null;
    public OriginOfMeshBound originOfMeshBound = null;
    public FileCenterline fileCenterline = null;
    public Transform centerNdirection = null;

    private void Start()
    {
        FullAuto();
    }

    [Header("On Off")]
    public bool isOn = true;

    [ContextMenu("ToggleOnOff")]
    public void ToggleOnOff()
    {
        isOn = !isOn;
        Color c = meshRenderer.sharedMaterials[0].color;
        meshRenderer.sharedMaterials[0].color = isOn ? new Color(c.r, c.g, c.b, 1f) : new Color(c.r, c.g, c.b, .5f);
        //if (Application) showMeshBounds_EditMode.draw = !isOn;
    }

    [ContextMenu("Full Auto")]
    public void FullAuto()
    {
        StartCoroutine(C_FullAuto());
    }

    IEnumerator C_FullAuto()
    {
        if (!autoAtStart)
        {
            if (isDebug) Debug.Log("Case !autoAtStart, just AfterFullAuto()");
            AfterFullAuto();
            yield break;
        }

        if (isDebug) Debug.Log("start yield return new WaitUntil(() => fileCenterline.doneInput == true)");
        yield return new WaitUntil(() => fileCenterline.doneInput == true);

        if (isDebug) Debug.Log("start CheckDoneAutob4()");
        CheckDoneAutob4();

        if (isDebug) Debug.Log("start yield return new WaitUntil(() => doneCheckAutob4 == true)");
        yield return new WaitUntil(() => doneCheckAutob4 == true);
        if (autob4) { fileCenterline.SetOutput(); AfterFullAuto(); yield break; }

        float timeAuto = 0;
        float time = Time.time;

        if (isDebug) Debug.Log("start Full Auto");
        autoXPanelMethod.SayAndClear("start Automatic find Mesh's direction");

        autoXPanelMethod.overlayOn();

        Init();
        if (isDebug) Debug.Log("yielding Init()");
        yield return new WaitUntil(() => doneInit == true);

        AutoDirection();
        if (isDebug) Debug.Log("yielding AutoDirection()");
        yield return new WaitUntil(() => doneAutoDirection == true);

        timeAuto = Time.time - time;
        AfterFullAuto(timeAuto);

        yield break;
    }

    bool autob4 = false;
    bool doneCheckAutob4 = false;

    void CheckDoneAutob4()
    {
        StartCoroutine(C_CheckDoneAutob4());
    }

    IEnumerator C_CheckDoneAutob4()
    {
        doneCheckAutob4 = false;
        autob4 = false;

        yield return new WaitUntil(() => fileCenterline.doneInput == true);

        autob4 = (fileCenterline.xfileJson.oriPos != Vector3.zero) || (fileCenterline.xfileJson.oriRot != Quaternion.identity);

        doneCheckAutob4 = true;
        yield break;
    }

    void AfterFullAuto(float timeAuto = 0)
    {
        Color c = meshRenderer.sharedMaterials[0].color;
        meshRenderer.sharedMaterials[0].color = new Color(c.r, c.g, c.b, 1f);

        if (isDebug) Debug.Log("done auto in " + timeAuto);
        autoXPanelMethod.overlayOff();
        autoXPanelMethod.SayAndClear("done auto in " + timeAuto);
    }

    [Header("Init")]
    public bool doneInit = false;

    [ContextMenu("Init")]
    public void Init()
    {        
        StartCoroutine(C_Init());
    }

    IEnumerator C_Init()
    {
        if (isDebug) Debug.Log("start Init");
        doneInit = false;

        pointControl.Reset0();

        if (isDebug) Debug.Log("start meshOut.Empty();");
        meshOut.Empty();
        yield return new WaitUntil(() => meshOut.doneEmpty == true);
        if (isDebug) Debug.Log("stop meshOut.Empty();");

        pointControl.oriOnOff(false);
        pointControl.truncOnOff(false);

        if (!Application.isPlaying) showMeshBounds_EditMode.draw = true;
        else showMeshBounds_Runtime.draw = true;        

        if (isDebug) Debug.Log("start meshIO_X.MeshOut");
        autoMeshIO.GetParams();
        if (isDebug) Debug.Log("yielding meshIO_X.MeshOut");
        yield return new WaitUntil(() => autoMeshIO.doneGetParams == true);
        if (isDebug) Debug.Log("done meshIO_X.MeshOut");

        ///Chỗ này chỉ lấy được origin of mesh
        autoMeshIO.PasteToOrigin();

        pointControl.SetIndex(autoMeshIO.iPos0, autoMeshIO.iPos1, autoMeshIO.iPos2);

        if (isDebug) Debug.Log("start xPointControl.GetInfoOrigin");
        pointControl.GetInfoOrigin_byVert();
        if (isDebug) Debug.Log("done xPointControl.GetInfoOrigin");

        setPivotMethod.ZeroPosition();
        followOrigin.SnapToTarget();

        pointControl.oriOnOff(true);

        doneInit = true;
        if (isDebug) Debug.Log("done Init");

        yield break;
    }

    [Header("Init")]
    public bool doneAutoDirection = false;

    [ContextMenu("AutoDirection")]
    public void AutoDirection()
    {    
        StartCoroutine(C_AutoDirection());
    }        
    
    IEnumerator C_AutoDirection()
    {
        if (isDebug) Debug.Log("Starting AutoDirection");
        doneAutoDirection = false;

        if (isDebug) Debug.Log("start boundingBox.Truncate");
        truncate.DoTruncate();
        if (isDebug) Debug.Log("yielding boundingBox.Truncate");
        yield return new WaitUntil(() => truncate.boolTruncate == false && truncate.hasX == false && truncate.hasNX == false && truncate.hasY == false && truncate.hasNY == false && truncate.hasZ == false && truncate.hasNZ == false);
        if (isDebug) Debug.Log("done boundingBox.Truncate");

        if (isDebug) Debug.Log("start xPointControl.GetInfoTruncate");
        pointControl.GetInfoTruncate_byVert();
        if (isDebug) Debug.Log("done boundingBox.Truncate");

        if (isDebug) Debug.Log("xPointControl.truncOnOff(true);");
        pointControl.truncOnOff(true);
        
        if (!Application.isPlaying)
        {
            if (isDebug) Debug.Log("start xPointControl.GetInfoTruncate");
            pointControl.SnapAngle_Update();

            if (isDebug) Debug.Log("yielding xPointControl.SnapAngle Update");
            yield return new WaitUntil(() => pointControl.boolUpdate == false && pointControl.boolSnaping == false);
        }
        else
        {
            if (isDebug) Debug.Log("start xPointControl.SnapAngle_Courotine");
            pointControl.SnapAngle_Coroutine();

            if (isDebug) Debug.Log("yielding xPointControl.SnapAngle Coroutine");
            yield return new WaitUntil(() => pointControl.doneAngleCoroutine == true);
        }
        if (isDebug) Debug.Log("done xPointControl.SnapAngle");

        if (isDebug) Debug.Log("Local positon = 0");
        setPivotMethod.SetPivotTo(transform.parent.position);

        if (isDebug) Debug.Log("originOfMeshBound.Center();");
        originOfMeshBound.Center();

        if (isDebug) Debug.Log("boundingBox.DefinePeek()");
        boundingBox.DefinePeek();        

        pointControl.oriOnOff(false);
        pointControl.truncOnOff(false);

        if (!Application.isPlaying) showMeshBounds_EditMode.draw = false;
        else showMeshBounds_Runtime.draw = false;

        if (isDebug) Debug.Log("start x_fileCenterline.outFile");

        centerFileJson centerlineFile = new centerFileJson();
        centerlineFile.oriPos = boundingBox.meshCenterBound;
        centerlineFile.oriRot = boundingBox.QuaternionOfTheLongest();

        centerNdirection.position = boundingBox.meshCenterBound; ;
        centerNdirection.rotation = boundingBox.QuaternionOfTheLongest(); ;

        fileCenterline.outFile(centerlineFile.toJson());
        yield return new WaitUntil(() => fileCenterline.doneOutFile == true);

        if (isDebug) Debug.Log("done x_fileCenterline.outFile");

        doneAutoDirection = true;
        if (isDebug) Debug.Log("Done AutoDirection");

        yield break;
    }

    [ContextMenu("Stop")]
    public void Stop()
    {
        StopAllCoroutines();
        autoMeshIO.StopAllCoroutines();
        truncate.StopAllCoroutines();
        truncate.boolTruncate = false;
        showMeshBounds_EditMode.StopAllCoroutines();
        showMeshBounds_Runtime.StopAllCoroutines();
        followOrigin.StopAllCoroutines();
        pointControl.StopAllCoroutines();
        pointControl.boolUpdate = false;

        AfterFullAuto();
    }
}
