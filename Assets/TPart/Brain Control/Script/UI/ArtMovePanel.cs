﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArtMovePanel : Singleton<ArtMovePanel>
{
    public Button btnReset = null;
    public Button btnGoUp = null;
    public Toggle tgManual = null;

    PicaUp _picaUp = null;
    PicaUp picaUp { get { if (_picaUp == null) _picaUp = FindObjectOfType<PicaUp>(); return _picaUp; } }

    private void OnEnable()
    {
        picaUp.beforePicaGo += picaUpbeforePicaGo;
        picaUp.afterPicaGo += picaUpafterPicaGo;
    }

    private void OnDisable()
    {
        //picaUp.beforePicaGo -= picaUpbeforePicaGo;
        //picaUp.afterPicaGo -= picaUpafterPicaGo;
    }

    void picaUpbeforePicaGo()
    {
        //OnOff3button(false);
    }

    void picaUpafterPicaGo()
    {
        OnOff3button(true);
        Reset2button();
    }

    public void picaUpGoUp()
    {
        btnGoUp.GetComponent<tButton>().OnOffSprite(true);        
        picaUp.GoUp();
    }

    public void picaUpGoDown()
    {
        btnReset.GetComponent<tButton>().OnOffSprite(true);
        picaUp.GoDown();
    }

    public void picaUpGoREZ()
    {
        btnGoUp.GetComponent<tButton>().OnOffSprite(true);
        picaUp.AutoGo();
    }

    public void OnOff(bool On)
    {
        gameObject.SetActive(On);
    }

    public void OnOff(Toggle tg)
    {
        gameObject.SetActive(tg.isOn);
    }    

    public void OnOff3button(bool On)
    {
        btnReset.interactable = On;
        btnGoUp.interactable = On;
        tgManual.interactable = On;
    }

    public void Reset2button()
    {
        btnReset.GetComponent<tButton>().OnOffSprite(false);
        btnGoUp.GetComponent<tButton>().OnOffSprite(false);        
    }

    public void PicaOnOffLock()
    {
        PICA.Instance.picaDrag.OnOffLock(tgManual);
    }
}
