﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AutoXPanel : Singleton<AutoXPanel>
{
    public Text txtAutoTime = null;

    [Header("Text")]
    public float delay = 2f;

    public void Say(string text)
    {
        txtAutoTime.text = text;
    }

    public void SayAndClear(string text)
    {
        txtAutoTime.text = text;
        ClearDelay();
    }

    public void Clear()
    {
        txtAutoTime.text = string.Empty;
    }

    public void ClearDelay()
    {
        StartCoroutine(C_ClearDelay());
    }

    IEnumerator C_ClearDelay()
    {
        yield return new WaitForSeconds(delay);
        Clear();
        yield break;
    }

    [Header("Overlay")]
    public Image overlay = null;
    public CamPivot_Movement cmc = null;

    [ContextMenu("overlayOn")]
    public void overlayOn()
    {
        overlay.gameObject.SetActive(true);
        cmc.IsOn = false;
    }

    [ContextMenu("overlayOff")]
    public void overlayOff()
    {
        overlay.gameObject.SetActive(false);
        cmc.IsOn = true;
    }
}
