﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class tgDriveObj : MonoBehaviour
{
    public Toggle tg = null;
    public GameObject go = null;

    [ContextMenu("DriveGO")]
    public void DriveGO()
    {
        go.SetActive(tg.isOn);
    }
}
