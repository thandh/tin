﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrainPanel : MonoBehaviour 
{
    [SerializeField] List<MeshRenderer> matList = new List<MeshRenderer>();

    [ContextMenu("Solid")]
    public void Solid()
    {
        SetTransparentMat(0f);
    }

    [ContextMenu("Dim")]
    public void Dim()
    {
        SetTransparentMat(0.5f);
    }

    [ContextMenu("Transparent")]
    public void Transparent()
    {
        SetTransparentMat(1f);
    }

    void SetTransparentMat(float a)
    {
        foreach (MeshRenderer m in matList)
        {
            if (m == null) continue;
            m.material.SetFloat("_Transparency", a);
        }
    }
}
