﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CerebellumDeformPanel : Singleton<CerebellumDeformPanel>
{
    [Serializable]
    public enum cerePanelState
    {
        reset,
        auto,
        manual
    }

    [Header("Debug")]
    public bool isDebug = false;

    public GameManager gm = null;
    public Controls controls = null;

    [Header("btnReset")]
    public Button btnReset = null;    

    [Header("btnAutoPush")]
    public Button btnAuto = null;    

    [Header("tgManualPush")]
    public Toggle tgManualPush = null;
    public Button btnManualPush = null;

    [Header("panelState")]
    public cerePanelState panelState = cerePanelState.reset;
    public cerePanelState savepanelState = cerePanelState.reset;

    MeshDeformer md = null;

    public tHera thera = null;

    private void Awake()
    {
        md = cerebellum_180416.Instance.md;
    }

    private void Start()
    {
        
    }

    public void btnReset_OnClick()
    {
        StartCoroutine(C_btnReset_OnClick());
    }

    IEnumerator C_btnReset_OnClick()
    {
        panelState = cerePanelState.reset;       

        dkAutoPush.UnAutoPushAtCurrent();
        brainReset.BrainReset();        

        yield return new WaitUntil(() => dkAutoPush.doneAutoMove == true);

        ivoriSystem.doneRelease = true;

        yield break;
    }
        
    [Header("Config Auto Push")]
    public int stepPushAuto = 8;
    public TransRelationship relationHera = null;

    public void btnAutoPush_OnClick()
    {           
        btnPush5_OnClick();
    }

    public void tgAutoPush_OnValueChange(Toggle tg)
    {
        
    }

    public void tgManualPush_OnValueChange()
    {
        if (isDebug) Debug.Log("tgManualPush.isOn : " + tgManualPush.isOn);

        if (tgManualPush.isOn)
        {
            savepanelState = btnReset.IsInteractable() ? cerePanelState.reset : cerePanelState.auto;
            panelState = cerePanelState.manual;

            btnReset.interactable = false;
            btnAuto.interactable = false;

            thera.heraImpact.cereColliderByMesh.ColliderSyncFilter();

            btnManualPush.interactable = true;
        }
        else
        {
            StopAllCoroutines();
            thera.heraImpact.StopAllCoroutines();

            switch (savepanelState)
            {
                case cerePanelState.reset:
                    btnReset.interactable = true;
                    btnAuto.interactable = false;
                    break;
                case cerePanelState.auto:
                    btnReset.interactable = false;
                    btnAuto.interactable = true;
                    break;
            }

            btnManualPush.interactable = false;
        }
    }

    [Header("Push5")]
    public int countPush = 5;
    public float timeAddToWaitAfterPush = 0.5f;

    [Header("Config normal Push5")]
    public int stepPush5 = 5;

    [Header("Config normal Push5")]
    public dkHera_AutoPush dkAutoPush = null;
    public BrainResetMesh brainReset = null;
    public ManngerSystem ivoriSystem = null;

    public void btnPush5_OnClick()
    {        
        StartCoroutine(C_btnPush5_DKHera_OnClick());
    }

    IEnumerator C_btnPush5_DKHera_OnClick()
    {        
        dkAutoPush.ScriptMove();
        yield return new WaitUntil(() => dkAutoPush.doneScriptMove == true);

        ivoriSystem.doneDeform = true;

        yield break;
    }

    /// <summary>
    /// Use Hera Deformer Runtime
    /// </summary>
    /// <returns></returns>
    IEnumerator C_btnPush5_OnClick()
    {
        int count = 0;
        countPush = stepPush5;
        while (count < countPush)
        {
            if (isDebug) Debug.Log("Perform a push!! When count : " + count);
            btnPush_OnClick();

            if (isDebug) Debug.Log("yield donebtnPush_OnClick = true);");
            yield return new WaitUntil(() => donebtnPush_OnClick = true);
            if (isDebug) Debug.Log("done yield donebtnPush_OnClick = true);");

            if (isDebug) Debug.Log("yield wait second : " + timeAddToWaitAfterPush);
            yield return new WaitForSeconds(timeAddToWaitAfterPush);
            if (isDebug) Debug.Log("done yield wait second : " + timeAddToWaitAfterPush);

            count++;
        }

        yield break;
    }

    [Header("OnClick")]
    public bool donebtnPush_OnClick = true;

    public void btnPush_OnClick()
    {
        StartCoroutine(C_btnPush_OnClick());
    }

    IEnumerator C_btnPush_OnClick()
    {
        if (isDebug) Debug.Log("<color=red> Start C_btnPush_OnClick() </color>");

        donebtnPush_OnClick = false;

        if (savepanelState == cerePanelState.auto) savepanelState = cerePanelState.reset;
                
        md.allowDeform = false;
        md.lockDeform = false;
                
        if (isDebug) Debug.Log("Perform a push!!");
        thera.heraImpact.DeformUseLineDistribute();        

        btnManualPush.interactable = false;
        if (isDebug) Debug.Log("yield doneDeformUseLineDistribute == true);");
        yield return new WaitUntil(() => thera.heraImpact.doneDeformUseLineDistribute == true);
        if (isDebug) Debug.Log("done yield doneDeformUseLineDistribute == true);");
        btnManualPush.interactable = tgManualPush.isOn;

        donebtnPush_OnClick = true;

        if (isDebug) Debug.Log("<color=red> Done C_btnPush_OnClick() </color>");

        yield break;
    }

    [SerializeField] bool IsQuiting = false;

    private void OnApplicationQuit()
    {
        IsQuiting = true;
    }

    private void OnEnable()
    {        
        md.delafterAutoDeform += thera.heraImpact.cereColliderByMesh.ColliderSyncFilter;
        md.delafterResetDeform += thera.heraImpact.cereColliderByMesh.ColliderSyncFilter;
    }

    private void OnDisable()
    {
        if (IsQuiting) return;
        
        md.delafterAutoDeform -= thera.heraImpact.cereColliderByMesh.ColliderSyncFilter;
        md.delafterResetDeform += thera.heraImpact.cereColliderByMesh.ColliderSyncFilter;
    }

    public void btnResetOn()
    {
        btnReset.interactable = true;        
    }

    public void btnAutoPushOn()
    {
        btnAuto.interactable = true;
    }

    public void btnAutoPushOffInteract()
    {
        btnManualPush.interactable = false;
    }

    [ContextMenu("Stop")]
    public void Stop()
    {
        StopAllCoroutines();
    }
}
