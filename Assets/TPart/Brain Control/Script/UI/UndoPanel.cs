﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UndoPanel : Singleton<UndoPanel>
{ 
    UndoManager _undoManager = null;
    UndoManager undoManager { get { if (_undoManager == null) _undoManager = FindObjectOfType<UndoManager>(); return _undoManager; } }

    public Transform panelHolder = null;
    public Button btnUndo = null;

    public void OnOff(bool On)
    {
        panelHolder.gameObject.SetActive(On);
    }

    public void OnEnable()
    {
        Check();
    }

    public void Check()
    {        
        btnUndo.GetComponent<tButton>().OnOffSprite(undoManager.stack.Count > 0);        
    }

    public void Undo()
    {
        undoManager.Undo();
        Check();
    }
}
