﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TransparentPanel : MonoBehaviour 
{
    public Transform panelDo = null;
    public Toggle tg100 = null;
    public Toggle tg50 = null;
    public Toggle tg0 = null;

    public Toggle activeTg = null;

    public void SetActiveTg(Toggle tg)
    {
        if (tg.isOn)
        {
            activeTg = tg;
            panelDo.position = tg.transform.Find("panelDoPos").position;

            panelDo.gameObject.SetActive(true);
            if (tg.GetComponent<tgTransparent>().objs.Count > 0)
            {
                Reset3ToggleDoTransparent(tg.GetComponent<tgTransparent>().currentAlpha);
            }
        } else
        {
            activeTg = null;
            panelDo.gameObject.SetActive(false);
        }
    }

    void Reset3ToggleDoTransparent(float alpha)
    {
        tg100.isOn = alpha > 0.5 ? true : false;
        tg50.isOn = alpha == 0.5f ? true : false;
        tg0.isOn = alpha < 0.5f ? true : false;
    }

    public void AppearObject(Toggle tg)
    { 
        if (activeTg == null) return;
        if (!tg.isOn) return;
        activeTg.GetComponent<tgTransparent>().ObjOnOff(true);
        activeTg.GetComponent<tgTransparent>().SetAlpha(1f);        
    }

    public void TransAnObject(Toggle tg)
    {
        if (activeTg == null) return;
        if (!tg.isOn) return;
        activeTg.GetComponent<tgTransparent>().ObjOnOff(true);
        activeTg.GetComponent<tgTransparent>().SetAlpha(0.5f);
    }

    public void HideObject(Toggle tg)
    {
        if (activeTg == null) return;
        if (!tg.isOn) return;
        activeTg.GetComponent<tgTransparent>().ObjOnOff(false);
    }
}
