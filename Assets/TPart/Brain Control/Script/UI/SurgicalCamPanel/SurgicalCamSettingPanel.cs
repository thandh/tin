﻿using SFB;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class SurgicalCamSettingPanel : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;    

    private void Awake()
    {
        MeshRenderer mr = Rez.Instance.GetComponent<MeshRenderer>();
        tgREZ.isOn = mr.enabled;
    }

    [Header("Params")]
    public GameObject uiHolder = null;

    [ContextMenu("OpenUI")]
    public void ToggleUI()
    {
        uiHolder.SetActive(!uiHolder.activeSelf);
    }

    [ContextMenu("SyncFromScrip")]
    public void SyncFromScrip()
    {

    }

    [Header("REZ")]
    public Toggle tgREZ = null;

    [ContextMenu("toggleREZ")]
    public void toggleREZ()
    {
        MeshRenderer mr = Rez.Instance.GetComponent<MeshRenderer>();
        mr.enabled = tgREZ.isOn;
        Rez.Instance.boolAllowMove = mr.enabled;
        Rez.Instance.GetComponent<Rez_PickNDrag>().isLock = !Rez.Instance.boolAllowMove;
    }

    [Header("Keyview")]
    public Keyview_Method keyviewMethod = null;
    public Keyview_FromRuntime keyviewRuntime = null;
    public CamPivot_Keyview camKeyview = null;

    [ContextMenu("btnAutoByCurrentCylinder")]
    public void btnAutoByCurrentCylinder()
    {
        keyviewRuntime.KeyviewFromRuntime();
    }

    [ContextMenu("btnBack")]
    public void btnBack()
    {
        keyviewMethod.Stepback();
    }

    [ContextMenu("btnForward")]
    public void btnForward()
    {
        keyviewMethod.Stepforward();
    }

    [ContextMenu("btnSave_SurgicalCamera")]
    public void btnSave_SurgicalCamera()
    {
        camKeyview.SaveKeyView();
    }

    [ContextMenu("btnLoadRez")]
    public void btnLoadRez()
    {
        string[] paths = StandaloneFileBrowser.OpenFilePanel("Open File", Application.persistentDataPath, "txt", false);
        SlashChanges(paths);
        HandleOpenFolder(paths);
    }    

    void SlashChange(string strIn, out string strOut)
    {
        strOut = strIn.Replace('\\', '/');
    }

    void SlashChanges(string[] paths)
    {
        for (int i = 0; i < paths.Length; i++) SlashChange(paths[i], out paths[i]);
    }

    public void HandleOpenFolder(string[] paths)
    {
        if (isDebug) Debug.Log("<color=red> Start HandleOpenFolder!!</color>");

        if (isDebug) for (int i = 0; i < paths.Length; i++) Debug.Log("At " + i + " : " + paths[i]);

        if (paths.Length == 0) return;
        DirectoryInfo dirInfo = new DirectoryInfo(paths[0]);
        Rez.Instance.GetComponent<RezIO>().RezIn(paths[0]);

        if (isDebug) Debug.Log("<color=red> Done HandleOpenFolder!!</color>");
    }

    [ContextMenu("btnSave_Rez")]
    public void btnSave_Rez()
    {
        Rez.Instance.GetComponent<RezIO>().RezOut_Current();
    }
}
