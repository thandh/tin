﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MeshIOPanel : MonoBehaviour 
{
    public Transform panelMeshIO = null;    

    public void SetActiveTg(Toggle tg)
    {   
        panelMeshIO.gameObject.SetActive(tg.isOn);            
    }    
}
