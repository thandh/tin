﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BrainDeformSetting : Singleton<BrainDeformSetting>
{
    [Header("Params")]
    public InputField ipfForce = null;
    public InputField ipfMove = null;
    public InputField ipfRotate = null;
    public Toggle tgDebugHera = null;
    public InputField ipfNumberPush = null;

    public tHeraImpact impact = null;
    public NextStep_ByTransform heraMove = null;
    public MoveMethod heraRotate = null;
    public CerebellumDeformPanel cerePanel = null;

    private void Start()
    {
        ipfForce.text = impact.force.ToString();
        ipfMove.text = heraMove.d.ToString();
        ipfRotate.text = heraRotate.stepRot.ToString();
        tgDebugHera.isOn = impact.showImpactPoint;
        ipfNumberPush.text = cerePanel.stepPush5.ToString();
    }

    public void SyncForce()
    {
        float force = impact.force;
        bool canParse = float.TryParse(ipfForce.text, out force);
        if (canParse) impact.force = force;
    }

    public void SyncMove()
    {
        float move = heraMove.d;
        bool canParse = float.TryParse(ipfMove.text, out move);
        if (canParse) heraMove.d = move;
    }

    public void SyncRotate()
    {
        float rotate = heraRotate.stepRot;
        bool canParse = float.TryParse(ipfRotate.text, out rotate);
        if (canParse) heraRotate.stepRot = rotate;
    }

    public void SyncHeraDebug()
    {
        impact.showImpactPoint = tgDebugHera.isOn;
        impact.ShowCurrentState_ImpactPoint();
    }

    public void SyncNumberPush()
    {
        int count = cerePanel.stepPush5;
        bool canParse = int.TryParse(ipfNumberPush.text, out count);
        if (canParse) cerePanel.stepPush5 = count;
    }

    [Header("Control")]
    public Transform trPanel = null;

    private void Update()
    {
        //if ((Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
        //    && Input.GetKey(KeyCode.Period))

        if (Input.GetKey(KeyCode.Period))
        {
            trPanel.gameObject.SetActive(!trPanel.gameObject.activeSelf);
        }
    }
}
