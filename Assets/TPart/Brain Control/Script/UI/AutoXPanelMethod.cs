﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AutoXPanelMethod : MonoBehaviour
{
    public void SayAndClear(string text)
    {
        AutoXPanel.Instance.Say(text);
        AutoXPanel.Instance.ClearDelay();
    }    
    
    [ContextMenu("overlayOn")]
    public void overlayOn()
    {
        AutoXPanel.Instance.overlayOn();
    }

    [ContextMenu("overlayOff")]
    public void overlayOff()
    {
        AutoXPanel.Instance.overlayOff();
    }
}
