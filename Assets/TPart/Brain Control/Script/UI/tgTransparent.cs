﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tgTransparent : MonoBehaviour
{   
    public List<Transform> objs = null;
    public float currentAlpha = -1f;

    private void Start()
    {
        SetAlpha(1f);
    }

    public void SetAlpha(float alpha)
    {
        currentAlpha = alpha;
        foreach (Transform tr in objs)
        {
            if (tr == null) continue;
            MeshRenderer mr = tr.GetComponent<MeshRenderer>();
            if (mr != null) mr.material.color = Alpha(mr.material.color, alpha);
            SkinnedMeshRenderer smr = tr.GetComponent<SkinnedMeshRenderer>();
            if (smr != null) smr.material.color = Alpha(smr.material.color, alpha);
        }
    }

    public void ObjOnOff(bool On)
    {
        foreach (Transform tr in objs)
        {
            tr.gameObject.SetActive(On);
        }
    }

    Color Alpha(Color color, float alpha)
    {
        return new Color(color.r, color.g, color.b, alpha);
    }
}
