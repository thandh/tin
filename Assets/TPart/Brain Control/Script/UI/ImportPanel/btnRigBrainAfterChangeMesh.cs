﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class btnRigBrainAfterChangeMesh : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("AfterChangeMesh")]
    public CreateMesh createMesh = null;
    public CreateMesh removeMesh = null;
    public CreateMesh betweenMeshC = null;

    [ContextMenu("AfterChangeMesh")]
    public void AfterChangeMesh()
    {
        tRig_autoCreate_Brain tRigBrain = FindObjectOfType<tRig_autoCreate_Brain>();
        if (tRigBrain == null) { if (isDebug) Debug.Log("tRig_Brain null!"); return; }
        tRigBrain.dknodes.DeleteAll();

        createMesh.gameObject.SetActive(false);
        removeMesh.gameObject.SetActive(false);
        betweenMeshC.gameObject.SetActive(false);
    }        
}
