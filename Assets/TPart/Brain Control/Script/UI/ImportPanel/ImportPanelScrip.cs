﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ImportPanelScrip : ScriptableObject
{
    [Serializable]
    public class OrganDirectory
    {
        public string organStr = string.Empty;
        public string dirStr = string.Empty;
        public Vector3 scale = Vector3.one;
    }

    public List<OrganDirectory> organList = new List<OrganDirectory>();
}