﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class btnAutoXImportModel : MonoBehaviour
{
    [ContextMenu("CallAutoX")]
    public void CallAutoX()
    {
        X_AutoDirection x_AutoDirection = FindObjectOfType<X_AutoDirection>();
        if (x_AutoDirection != null) x_AutoDirection.FullAuto();
    }

    [ContextMenu("ForceAutoX")]
    public void ForceAutoX()
    {
        StartCoroutine(C_ForceAutoX());
    }

    IEnumerator C_ForceAutoX()
    {
        X_AutoDirection x_AutoDirection = FindObjectOfType<X_AutoDirection>();
        if (x_AutoDirection != null)
        {
            x_AutoDirection.transform.localPosition = Vector3.zero;
            x_AutoDirection.transform.localRotation = Quaternion.identity;

            x_AutoDirection.autoxMeshIO.DeleteCurrentFile();
            x_AutoDirection.meshOut.DeleteCurrentFile();
            x_AutoDirection.x_fileCenterline.DeleteCurrentFile();
            x_AutoDirection.ResetParams();

            yield return new WaitUntil(() => x_AutoDirection.autoxMeshIO.doneDelete == true && x_AutoDirection.meshOut.doneDelete == true && x_AutoDirection.x_fileCenterline.doneDelete == true);

            x_AutoDirection.FullAuto();
        }
        yield break;
    }
}
