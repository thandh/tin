﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleImportOptions : MonoBehaviour
{
    [Header("Params")]
    public Toggle tg = null;
    public CanvasGroup cg = null;

    [ContextMenu("SetCurrentTg")]
    public void SetCurrentTg()
    {
        cg.alpha = tg.isOn ? 1f : 0.5f;
        cg.interactable = tg.isOn;
    }
}
