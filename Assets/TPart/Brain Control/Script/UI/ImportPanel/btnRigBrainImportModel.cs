﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class btnRigBrainImportModel : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [ContextMenu("RigBrain")]
    public void RigBrain()
    {
        StartCoroutine(C_RigBrain());
    }

    IEnumerator C_RigBrain()
    {
        if (isDebug) Debug.Log("start C_RigBrain()");

        tRig_autoCreate_Brain tRigBrain = FindObjectOfType<tRig_autoCreate_Brain>();
        if (tRigBrain == null) { if (isDebug) Debug.Log("tRig_Brain null!"); yield break; }

        SplitMesh splitMesh = tRigBrain.GetComponent<SplitMesh>();
        if (splitMesh == null) { if (isDebug) Debug.Log("splitMesh null!"); yield break; }

        if (isDebug) Debug.Log("start yield splitMesh.doneFullSplit == true");
        splitMesh.FullSplit();
        yield return new WaitUntil(() => splitMesh.doneFullSplit == true);
        if (isDebug) Debug.Log("done yield splitMesh.doneFullSplit == true");

        FindVertClose findVert = tRigBrain.GetComponent<FindVertClose>();
        dkNodes dknodes = tRigBrain.GetComponentInChildren<dkNodes>();

        findVert.vertCollectCount = splitMesh.brainCreate.vertList.Count;
        dknodes.numberCreate = splitMesh.brainCreate.vertList.Count;

        if (isDebug) Debug.Log("start yield tRigBrain.doneAuto == true");
        tRigBrain.FullAuto();
        yield return new WaitUntil(() => tRigBrain.doneAuto == true);
        if (isDebug) Debug.Log("done yield tRigBrain.doneAuto == true");

        tRigBrain.tRig.updateFromNodes = true;

        if (isDebug) Debug.Log("done C_RigBrain()");
    }

    [ContextMenu("ToggleDebug")]
    public void ToggleDebug()
    {
        tRig_autoCreate_Brain tRigBrain = FindObjectOfType<tRig_autoCreate_Brain>();
        if (tRigBrain == null) { if (isDebug) Debug.Log("tRig_Brain null!"); return; }

        tRigBrain.GetComponentInChildren<dkNodes_ShapesJob>().GetList();

        tRigBrain.showShape = !tRigBrain.showShape;
        tRigBrain.ShowShape();
    }
}
