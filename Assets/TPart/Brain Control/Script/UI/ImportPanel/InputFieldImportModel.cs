﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputFieldImportModel : MonoBehaviour
{
    [Header("Input")]
    public InputField ipf = null;

    [ContextMenu("LoadIpf")]
    public void LoadIpf()
    {
        ipf = GetComponent<InputField>();
    }

    [Header("Scrip")]
    public string organStr = string.Empty;
    public ImportPanelScrip panelScrip = null;

    private void Start()
    {
        SyncFromScrip();
        ipf.onEndEdit.AddListener(delegate { SyncToScrip(); });
    }

    [ContextMenu("SyncFromScrip")]
    public void SyncFromScrip()
    {
        if (string.IsNullOrEmpty(organStr) || panelScrip == null) return;
        ImportPanelScrip.OrganDirectory organ = panelScrip.organList.Find((x) => x.organStr == this.organStr);
        if (organ != null) ipf.text = organ.dirStr;
    }

    [ContextMenu("SyncToScrip")]
    public void SyncToScrip()
    {
        if (string.IsNullOrEmpty(organStr) || panelScrip == null) return;
        int indexOrgan = panelScrip.organList.FindIndex((x) => x.organStr == this.organStr);
        if (indexOrgan != -1) panelScrip.organList[indexOrgan].dirStr = ipf.text;
    }
}