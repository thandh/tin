﻿using SFB;
using System;
using System.Collections;
using System.Collections.Generic;
using TriLib;
using TriLib.Samples;
using UnityEngine;
using UnityEngine.UI;

public class btnApplyImportModel : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Params")]
    public InputField ipf = null;
    public Button btn = null;

    [ContextMenu("LoadIpf")]
    public void LoadIpf()
    {
        ipf = transform.parent.GetComponentInChildren<InputField>();
    }

    [ContextMenu("LoadBtn")]
    public void LoadBtn()
    {
        btn = GetComponent<Button>();
    }

    private void Start()
    {
        btn.onClick.AddListener(delegate { ChangeMesh(); });
    }

    [ContextMenu("ChangeMesh")]
    public void ChangeMesh()
    {
        LoadInternal(ipf.text, null);
    }

    [Header("Import process")]
    public bool Async = true;
    public GameObject _rootGameObject = null;

    private void LoadInternal(string filename, byte[] fileBytes = null)
    {
        if (string.IsNullOrEmpty(filename)) return;

        PreLoadSetup();
        var assetLoaderOptions = GetAssetLoaderOptions();
        if (!Async)
        {
            using (var assetLoader = new AssetLoader())
            {
                assetLoader.OnMetadataProcessed += AssetLoader_OnMetadataProcessed;
                try
                {
#if (UNITY_WINRT && !UNITY_EDITOR_WIN)
                        var extension = FileUtils.GetFileExtension(filename);
                        _rootGameObject = assetLoader.LoadFromMemoryWithTextures(fileBytes, extension, assetLoaderOptions, _rootGameObject);
#else
                    if (fileBytes != null && fileBytes.Length > 0)
                    {
                        var extension = FileUtils.GetFileExtension(filename);
                        _rootGameObject = assetLoader.LoadFromMemoryWithTextures(fileBytes, extension, assetLoaderOptions, _rootGameObject);
                    }
                    else if (!string.IsNullOrEmpty(filename))
                    {
                        _rootGameObject = assetLoader.LoadFromFileWithTextures(filename, assetLoaderOptions);
                    }
                    else
                    {
                        throw new System.Exception("File not selected");
                    }
#endif
                }
                catch (System.Exception exception)
                {
                    ErrorDialog.Instance.ShowDialog(exception.ToString());
                }
            }
            if (_rootGameObject != null)
            {
                PostLoadSetup();
            }
        }
        else
        {
            using (var assetLoader = new AssetLoaderAsync())
            {
                assetLoader.OnMetadataProcessed += AssetLoader_OnMetadataProcessed;
                try
                {
                    if (fileBytes != null && fileBytes.Length > 0)
                    {
                        var extension = FileUtils.GetFileExtension(filename);
                        assetLoader.LoadFromMemoryWithTextures(fileBytes, extension, assetLoaderOptions, null, delegate (GameObject loadedGameObject)
                        {
                            _rootGameObject = loadedGameObject;
                            if (_rootGameObject != null)
                            {
                                PostLoadSetup();
                            }
                        });
                    }
                    else if (!string.IsNullOrEmpty(filename))
                    {
                        assetLoader.LoadFromFileWithTextures(filename, assetLoaderOptions, null, delegate (GameObject loadedGameObject)
                        {
                            _rootGameObject = loadedGameObject;
                            if (_rootGameObject != null)
                            {
                                PostLoadSetup();
                            }
                        });
                    }
                    else
                    {
                        throw new System.Exception("File not selected");
                    }
                }
                catch (System.Exception exception)
                {
                    ErrorDialog.Instance.ShowDialog(exception.ToString());
                }
            }
        }
    }

    private void PreLoadSetup()
    {
        if (_rootGameObject != null)
        {
            Destroy(_rootGameObject);
            _rootGameObject = null;
        }
    }

    private AssetLoaderOptions GetAssetLoaderOptions()
    {
        var assetLoaderOptions = AssetLoaderOptions.CreateInstance();
        assetLoaderOptions.DontLoadCameras = false;
        assetLoaderOptions.DontLoadLights = false;
        assetLoaderOptions.AddAssetUnloader = true;
        return assetLoaderOptions;
    }

    private void AssetLoader_OnMetadataProcessed(AssimpMetadataType metadataType, uint metadataIndex, string metadataKey, object metadataValue)
    {
        if (isDebug) Debug.Log("Found metadata of type [" + metadataType + "] at index [" + metadataIndex + "] and key [" + metadataKey + "] with value [" + metadataValue + "]");
    }

    private void PostLoadSetup()
    {
        if (isDebug) Debug.Log("Start PostLoadSetup");
        ImplyCurrentMesh();
        ResetMesh0();
        SyncMesh();
        if (Application.isPlaying) Destroy(_rootGameObject); else DestroyImmediate(_rootGameObject);
        if (isDebug) Debug.Log("Done PostLoadSetup");
    }

    [Header("Output")]
    public Mesh currentMesh = null;
    public MeshFilter mf = null;
    public MeshCollider mc = null;

    [ContextMenu("GetCurrentMesh")]
    public void ImplyCurrentMesh()
    {
        if (_rootGameObject == null) return;
        MeshFilter mf = _rootGameObject.GetComponentInChildren<MeshFilter>();
        if (mf == null) return;
        currentMesh = mf.mesh;
    }

    [ContextMenu("SyncMesh")]
    public void SyncMesh()
    {
        if (mf != null) mf.sharedMesh = currentMesh;
        if (mc != null) mc.sharedMesh = currentMesh;
    }

    [ContextMenu("ResetMesh0")]
    public void ResetMesh0()
    {
        mf.transform.localPosition = Vector3.zero;
        mf.transform.localRotation = Quaternion.identity;
    }
}
