﻿using SFB;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class btnLoadFolderImportModel : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Params")]
    public InputField ipf = null;
    public Button btn = null;

    [ContextMenu("LoadIpf")]
    public void LoadIpf()
    {
        ipf = transform.parent.GetComponentInChildren<InputField>();
    }

    [ContextMenu("LoadBtn")]
    public void LoadBtn()
    {
        btn = GetComponent<Button>();
    }

    private void Start()
    {
        btn.onClick.AddListener(delegate { OpenFilePanel(); });
    }

    [ContextMenu("OpenFilePanel")]
    public void OpenFilePanel()
    {
        var extensions = new[] {                
                new ExtensionFilter("All Files", "*" )
            };

        if (ipf == null) return;
        string[] paths = StandaloneFileBrowser.OpenFilePanel("Load File", "", extensions, false);
        SlashChanges(paths);
        HandleOpenFolder(paths);
    }

    void SlashChanges(string[] paths)
    {
        for (int i = 0; i < paths.Length; i++) SlashChange(paths[i], out paths[i]);
    }

    void SlashChange(string strIn, out string strOut)
    {
        strOut = strIn.Replace('\\', '/');
    }

    public void HandleOpenFolder(string[] paths)
    {
        if (isDebug) Debug.Log("<color=red> Start HandleOpenFolder!!</color>");

        if (isDebug) for (int i = 0; i < paths.Length; i++) Debug.Log("At " + i + " : " + paths[i]);

        if (paths.Length == 0) return;
        ipf.text = paths[0];
    }
}
