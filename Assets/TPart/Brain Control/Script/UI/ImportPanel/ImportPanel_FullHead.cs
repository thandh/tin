﻿using SFB;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ImportPanel_FullHead : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Clickable")]
    public bool ClickHere = false;

    [Serializable]
    public class APartData
    {
        public string dataName = string.Empty;
        public string dataPath = string.Empty;        
        public APartData() { }
        public APartData(string dataName, string dataPath) { this.dataName = dataName; this.dataPath = dataPath; }
    }

    [Serializable]
    public class HeadData
    {
        public List<APartData> dataList = new List<APartData>()
        {
            new APartData("pica", string.Empty),
            new APartData("bs", string.Empty),
            new APartData("cere", string.Empty),
            new APartData("artery", string.Empty),
            new APartData("sinus", string.Empty),
            new APartData("skull", string.Empty),
            new APartData("v", string.Empty),
            new APartData("vii", string.Empty),
            new APartData("viii", string.Empty),
            new APartData("x", string.Empty)
        };

        public HeadData() { }
        public string toJson() { return JsonUtility.ToJson(this); }
    }

    [Header("HeadData")]
    public HeadData currentHeadData = new HeadData();
    public string currentFolder = string.Empty;
    public string fileName = "FullHeadDataPath.txt";

    [ContextMenu("OpenFullHead")]
    public void OpenFullHead()
    {
        string[] paths = StandaloneFileBrowser.OpenFilePanel("Open File", Application.dataPath, "txt", false);
        SlashChanges(paths);
        HandleOpenFile(paths);
    }

    void SlashChange(string strIn, out string strOut)
    {
        strOut = strIn.Replace('\\', '/');
    }

    void SlashChanges(string[] paths)
    {
        for (int i = 0; i < paths.Length; i++) SlashChange(paths[i], out paths[i]);
    }

    [Header("Load all paths")]
    public List<btnApplyImportModel> applyList = new List<btnApplyImportModel>();

    [ContextMenu("GetApplyList")]
    public void GetApplyList()
    {
        applyList = FindObjectsOfType<btnApplyImportModel>().ToList();
    }

    public void HandleOpenFile(string[] paths)
    {
        if (isDebug) Debug.Log("<color=red> Start HandleOpenFile!!</color>");

        if (isDebug) for (int i = 0; i < paths.Length; i++) Debug.Log("At " + i + " : " + paths[i]);

        if (paths.Length == 0) return;
        DirectoryInfo dirInfo = new DirectoryInfo(paths[0]);

        currentHeadData = JsonUtility.FromJson<HeadData>(File.ReadAllText(paths[0]));

        foreach (InputFieldImportModel ipfPath in pathList)
        {
            int index = this.currentHeadData.dataList.FindIndex((x) => x.dataName == ipfPath.organStr);
            if (index == -1)
            {
                if (isDebug) Debug.Log("index = -1 " + ipfPath.organStr);
                continue;
            }
            ipfPath.GetComponent<InputField>().text = currentHeadData.dataList[index].dataPath;
        }

        foreach(btnApplyImportModel btnApply in applyList)
        {
            if (btnApply == null) continue;
            btnApply.ChangeMesh();
        }

        if (isDebug) Debug.Log("<color=red> Done HandleOpenFile!!</color>");
    }

    [ContextMenu("FullHeadOutCurrent")]
    public void FullHeadOutCurrent()
    {
        if (isDebug) Debug.Log("Start FullHeadOutCurrent");

        if (string.IsNullOrEmpty(currentFolder) || !Directory.Exists(currentFolder))
        {
            string[] paths = StandaloneFileBrowser.OpenFolderPanel("Open File", Application.dataPath, false);
            SlashChanges(paths);
            currentFolder = paths[0];
        }

        if (currentFolder[currentFolder.Length - 1] != '/') currentFolder += '/';
        File.WriteAllText(currentFolder + fileName, currentHeadData.toJson());
    }

    [Header("Save all paths")]
    public List<InputFieldImportModel> pathList = new List<InputFieldImportModel>();

    [ContextMenu("GetPathList")]
    public void GetPathList()
    {
        pathList = FindObjectsOfType<InputFieldImportModel>().ToList();
    }

    [ContextMenu("SaveAllPaths")]
    public void SaveAllPaths()
    {
        foreach (InputFieldImportModel inputPath in pathList)
        {
            if (inputPath == null) continue;
            int index = this.currentHeadData.dataList.FindIndex((x) => x.dataName == inputPath.organStr);
            if (index == -1)
            {
                if (isDebug) Debug.Log("index = -1 " + inputPath.organStr);
                continue;
            }
            currentHeadData.dataList[index].dataPath = inputPath.GetComponent<InputField>().text;
        }

        currentFolder = string.Empty;
        FullHeadOutCurrent();
    }
}
