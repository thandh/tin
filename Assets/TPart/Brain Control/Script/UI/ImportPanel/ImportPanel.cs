﻿using SFB;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImportPanel : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;
    public bool useUpdate = true;

    [Header("Params")]
    public GameObject uiHolder = null;

    private void Update()
    {
        if (!useUpdate) return;

        if ((Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl)) && Input.GetKey(KeyCode.I))
        {
            ToggleUI();
        }
    }    

    [ContextMenu("OpenUI")]
    public void ToggleUI()
    {
        uiHolder.SetActive(!uiHolder.activeSelf);
    }
}
