﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class btnRigPica : MonoBehaviour
{
    [ContextMenu("RigPica")]
    public void RigPica()
    {
        Pica_AutoRig picaRig = FindObjectOfType<Pica_AutoRig>();
        if (picaRig == null) return;
        picaRig.FullAuto();
    }
}
