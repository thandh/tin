﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ScaleInputFieldImportModel : MonoBehaviour
{
    [Header("Input")]
    public List<InputField> ipfList = new List<InputField>();
    public Transform tr = null;

    [ContextMenu("LoadIpf")]
    public void LoadIpf()
    {
        ipfList = transform.GetComponentsInChildren<InputField>().ToList();
    }
        
    private void Start()
    {
        SyncFromTrans();        
    }

    [Header("Scrip")]
    public string organStr = string.Empty;
    public ImportPanelScrip panelScrip = null;

    [ContextMenu("SyncFromTrans")]
    public void SyncFromTrans()
    {
        if (ipfList.Count != 3 || tr == null) return;
        ipfList[0].text = tr.localScale.x.ToString();
        ipfList[1].text = tr.localScale.y.ToString();
        ipfList[2].text = tr.localScale.z.ToString();

        if (string.IsNullOrEmpty(organStr) || panelScrip == null) return;
        ImportPanelScrip.OrganDirectory organ = panelScrip.organList.Find((x) => x.organStr == this.organStr);
        if (organ != null) organ.scale = new Vector3(tr.localScale.x, tr.localScale.y, tr.localScale.z);
        
    }

    [ContextMenu("SyncToTrans")]
    public void SyncToTrans()
    {
        if (ipfList.Count != 3 || tr == null) return;
        tr.localScale = new Vector3(float.Parse(ipfList[0].text), float.Parse(ipfList[1].text), float.Parse(ipfList[2].text));

        if (string.IsNullOrEmpty(organStr) || panelScrip == null) return;
        int indexOrgan = panelScrip.organList.FindIndex((x) => x.organStr == this.organStr);
        if (indexOrgan != -1) panelScrip.organList[indexOrgan].scale = tr.localScale;
    }
}