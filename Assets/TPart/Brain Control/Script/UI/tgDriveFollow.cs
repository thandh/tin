﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class tgDriveFollow : MonoBehaviour
{
    public Toggle tg = null;
    public Follow fl = null;

    [ContextMenu("SnapToTarget")]
    public void SnapToTarget()
    {
        if (tg.isOn) fl.SnapToTarget();
    }
}
