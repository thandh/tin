﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class tButton : MonoBehaviour
{
    public Sprite imgOn = null;
    public Sprite imgOff = null;

    Image _img = null;
    Image img { get { if (_img == null) _img = GetComponent<Image>(); return _img; } }
    
    public void OnOffSprite(bool On)
    {
        img.sprite = On ? imgOn : imgOff;
    }

    public void OnOffInteract(bool On)
    {
        GetComponent<Button>().interactable = On;
    }
}
