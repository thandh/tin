﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SattelliteShape : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;
    public bool ClickHere = false;

    [Header("ChildList")]
    public List<SatteliteRound> roundList = new List<SatteliteRound>();

    [ContextMenu("GetChild")]
    public void GetChild()
    {
        roundList = GetComponentsInChildren<SatteliteRound>().ToList();
        for (int i = 0; i < roundList.Count; i++) roundList[i].GetDorm();
    }

    [Header("Expand")]
    public float disExpand = 10f;

    [ContextMenu("Expand")]
    public void Expand()
    {
        if (isDebug) Debug.Log("Expand : " + disExpand);

        foreach (SatteliteRound round in roundList)
        {
            Vector3 lastLocalEulerAngles = round.transform.localEulerAngles;
            round.transform.localEulerAngles = Vector3.zero;
            foreach (SatteliteDorm dorm in round.dormList)
                dorm.transform.localPosition = dorm.transform.forward.normalized * disExpand;
            round.transform.localEulerAngles = lastLocalEulerAngles;
        }
    }

    [Header("CloseIn")]
    public HitByRay hit = null;
    public LayerMask currentHitLM = -1;

    [Header("CloseIn Output")]
    public bool doneCloseIn = true;
    public bool allCloseIn = false;

    [ContextMenu("CloseIn")]
    public void CloseIn()
    {
        StartCoroutine(C_CloseIn());
    }

    IEnumerator C_CloseIn()
    {
        if (isDebug) Debug.Log("Start C_CloseIn");

        doneCloseIn = false; allCloseIn = false;

        hit.lmHit = currentHitLM;
        foreach (SatteliteRound round in roundList)
        {
            foreach (SatteliteDorm dorm in round.dormList)
            {
                hit.trFrom = dorm.transform;
                hit.trTo = this.transform;
                Vector3 posHit = hit.PosHitUseCurrentToFinal();
                if (posHit != -Vector3.one) hit.trFrom.position = posHit;
                else { doneCloseIn = true; allCloseIn = false; yield break; }
            }

            round.CalculateCenter();
            round.CalculateForward();
        }

        doneCloseIn = true; allCloseIn = true;
        if (isDebug) Debug.Log("Done C_CloseIn");
        yield break;
    }

    [Header("Core")]
    public float minExpand = 1f;
    public float maxExpand = 100f;
    public float stepExpand = 1f;
    public bool tryHasResult = false;
    public bool doneCore = true;

    [ContextMenu("Core")]
    public void Core()
    {
        StartCoroutine(C_Core());
    }

    IEnumerator C_Core()
    {
        if (isDebug) Debug.Log("Start C_Core");

        doneCore = false;
        tryHasResult = false;
        disExpand = minExpand;
        currentMinRound = null;

        while (tryHasResult == false && disExpand < maxExpand)
        {
            Expand();
            CloseIn();
            tryHasResult = doneCloseIn == true && allCloseIn == true;
            if (tryHasResult == false) disExpand += stepExpand;
            yield return new WaitForEndOfFrame();
        }

        if (tryHasResult)
        {
            Center();
            CalculateForward();
        }

        doneCore = true;
        if (isDebug) Debug.Log("Done C_Core " + tryHasResult);
        yield break;
    }

    [Header("Center")]
    public SatteliteRound currentMinRound = null;

    [ContextMenu("Center")]
    public void Center()
    {
        roundList.Sort((a, b) => (a.currentMinMaxDivide).CompareTo(b.currentMinMaxDivide));
        foreach (SatteliteRound round in roundList) round.DisconnectDorm();
        transform.position = roundList[0].currentCenter;
        this.currentMinRound = roundList[0];
        foreach (SatteliteRound round in roundList) round.ReconnectDorm();
    }

    [Header("Forward")]
    public Vector3 currentForward = Vector3.zero;
    public eForwardType eforwardType = eForwardType.minRound;

    public enum eForwardType { minRound, aveRound }

    [ContextMenu("CalculateForward")]
    public void CalculateForward()
    {
        switch (eforwardType)
        {
            case eForwardType.minRound:
                Vector3 aveForward = Vector3.zero;

                for (int i = 0; i < roundList.Count; i++)
                {
                    roundList[i].CalculateForward();
                    aveForward += roundList[i].currentForward;
                }

                currentForward = aveForward /= roundList.Count;
                break;

            case eForwardType.aveRound:
                currentMinRound.CalculateForward();
                currentForward = currentMinRound.currentForward;
                break;
        }
    }

    public void CoreAt(Vector3 pos)
    {
        transform.position = pos;
        Core();
    }

    [ContextMenu("Stop")]
    public void Stop()
    {
        StopAllCoroutines();
    }
}
