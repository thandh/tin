﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SatteliteRound : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;
    public bool ClickHere = false;

    [Header("ChildList")]
    public List<SatteliteDorm> dormList = new List<SatteliteDorm>();

    [ContextMenu("GetDorm")]
    public void GetDorm()
    {
        dormList = new List<SatteliteDorm>();
        SatteliteDorm[] childs = GetComponentsInChildren<SatteliteDorm>();
        for (int i = 0; i < childs.Length; i++)
            if (childs[i] != this.transform) dormList.Add(childs[i]);
    }

    [Header("Center")]
    public Vector3 currentCenter = Vector3.zero;
    public float currentMinDis = 0f;
    public float currentMaxDis = 0f;
    public float currentMinMaxDivide = 0f;

    [ContextMenu("CalculateCenter")]
    public void CalculateCenter()
    {
        this.currentCenter = Vector3.zero;
        foreach (SatteliteDorm dorm in dormList) this.currentCenter += dorm.transform.position;
        this.currentCenter /= dormList.Count;

        dormList.Sort((a, b) => (this.currentCenter - a.transform.position).magnitude.CompareTo((this.currentCenter - b.transform.position).magnitude));
        this.currentMinDis = (this.transform.position - dormList[0].transform.position).magnitude;
        this.currentMaxDis = (this.transform.position - dormList[dormList.Count - 1].transform.position).magnitude;
        this.currentMinMaxDivide = (this.transform.position - (dormList[0].transform.position + dormList[dormList.Count - 1].transform.position) / 2f).magnitude;
    }

    [Header("Forward")]
    public Vector3 currentForward = Vector3.zero;

    [ContextMenu("CalculateForward")]
    public void CalculateForward()
    {
        int countTris = 0;
        Vector3 aveForward = Vector3.zero;

        for (int i = 0; i < dormList.Count - 2; i++)
            for (int j = i + 1; j < dormList.Count - 1; j++)
                for (int k = j + 1; k < dormList.Count; k++)
                {
                    countTris++;
                    Plane p = new Plane(dormList[i].transform.position, dormList[j].transform.position, dormList[k].transform.position);
                    Vector3 positiveNormal = Vector3.Dot(p.normal, Vector3.one) >= 0 ? p.normal : -p.normal;
                    aveForward += positiveNormal;
                }

        currentForward = aveForward /= countTris;
    }

    public void DisconnectDorm()
    {
        foreach (SatteliteDorm dorm in dormList) dorm.transform.parent = null;
    }

    public void ReconnectDorm()
    {
        foreach (SatteliteDorm dorm in dormList) dorm.transform.parent = transform;
    }
}
