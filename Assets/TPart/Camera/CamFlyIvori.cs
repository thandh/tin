﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamFlyIvori : MonoBehaviour
{
    [Header("Inputs")]
    public CamFly camFly = null;
    public TransStorage transStorage = null;
    
    [ContextMenu("Fly01")]
    public void Fly01()
    {
        Fly(0, 1);
    }

    [ContextMenu("Fly12")]
    public void Fly12()
    {
        Fly(1, 2);
    }

    void Fly(int indexBegin, int indexEnd)
    {
        transStorage.SetState(indexBegin);
        camFly.posEnd = transStorage.trStorageList[indexEnd].position;
        camFly.eulerEnd = transStorage.trStorageList[indexEnd].euler;
        camFly.Fly();
    }
}
