﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamFly : MonoBehaviour
{
    [Header("Inputs")]
    public Vector3 posEnd = Vector3.zero;
    public Vector3 eulerEnd = Vector3.zero;

    [Header("Params")]
    public float deltaPos = 1f;
    public float deltaEuler = 1f;

    [Header("Process")]
    public bool doneFly = true;
    public float deltaEndPos = 0.03f;
    public float deltaEndRot = 0.03f;

    [ContextMenu("Fly")]
    public void Fly()
    {
        StartCoroutine(C_Fly());
    }

    IEnumerator C_Fly()
    {
        doneFly = false;

        Quaternion endQua = Quaternion.Euler(eulerEnd);

        while ((transform.position - posEnd).magnitude > deltaEndPos && (transform.rotation.eulerAngles - eulerEnd).magnitude > deltaEndRot)
        {
            transform.position = Vector3.Lerp(transform.position, posEnd, Time.deltaTime * deltaPos);
            transform.rotation = Quaternion.Lerp(transform.rotation, endQua, Time.deltaTime * deltaEuler);
            yield return new WaitForEndOfFrame();
        }

        doneFly = true;

        yield break;
    }

    public void Fly(Vector3 endPos, Vector3 endRot)
    {   
        posEnd = endPos;
        eulerEnd = endRot;
        Fly();
    }

    public void Fly(Vector3 beginPos, Vector3 beginRot, Vector3 endPos, Vector3 endRot)
    {
        transform.position = beginPos;
        transform.rotation = Quaternion.Euler(beginRot);
        posEnd = endPos;
        eulerEnd = endRot;
        Fly();
    }

    [ContextMenu("Stop")]
    public void Stop()
    {
        StopAllCoroutines();
        doneFly = true;
    }
}
