﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformLock : MonoBehaviour
{
    [Header("lock")]
    public bool l_Position = false;
    public bool l_Rotation = false;
    public bool l_Scale = false;

    [Header("step")]
    public float stepPosition = 0.1f;
    public float stepRotation = 0.1f;
    public float stepScale = 0.1f;

    public Vector3 _pos = Vector3.zero;
    public Quaternion _rot = Quaternion.identity;
    public Vector3 _sca = Vector3.one;   
    
    [ContextMenu("CopyTransform")]
    public void CopyTransform()
    {
        _pos = transform.position;
        _rot = transform.rotation;
        _sca = transform.localScale;
    }

    private void Update()
    {
        if (l_Position && _pos != transform.position)
        {
            transform.position = new Vector3(Mathf.Lerp(transform.position.x, _pos.x, stepPosition),
                Mathf.Lerp(transform.position.y, _pos.y, stepPosition),
                Mathf.Lerp(transform.position.z, _pos.z, stepPosition));
        }

        if (l_Rotation && _rot != transform.rotation)
        {
            transform.rotation = new Quaternion(Mathf.Lerp(transform.rotation.x, _rot.x, stepRotation),
                Mathf.Lerp(transform.rotation.y, _rot.y, stepRotation),
                Mathf.Lerp(transform.rotation.z, _rot.z, stepRotation),
                Mathf.Lerp(transform.rotation.w, _rot.w, stepRotation));
        }

        if (l_Scale && _sca != transform.localScale)
        {
            transform.localScale = new Vector3(Mathf.Lerp(transform.localScale.x, _sca.x, stepScale),
                Mathf.Lerp(transform.localScale.y, _sca.y, stepScale),
                Mathf.Lerp(transform.localScale.z, _sca.z, stepScale));
        }
    }
}
