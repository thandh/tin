﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VertClosestToTransform : MonoBehaviour
{
    [Header("Note")]
    public string note = string.Empty;

    [Header("Inputs")]
    public MeshFilter mf = null;
    public Transform trCloseTo = null;

    [ContextMenu("GetClose")]
    public void GetClose()
    {
        transform.position = V3Closest();
    }

    public Vector3 V3Closest()
    {
        Vector3 v3closest = Vector3.zero;
        Vector3 v3CloseTo = trCloseTo.position;
        float d = Mathf.Infinity;
        Vector3[] verts;
        if (Application.isPlaying) verts = mf.mesh.vertices; else verts = mf.sharedMesh.vertices;
        Transform coreTr = mf.transform;
        for (int i = 0; i < verts.Length; i++)
        {
            if ((coreTr.TransformPoint(verts[i]) - v3CloseTo).magnitude < d)
            {
                d = (coreTr.TransformPoint(verts[i]) - v3CloseTo).magnitude;
                v3closest = coreTr.TransformPoint(verts[i]);
            }
        }
        return v3closest;
    }
}
