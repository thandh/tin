﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class VertGroupClosestToTransform : MonoBehaviour
{
    [Header("Inputs")]    
    public MeshFilter mf = null;
    public Transform trCloseTo = null;

    [Header("Params")]
    public int numberOfVert = 200;
    private List<VerticeType> vertList = new List<VerticeType>();

    [ContextMenu("GetClose")]
    public void GetClose()
    {
        Transform coreTr = mf.transform;
        Vector3 v3CloseTo = trCloseTo.position;
        
        Vector3[] verts = new Vector3[0];
        
        if (Application.isPlaying) verts = mf.mesh.vertices; else verts = mf.sharedMesh.vertices;
        if (numberOfVert > verts.Length) numberOfVert = verts.Length;
        
        vertList = new List<VerticeType>();
        for (int i = 0; i < verts.Length; i++)
        {
            vertList.Add(new VerticeType(i, verts[i]));            
        }

        vertList.Sort((a, b) => a.DistanceTo(coreTr, v3CloseTo).CompareTo(b.DistanceTo(coreTr, v3CloseTo)));
        while (vertList.Count > numberOfVert) vertList.RemoveAt(numberOfVert);

        Vector3 center = Vector3.zero;
        for (int i = 0; i < numberOfVert; i++) center += vertList[i].posInMesh;
        center /= numberOfVert;
        transform.position = coreTr.TransformPoint(center);
    }
}
