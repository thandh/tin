﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class XToBrain_FIleIO : MonoBehaviour
{
    public bool isDebug = false;

    [Header("Params")]
    public string filename = "XToBrain.txt";

    [Header("Params View Only")]
    [SerializeField] string _currentFolder = string.Empty;
    string currentFolder { get { if (_currentFolder != Head.Instance.caseInfo.GetCurrentCaseFolder()) _currentFolder = Head.Instance.caseInfo.GetCurrentCaseFolder(); return _currentFolder; } }

    [Header("Get Params")]
    public bool doneOutFile = true;
    public XToBrainFileJson fileJson = new XToBrainFileJson();

    [Serializable]
    public class XToBrainFileJson
    {
        public Vector3 pos = Vector3.zero;
        public Vector3 dir = Vector3.zero;
        public XToBrainFileJson() { }
        public XToBrainFileJson(Vector3 pos, Vector3 dir) { this.pos = pos; this.dir = dir; }
        public string toJson() { return JsonUtility.ToJson(this); }
    }

    private void Start()
    {
        DoInput();
    }

    [ContextMenu("ResetParams")]
    public void ResetParams()
    {
        fileJson.pos = Vector3.zero;
        fileJson.dir = Vector3.zero;
    }

    [Header("Input process")]
    public bool doneInput = false;

    [ContextMenu("DoInput")]
    public void DoInput()
    {
        StartCoroutine(C_Input());
    }

    IEnumerator C_Input()
    {
        doneInput = false;

        CheckExisted();

        if (isDebug) Debug.Log("start yield return new WaitUntil(() => Existed() == true)");
        yield return new WaitUntil(() => Existed() == true);

        if (isDebug) Debug.Log("start yield return new WaitUntil(() => doneCheckExisted == true)");
        yield return new WaitUntil(() => doneCheckExisted == true);

        ReadExisted();
        if (isDebug) Debug.Log("start yield return new WaitUntil(() => doneReadExisted == true)");
        yield return new WaitUntil(() => doneReadExisted == true);

        doneInput = true;

        yield break;
    }

    bool doneCheckExisted = false;

    [ContextMenu("CheckExisted")]
    public void CheckExisted()
    {
        StartCoroutine(C_CheckExisted());
    }

    IEnumerator C_CheckExisted()
    {
        doneCheckExisted = false;

        if (isDebug) Debug.Log("Checking " + currentFolder + filename);

        if (!File.Exists(currentFolder + filename))
        {
            StreamWriter sw = new StreamWriter(currentFolder + filename);
            sw.WriteLine(new centerFileJson());
            sw.Close();

            if (isDebug) Debug.Log("start yield return new WaitForSeconds(1f)");
            yield return new WaitForSeconds(1f);

            outFile(fileJson.toJson());
            if (isDebug) Debug.Log("yield return new WaitUntil(() => doneOutFile == true)");
            yield return new WaitUntil(() => doneOutFile == true);
        }

        yield return new WaitForSeconds(1f);

        doneCheckExisted = true;

        yield break;
    }

    bool doneReadExisted = false;

    void ReadExisted()
    {
        StartCoroutine(C_ReadExisted());
    }

    IEnumerator C_ReadExisted()
    {
        doneReadExisted = false;

        if (isDebug) Debug.Log("start yield return new WaitUntil(() => Existed() == true)");
        yield return new WaitUntil(() => Existed() == true);

        StreamReader sr = new StreamReader(currentFolder + filename);
        fileJson = JsonUtility.FromJson<XToBrainFileJson>(sr.ReadToEnd());
        sr.Close();

        doneReadExisted = true;
    }

    bool Existed()
    {
        return File.Exists(currentFolder + filename);
    }

    [ContextMenu("outCurreltFile")]
    public void outCurreltFile()
    {
        StartCoroutine(C_outFile(this.fileJson.toJson()));
    }

    public void outFile(string output)
    {
        StartCoroutine(C_outFile(output));
    }

    IEnumerator C_outFile(string output)
    {
        if (isDebug) Debug.Log("start C_outFile");
        if (isDebug) Debug.Log("start yield return new WaitUntil(() => Existed() == true)");
        yield return new WaitUntil(() => Existed() == true);

        doneOutFile = false;

        StreamWriter sw = new StreamWriter(currentFolder + filename);
        sw.WriteLine(output);
        sw.Close();

        if (isDebug) Debug.Log("start yield return new WaitUntil(() => IsFileLocked()");
        yield return new WaitUntil(() => IsFileLocked(new FileInfo(currentFolder + filename)) == false);

        doneOutFile = true;
        if (isDebug) Debug.Log("done C_outFile");

        yield break;
    }

    bool IsFileLocked(FileInfo file)
    {
        FileStream stream = null;

        try
        {
            stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
        }
        catch (IOException)
        {
            return true;
        }
        finally
        {
            if (stream != null)
                stream.Close();
        }

        return false;
    }

    [Header("Out put")]
    public DrawARayAt_PosDir rayXToBrain = null;    

    [ContextMenu("SetOutput")]
    public void SetOutput()
    {
        rayXToBrain.pos = fileJson.pos;
        rayXToBrain.dir = fileJson.dir;
    }
}
