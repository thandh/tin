﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Direction_XToBrain : MonoBehaviour
{
    [Header("Inputs")]
    public MeshFilter mf = null;

    [Header("Params")]
    public Transform xOrigin = null;
    public VertClosestToTransform brainClose = null;
    List<XPlane> xplaneList = new List<XPlane>();

    [Header("Outputs")]
    public DrawARayAt_PosDir drawRay = null;

    /// <summary>
    /// Calculate the vector that 
    /// + its plane has biggest perimeter
    /// + same direction to "X To Brain" point
    /// </summary>    
    
    [ContextMenu("DefineXToGo")]
    public void DefineXToGo()
    {        
        Mesh m = Application.isPlaying ? mf.mesh : mf.sharedMesh;
        Transform mfTrans = mf.transform;
        List<XPlane> xplaneList = new List<XPlane>()
        {
            new XPlane(mfTrans.right.normalized * m.bounds.extents.x, mfTrans.up.normalized * m.bounds.extents.y, mfTrans.forward.normalized * m.bounds.extents.z),
            new XPlane(mfTrans.right.normalized * m.bounds.extents.x, mfTrans.up.normalized * m.bounds.extents.y, -mfTrans.forward.normalized * m.bounds.extents.z),
            new XPlane(mfTrans.right.normalized * m.bounds.extents.x, mfTrans.forward.normalized * m.bounds.extents.z, mfTrans.up.normalized * m.bounds.extents.y),
            new XPlane(mfTrans.right.normalized * m.bounds.extents.x, mfTrans.forward.normalized * m.bounds.extents.z, -mfTrans.up.normalized * m.bounds.extents.y),
            new XPlane(mfTrans.forward.normalized * m.bounds.extents.x, mfTrans.up.normalized * m.bounds.extents.y, mfTrans.right.normalized * m.bounds.extents.x),
            new XPlane(mfTrans.forward.normalized * m.bounds.extents.x, mfTrans.up.normalized * m.bounds.extents.y, -mfTrans.right.normalized * m.bounds.extents.x),
        };

        brainClose.GetClose();
        Vector3 dirToCompare = brainClose.transform.position - xOrigin.position;
        xplaneList.Sort((a, b) => a.perimeter().CompareTo(b.perimeter()));
        for (int i = xplaneList.Count - 1; i >= 0; i--)
        {
            if (Vector3.Dot(xplaneList[i].normal, dirToCompare) > 0)
            {
                drawRay.pos = xOrigin.position;
                drawRay.dir = xplaneList[i].normal;
                break;
            }
        }
    }

    public class XPlane
    {
        public Vector3 width = Vector3.zero;
        public Vector3 height = Vector3.zero;
        public Vector3 normal = Vector3.zero;
        public XPlane() { }
        public XPlane(Vector3 width, Vector3 height, Vector3 normal) { this.width = width; this.height = height; this.normal = normal; }
        public float perimeter() { return width.magnitude * height.magnitude; }
    }
}
