﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(ExportIvori_InspectorMenu))]
public class IvoriExportEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        ExportIvori_InspectorMenu myScript = (ExportIvori_InspectorMenu)target;        
        GUILayoutOption[] buttonOption = new GUILayoutOption[1] { GUILayout.Width(73) };

        GUILayout.BeginHorizontal();
        GUILayout.Label("From CSV");
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("ReadExisted", buttonOption)) { myScript.ReadExisted(); }
        if (GUILayout.Button("ReadCase", buttonOption)) { myScript.ReadCase(); }
        if (GUILayout.Button("Export", buttonOption)) { myScript.Export(); }        
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Line");
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("SaveLine", buttonOption)) { myScript.SaveLine(); }        
        if (GUILayout.Button("Set Line", buttonOption)) { myScript.SetLine(); }
        if (GUILayout.Button("Reset Line", buttonOption)) { myScript.ResetLine(); }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Cam");
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("To Cam0", buttonOption)) { myScript.ToCam0(); }
        if (GUILayout.Button("To Cam1", buttonOption)) { myScript.ToCam1(); }
        if (GUILayout.Button("To Cam2", buttonOption)) { myScript.ToCam2(); }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();        
        if (GUILayout.Button("Save Cam0", buttonOption)) { myScript.SaveCam0(); }
        if (GUILayout.Button("Save Cam1", buttonOption)) { myScript.SaveCam1(); }
        if (GUILayout.Button("Save Cam2", buttonOption)) { myScript.SaveCam2(); }        
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Hera");
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Get Hera", buttonOption)) { myScript.GetHera(); }
        if (GUILayout.Button("Save Hera", buttonOption)) { myScript.SaveHera(); }
        GUILayout.EndHorizontal();        

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Get DKHera", buttonOption)) { myScript.GetDKHera(); }
        if (GUILayout.Button("Save DKHera", buttonOption)) { myScript.SaveDKHera(); }
        GUILayout.EndHorizontal();
    }
}