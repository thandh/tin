﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class MakeIvoriScrip
{
    [MenuItem("TIN/Create/IvoriScrip %#_i")]
    public static void CreateMyAsset()
    {
        IvoriScrip asset = ScriptableObject.CreateInstance<IvoriScrip>();

        AssetDatabase.CreateAsset(asset, "Assets/PQAsset/iVori/NewIvoriScrip.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
}