﻿using UnityEngine;
using System.Collections;

//[CreateAssetMenu(fileName = "Data", menuName = "Inventory/List", order = 1)]
public class IvoriScrip : ScriptableObject
{
    public ExportIvori.AnIvoriData currentData = new ExportIvori.AnIvoriData();

    [ContextMenu("AutoLoad")]
    public void AutoLoad()
    {

    }
}