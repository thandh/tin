﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class X : Singleton<X>
{
    public Transform picaUpDir = null;
    public Transform peek = null;
    public X_AutoDirection auto = null;
    public Transform origin = null;
    public MeshFilter mf = null;
}
