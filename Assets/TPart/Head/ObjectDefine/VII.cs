﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VII : Singleton<VII>
{
    [Header("Params")]
    public Transform peek;
    public Transform centerNDirection = null;
    public MeshFilter mf = null;
}
