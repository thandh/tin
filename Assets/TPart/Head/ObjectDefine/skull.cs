﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class skull : Singleton<skull>
{
    public bool isDebug = false;

    public bool showAtStart = true;

    [Header("Params")]
    public CutHole cutHole = null;
    public MeshRenderer mr = null;
    [SerializeField]
    MeshCollider _mc = null;
    public MeshCollider mc { get { if (_mc == null) _mc = mr.GetComponent<MeshCollider>(); return _mc; } }

    void Start ()
    {
        OnOffMesh(showAtStart);
	}
	
	public void OnOffMesh(bool On)
    {
        mr.enabled = On;
        mc.enabled = On;
    }

    [Header("Show Mesh")]
    public bool isShow = false;

    [ContextMenu("MeshOnOff")]
    public void MeshOnOff()
    {
        isShow = !isShow;
        mr.enabled = isShow;
        mc.enabled = isShow;
    }

    [Header("Skull Collision")]
    public List<SkullCollider> skullCollis = new List<SkullCollider>();

    [ContextMenu("GetList_SkullCollider")]
    public void GetList_SkullCollider()
    {
        skullCollis = FindObjectsOfType<SkullCollider>().ToList();
    }

    [ContextMenu("ResetskullCollis")]
    public void ResetskullCollis()
    {
        foreach (SkullCollider sc in skullCollis)
        {
            sc.detect.collis = new List<CollisionDetect.ArColli>();
        }
    }

    public void Add_onChange(onChange_collis onchange, Transform tr = null)
    {
        if (isDebug) if (tr != null) Debug.Log("adding onchange collis " + tr.name);
        foreach (SkullCollider sc in skullCollis)
        {
            sc.detect.onchange_collis += onchange;
        }
    }

    public void Remove_onChange(onChange_collis onchange, Transform tr = null)
    {
        if (isDebug) if (tr != null) Debug.Log("removing onchange collis " + tr.name);
        foreach (SkullCollider sc in skullCollis)
        {
            sc.detect.onchange_collis -= onchange;
        }
    }
}
