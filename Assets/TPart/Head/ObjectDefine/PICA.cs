﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PICA : Singleton<PICA>
{
    [Header("Inputs")]
    public Pica pica = null;
    public PicaUp picaUp = null;
    public Pica_PickNDrag picaDrag = null;

    [Header("Inputs")]
    public bool showAtStart = true;

    private void Start()
    {
        if (!showAtStart) StartCoroutine(C_Hide());
    }

    IEnumerator C_Hide()
    {
        yield return new WaitUntil(() => Instance != null);
        pica.gameObject.SetActive(false);
        yield break;
    }
}
