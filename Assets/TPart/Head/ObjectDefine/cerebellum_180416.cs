﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cerebellum_180416 : Singleton<cerebellum_180416>
{
    [Header("Params")]
    public MeshDeformer md = null;
    public MeshColliderByMeshFilter colliderByFilter = null;
    public MeshFilter mf = null;
}
