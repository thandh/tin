﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using ObjectList;
using System.Linq;

[Serializable]
public class HeadObjects
{
    public string childName = string.Empty;
    public string childPath = string.Empty;
}

public class Head : Singleton<Head>
{
    [Header("Managing self")]
    [SerializeField] Vector3 oriPosition = Vector3.zero;
    [SerializeField] Quaternion oriRotation = Quaternion.identity;
    [SerializeField] Vector3 oriScale = Vector3.zero;

    [ContextMenu("SaveOrigin")]
    public void SaveOrigin()
    {
        oriPosition = transform.position;
        oriRotation = transform.rotation;
        oriScale = transform.lossyScale;
    }

    [ContextMenu("GoOrigin")]
    public void GoOrigin()
    {
        transform.position = oriPosition;
        transform.rotation = oriRotation;
        transform.localScale = oriScale;
    }

    [Header("Managing child Objects")]
    [Information("Click Refresh to reload all child Object for HeadModels. If any error, it will be debuged.", InformationAttribute.InformationType.Info, false)]

    public List<HeadObjects> listByName = new List<HeadObjects>();

    #region local
    ObjsList _myobjList = null;
    ObjsList myobjList { get { if (_myobjList == null) _myobjList = GetComponent<ObjsList>(); return _myobjList; } }
        
    #endregion local

    private void Start()
    {
            
    }

    [ContextMenu("Refresh")]
    public void Refresh()
    {            
        //Get objects in list
        myobjList.list = new List<ObjectList.HeadObjects>();
        for (int i = 0; i < listByName.Count; i++)
        {
            ObjectList.HeadObjects ho = new ObjectList.HeadObjects();
            ho.childName = listByName[i].childName;
            ho.childGameObject = transform.Find(listByName[i].childPath).gameObject;
            myobjList.list.Add(ho);
        }

        //Update all Head methods
        List<HeadMethod> headMethods = FindObjectsOfType<HeadMethod>().ToList();
        foreach (HeadMethod hm in headMethods) hm.UpdateFromHead();
    }        

    [ContextMenu("Refresh")]
    public GameObject GetObject(string childName)
    {
        ObjectList.HeadObjects obj = myobjList.list.Find((x) => x.childName == childName);

        if (obj != null) return obj.childGameObject;
        else
        {
            Debug.Log(childName + " null");
            return null;
        }
    }

    private void OnEnable()
    {
        
    }

    [Header("TIN Case Info")]
    public TINCaseInfo caseInfo = null;
}
