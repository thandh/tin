﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class HeadInit_AllCenterTransform : HeadInitBase, iHeadInitBase
{
    [Header("InitParams")]
    public TransStorage centerOfTransform_TransStore = null;

    [ContextMenu("InitParams")]
    public void InitParams()
    {
        centerList = new List<CenterOfTransform>();
        for (int i = 0; i < centerOfTransform_TransStore.trStorageList.Count; i++)
            centerList.Add(centerOfTransform_TransStore.trStorageList[i].trs.GetComponent<CenterOfTransform>());
    }

    [Header("Center Transform")]
    public List<CenterOfTransform> centerList = new List<CenterOfTransform>();    

    [ContextMenu("GetAll CenterTransform")]
    public void GetAll_CenterTransform()
    {
        centerList = FindObjectsOfType<CenterOfTransform>().ToList();
    }

    [ContextMenu("DoInit")]
    public override void DoInit()
    {
        if (!allowInit) return;
        base.DoInit();
        StartCoroutine(C_Init());
    }

    IEnumerator C_Init()
    {
        doneInit = false;
        foreach (CenterOfTransform center in centerList) center.Center();
        doneInit = true;

        yield break;
    }
}