﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class HeadInit_CamKeyview : HeadInitBase, iHeadInitBase
{
    [ContextMenu("InitParams")]
    public void InitParams()
    {
        ivoriCylinder = FindObjectOfType<Ivori_ExportCylinder>();
        ivoriCylinder.InitParams();
    }

    [Header("Center Transform")]
    public Ivori_ExportCylinder ivoriCylinder = null;        

    [ContextMenu("DoInit")]
    public override void DoInit()
    {
        if (!allowInit) return;
        base.DoInit();
        StartCoroutine(C_Init());
    }

    IEnumerator C_Init()
    {   
        doneInit = false;

        ivoriCylinder.Auto_GetCylinder();

        doneInit = true;

        yield break;
    }
}