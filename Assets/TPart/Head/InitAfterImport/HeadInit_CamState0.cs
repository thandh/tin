﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class HeadInit_CamState0 : HeadInitBase, iHeadInitBase
{
    [Header("InitParams")]
    public Transform camPivot = null;

    [ContextMenu("InitParams")]
    public void InitParams()
    {
        camPivot = FindObjectOfType<CamPivot>().transform;
        state0 = camPivot.GetComponent<CamPivot_State0>();
        keyview = camPivot.GetComponent<CamPivot_Keyview>();
        possible = camPivot.GetComponent<Keyview_Possible>();
    }

    [Header("Center Transform")]
    public CamPivot_State0 state0 = null;
    public CamPivot_Keyview keyview = null;
    public Keyview_Possible possible = null;

    [ContextMenu("DoInit")]
    public override void DoInit()
    {
        if (!allowInit) return;
        base.DoInit();
        StartCoroutine(C_Init());
    }

    IEnumerator C_Init()
    {
        doneInit = false;
        state0.State0();
        keyview.SaveState0();
        possible.Output(0);
        doneInit = true;

        yield break;
    }
}