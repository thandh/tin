﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using tMeshIO;

public class HeadInit_AllMeshFilter : HeadInitBase, iHeadInitBase
{    
    [ContextMenu("InitParams")]
    public void InitParams()
    {
        Transform x = FindObjectOfType<X_AutoDirection>().transform;
        xPointControl = x.GetComponent<XPointControl>();
        autoxMeshIO = x.GetComponent<autoXMeshIO>();
        xSetPivotMethod = x.GetComponent<SetPivotMethod>();
    }

    [Header("From MeshFilter")]
    public XPointControl xPointControl = null;
    public autoXMeshIO autoxMeshIO = null;
    public SetPivotMethod xSetPivotMethod = null;

    [ContextMenu("DoInit")]
    public override void DoInit()
    {
        if (!allowInit) return;
        base.DoInit();
        StartCoroutine(C_Init());
    }

    IEnumerator C_Init()
    {
        doneInit = false;

        xPointControl.Reset0();
        autoxMeshIO.Reset0();
        xSetPivotMethod.Reset0();

        doneInit = true;

        yield break;
    }
}