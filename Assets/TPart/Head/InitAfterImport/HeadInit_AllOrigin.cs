﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class HeadInit_AllOrigin : HeadInitBase, iHeadInitBase
{
    [Header("InitParams")]
    public TransStorage originOfMesh_TransStore = null;
    public TransStorage OriginOfMeshBound_TransStore = null;

    [ContextMenu("InitParams")]
    public void InitParams()
    {
        originList = new List<OriginOfMesh>();
        for (int i = 0; i < originOfMesh_TransStore.trStorageList.Count; i++)        
            originList.Add(originOfMesh_TransStore.trStorageList[i].trs.GetComponent<OriginOfMesh>());
        
        originBoundList = new List<OriginOfMeshBound>();
        for (int i = 0; i < OriginOfMeshBound_TransStore.trStorageList.Count; i++)
            originBoundList.Add(OriginOfMeshBound_TransStore.trStorageList[i].trs.GetComponent<OriginOfMeshBound>());
    }

    [Header("Origin of Mesh")]
    public List<OriginOfMesh> originList = new List<OriginOfMesh>();
    public List<OriginOfMeshBound> originBoundList = new List<OriginOfMeshBound>();

    [ContextMenu("GetAll OriginOfMesh")]
    public void GetAll_OriginOfMesh()
    {
        originList = FindObjectsOfType<OriginOfMesh>().ToList();
    }

    [ContextMenu("GetAll OriginOfMeshBound")]
    public void GetAll_OriginOfMeshBound()
    {
        originBoundList = FindObjectsOfType<OriginOfMeshBound>().ToList();
    }

    [ContextMenu("DoInit")]
    public override void DoInit()
    {
        if (!allowInit) return;
        base.DoInit();
        StartCoroutine(C_Init());
    }

    IEnumerator C_Init()
    {
        doneInit = false;

        foreach (OriginOfMesh origin in originList) if (origin != null) origin.Center();
        foreach (OriginOfMeshBound origin in originBoundList) if (origin != null) origin.Center();

        doneInit = true;

        yield break;
    }
}