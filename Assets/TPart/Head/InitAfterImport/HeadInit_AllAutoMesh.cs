﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class HeadInit_AllAutoMesh : HeadInitBase, iHeadInitBase
{
    [ContextMenu("InitParams")]
    public void InitParams()
    {
        xAuto = X.Instance.GetComponent<X_AutoDirection>();
        //to-do : add other auto mesh in here
        //autoList
    }

    [Header("Auto List")]
    public List<MeshAutoDirection> autoList = new List<MeshAutoDirection>();
    public X_AutoDirection xAuto = null;

    [ContextMenu("GetAll_AutoMesh")]
    public void GetAll_AutoMesh()
    {
        autoList = FindObjectsOfType<MeshAutoDirection>().ToList();
    }

    [ContextMenu("DoInit")]
    public override void DoInit()
    {
        if (!allowInit) return;
        base.DoInit();
        StartCoroutine(C_Init());
    }

    IEnumerator C_Init()
    {
        doneInit = false;

        xAuto.FullAuto();
        if (isDebug) Debug.Log("Start yield xAuto.doneAutoDirection == true");
        yield return new WaitUntil(() => xAuto.doneAutoDirection == true);
        if (isDebug) Debug.Log("Done yield xAuto.doneAutoDirection == true");
        //foreach (MeshAutoDirection auto in autoList) auto.FullAuto();

        doneInit = true;

        yield break;
    }
}