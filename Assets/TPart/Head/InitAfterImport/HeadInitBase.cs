﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class HeadInitBase : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Process")]
    public bool allowInit = true;

    [Header("Init")]
    public bool doneInit = true;

    public virtual void DoInit()
    {
        
    }
}