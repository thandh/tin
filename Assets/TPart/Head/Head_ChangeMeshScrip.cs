﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using ObjectList;
using System.Linq;
using System.IO;
using SFB;
using UnityEditor;

public class Head_ChangeMeshScrip : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Clickable")]
    public bool ClickHere = false;

    [Header("MeshScrip")]
    public List<UseMeshScrip_SyncMesh> useMeshList = new List<UseMeshScrip_SyncMesh>();

    [ContextMenu("GetMeshScripList")]
    public void GetMeshScripList()
    {
        useMeshList = GetComponentsInChildren<UseMeshScrip_SyncMesh>().ToList();
    }

    [ContextMenu("OpenFolder")]
    public void OpenFolder()
    {
        string[] paths = StandaloneFileBrowser.OpenFolderPanel("Open Directory", Application.dataPath, false);
        SlashChanges(paths);
        HandleOpenFolder(paths);
    }

    void SlashChange(string strIn, out string strOut)
    {
        strOut = strIn.Replace('\\', '/');
    }

    void SlashChanges(string[] paths)
    {
        for (int i = 0; i < paths.Length; i++) SlashChange(paths[i], out paths[i]);
    }

    [Header("Path")]
    public TINCaseInfo caseInfo = null;

    [Header("InitBase")]
    public bool initAfterImport = true;
    public Head_InitBase headInit = null;

    public void HandleOpenFolder(string[] paths)
    {
        if (isDebug) Debug.Log("<color=red> Start HandleOpenFolder!!</color>");

        if (isDebug) for (int i = 0; i < paths.Length; i++) Debug.Log("At " + i + " : " + paths[i]);

        if (paths.Length == 0) return;
        DirectoryInfo dirInfo = new DirectoryInfo(paths[0]);

        SetupCaseInfo(paths[0]);        

        List<FileInfo> fileInfos = dirInfo.GetFiles().ToList();
        foreach (UseMeshScrip_SyncMesh useMesh in useMeshList)
        {
            if (useMesh == null) continue;
            int index = fileInfos.FindIndex((fi) => (fi.Extension.ToLower().Equals(".asset")) && NameOnly(fi).ToLower().Equals(useMesh.meshScripObjname.ToLower()));

            if (index == -1)
            {
                if (isDebug) Debug.Log("Can't find " + useMesh.meshScripObjname.ToLower());
                continue;
            } else
            {
                if (isDebug) Debug.Log("Changing " + caseInfo.DIRasset + "/" + NameOnly(fileInfos[index]));
                useMesh.meshScrip = (MeshScrip)AssetDatabase.LoadAssetAtPath(caseInfo.DIRasset + "/" + fileInfos[index].Name, typeof(MeshScrip));
                useMesh.SyncMesh();
            }
        }

        SetupCaseInfo(paths[0]);

        if (initAfterImport) headInit.DoInit();

        if (isDebug) Debug.Log("<color=red> Done HandleOpenFolder!!</color>");
    }

    void SetupCaseInfo(string path)
    {
        if (isDebug) Debug.Log("SetupCaseInfo");

        caseInfo.DIRfullPath = path;

        int indexOfAsset = caseInfo.DIRfullPath.IndexOf("Assets");
        if (indexOfAsset == -1)
        {
            if (isDebug) Debug.Log("Khong phai thu muc Asset!!");
            return;
        }

        caseInfo.DIRasset = caseInfo.DIRfullPath.Substring(indexOfAsset, caseInfo.DIRfullPath.Length - indexOfAsset);

        DirectoryInfo dirInfo = new DirectoryInfo(caseInfo.DIRfullPath);
        caseInfo.DIRname = dirInfo.Name;

        caseInfo.currentCaseFolder = string.Empty;
        caseInfo.PrepareFolder();
    }

    string NameOnly(FileInfo fi)
    {
        string nameOnly = fi.Name;
        nameOnly = nameOnly.Remove(nameOnly.IndexOf(fi.Extension), fi.Extension.Length);
        if (nameOnly[nameOnly.Length - 1] == '.') nameOnly = nameOnly.Remove(nameOnly.Length - 1, 1);
        return nameOnly;
    }    

    [ContextMenu("Stop")]
    public void Stop()
    {
        StopAllCoroutines();
    }
}
#endif