﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using ObjectList;

public class HeadMethod : MonoBehaviour
{
    [ContextMenu("UpdateFromHead")]
    public void UpdateFromHead()
    {
        UpdatetgTransparent();
        UpdateMeshFilter();
        UpdateFollow();
    }

    [Header("tgTransparentList")]
    public List<string> tgTransparentList = new List<string>();

    [ContextMenu("UpdatetgTransparent")]
    public void UpdatetgTransparent()
    {
        tgTransparent tg = GetComponent<tgTransparent>();
        if (tg == null) return;

        tg.objs = new List<Transform>();
        for (int i = 0; i < tgTransparentList.Count; i++)
        {
            tg.objs.Add(Head.Instance.GetObject(tgTransparentList[i]).transform);
        }
    }

    [Header("MeshFilter")]
    public string meshFilter = string.Empty;

    public void UpdateMeshFilter()
    {
        OriginOfMesh originOfMesh = GetComponent<OriginOfMesh>();
        if (originOfMesh != null) originOfMesh.meshFilter = Head.Instance.GetObject(meshFilter).GetComponent<MeshFilter>();

        OriginOfMeshBound originOfMeshBound = GetComponent<OriginOfMeshBound>();
        if (originOfMeshBound != null) originOfMeshBound.meshFilter = Head.Instance.GetObject(meshFilter).GetComponent<MeshFilter>();
    }

    [Header("Follow")]
    public string followObj = string.Empty;

    [ContextMenu("UpdateFollow")]
    public void UpdateFollow()
    {
        if (string.IsNullOrEmpty(followObj)) return;
        Follow follow = GetComponent<Follow>();
        if (follow != null) follow.target = Head.Instance.GetObject(followObj).transform;
    }

    [ContextMenu("InitBase")]
    public void InitBase()
    {
        Head_InitBase initBase = FindObjectOfType<Head_InitBase>();
        initBase.DoInit();
    }
}
