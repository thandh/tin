﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace ObjectList
{
    [Serializable]
    public class HeadObjects
    {
        public string childName = string.Empty;
        public GameObject childGameObject = null;
        public HeadObjects() { }
    }
    
    public class ObjsList : MonoBehaviour
    {
        public List<HeadObjects> list = new List<HeadObjects>();
    }
}