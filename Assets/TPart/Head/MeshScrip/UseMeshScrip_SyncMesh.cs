﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UseMeshScrip_SyncMesh : MonoBehaviour
{
    [Header("Inputs")]
    public string meshScripObjname = string.Empty;
    public MeshScrip meshScrip = null;

    [Header("Params")]
    public MeshFilter mf = null;
    public MeshCollider mc = null;

    [ContextMenu("GetSelfParams")]
    public void GetSelfParams()
    {
        mf = GetComponent<MeshFilter>();
        mc = GetComponent<MeshCollider>();
    }

    [ContextMenu("SyncMesh")]
    public void SyncMesh()
    {
        if (Application.isPlaying) { Debug.Log("Khong thay mesh khi dang runtime"); return; }
        if (meshScrip == null) { Debug.Log("meshScrip null"); return; }
        if (mf != null)
        {
            mf.mesh = meshScrip.mesh;
            mf.sharedMesh = meshScrip.mesh;
        }
        if (mc != null)
        {            
            mc.sharedMesh = meshScrip.mesh;
        }
    } 
}