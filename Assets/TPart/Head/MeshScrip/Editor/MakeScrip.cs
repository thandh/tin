﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class MakeScrip
{
    [MenuItem("TIN/Create/MeshScrip %#_m")]
    public static void CreateMeshScrip()
    {
        MeshScrip asset = ScriptableObject.CreateInstance<MeshScrip>();

        AssetDatabase.CreateAsset(asset, "Assets/TPart/Head/MeshScrip/Data/NewMeshScrip.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }

    [MenuItem("TIN/Create/ImportPanelScrip")]
    public static void CreateImportPanelScrip()
    {
        ImportPanelScrip asset = ScriptableObject.CreateInstance<ImportPanelScrip>();

        AssetDatabase.CreateAsset(asset, "Assets/NewImportScrip.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }

    [MenuItem("TIN/Create/PicaRigMeshScrip")]
    public static void CreatePicaRigScrip()
    {
        PicaRigScip asset = ScriptableObject.CreateInstance<PicaRigScip>();

        AssetDatabase.CreateAsset(asset, "Assets/PicaRigScip.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
}