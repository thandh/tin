﻿using UnityEngine;
using System.Collections;

//[CreateAssetMenu(fileName = "Data", menuName = "Inventory/List", order = 1)]
public class MeshScrip : ScriptableObject
{   
    public Mesh mesh = null;

    [ContextMenu("AutoLoad")]
    public void AutoLoad()
    {

    }
}