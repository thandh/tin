﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using ObjectList;
using System.Linq;
using System.IO;
using SFB;

[ExecuteInEditMode]
public class Head_InitBase : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;    

    [Header("InitBase")]    
    public List<HeadInitBase> initList = new List<HeadInitBase>();
    public List<iHeadInitBase> iinitList = new List<iHeadInitBase>();

    [ContextMenu("DebugInitList")]
    public void DebugInitList()
    {
        for (int i = 0; i < initList.Count; i++)
        {
            Debug.Log(initList[i].name + " : " + initList[i].allowInit);
        }
    }

    [ContextMenu("GetAllInitBase")]
    public void GetAllInitBase()
    {
        initList = GetComponentsInChildren<HeadInitBase>().ToList();
        UpdateiInitBase();
    }

    [ContextMenu("UpdateiInitBase")]
    public void UpdateiInitBase()
    {
        iinitList = new List<iHeadInitBase>();
        for (int i = 0; i < initList.Count; i++)
        {
            iinitList.Add(initList[i].GetComponent<iHeadInitBase>());
            iinitList[i].InitParams();
        }
    }

    [Header("Progress")]
    public bool doneInit = true;

    [ContextMenu("DoInit")]
    public void DoInit()
    {
        StartCoroutine(C_Init());
    }
    
    IEnumerator C_Init()
    {
        doneInit = false;
        
        if (isDebug) Debug.Log("Start C_Init");
        for (int i = 0; i < initList.Count; i++)
        {
            if (isDebug) Debug.Log("Start initList[" + i.ToString() + "].DoInit()");
            initList[i].DoInit();
            if (isDebug) Debug.Log("Start yield initList[" + i.ToString() + "].doneInit");
            yield return new WaitUntil(() => initList[i].doneInit == true);
        }
        if (isDebug) Debug.Log("Done C_Init");
        doneInit = true;
        yield break;
    }    

    [ContextMenu("Stop")]
    public void Stop()
    {
        StopAllCoroutines();
    }
}