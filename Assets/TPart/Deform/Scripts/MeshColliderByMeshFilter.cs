﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshColliderByMeshFilter : MonoBehaviour
{
    [Header("Params")]
    public MeshFilter mf = null;
    public MeshCollider mc = null;

    [ContextMenu("ColliderSyncFilter")]
    public void ColliderSyncFilter()
    {
        mc.sharedMesh = null;
        mc.sharedMesh = mf.mesh;
    }
}
