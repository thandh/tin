﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshDeformer))]
public class CerebellumDeform : MonoBehaviour
{
    public bool AllowKeyboard = true;

    MeshDeformer md = null;    

    private void Start()
    {
        md = GetComponent<MeshDeformer>();
    }

    private void Update()
    {
        if (AllowKeyboard)
        {
            if (Input.GetKey(KeyCode.F))
            {
                if (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift))
                {
                    md.AddDeformingForce();
                }
                else
                {
                    md.AddDeformingReverseForce();
                }
            }

            if (Input.GetKey(KeyCode.R))
            {
                md.allowDeform = !md.allowDeform;
            }
        }
    }
}
