﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Seri : MonoBehaviour
{
    public float increaseRate = 0.05f;
    public float deltaSum = 0f;

    [ContextMenu("Calculate")]
    public void Calculate()
    {
        StartCoroutine(C_Sum());
    }

    IEnumerator C_Sum()
    {
        float sum = 1;
        int countWeek = 0;

        while (sum < 2 - deltaSum)
        {
            countWeek++;
            sum += increaseRate * sum;

            Debug.Log(countWeek + " " + sum);
            yield return new WaitForEndOfFrame();
        }

        yield break;
    }

    [ContextMenu("Stop")]
    public void Stop()
    {
        StopAllCoroutines();
    }
}
