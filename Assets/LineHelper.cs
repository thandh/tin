﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineHelper : MonoBehaviour
{

	public string line;

	[ContextMenu ("Get String")]
	public void GetStringLineForIvory ()
	{
		LineRenderer lr = GetComponent<LineRenderer> ();
		for (int i = 0; i < lr.positionCount; i++) {
			Vector3 pos = lr.GetPosition (i);
			line += "" + pos.x + "," + pos.y + "," + pos.z + ",";
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}
}
