﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CamMovementMethod : MonoBehaviour
{
    public OriginOfMeshBound originSkull = null;
    public CamSnap camSnap = null;

    [ContextMenu("InitAChange")]
    public void InitAChange()
    {
        originSkull.Center();
        camSnap.trRotate.position = originSkull.transform.position;
        camSnap.trRotate.localRotation = Quaternion.identity;
        camSnap.Distance();
    }
}