﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class CamPivot : MonoBehaviour
{
    public bool isDebug = false;    

    [Header("mainCam")]
    public Transform mainCam = null;

    [Header("Reset0")]
    public float resetZPos = -20f;

    [ContextMenu("Reset0")]
    public void Reset0()
    {
        mainCam.localPosition = new Vector3(0, 0, resetZPos);
        mainCam.localRotation = Quaternion.Euler(Vector3.zero);
        mainCam.localScale = Vector3.one;
    }

    public float GetZ()
    {
        return mainCam.localPosition.z;
    }

    [ContextMenu("SetZ")]
    public void SetZ(float z)
    {
        mainCam.localPosition = new Vector3(0, 0, z);
    }

    [ContextMenu("LookRezAndStraightX")]
    public void LookRezAndStraightX()
    {
        transform.rotation = Quaternion.LookRotation(Rez.Instance.transform.position - transform.position, X.Instance.origin.forward);
    }
}