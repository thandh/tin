﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Keyview_AutoStepback : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Inputs")]
    public CamPivot_Keyview keyview = null;
    public Keyview_Method keyviewMethod = null;
    public List<VectorOnCam> oncamList = new List<VectorOnCam>();

    [Header("Inputs Params")]
    public BoundingBox xBound = null;

    [ContextMenu("Init")]
    public void Init()
    {        
        oncamList = new List<VectorOnCam>();
        oncamList.Add(xBound.peekX.GetComponent<VectorOnCam>());
        oncamList.Add(xBound.peekY.GetComponent<VectorOnCam>());
        oncamList.Add(xBound.peekZ.GetComponent<VectorOnCam>());
        oncamList.Add(xBound.peekNX.GetComponent<VectorOnCam>());
        oncamList.Add(xBound.peekNY.GetComponent<VectorOnCam>());
        oncamList.Add(xBound.peekNZ.GetComponent<VectorOnCam>());

        foreach (VectorOnCam onCam in oncamList)
        {
            onCam.camIn = Camera.main;
        }
    }

    bool IsValid_OnCamList()
    {
        if (oncamList.Count != 6) return false;
        for (int i = 0; i < oncamList.Count; i++) if (oncamList[i] == null) return false;
        return true;
    }

    [Header("Stepback X Inside Params")]
    public float ratioWidth = 0.01f;
    public float ratioHeight = 0.01f;

    [Header("Stepback Params")]
    public float stepDefault = 1f;
    public float timeDelay = 0.5f;

    [ContextMenu("AutoStepback_GetXInsideView")]
    public void AutoStepback_GetXInsideView()
    {
        StartCoroutine(C_AutoStepback());
    }

    IEnumerator C_AutoStepback()
    {
        if (isDebug) Debug.Log("Start C_AutoStepback");

        if (!IsValid_OnCamList()) Init();

        keyview.stepBack = stepDefault;

        while (!CheckXInside())
        {
            if (isDebug) Debug.Log("One Stepback!!");
            keyviewMethod.Stepback();
            yield return new WaitForSeconds(timeDelay);
        }

        if (isDebug) Debug.Log("Done C_AutoStepback");
        yield break;
    }

    [ContextMenu("DoACheckXInside")]
    public void DoACheckXInside()
    {
        Debug.Log("Is X inside? : " + CheckXInside());
    }

    bool CheckXInside()
    {
        Resolution currentRes = Screen.currentResolution;
        if (isDebug) Debug.Log("currentRes " + currentRes);

        foreach (VectorOnCam oncam in oncamList)
        {
            oncam.FindVector();

            if (isDebug) Debug.Log(oncam.gameObject.name + " oncam : " + oncam.pRoot.x + ", " + oncam.pRoot.y);

            if ((oncam.pRoot.x < ratioWidth * currentRes.width || oncam.pRoot.x > (1 - ratioWidth) * currentRes.width)
                || (oncam.pRoot.y < ratioHeight * currentRes.height || oncam.pRoot.y > (1 - ratioHeight) * currentRes.height)) return false;

            
        }
        return true;
    }

    [ContextMenu("Stop")]
    public void Stop()
    {
        StopAllCoroutines();
    }

    [Header("Stepback Fixed Turns Params")]
    public int turnCount = 12;

    [ContextMenu("AutoStepback_FixedTurn")]
    public void AutoStepback_FixedTurn()
    {
        keyviewMethod.State0();
        for (int i = 0; i < turnCount; i++)
        {
            keyviewMethod.Stepback();
        }
    }
}
