﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyviewChoices : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Params")]
    public Keyview_FromRuntime keyviewRuntime = null;
    public TransStorage_SetState keyviewEditor = null;
    public Craniotomy crain = null;

    [ContextMenu("SetKeyview")]
    public void SetKeyview()
    {
        if (crain.hasOutput)
        {
            if (isDebug) Debug.Log("Tim duoc crain!! Chay cylinder runtime!!");
            keyviewRuntime.KeyviewFromRuntime();
        }
        else
        {
            if (isDebug) Debug.Log("Khong tim duoc crain!! Chay set ID!!");
            keyviewEditor.SetStorageFollowID();
        }
    }
}
