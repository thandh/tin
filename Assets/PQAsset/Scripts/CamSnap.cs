﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CamSnap : MonoBehaviour
{
    public bool IsOn = true;

    public float flySpeed = 10f;
    public float walkSpeed = 10f;

    public float rotateSpeed = 200;

    bool isMouse;
    float nowPosX, nowPosY;

    public Transform trRotate = null;
    public Transform trTarget = null;

    Camera camera;

    private CharacterController characterController;

    //public Camera Ccamera;

    void Start()
    {

        // this.transform.position = new Vector3(11.2f,5.5f,0.5f);


        if (GetComponent<Rigidbody>())
            GetComponent<Rigidbody>().freezeRotation = true;
        characterController = GetComponent<CharacterController>();
        camera = Camera.main.GetComponent<Camera>();
    }

    void Update()
    {
        if (!IsOn) return;

        // 右下の断面メニューが表示されていたら動かない.
        if (Input.GetMouseButtonDown(1))
        {
            isMouse = true;
        }
        if (Input.GetMouseButtonUp(1))
        {
            isMouse = false;
        }

        keyboardController();

        if (trTarget != null && trRotate != null && doneLookAt == false)
        {
            trTarget.LookAt(trRotate);
            doneLookAt = true;
        }

        //  resteOrigin();
    }

    //============================================================================
    //                    C  A  M  E  R  A     W  A  L  K
    //============================================================================

    void cameraWalk()
    {
        // left
        if (Input.GetMouseButton(0))
        {
            leftClickCameraWalk();
        }
        // right
        if (Input.GetMouseButton(1))
        {
            rightClickCameraWalk();
        }
        // wheel
        if (Input.GetMouseButton(2))
        {
            wheelClickCameraWalk();
        }
    }

    //=======================================
    // left click walk
    //=======================================

    void leftClickCameraWalk()
    {
        const int buttonId = 0;

        // down
        if (Input.GetMouseButtonDown(buttonId))
        {
            beginHumanMove();
        }

        // update
        else if (Input.GetMouseButton(buttonId))
        {
            updateHumanMove();
        }

        // up
        else if (Input.GetMouseButtonUp(buttonId))
        {
            endHumanMove();

        }
    }

    //=======================================
    // right click walk
    //=======================================

    void rightClickCameraWalk()
    {
        const int buttonId = 1;

        // down
        if (Input.GetMouseButtonDown(buttonId))
        {
            beginRotateView();
        }
        // update
        else if (Input.GetMouseButton(buttonId))
        {
            updateRotateView();
        }
        // up
        else if (Input.GetMouseButtonUp(buttonId))
        {
            endRotateView();
        }
    }

    //=======================================
    // wheel click walk
    //=======================================

    void wheelClickCameraWalk()
    {
        const int buttonId = 2;

        // down
        if (Input.GetMouseButtonDown(buttonId))
        {
            ;
        }
        // update
        else if (Input.GetMouseButton(buttonId))
        {
            ;
        }
        // up
        else if (Input.GetMouseButtonUp(buttonId))
        {
            ;
        }
    }

    //============================================================================
    //                    C  A  M  E  R  A     F  L  Y
    //============================================================================
    void cameraFly()
    {
        // left
        if (Input.GetMouseButton(0))
        {
            leftClickCameraFly();
        }
        // right
        if (Input.GetMouseButton(1))
        {
            rightClickCameraFly();
        }
        // wheel
        if (Input.GetMouseButton(2))
        {
            wheelClickCameraFly();
        }
    }

    //=================================
    // left click fly
    //=================================
    void leftClickCameraFly()
    {
        const int buttonId = 0;

        // down
        if (Input.GetMouseButtonDown(buttonId))
        {
            beginHumanMove();
        }

        // update
        else if (Input.GetMouseButton(buttonId))
        {
            updateHumanMove();
        }

        // up
        else if (Input.GetMouseButtonUp(buttonId))
        {
            endHumanMove();
        }
    }

    //=================================
    // right click fly
    //=================================

    void rightClickCameraFly()
    {
        const int buttonId = 1;

        // down
        if (Input.GetMouseButtonDown(buttonId))
        {
            beginParallelMove();
        }
        // update
        else if (Input.GetMouseButton(buttonId))
        {
            updateParallelMove();
        }
        // up
        else if (Input.GetMouseButtonUp(buttonId))
        {
            endParallelMove();
        }
    }

    //=================================
    // wheel click fly
    //=================================

    void wheelClickCameraFly()
    {
        const int buttonId = 2;

        // down
        if (Input.GetMouseButtonDown(buttonId))
        {
            beginRotateView();
        }
        // update
        else if (Input.GetMouseButton(buttonId))
        {
            updateRotateView();
        }

        // up
        else if (Input.GetMouseButtonUp(buttonId))
        {
            endRotateView();
        }
    }

    //=================================================================
    //             M  O  V  E     U  T  I  L  S
    //=================================================================

    //===============================================
    //       M  O  V  E     H  U  M  A  N
    //===============================================
    Vector3 moveHumanStartPos;

    void beginHumanMove()
    {
        moveHumanStartPos = mousePosToViewPort();
    }

    void updateHumanMove()
    {
        var delta = mousePosToViewPort() - moveHumanStartPos;

        // move forward / back.
        float moveSpeed = delta.y * Time.deltaTime * flySpeed;
        var pos = transform.position;
        pos = pos + transform.forward * moveSpeed;

        // rotate Y axis
        float rotSpeed = delta.x * Time.deltaTime * rotateSpeed;
        var rot = transform.eulerAngles;
        rot = rot + new Vector3(0, rotSpeed, 0);
        //transform.eulerAngles = rot;
        rotate(camera.transform.localEulerAngles.x, rot.y);
    }

    void endHumanMove()
    {
        moveHumanStartPos = Vector3.zero;
    }

    //===================================================
    //       M  O  V  E     P  A  R  A  L  L  E  L
    //===================================================
    Vector3 moveParallelStartPos;

    void beginParallelMove()
    {
        moveParallelStartPos = mousePosToViewPort();
    }

    void updateParallelMove()
    {
        var delta = mousePosToViewPort() - moveParallelStartPos;

        // move Xaxis / Zaxis
        Vector3 moveSpeed = new Vector3(delta.x, delta.y, 0) * Time.deltaTime * flySpeed;
        var moveSpeedRelative = transform.TransformDirection(moveSpeed);
        var pos = transform.position;
        pos = pos + moveSpeedRelative;


    }

    void endParallelMove()
    {
        moveParallelStartPos = Vector3.zero;
    }

    //===================================================
    //       R  O  T  A  T  E     V  I  E  W
    //===================================================
    Vector3 rotateViewStartPos;

    void beginRotateView()
    {
        rotateViewStartPos = mousePosToViewPort();
    }

    void updateRotateView()
    {
        var delta = mousePosToViewPort() - rotateViewStartPos;

        // rotate Yaxis move Self Transform.
        Vector3 rotSpeedSelf = new Vector3(0, delta.x, 0) * Time.deltaTime * rotateSpeed;
        var selfRot = transform.eulerAngles;
        selfRot = selfRot + rotSpeedSelf;

        // rotate Xaxis move Camea Transform.
        Vector3 rotSpeedCamera = new Vector3(-delta.y/* Inverse */, 0, 0) * Time.deltaTime * rotateSpeed;
        var cameraRot = camera.transform.eulerAngles;
        cameraRot = cameraRot + rotSpeedCamera;

        rotate(cameraRot.x, selfRot.y);
    }

    void endRotateView()
    {
        rotateViewStartPos = Vector3.zero;
    }

    //=================================================================
    //                   U  T  I  L  S
    //=================================================================

    Vector3 mousePosToViewPort()
    {
        var pos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
        pos = normalizeViewPort(pos);
        return pos;
    }

    Vector3 normalizeViewPort(Vector3 viewPort)
    {
        float aspect = (float)Screen.height / (float)Screen.width;

        viewPort.x = viewPort.x * aspect;

        return viewPort;
    }

    void moveFly(Vector3 nextPos)
    {
        transform.position = nextPos;
    }

    //================================
    // character controller
    //================================

    Vector3 lastInputDirection;

    void moveCharacterController(Vector3 moveDirection)
    {
        //c.Move(moveDirection);
        lastInputDirection = moveDirection;
    }

    void updateCharacterController()
    {
        if (characterController.isGrounded)
        {
            lastInputDirection.y = 0;
        }
        lastInputDirection.y -= 20f * Time.deltaTime;
        characterController.Move(lastInputDirection);

        if (characterController.isGrounded)
        {
            lastInputDirection = Vector3.zero;
        }
    }

    void rotate(float nextX, float nextY)
    {
        transform.eulerAngles = new Vector3(0, nextY, 0);
        camera.transform.localEulerAngles = new Vector3(nextX, 0, 0);
    }

    [Header("keyboardController ")]
    public float minDistance = 1f;
    public float maxDistance = 100f;

    public float stepPos = 10f;
    public float stepRot = 10f;

    [SerializeField]
    bool doneLookAt = true;

    void keyboardController()
    {
        // Up down camera
        if (Input.GetKey(KeyCode.Q))
        {
            if ((trTarget.position - trRotate.position).magnitude > minDistance)
            {
                trTarget.Translate(trTarget.forward * Time.deltaTime * stepPos);
                Clam();
            }

            doneLookAt = false;
        }

        if (Input.GetKey(KeyCode.E))
        {
            if ((trTarget.position - trRotate.position).magnitude < maxDistance)
            {
                trTarget.Translate(-trTarget.forward * Time.deltaTime * stepPos);
                Clam();
            }

            doneLookAt = false;
        }

        // rotate camera
        if (Input.GetKey(KeyCode.D))
        {
            trRotate.Rotate(trRotate.up * Time.deltaTime * stepRot);

            doneLookAt = false;
        }

        if (Input.GetKey(KeyCode.A))
        {
            trRotate.Rotate(-trRotate.up * Time.deltaTime * stepRot);

            doneLookAt = false;
        }

        if (Input.GetKey(KeyCode.W))
        {
            trRotate.Rotate(trRotate.right * Time.deltaTime * stepRot);

            doneLookAt = false;
        }

        if (Input.GetKey(KeyCode.S))
        {
            trRotate.Rotate(-trRotate.right * Time.deltaTime * stepRot);

            doneLookAt = false;
        }

        if (isMouse)
        {
            trRotate.Rotate(new Vector3(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"), 0) * Time.deltaTime * stepRot);

            doneLookAt = false;
        }
    }

    void Clam()
    {
        if ((trTarget.position - trRotate.position).magnitude < minDistance)
        {
            Vector3 dir = trTarget.position - trRotate.position;
            trTarget.position = trRotate.position + dir.normalized * minDistance;
        }
        else if ((trTarget.position - trRotate.position).magnitude > maxDistance)
        {
            Vector3 dir = trTarget.position - trRotate.position;
            trTarget.position = trRotate.position + dir.normalized * maxDistance;
        }
    }

    [Header("Distance")]
    public float distance = 50f;

    [ContextMenu("Distance")]
    public void Distance()
    {
        Vector3 dir = trTarget.position - trRotate.position;
        trTarget.position = trRotate.position + dir.normalized * distance;
        Clam();
    }

    public static float ClampAngle(float angle, float min, float max)
    {
        angle = angle % 360;
        if ((angle >= -360F) && (angle <= 360F))
        {
            if (angle < -360F)
            {
                angle += 360F;
            }
            if (angle > 360F)
            {
                angle -= 360F;
            }
        }
        return Mathf.Clamp(angle, min, max);
    }
}