﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Keyview_Possible : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Output")]
    public List<TransStorage> keys = new List<TransStorage>();

    [Header("Review")]
    public ApplybyWorldTransform pivotApplyTransform = null;
    public ApplybyLocalTransform camApplyTransform = null;
    public int viewAt = 0;

    [ContextMenu("ViewAt")]
    public void ViewAt()
    {
        if (this.keys.Count == 0) { return; }
        TransStorage aTransStore = keys[viewAt];
        if (aTransStore.trStorageList.Count != 2) { Debug.Log("Invalid Key!"); return; }

        pivotApplyTransform.applyPos = aTransStore.trStorageList[0].position;
        pivotApplyTransform.applyPosition();
        pivotApplyTransform.applyRotEuler = aTransStore.trStorageList[0].euler;
        pivotApplyTransform.applyRotation_byEuler();

        camApplyTransform.applyPos = aTransStore.trStorageList[1].position;
        camApplyTransform.ApplyPosition();
        camApplyTransform.applyRotEuler = aTransStore.trStorageList[1].euler;
        camApplyTransform.ApplyRotation_byEuler();
    }

    [ContextMenu("ViewNext")]
    public void ViewNext()
    {
        if (this.keys.Count == 0) return;
        viewAt = viewAt < this.keys.Count - 1 ? viewAt + 1 : 0;
        ViewAt();
    }

    [ContextMenu("ViewPrevious")]
    public void ViewPrevious()
    {
        if (this.keys.Count == 0) return;
        viewAt = viewAt > 0 ? viewAt - 1 : this.keys.Count - 1;
        ViewAt();
    }

    [Header("Output")]
    public TransStorage outState0Store = null;
    public TransStorage outKeyviewStore = null;

    [ContextMenu("Output")]
    public void Output()
    {
        if (isDebug) Debug.Log("Start Output");
        outState0Store.SyncFrom(keys[0]);
        outKeyviewStore.SyncFrom(keys[1]);
        if (isDebug) Debug.Log("Done Output");
    }

    public void Output(int index = -1)
    {
        if (index == -1)
        {
            outState0Store.SyncFrom(keys[0]);
            outKeyviewStore.SyncFrom(keys[1]);
        } else outKeyviewStore.SyncFrom(keys[index]);
    }
}
