﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class CamPivot_State0 : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("mainCam")]
    public CamPivot camPivot = null;

    [Header("State0")]
    public float state0ZPos = -200f;    
    public LookAtTransform camLookAtPivot = null;

    [ContextMenu("State0")]
    public void State0()
    {        
        camPivot.Reset0();
        camPivot.SetZ(state0ZPos);
        Vector3 dirFromRezToX = X.Instance.origin.position - Rez.Instance.transform.position;
        camPivot.transform.position = Rez.Instance.transform.position + dirFromRezToX.normalized;
        camPivot.LookRezAndStraightX();
    }

    [ContextMenu("State0FromKeyview")]
    public void State0FromKeyview()
    {
        //camPivot.mainCam.parent = null;
        //snapRez.SnapPosition();
        //camPivot.transform.rotation = 
    }
}