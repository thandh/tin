﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class CamPivot_Keyview : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Clickable")]
    public bool ClickHere = false;

    [Header("Input State 0")]
    public CamPivot camPivot = null;
    public Snap snapPivotToCenter = null;
    public Snap snapVIIToOrigin = null;
    public VectorOnCam oncamVII = null;
    public Snap snapVIIIToOrigin = null;
    public VectorOnCam oncamVIII = null;
    public VertClosestToTransform sinusCloseX = null;
    public VertClosestToTransform brainCloseX = null;        
    public CenterOfTransform centerS_Br = null;
    public VertClosestToTransform sinusCloseCenter = null;
    public VertClosestToTransform brainCloseCenter = null;
    public Direction_XToBrain dirXToBrain = null;

    [ContextMenu("Stop")]
    public void Stop()
    {
        if (isDebug) Debug.Log("Stop");
        StopAllCoroutines();
    }

    [ContextMenu("InitParams")]
    public void InitParams()
    {
        if (isDebug) Debug.Log("InitParams");

        camPivot.Reset0();
        camPivot.SetZ(0);
        snapVIIToOrigin.target = VII.Instance.centerNDirection;
        oncamVII.dirTr.trans = VII.Instance.centerNDirection;
        snapVIIIToOrigin.target = VIII.Instance.centerNDirection;
        oncamVIII.dirTr.trans = VIII.Instance.centerNDirection;
        sinusCloseX.mf = sinus.Instance.mf;
        sinusCloseX.trCloseTo = X.Instance.origin;
        brainCloseX.mf = cerebellum_180416.Instance.mf;
        brainCloseX.trCloseTo = X.Instance.origin;        
        sinusCloseCenter.mf = sinus.Instance.mf;
        sinusCloseCenter.trCloseTo = centerS_Br.transform;
        brainCloseCenter.mf = cerebellum_180416.Instance.mf;
        brainCloseCenter.trCloseTo = centerS_Br.transform;
        dirXToBrain.mf = X.Instance.mf;
        dirXToBrain.xOrigin = X.Instance.origin;
    }

    /// <summary>
    /// Using vert close
    /// </summary>
    [ContextMenu("State0")]
    public void State0()
    {
        if (isDebug) Debug.Log("State0");

        dirXToBrain.DefineXToGo();

        sinusCloseX.GetClose();
        brainCloseX.GetClose();
        centerS_Br.Center();
        snapPivotToCenter.SnapPosition();
        camPivot.LookRezAndStraightX();
    }    

    [Header("Calibration")]
    public float stepBack = 1f;

    [Header("Stepback Direction")]
    public DrawARayAt_PosDir drawARay = null;

    [ContextMenu("Stepback")]
    public void Stepback()
    {
        if (isDebug) Debug.Log("Stepback");

        centerS_Br.transform.position += drawARay.dir.normalized * stepBack;
        sinusCloseCenter.GetClose();
        brainCloseCenter.GetClose();
        centerS_Br.Center();
        snapPivotToCenter.SnapPosition();
        camPivot.LookRezAndStraightX();
    }

    [ContextMenu("SaveState0")]
    public void SaveState0()
    {
        SaveAtIndex(0);
    }

    [ContextMenu("SaveKeyView")]
    public void SaveKeyView()
    {
        SaveAtIndex(1);
    }

    void SaveAtIndex(int index)
    {
        possibleCam.keys[index].trStorageList = new List<TransStorage.aTransStorage>();
        possibleCam.keys[index].trStorageList.Add(new TransStorage.aTransStorage(camPivot.transform, TransStorage.Space.World, camPivot.transform.position, camPivot.transform.rotation.eulerAngles, camPivot.transform.lossyScale));
        possibleCam.keys[index].trStorageList.Add(new TransStorage.aTransStorage(camPivot.mainCam, TransStorage.Space.Local, camPivot.mainCam.localPosition, camPivot.mainCam.localRotation.eulerAngles, camPivot.mainCam.localScale));        
    }

    bool CheckOnCam(VectorOnCam oncam, float minX, float maxX, float minY, float maxY)
    {
        Resolution currentRes = Screen.currentResolution;
        return (oncam.pRoot.x >= minX && oncam.pRoot.x <= maxX && oncam.pRoot.y >= minY && oncam.pRoot.y <= maxY);
    }

    bool CheckOnCam_SinusNotSameBrain(VectorOnCam oncamSinus, VectorOnCam oncamBrain)
    {
        Resolution currentRes = Screen.currentResolution;
        return (CheckOnCam(oncamSinus, 0f, currentRes.width / 2f, currentRes.height / 2f, currentRes.height))
            && !(CheckOnCam(oncamBrain, 0f, currentRes.width / 2f, currentRes.height / 2f, currentRes.height));
    }

    [Header("Output RotateInStep")]
    public float minAngleVIIVIII = Mathf.Infinity;    
    public Keyview_Possible possibleCam = null;

    [Header("Use Crain Editor")]
    public Ivori_ExportCylinder ivoriExport = null;
    public FileOutput filetCrain = null;
    public FileOutput fileCrain = null;
    public tCraniotomy tCrain = null;        
    public float crainHalfWidth = 20.04f;

    [ContextMenu("InitParams_UseCrainFile")]
    public void InitParams_UseCrainFile()
    {
        if (isDebug) Debug.Log("InitParams_UseCrainFile");

#if UNITY_EDITOR
        ivoriExport = FindObjectOfType<Ivori_ExportCylinder>();
        filetCrain = ivoriExport.tCrainFileOut;
        fileCrain = ivoriExport.crainFileOut;
#endif

        filetCrain.Import();
        TransStorage.aTransStorage dat_tCrain = JsonUtility.FromJson<TransStorage.aTransStorage>(filetCrain.data);
        fileCrain.Import();
        TransStorage.aTransStorage dat_Crain = JsonUtility.FromJson<TransStorage.aTransStorage>(fileCrain.data);

        tCrain.transform.position = dat_tCrain.position;
        tCrain.transform.rotation = Quaternion.Euler(dat_tCrain.euler);
        crain.transform.localPosition = dat_Crain.position;
        crain.transform.localRotation = Quaternion.Euler(dat_Crain.euler);
    }

    /// <summary>
    /// Using vert close
    /// </summary>
    [ContextMenu("State0_UseCrainEditor")]
    public void State0_UseCrainEditor()
    {
        if (isDebug) Debug.Log("State0_UseCrainEditor");

        camPivot.Reset0();
        camPivot.SetZ(0);

        posOnCrain.position = crain.crainDir.trTarget.position;
        camPivot.transform.position = posOnCrain.position;
        camPivot.LookRezAndStraightX();
    }

    [ContextMenu("Stepback_UseCrainEditor")]
    public void Stepback_UseCrainEditor()
    {
        if (isDebug) Debug.Log("Stepback_UseCrainEditor");

        posOnCrain.position += -crain.crainDir.GetDir().normalized * stepBack;

        if (posCrainHitBrain.IsHit()) camPivot.transform.position = posOnCrain.position;
        else
        {
            brainCloseToCrain.GetClose();
            Vector3 dir = brainCloseToCrain.transform.position - posOnCrain.position;
            tempPosOnCrain.position = brainCloseToCrain.transform.position + dir.normalized * dOutOfBrain;
            camPivot.transform.position = tempPosOnCrain.position;
        }

        camPivot.LookRezAndStraightX();
    }

    [Header("Use Crain Runtime")]
    public Craniotomy crain = null;
    public VertGroupClosestToTransform brainInside = null;
    public HitByRay_FromTo posCrainHitBrain = null;
    public Transform posOnCrain = null;
    public Transform tempPosOnCrain = null;
    public VertClosestToTransform brainCloseToCrain = null;
    public float dOutOfBrain = 2f;

    [ContextMenu("InitParams_UseCrain")]
    public void InitParams_UseCrain()
    {
        if (isDebug) Debug.Log("InitParams_UseCrain");

        camPivot.Reset0();
        camPivot.SetZ(0);

        brainInside.mf = cerebellum_180416.Instance.mf;
        brainInside.trCloseTo = X.Instance.origin;
        brainInside.GetClose();

        posCrainHitBrain.trFrom = posOnCrain;
        posCrainHitBrain.trTo = brainInside.transform;

        brainCloseToCrain.mf = cerebellum_180416.Instance.mf;
        brainCloseToCrain.trCloseTo = posOnCrain;
    }

    /// <summary>
    /// Using vert close
    /// </summary>
    [ContextMenu("State0_UseCrain")]
    public void State0_UseCrain()
    {
        if (isDebug) Debug.Log("State0_UseCrain");

        posOnCrain.position = crain.crainDir.trTarget.position;
        camPivot.transform.position = posOnCrain.position;
        camPivot.LookRezAndStraightX();        
    }

    [ContextMenu("Stepback_UseCrain")]
    public void Stepback_UseCrain()
    {
        if (isDebug) Debug.Log("Stepback_UseCrain");

        posOnCrain.position += -crain.crainDir.GetDir().normalized * stepBack;

        if (posCrainHitBrain.IsHit()) camPivot.transform.position = posOnCrain.position;
        else
        {
            brainCloseToCrain.GetClose();
            Vector3 dir = brainCloseToCrain.transform.position - posOnCrain.position;
            tempPosOnCrain.position = brainCloseToCrain.transform.position + dir.normalized * dOutOfBrain;
            camPivot.transform.position = tempPosOnCrain.position;
        }

        camPivot.LookRezAndStraightX();
    }
}