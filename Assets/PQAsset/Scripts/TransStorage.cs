﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransStorage : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("note")]
    public string note = string.Empty;

    [Serializable]    
    public enum Space { World, Local }

    [Header("Override")]
    public bool boolPos = true;
    public bool boolRot = true;
    public bool boolSca = true;

    [Serializable]
    public class aTransStorage
    {
        public Transform trs = null;        
        public Space _space = Space.World;
        public Vector3 position = Vector3.zero;
        public Vector3 euler = Vector3.zero;
        public Vector3 scale = Vector3.one;

        public aTransStorage() { this.trs = null; this._space = Space.World; this.position = Vector3.zero; this.euler = Vector3.zero; this.scale = Vector3.one; }
        public aTransStorage(Transform trs, Space space, Vector3 pos, Vector3 euler, Vector3 scale)
        { this.trs = trs;this._space = space; this.position = pos; this.euler = euler; this.scale = scale; }
        public string toJson() { return JsonUtility.ToJson(this); }

        public void SelfCopy(bool boolPos, bool boolRot, bool boolSca)
        {
            switch (_space)
            {
                case Space.World:
                    if (boolPos) position = trs.transform.position;
                    if (boolRot) euler = trs.transform.rotation.eulerAngles;
                    if (boolSca) scale = trs.transform.localScale;
                    break;
                case Space.Local:
                    if (boolPos) position = trs.transform.localPosition;
                    if (boolRot) euler = trs.transform.localRotation.eulerAngles;
                    if (boolSca) scale = trs.transform.localScale;
                    break;
            }
        }
    }

    [Header("Input")]
    public List<aTransStorage> trStorageList = new List<aTransStorage>();
        
    public void SetPosAt(int i, Vector3 pos)
    {
        if (i >= trStorageList.Count)
        {
            if (isDebug) Debug.Log("Invalid : " + i.ToString() + " > " + trStorageList.Count.ToString());
            return;
        }

        trStorageList[i].position = pos;
    }

    public void SetRotAt(int i, Vector3 eul)
    {
        if (i >= trStorageList.Count)
        {
            if (isDebug) Debug.Log("Invalid : " + i.ToString() + " > " + trStorageList.Count.ToString());
            return;
        }

        trStorageList[i].euler = eul;
    }

    public void SetScaAt(int i, Vector3 sca)
    {
        if (i >= trStorageList.Count)
        {
            if (isDebug) Debug.Log("Invalid : " + i.ToString() + " > " + trStorageList.Count.ToString());
            return;
        }

        trStorageList[i].scale = sca;
    }

    public string StoreList_ToJson()
    {
        return JsonUtility.ToJson(this.trStorageList);
    }
    
    public void SetState(int i)
    {
        aTransStorage storage = trStorageList[i];
        switch (storage._space)
        {
            case Space.World:
                if (this.boolPos) storage.trs.transform.position = storage.position;
                if (this.boolRot) storage.trs.transform.rotation = Quaternion.Euler(storage.euler);
                if (this.boolSca) storage.trs.transform.localScale = storage.scale;
                break;
            case Space.Local:
                if (this.boolPos) storage.trs.transform.localPosition = storage.position;
                if (this.boolRot) storage.trs.transform.localRotation = Quaternion.Euler(storage.euler);
                if (this.boolSca) storage.trs.transform.localScale = storage.scale;
                break;
        }
    }

    [ContextMenu("SetAllState")]
    public void SetAllState()
    {
        for (int i = 0; i < trStorageList.Count; i++) SetState(i);
    }

    [ContextMenu("SelfCopyFromTrans")]
    public void SelfCopyFromTrans()
    {
        for (int i = 0; i < trStorageList.Count; i++)
            trStorageList[i].SelfCopy(this.boolPos, this.boolRot, this.boolSca);
    }    

    public void SyncFrom(TransStorage trsStore)
    {
        trStorageList = trsStore.trStorageList;
    }
}
