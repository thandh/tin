﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using System.Text.RegularExpressions;
using UnityEngine.UI;
using System.Text;
using System.Windows.Forms;

public class DemoCreate : Singleton<DemoCreate>
{
    protected FileInfo theSourceFile = null;
    protected StreamReader reader = null;
    protected string text = " ";
    // assigned to allow first line to be read below
    protected String[] oringinData = new String[1000];
    protected char[] delimiterChars = { ',' };
    string[] queries = new string[100];

    public GameObject cam;
    public static float[][] campos = new float[1000][];
    public static float[][] camrot = new float[1000][];
    public static float[] campar = new float[1000];
    public static float[] camjump = new float[1000];
    public static float[] camspeedC = new float[1000];
    public GameObject nowcamT, camTimex, moveG, jumpG, demoG;
    float camtime;
    int camnumber = 0;
    float camspeed = 1;
    bool demo;
    bool posend;
    bool jumpcam;
    bool loaded;
    float demotime = 0;
    float Cangetime = 0;
    int maxcam = 0;
    int demonumber = 0;
    int ivorinumber = 0;
    Vector3 firstcam = Vector3.zero;
    Quaternion firstcamr = Quaternion.identity;

    void Start()
    {
        firstcam = cam.transform.position;
        firstcamr = cam.transform.rotation;
        ivorinumber = 0;
        jumpcam = false;
        loaded = false;
        demo = false;
        demonumber = 0;
        camtime = 0;
    }

    void Update()
    {
        if (demo == true && campar[demonumber] >= 0)
        {
            if (posend == true)
            {
                demotime += Time.deltaTime;
            }

            if (demonumber > 0)
            {
                if (demotime > campar[demonumber])
                {
                    posend = false;

                    if (camrot[demonumber - 1][1] > camrot[demonumber][1] && camrot[demonumber - 1][1] - camrot[demonumber][1] > 180)
                        camrot[demonumber - 1][1] = camrot[demonumber - 1][1] - 360;

                    if (camrot[demonumber - 1][1] < camrot[demonumber][1] && camrot[demonumber][1] - camrot[demonumber - 1][1] > 180)
                        camrot[demonumber][1] = camrot[demonumber][1] - 360;

                    camposC(campos[demonumber - 1][0], campos[demonumber - 1][1], campos[demonumber - 1][2], campos[demonumber][0], campos[demonumber][1], campos[demonumber][2], camrot[demonumber - 1][0], camrot[demonumber - 1][1], camrot[demonumber - 1][2], camrot[demonumber][0], camrot[demonumber][1], camrot[demonumber][2], camjump[demonumber], camspeedC[demonumber]);
                }
            }
        }

        if (demonumber > maxcam && ivorinumber == 0) demo = false;
    }

    void camposC(float x, float y, float z, float x1, float y1, float z1, float rx, float ry, float rz, float rx1, float ry1, float rz1, float par, float speed)
    {
        float dis;
        dis = (float)System.Math.Sqrt((x - x1) * (x - x1) + (y - y1) * (y - y1) + (z - z1) * (z - z1));
        Cangetime += Time.deltaTime * camspeed * speed;

        if (Cangetime <= dis && par != 1)
        {
            cam.transform.position = new Vector3(x + (x1 - x) * Cangetime / dis, y + (y1 - y) * Cangetime / dis, z + (z1 - z) * Cangetime / dis);
            cam.transform.rotation = Quaternion.Euler(rx + (rx1 - rx) * Cangetime / dis, ry + (ry1 - ry) * Cangetime / dis, rz + (rz1 - rz) * Cangetime / dis);
            if (Cangetime < dis / 5)
                camspeed = (4 * Cangetime + 1f / 5 * dis) / dis;
            else if (Cangetime > dis * 4f / 5f)
                camspeed = (-4 * Cangetime + 21f / 5 * dis) / dis;
            else
                camspeed = 1;
        }
        else
        {
            cam.transform.position = new Vector3(x1, y1, z1);
            cam.transform.rotation = Quaternion.Euler(rx1, ry1, rz1);
            demotime = 0;
            Cangetime = 0;
            demonumber++;
            posend = true;

            if (ivorinumber > 0)
            {
                demo = false;
                ManngerSystem.Instance.start_number++;
            }
        }
    }

    public void inputCameratime()
    {
        camtime = float.Parse(camTimex.GetComponent<InputField>().text);
    }

    public void savecam()
    {
        if (demo == false)
        {
            campos[camnumber] = new float[3];
            camrot[camnumber] = new float[3];
            campos[camnumber][0] = cam.transform.position.x;
            campos[camnumber][1] = cam.transform.position.y;
            campos[camnumber][2] = cam.transform.position.z;
            camrot[camnumber][0] = cam.transform.eulerAngles.x;
            camrot[camnumber][1] = cam.transform.eulerAngles.y;
            camrot[camnumber][2] = cam.transform.eulerAngles.z;
            campar[camnumber] = camtime;

            if (jumpcam == false)
                camjump[camnumber] = 0;
            else
                camjump[camnumber] = 1;

            maxcam = camnumber;
            camnumber++;
        }
    }

    public void savefile()
    {
        demostop();
        SaveFileDialog saveLog = new SaveFileDialog();
        saveLog.InitialDirectory = UnityEngine.Application.dataPath + "/Resources";
        saveLog.Filter = "Text Files(*.txt)|*.txt|All files (*.*)|*.*";
        saveLog.Title = "iVoRi Demo Save As";
        DialogResult result = saveLog.ShowDialog();
        if (result == DialogResult.OK)
        {
            string path = saveLog.FileName;            
            FileStream fs = new FileStream(path, FileMode.Create);
            StreamWriter sw = new StreamWriter(fs);

            for (int i = 0; i < camnumber; i++)            
                sw.WriteLine("Demo," + campos[i][0] + "," + campos[i][1] + "," + campos[i][2] + "," + camrot[i][0] + "," + camrot[i][1] + "," + camrot[i][2] + "," + campar[i] + "," + camjump[i] + "," + camspeedC[i]);            

            sw.Flush();
            sw.Close();
            fs.Close();
        }
    }

    public void loadfile()
    {
        demostop();
        OpenFileDialog openLog = new OpenFileDialog();
        openLog.InitialDirectory = UnityEngine.Application.dataPath + "/Resources";
        openLog.Filter = "Text Files(*.txt)|*.txt|Csv Files(*.csv)|*.csv|All files (*.*)|*.*";
        openLog.Title = "iVoRi Demo Load";
        DialogResult result = openLog.ShowDialog();

        if (result == DialogResult.OK)
        {
            string path = openLog.FileName;            
            theSourceFile = new FileInfo(path);
            reader = theSourceFile.OpenText();
            int i = 0;
            text = reader.ReadLine();

            while (text != null)
            {                
                oringinData[i] = text;
                queries = text.Split(',');                
                campos[i] = new float[3];
                camrot[i] = new float[3];

                if (queries[0] == "Demo")
                {
                    campos[i][0] = Convert.ToSingle(queries[1]);
                    campos[i][1] = Convert.ToSingle(queries[2]);
                    campos[i][2] = Convert.ToSingle(queries[3]);
                    camrot[i][0] = Convert.ToSingle(queries[4]);
                    camrot[i][1] = Convert.ToSingle(queries[5]);
                    camrot[i][2] = Convert.ToSingle(queries[6]);
                    campar[i] = Convert.ToSingle(queries[7]);
                    camjump[i] = Convert.ToSingle(queries[8]);
                    camspeedC[i] = Convert.ToSingle(queries[9]);
                }

                maxcam = i;
                i++;
                text = reader.ReadLine();
            }

            reader.Close();
            loaded = true;
        }
    }

    public void demomodeivori(float x, float y, float z, float rx, float ry, float rz, float t, float j, float s)
    {
        if (demonumber == 0 && ivorinumber == 0)
        {
            campos[demonumber] = new float[3];
            camrot[demonumber] = new float[3];
            demotime = 0;
            demonumber = 0;
            Cangetime = 0;
            campos[demonumber][0] = x;
            campos[demonumber][1] = y;
            campos[demonumber][2] = z;
            camrot[demonumber][0] = rx;
            camrot[demonumber][1] = ry;
            camrot[demonumber][2] = rz;
            campar[demonumber] = t;
            camjump[demonumber] = j;
            camspeedC[demonumber] = s;
            cam.transform.position = new Vector3(campos[demonumber][0], campos[demonumber][1], campos[demonumber][2]);
            cam.transform.rotation = Quaternion.Euler(camrot[demonumber][0], camrot[demonumber][1], camrot[demonumber][2]);
            ivorinumber++;
            posend = true;
        }
        else if (demo == false)
        {
            demonumber++;
            campos[demonumber] = new float[3];
            camrot[demonumber] = new float[3];
            campos[demonumber - 1] = new float[3];
            camrot[demonumber - 1] = new float[3];
            campos[demonumber][0] = x;
            campos[demonumber][1] = y;
            campos[demonumber][2] = z;
            camrot[demonumber][0] = rx;
            camrot[demonumber][1] = ry;
            camrot[demonumber][2] = rz;
            campos[demonumber - 1][0] = cam.transform.position.x;
            campos[demonumber - 1][1] = cam.transform.position.y;
            campos[demonumber - 1][2] = cam.transform.position.z;
            camrot[demonumber - 1][0] = cam.transform.eulerAngles.x;
            camrot[demonumber - 1][1] = cam.transform.eulerAngles.y;
            camrot[demonumber - 1][2] = cam.transform.eulerAngles.z;
            campar[demonumber] = t;
            camjump[demonumber] = j;
            camspeedC[demonumber] = s;
            demotime = 0;
            Cangetime = 0;
            demo = true;
            posend = true;
        }
    }

    public void demomode()
    {
        if (demo == true)
        {
            demostop();
        }
        else
        {
            if (camnumber <= 1 && loaded == false)
            {
                loadfile();
                demomode();
            }
            else
            {
                demotime = 0;
                demonumber = 0;
                Cangetime = 0;
                cam.transform.position = new Vector3(campos[demonumber][0], campos[demonumber][1], campos[demonumber][2]);
                cam.transform.rotation = Quaternion.Euler(camrot[demonumber][0], camrot[demonumber][1], camrot[demonumber][2]);
                demonumber++;
                demo = true;
                posend = true;
            }
        }
    }

    public void resteOrigin()
    {
        cam.transform.position = firstcam;
        cam.transform.rotation = firstcamr;

        cam.GetComponent<Camera>().fieldOfView = 60.0f;
        //cam.transform.parent.localScale = new Vector3 (cam.transform.parent.localScale.x,cam.transform.parent.localScale.y,300.0f);

    }

    [UnityEngine.ContextMenu("cammoveA")]
    public void cammoveA()
    {
        #region  original

        // cam.transform.position = new Vector3(-5.85f, -53.55f, -29.55f);
        //  cam.transform.rotation = Quaternion.Euler(0.103f, 17.984f, 142.166f);

        //dcam.transform.rotation = Quaternion.LookRotation(cerebellumControl.Instance.rezTarget.position - cam.transform.position);
        cam.transform.position = new Vector3(-14.4f, -58.3f, -37f);// new Vector3(-14.77652f, -58.91753f, -47.62952f);
        cam.transform.rotation = Quaternion.Euler(-14.9f, 48.988f, 507.082f);// Quaternion.Euler(-14.763f, 31.988f, 496.082f);
                                                                             //cam.transform.parent.localScale = new Vector3 (cam.transform.parent.localScale.x,cam.transform.parent.localScale.y,164);
        cam.GetComponent<Camera>().fieldOfView = 60.0f;
        // cam.transform.position = new Vector3(-28.78187f, -30.00707f, -42.56363f);
        // cam.transform.rotation = Quaternion.Euler(23.41f, 48.751f, 166.13f);

        #endregion
        //		cam.transform.position = cerebellumControl.Instance.targetcy.transform.position - cerebellumControl.Instance.targetcy.transform.up * 10;// new Vector3(-14.77652f, -58.91753f, -47.62952f);
        //		cam.transform.rotation = cerebellumControl.Instance.targetcy.transform.rotation * Quaternion.Euler (-90, 180, 0);// Quaternion.Euler(-14.763f, 31.988f, 496.082f);
        //		//cam.transform.parent.localScale = new Vector3 (cam.transform.parent.localScale.x,cam.transform.parent.localScale.y,164);
        //		cam.GetComponent<Camera> ().fieldOfView = 60.0f;

    }

    /// <summary>
    /// 1003のデータのカメラの移動
    /// </summary>
    public void cammoveB()
    {
        //1003データ
        cam.transform.position = new Vector3(4.447491f, -52.2f, -4.508782f);
        cam.transform.rotation = Quaternion.Euler(11.836f, 35.019f, 152.283f);

        cam.transform.parent.localScale = new Vector3(cam.transform.parent.localScale.x, cam.transform.parent.localScale.y, 19.4f);

        cam.GetComponent<Camera>().fieldOfView = 47.0f;

    }

    public void jumpicon()
    {
        if (jumpcam == true)
            jumpcam = false;
        else
            jumpcam = true;
    }

    public void reset()
    {
        jumpcam = false;
        loaded = false;
        demo = false;
        demonumber = 0;
        camtime = 0;
        demotime = 0;
        Cangetime = 0;
        camnumber = 0;
        camTimex.GetComponent<InputField>().text = "0";
        campos = new float[1000][];
        camrot = new float[1000][];
        campar = new float[1000];
        camjump = new float[1000];
    }


    public void demostop()
    {
        demo = false;
        demotime = 0;
        demonumber = 0;
        Cangetime = 0;
    }

}

