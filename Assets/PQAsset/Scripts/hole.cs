﻿using UnityEngine;
using System.Collections;

public class hole : Singleton<hole>
{
   public GameObject target,targetRev,targetRevWorld;
	public Transform rezTartget;
    float dis = 0;
    float rot = 0;
    float startz;
    bool isEnd = false;
    public bool isHole=false;
    Vector3[] lineV=new Vector3[100];
    float[] lineDis = new float[100];
    int nowLine=0;
    int a = 0;

    // Use this for initialization
    void Start () {
		//target.transform.position = cerebellumControl.Instance.linelist[0];
	}
	
	// Update is called once per frame
	void Update () {
        dis += Time.deltaTime;
     if (isHole)
        {
            if(a==0)
            {
                dis = 0;
                target.transform.position = cerebellumControl.Instance.linelist[0];

                for (int i = 0; i < cerebellumControl.Instance.line_index - 1; i++)
                {
                    lineV[i] = cerebellumControl.Instance.linelist[i + 1] - cerebellumControl.Instance.linelist[i];
                    lineDis[i] = Vector3.Distance(cerebellumControl.Instance.linelist[i + 1], cerebellumControl.Instance.linelist[i]);
					//target.transform.rotation = Quaternion.LookRotation(rezTartget.position - target.transform.position);
                  //  print(lineDis[i]);
                }
                a++;
            }
        }
     }
    void OnTriggerEnter(Collider aaa) //aaa為自定義碰撞事件
    {
        if (aaa.gameObject.tag == "sinus"&& isHole&&a==1) //如果aaa碰撞事件的物件名稱是CubeA
        {
            startz = target.transform.localPosition.z;
            //print("OK"); //在除錯視窗中顯示OK
        }
    }
    void OnTriggerStay(Collider aaa) //aaa為自定義碰撞事件
    {
        if (aaa.gameObject.tag == "sinus" && isHole && a == 1) //如果aaa碰撞事件的物件名稱是CubeA
        {
            move(target);
        }
    }
    void OnTriggerExit(Collider aaa) //aaa為自定義碰撞事件
    {
        if (aaa.gameObject.tag == "sinus"&& isHole && a == 1) //如果aaa碰撞事件的物件名稱是CubeA
        {
            isHole = false;
            //  print("DONE"); //在除錯視窗中顯示OK
            // print(startz);
            //  print(dis);
            if(dis>0.8)
            target.transform.position = cerebellumControl.Instance.linelist[nowLine] + (lineV[nowLine] / lineDis[nowLine]) * (dis-0.8f);
            else
            {
                if (nowLine >= 1)
                    target.transform.position = cerebellumControl.Instance.linelist[nowLine] - (lineV[nowLine - 1] / lineDis[nowLine - 1]) * (0.8f - dis);
                
            }
            targetRevWorld.transform.position = targetRev.transform.position;
            targetRevWorld.transform.rotation = targetRev.transform.rotation;
        }
    }

    void move(GameObject target)
    {
		
        if(nowLine< cerebellumControl.Instance.line_index-1)
        {
            if (dis < lineDis[nowLine])
            {
                target.transform.position = cerebellumControl.Instance.linelist[nowLine] + (lineV[nowLine] / lineDis[nowLine]) * dis;

             }
            else
            {
                target.transform.position = cerebellumControl.Instance.linelist[nowLine + 1];
                dis = 0;
                nowLine++;
            }

        }
      //  print("XXX_"+dis);
    }
    public void holeset()
    {
        if(!cerebellumControl.Instance.isdrawing)
        {
            a = 0;
            nowLine = 0;
            isHole = true;
        }
        else
        {
            cerebellumControl.Instance.drawend();
                a = 0;
            nowLine = 0;
            isHole = true;
        }
       
    }
}
