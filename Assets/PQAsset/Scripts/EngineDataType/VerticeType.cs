﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class VerticeType
{
    public int IDInMesh = -1;
    public Vector3 posInMesh = -Vector3.one;
    public VerticeType() { }
    public VerticeType(int IDInMesh, Vector3 posInMesh) { this.IDInMesh = IDInMesh; this.posInMesh = posInMesh; }
    public float DistanceTo(Transform coreTr, Vector3 pos) { return (coreTr.TransformPoint(this.posInMesh) - pos).magnitude; }
}
