﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum eDirType
{
    right,
    nright,
    up,
    nup,
    forward,
    nforward
}

[Serializable]
public class DirectionType
{
    public eDirType type = eDirType.forward;
    public Transform trans = null;
    public DirectionType() { }
    public DirectionType(eDirType type, Transform trans = null) { this.type = type; this.trans = trans; }

    public Vector3 GetDir()
    {
        Vector3 dir = Vector3.zero;
        if (trans != null)
        {
            switch (this.type)
            {
                case eDirType.right: dir = trans.right; break;
                case eDirType.nright: dir = -trans.right; break;
                case eDirType.up: dir = trans.up; break;
                case eDirType.nup: dir = -trans.up; break;
                case eDirType.forward: dir = trans.forward; break;
                case eDirType.nforward: dir = -trans.forward; break;
            }
        }
        return dir;
    }

    public void SetDir(Vector3 dir)
    {
        if (trans != null)
        {
            switch (this.type)
            {
                case eDirType.right: trans.right = dir; break;
                case eDirType.nright: trans.right = -dir; break;
                case eDirType.up: trans.up = dir; break;
                case eDirType.nup: trans.up = -dir; break;
                case eDirType.forward: trans.forward = dir; break;
                case eDirType.nforward: trans.forward = -dir; break;
            }
        }
    }
}
