﻿using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class Ivori_ExportCylinder : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Inputs")]
    public ManngerSystem ivori = null;
    public tCraniotomy tCrain = null;
    public Craniotomy crain = null;

    [Header("Get Cylinder")]
    public bool allowOutFile = false;
    public TINFileOutput tCrainFileOut = null;
    public TINFileOutput crainFileOut = null;

    [ContextMenu("InitParams")]
    public void InitParams()
    {
        ivori = FindObjectOfType<ManngerSystem>();        
        tCrain = FindObjectOfType<tCraniotomy>();
        crain = tCrain.cylinder.GetComponent<Craniotomy>();

        Transform camPivot = FindObjectOfType<CamPivot>().transform;
        keyview = camPivot.GetComponent<CamPivot_Keyview>();
        keyviewMethod = camPivot.GetComponent<Keyview_Method>();
        keyviewAutoStepback = camPivot.GetComponent<Keyview_AutoStepback>();
        possible = camPivot.GetComponent<Keyview_Possible>();
    }

    [ContextMenu("Auto_GetCylinder")]
    public void Auto_GetCylinder()
    {
        StartCoroutine(C_AutoGetCylinder());
    }

    IEnumerator C_AutoGetCylinder()
    {
        CheckImport();
        yield return new WaitUntil(() => doneCheckImport == true);

        if (hasOutput)
        {
            KeyviewFromIvori();
            yield break;
        }

#if UNITY_EDITOR
        if (!Application.isPlaying) EditorApplication.ExecuteMenuItem("Edit/Play");
#endif

        yield return new WaitUntil(() => Application.isPlaying == true);
        if (isDebug) Debug.Log("App running!!");

        ivori.SwitchOnOffIvori();

        yield break;
    }

    [ContextMenu("GetCylinder")]
    public void GetCylinder()
    {
        if (!Application.isPlaying)
        {
            Debug.Log("Must hit play - Runtime mode only!!");
            return;
        }

        allowOutFile = true;
        ivori.SwitchOnOffIvori();
    }

    [ContextMenu("KeyviewFromIvori")]
    public void KeyviewFromIvori()
    {
        if (hasOutput)
        {
            if (isDebug) Debug.Log("Has Output");
            keyviewMethod.typeKeyview = Keyview_Method.TypeKeyview.CrainEditor;
            keyviewMethod.CurrentSet();
            keyviewMethod.Init();
            keyviewMethod.State0();
            keyviewAutoStepback.AutoStepback_FixedTurn();
            keyview.SaveKeyView();
            possible.Output(1);            
        } else
        {
            Debug.Log("No data");
        }
    }

    [Header("Check Import")]
    bool doneCheckImport = true;
    public bool hasOutput = false;

    [Header("Apply tCrain")]
    public bool applytCrain = true;

    [ContextMenu("CheckImport")]
    public void CheckImport()
    {
        StartCoroutine(C_CheckImport());
    }

    IEnumerator C_CheckImport()
    {
        if (isDebug) Debug.Log("Start C_CheckImport");

        allowOutFile = true; hasOutput = false; doneCheckImport = false; 
        if (!crainFileOut.Existed())
        {
            allowOutFile = true; hasOutput = false; doneCheckImport = true;
            crainFileOut.ResetParams();
            if (isDebug) Debug.Log("Done C_CheckImport : false"); yield break;
        }

        crainFileOut.Import();
        if (string.IsNullOrEmpty(crainFileOut.data)) { allowOutFile = true; hasOutput = false; doneCheckImport = true; if (isDebug) Debug.Log("Done C_CheckImport : false"); yield break; }

        try
        {
            TransStorage.aTransStorage atranstCrain = JsonUtility.FromJson<TransStorage.aTransStorage>(tCrainFileOut.data);
            TransStorage.aTransStorage atransCrain = JsonUtility.FromJson<TransStorage.aTransStorage>(crainFileOut.data);

            if (applytCrain)
            {
                tCrain.transform.position = atranstCrain.position;
                tCrain.transform.rotation = Quaternion.Euler(atranstCrain.euler);

                crain.transform.position = atransCrain.position;
                crain.transform.rotation = Quaternion.Euler(atransCrain.euler);
            }
        } catch
        {
            allowOutFile = true; hasOutput = false; doneCheckImport = true; if (isDebug) Debug.Log("Done C_CheckImport : false"); yield break;
        }

        allowOutFile = false; hasOutput = true; doneCheckImport = true;
        if (isDebug) Debug.Log("Done C_CheckImport : true");
        yield break;
    }    

    [ContextMenu("CylinderOut")]
    public void CylinderOut()
    {
        StartCoroutine(C_CylinderOut());
    }

    IEnumerator C_CylinderOut()
    {
        if (isDebug) Debug.Log("Start C_CylinderOut");        

        if (allowOutFile)
        {
            TransStorage.aTransStorage dattCrain = new TransStorage.aTransStorage(tCrain.transform, TransStorage.Space.World, tCrain.transform.position, tCrain.transform.rotation.eulerAngles, tCrain.transform.lossyScale);
            tCrainFileOut.outFile(dattCrain.toJson());

            TransStorage.aTransStorage datCrain = new TransStorage.aTransStorage(crain.transform, TransStorage.Space.World, crain.transform.position, crain.transform.rotation.eulerAngles, crain.transform.lossyScale);
            crainFileOut.outFile(datCrain.toJson());            

            if (isDebug) Debug.Log("start yield doneOutFile == true");
            yield return new WaitUntil(() => crainFileOut.doneOutFile == true && tCrainFileOut.doneOutFile == true);
            if (isDebug) Debug.Log("done yield doneOutFile == true");

            if (isDebug) Debug.Log("Done C_CylinderOut");
#if UNITY_EDITOR
            if (Application.isPlaying) EditorApplication.ExecuteMenuItem("Edit/Play");
#endif
        }
                
        yield break;
    }

    [Header("Next Possible Step")]
    public CamPivot_Keyview keyview = null;
    public Keyview_Method keyviewMethod = null;
    public Keyview_AutoStepback keyviewAutoStepback = null;
    public Keyview_Possible possible = null;

    private void OnEnable()
    {
        crain.doneCylinder += CylinderOut;
    }

    private void OnDisable()
    {
        if (IsQuiting) return;
    }

    [SerializeField]
    bool IsQuiting = false;

    private void OnApplicationQuit()
    {
        IsQuiting = true;
    }
}