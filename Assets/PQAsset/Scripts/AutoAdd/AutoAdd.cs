﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ist;
public class AutoAdd : MonoBehaviour {
	public GameObject m_ObjModel;

	public string TagSinus;
	public string TagPICA;
	public  Material m_MaterialCerebe1;
	public  Material m_MaterialSkull,m_MaterialDepth;

	public ObjMaterial[] list;
	// Use this for initializatio
	void Start () {
//		startAdd ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void startAdd(){
		if (m_ObjModel==null)
			return;
		AddTag ();
		AddRender ();
		AddMeshCerebe ();
		AddMeshSkull ();
	}

	public void AddTag(){
		Transform objPica=this.m_ObjModel.transform.Find("PICA/default");
		Transform objSinus = this.m_ObjModel.transform.Find ("sinus/default");
		objPica.gameObject.tag = TagPICA;
		objSinus.gameObject.tag = TagSinus;
	}

	public void AddMeshCerebe(){
		Transform obj=m_ObjModel.transform.Find("cerebellum/default");
		addMeshBase (obj.GetChild (0), m_MaterialCerebe1);
		addMeshBase (obj.GetChild (1), m_MaterialCerebe1);

	}

	public void AddMeshSkull(){
		Transform obj=this.m_ObjModel.transform.Find("skull/default");
		for(int i=0;i<obj.childCount;i++){
			addMeshBase (obj.GetChild (i), m_MaterialSkull);
			MeshCollider m = obj.GetChild(i).gameObject.AddComponent (typeof(MeshCollider)) as MeshCollider;

			SubReceiver sr = (obj.GetChild (i).gameObject).AddComponent <SubReceiver> () as SubReceiver;
			Material[] m1 =new Material[1];
			m1[0] = m_MaterialDepth;
			sr.m_depth_materials = m1;
		}
	}

	public void AddRender(){
//		Transform obj = transform.GetChild (0).GetChild(0);
//		MeshRenderer m = obj.gameObject.AddComponent (typeof(MeshRenderer)) as MeshRenderer;
//		m.material = m_Material;

		for(int i=0;i<m_ObjModel.transform.childCount;i++){
			Transform o=m_ObjModel.transform.GetChild(i);
			setMeshRenderObj (o);
		}
	}

	public void setMeshRenderObj (Transform obj)
	{
		if (obj.childCount > 0) {
			if ((!(obj.gameObject.name == "cerebellum")) || (!(obj.gameObject.name == "skull"))) {
				Transform o = obj.GetChild (0);
				MeshRenderer m = o.gameObject.AddComponent (typeof(MeshRenderer)) as MeshRenderer;
				ObjMaterial om = getListMaterialByName (obj.gameObject.name);
				if (om!=null)
					m.material = om.m_Material;
			}
			if(obj.gameObject.name=="sinus"){
				MeshCollider m = obj.GetChild(0).gameObject.AddComponent (typeof(MeshCollider)) as MeshCollider;
			}
		}
	}

	public ObjMaterial getListMaterialByName(string name){
		foreach(ObjMaterial i in list){
			if (i.nameObj == name)
				return i;
		}
		return null;
	}

	public void addMeshBase(Transform o,Material ma){
		MeshRenderer m = o.gameObject.AddComponent (typeof(MeshRenderer)) as MeshRenderer;
//		ObjMaterial om = ma;
		m.material = ma;
	}

	public void AddMeshCollider(GameObject obj){
		MeshCollider m = obj.AddComponent (typeof(MeshCollider)) as MeshCollider;

	}
}

