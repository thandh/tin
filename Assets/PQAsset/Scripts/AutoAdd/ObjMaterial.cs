﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class ObjMaterial
{
	public	string nameObj;
	public Material m_Material;

	public ObjMaterial (string n, Material m)
	{
		this.nameObj = n;
		this.m_Material = m;
	}


}
