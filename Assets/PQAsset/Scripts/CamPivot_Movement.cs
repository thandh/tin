﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class CamPivot_Movement : MonoBehaviour
{
    public bool isDebug = false;

    [Header("Control")]
    public bool IsOn = true;

    [Header("Process")]
    public bool isMouse = false;

    void Start()
    {
        
    }

    void Update()
    {
        if (!IsOn) return;
        
        if (Input.GetMouseButtonDown(1)) isMouse = true;
        if (Input.GetMouseButtonUp(1)) isMouse = false;

        keyboardController();
    }

    [Header("Keyboard Control")]
    [Header("Rotate")]
    public bool letRotate = true;
    public float stepRotate = 10f;

    [Header("Rotate")]
    public bool letMove = true;
    public float stepMove = 5f;

    [Header("Zoom")]
    public bool letZoom = true;
    public float stepZoom = 10f;
    public float minZoom = .1f;
    public float maxZoom = 10f;

    [Header("Mouse Control")]
    [Header("Rotate")]    
    public float stepMRotate = 10f;

    [Header("Rotate")]    
    public float stepMMove = 5f;

    [Header("Zoom")]    
    public float stepMZoom = 10f;    

    public Transform camTrans = null;
    public OriginOfMeshBound center = null;    

    void keyboardController()
    {
        DebugKeyPressed();

        ///rotate X
        //if (Input.GetKey(KeyCode.X))
        //{
        //if (Input.GetKey(KeyCode.RightArrow)) transform.Rotate(transform.forward * stepRotate);
        //if (Input.GetKey(KeyCode.LeftArrow)) transform.Rotate(-transform.forward * stepRotate);
        //if (Input.GetKey(KeyCode.UpArrow)) if (Mathf.Abs(camTrans.localPosition.z) > minZoom) camTrans.Translate(transform.forward * stepZoom, Space.World);
        //if (Input.GetKey(KeyCode.DownArrow)) if (Mathf.Abs(camTrans.localPosition.z) < maxZoom) camTrans.Translate(-transform.forward * stepZoom, Space.World);
        //}

        ///z-axis
        if (currentKeyCode.Count == 2 && (currentKeyCode.Contains(KeyCode.LeftShift) || currentKeyCode.Contains(KeyCode.RightShift)))
        {
            if (currentKeyCode.Contains(KeyCode.RightArrow)) transform.RotateAround(camTrans.up, stepRotate);
            if (currentKeyCode.Contains(KeyCode.LeftArrow)) transform.RotateAround(-camTrans.up, stepRotate);
            if (currentKeyCode.Contains(KeyCode.UpArrow)) transform.RotateAround(-camTrans.right, stepRotate);
            if (currentKeyCode.Contains(KeyCode.DownArrow)) transform.RotateAround(camTrans.right, stepRotate);
        }

        if (currentKeyCode.Count == 3 && (currentKeyCode.Contains(KeyCode.LeftShift) || currentKeyCode.Contains(KeyCode.RightShift)))
        {
            if (currentKeyCode.Contains(KeyCode.RightArrow) && currentKeyCode.Contains(KeyCode.UpArrow))
            {
                transform.RotateAround(camTrans.up, stepRotate);
                transform.RotateAround(-camTrans.right, stepRotate);
            }

            if (currentKeyCode.Contains(KeyCode.UpArrow) && currentKeyCode.Contains(KeyCode.LeftArrow))
            {
                transform.RotateAround(-camTrans.right, stepRotate);
                transform.RotateAround(-camTrans.up, stepRotate);
            }

            if (currentKeyCode.Contains(KeyCode.LeftArrow) && currentKeyCode.Contains(KeyCode.DownArrow))
            {
                transform.RotateAround(-camTrans.up, stepRotate);
                transform.RotateAround(camTrans.right, stepRotate);
            }

            if (currentKeyCode.Contains(KeyCode.DownArrow) && currentKeyCode.Contains(KeyCode.RightArrow))
            {
                transform.RotateAround(camTrans.right, stepRotate);
                transform.RotateAround(camTrans.up, stepRotate);
            }
        }

        if (currentKeyCode.Count == 3 
            && (currentKeyCode.Contains(KeyCode.LeftShift) || currentKeyCode.Contains(KeyCode.RightShift))
            && (currentKeyCode.Contains(KeyCode.LeftAlt) || currentKeyCode.Contains(KeyCode.RightAlt)))

        {
            if (currentKeyCode.Contains(KeyCode.LeftArrow))
            {
                transform.RotateAround(-camTrans.forward, stepRotate);
            }

            if (currentKeyCode.Contains(KeyCode.RightArrow))
            {
                transform.RotateAround(camTrans.forward, stepRotate);
            }
        }

        if (currentKeyCode.Count >= 4
            && ((currentKeyCode.Contains(KeyCode.LeftControl) || currentKeyCode.Contains(KeyCode.RightControl)) || (currentKeyCode.Contains(KeyCode.LeftCommand) || currentKeyCode.Contains(KeyCode.RightCommand)))
            && (currentKeyCode.Contains(KeyCode.LeftShift) || currentKeyCode.Contains(KeyCode.RightShift))
            && (currentKeyCode.Contains(KeyCode.LeftAlt) || currentKeyCode.Contains(KeyCode.RightAlt)))

        {
            if (currentKeyCode.Contains(KeyCode.UpArrow))
            {
                camTrans.localPosition = new Vector3(0, 0, Mathf.Clamp(camTrans.localPosition.z + stepZoom, minZoom, maxZoom));
            }

            if (currentKeyCode.Contains(KeyCode.DownArrow))
            {
                camTrans.localPosition = new Vector3(0, 0, Mathf.Clamp(camTrans.localPosition.z - stepZoom, minZoom, maxZoom));
            }
        }

        ///pan
        //if (Input.GetKey(KeyCode.M))
        if (currentKeyCode.Count == 1)
        {
            if (Input.GetKey(KeyCode.RightArrow)) transform.Translate(-camTrans.right * stepMove, Space.World);
            if (Input.GetKey(KeyCode.LeftArrow)) transform.Translate(camTrans.right * stepMove, Space.World);
            if (Input.GetKey(KeyCode.UpArrow)) transform.Translate(-camTrans.up * stepMove, Space.World);
            if (Input.GetKey(KeyCode.DownArrow)) transform.Translate(camTrans.up * stepMove, Space.World);
        }

        ///snap center
        if (Input.GetKey(KeyCode.Space))
        {
            SnapToCenter();
        }

        ///mouse
        if (isMouse)
        {
            if (currentKeyCode.Count == 1 && currentKeyCode.Contains(KeyCode.Mouse1))
            {
                transform.Rotate(Vector3.up * Input.GetAxis("Mouse X") * stepMRotate);
                transform.Rotate(-Vector3.right * Input.GetAxis("Mouse Y") * stepMRotate);
            }
            else if (currentKeyCode.Count == 2 
                && currentKeyCode.Contains(KeyCode.Mouse1)
                && (currentKeyCode.Contains(KeyCode.LeftShift) 
                || currentKeyCode.Contains(KeyCode.RightShift)))
            {
                transform.Translate(-transform.right * Input.GetAxis("Mouse X") * stepMMove + -transform.up * Input.GetAxis("Mouse Y") * stepMMove, Space.World);
            }
        }

        if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            camTrans.localPosition = new Vector3(0, 0, Mathf.Clamp(camTrans.localPosition.z + Input.GetAxis("Mouse ScrollWheel") * stepMZoom, minZoom, maxZoom));
        }
    }

    [ContextMenu("SnapToCenter")]
    public void SnapToCenter()
    {
        camTrans.SetParent(null);
        center.Center();
        camTrans.LookAt(transform);
        transform.rotation = camTrans.rotation;
        camTrans.SetParent(transform);
    }

    [Header("Keycodes")]
    Array _keyCodes = null;
    Array keyCodes { get { if (_keyCodes == null || _keyCodes.Length == 0) _keyCodes = Enum.GetValues(typeof(KeyCode)); return _keyCodes; } }
    public List<KeyCode> currentKeyCode = new List<KeyCode>();

    void DebugKeyPressed()
    {
        string strKeyPressed = string.Empty;
        bool hasKeyPressed = false;

        currentKeyCode = new List<KeyCode>();

        foreach (KeyCode kcode in keyCodes)
        {
            if (Input.GetKey(kcode))
            {
                if (!hasKeyPressed) hasKeyPressed = true;
                strKeyPressed += kcode + " ";
                currentKeyCode.Add(kcode);
            }
        }

        if (isDebug && hasKeyPressed) Debug.Log("KeyCode down: " + strKeyPressed);
    }

    public static float ClampAngle(float angle, float min, float max)
    {
        angle = angle % 360;
        if ((angle >= -360F) && (angle <= 360F))
        {
            if (angle < -360F)
            {
                angle += 360F;
            }
            if (angle > 360F)
            {
                angle -= 360F;
            }
        }
        return Mathf.Clamp(angle, min, max);
    }

    [ContextMenu("DebugVectorUnit")]
    public void DebugVectorUnit()
    {
        Vector3 a = new Vector3(10, 15, 20);
        if (isDebug) Debug.Log(a.normalized);
    }

}