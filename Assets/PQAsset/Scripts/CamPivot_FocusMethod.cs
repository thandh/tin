﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamPivot_FocusMethod : MonoBehaviour
{
    [Header("Focus")]
    public Transform focusTr = null;
    public float focusDistance = -50f;

    [ContextMenu("FocusCurrentTransform")]
    public void FocusCurrentTransform()
    {
        CamPivot camPivot = FindObjectOfType<CamPivot>();

        camPivot.mainCam.parent = null;
        transform.position = focusTr.position;
        camPivot.mainCam.LookAt(camPivot.transform);        
        camPivot.mainCam.parent = camPivot.transform;
        camPivot.mainCam.localPosition = new Vector3(0, 0, focusDistance);
        camPivot.mainCam.localRotation = Quaternion.identity;
    }

    [ContextMenu("FocusCurrentMeshBound")]
    public void FocusCurrentMeshBound()
    {
        MeshFilter mf = focusTr.GetComponent<MeshFilter>();
        if (mf == null) return;
        Vector3 centerPos = Application.isPlaying ? mf.mesh.bounds.center : mf.sharedMesh.bounds.center;
        centerPos = focusTr.TransformPoint(centerPos);        

        CamPivot camPivot = FindObjectOfType<CamPivot>();
        camPivot.mainCam.parent = null;
        camPivot.transform.position = centerPos;
        camPivot.mainCam.LookAt(camPivot.transform);        
        camPivot.mainCam.parent = camPivot.transform;
        camPivot.mainCam.localPosition = new Vector3(0, 0, focusDistance);
        camPivot.mainCam.localRotation = Quaternion.identity;
    }
}
