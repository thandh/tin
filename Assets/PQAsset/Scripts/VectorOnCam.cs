﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class VectorOnCam : MonoBehaviour
{
    public bool isDebug = false;

    [Header("Input")]
    public Camera camIn = null;
    public DirectionType dirTr = new DirectionType();

    [Header("Output")]    
    public Vector3 pRoot = new Vector3();
    public Vector3 pPeek = new Vector3();
    public Vector2 dir2Out = new Vector2();

    [ContextMenu("FindVector")]
    public void FindVector()
    {        
        pRoot = camIn.WorldToScreenPoint(dirTr.trans.position);
        pPeek = camIn.WorldToScreenPoint(dirTr.trans.position + dirTr.GetDir());
        dir2Out = new Vector2(pPeek.x - pRoot.x, pPeek.y - pRoot.y);
    }
}