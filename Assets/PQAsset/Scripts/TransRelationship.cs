﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransRelationship : TransStorage
{
    /// <summary>
    /// 1st element : Trans compare to
    /// 2nd element : Trans to compare
    /// </summary>

    [ContextMenu("Compare")]
    public void Compare()
    {
        if (isDebug) Debug.Log("Start Compare");

        if (trStorageList.Count != 2)
        {
            if (isDebug) Debug.Log("<color = green> Invalid!! </color>");
        }

        aTransStorage trsCompare = trStorageList[0];
        aTransStorage trsSelf = trStorageList[1];

        Transform saveParent = trsSelf.trs.parent;
        Vector3 lastSca = transform.localScale;
        trsSelf.trs.parent = trsCompare.trs;

        boolPos = true; boolRot = true; boolSca = false;        
        SetState(1);

        trsSelf.trs.parent = saveParent;
        trsSelf.trs.localScale = lastSca;

        if (isDebug) Debug.Log("Done Compare");
    }

    [ContextMenu("SaveCompare")]
    public void SaveCompare()
    {
        if (isDebug) Debug.Log("Start SaveCompare");

        if (trStorageList.Count != 2)
        {
            if (isDebug) Debug.Log("<color = green> Invalid!! </color>");
        }

        aTransStorage trsCompare = trStorageList[0];
        aTransStorage trsSelf = trStorageList[1];

        Transform saveParent = trsSelf.trs.parent;
        trsSelf.trs.parent = trsCompare.trs;

        boolPos = true; boolRot = true; boolSca = false;
        SelfCopyFromTrans();
        trsSelf.trs.parent = saveParent;

        if (isDebug) Debug.Log("Done SaveCompare");
    }
}
