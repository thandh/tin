﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Controls : Singleton<Controls>
{
    public GameObject hole, holetarget, holeline;
    public GameObject btnArtMove, UI_ContorlPanel;

    public bool isskull, ishole, istrans;
    public bool ishera;

    // Use this for initialization
    void Awake()
    {
        ishera = false;        

        ishole = false;
        hole.SetActive(false);

        isskull = true;
        skull.Instance.gameObject.SetActive(true);        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void skullOn()
    {
        if (!isskull)
        {            
            skull.Instance.OnOffMesh(true);
            isskull = true;
        }
        else
        {            
            skull.Instance.OnOffMesh(false);
            isskull = false;
        }
    }

    public void holeOn()
    {
        if (!ishole)
        {
            hole.SetActive(true);
            holeline.GetComponent<LineRenderer>().SetVertexCount(0);

            if (holetarget != null) holetarget.SetActive(false);

            ishole = true;
            CreateCircleRaycast(holetarget.transform.position - holetarget.transform.up * 100, holetarget.transform.up);
        }
        else
        {
            hole.SetActive(false);
            if (holetarget != null) holetarget.SetActive(false);
            ishole = false;
        }
    }

    #region Model A Main

    public void HoleOnMain()
    {
        if (!ishole)
        {            
            holeline.GetComponent<LineRenderer>().SetVertexCount(0);
            if (holetarget != null)
            {
                holetarget.SetActive(false);
            }
            ishole = true;

            CreateCircleRaycast(holetarget.transform.position - holetarget.transform.up * 100, holetarget.transform.up);
        }
        else
        {            
            if (holetarget != null)
            {
                holetarget.SetActive(false);
            }
            ishole = false;

            UndoManager.Instance.Undo();
        }
    }

    #endregion

    float radius = 7f;
    float distanceRaycast = 0.1f;

    List<Vector3> listDrawPos = new List<Vector3>();
    Vector3 direction;

    void CreateCircleRaycast(Vector3 center, Vector3 direct)
    {
        RaycastHit hit;

        List<CutHole> lstCutHole = new List<CutHole>();

        List<Vector3> listVec = CreateGlobalRaycast(center);

        foreach (Vector3 vec in listVec)
        {
            listDrawPos.Add(vec);
            direction = direct;

            Ray ray = new Ray(vec, direct);

            if (Physics.Raycast(ray, out hit, 1000.0f))
            {
                CutHole cuthole = hit.collider.gameObject.GetComponent<CutHole>();
                if (cuthole)
                {
                    cuthole.SetIDCut(hit.triangleIndex);
                    cuthole.lastHole.position = hit.point;
                    if (!lstCutHole.Contains(cuthole))
                        lstCutHole.Add(cuthole);
                }
            }
        }        

        if (lstCutHole.Count > 0)
        {
            UndoManager.Instance.Register(lstCutHole[0]);
            lstCutHole[0].UndoRegister();

            lstCutHole[0].enabled = true;
            lstCutHole[0].loopCut(true);
        }

        for (int i = 1; i < lstCutHole.Count; i++)
        {
            UndoManager.Instance.RegisterMember(lstCutHole[i]);
            lstCutHole[i].UndoRegister();

            lstCutHole[i].enabled = true;
            lstCutHole[i].loopCut(true);
        }
    }

    List<Vector3> CreateGlobalRaycast(Vector3 center)
    {
        List<Vector3> listVec = new List<Vector3>();

        for (float j = -radius; j < radius; j += distanceRaycast)
        {
            float y = j;
            float r = Mathf.Sqrt(radius * radius - j * j);
            for (float i = -r; i < r; i += distanceRaycast)
            {
                float x = i;

                float z = Mathf.Sqrt(radius * radius - (x) * (x) - (y) * (y));

                listVec.Add(new Vector3(x, y, z) + center);
                listVec.Add(new Vector3(-x, -y, -z) + center);
            }
        }

        return listVec;
    }

    public void heraOn()
    {
        if (!ishera)
        {
            tCraniotomy.Instance.On_tHera();
            ishera = true;
        }
        else
        {
            tCraniotomy.Instance.AllOff();
            ishera = false;
        }
    }

    public void transOn()
    {
        if (!istrans)
        {
            // UI_trans.GetComponent<Image> ().color = Color.red;
            cerebellumControl.Instance.isTransform = true;
            istrans = true;
        }
        else
        {
            // UI_trans.GetComponent<Image> ().color = Color.white;
            cerebellumControl.Instance.isTransform = false;
            istrans = false;
        }

    }

    public void reset()
    {
        holeline.GetComponent<LineRenderer>().SetVertexCount(0);

        if (holetarget != null)
        {
            holetarget.SetActive(false);
        }

        ishera = false;

        ishole = false;
        hole.SetActive(false);
                
        isskull = true;
        skull.Instance.gameObject.SetActive(true);        
        cerebellumControl.Instance.isTransform = false;
        istrans = false;        
        cerebellumControl.Instance.arteryreset();
    }

    private void OnEnable()
    {
        tCerebellumUp.Instance.afterCerebellumGo += craniotomyClickOff;
    }

    private void OnDisable()
    {

    }

    void craniotomyClickOff()
    {

    }
}

