﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class CamPivot_Keyview_AvarageRezAngle : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Input State 0")]
    public CamPivot camPivot = null;
    public OriginOfMesh oriVII = null;
    public OriginOfMesh oriVIII = null;
    public OriginOfMesh oriX = null;
    public Direction_XToBrain dirXToBrain = null;
    Transform RezTr = null;

    [ContextMenu("Stop")]
    public void Stop()
    {
        StopAllCoroutines();
    }    

    [ContextMenu("InitParams")]
    public void InitParams()
    {
        camPivot.Reset0();
        camPivot.SetZ(0);
        oriVII.meshFilter = VII.Instance.mf;
        oriVIII.meshFilter = VIII.Instance.mf;
        oriX.meshFilter = X.Instance.mf;
        RezTr = Rez.Instance.transform;
    }

    /// <summary>
    /// Using vert close
    /// </summary>
    [ContextMenu("State0")]
    public void State0()
    {
        dirXToBrain.DefineXToGo();

        Vector3 dirRez_VII = oriVII.transform.position - RezTr.position;
        if (Vector3.Dot(dirRez_VII, dirXToBrain.drawRay.dir) < 0) dirRez_VII = -dirRez_VII;
        Vector3 dirRez_VIII = oriVIII.transform.position - RezTr.position;
        if (Vector3.Dot(dirRez_VIII, dirXToBrain.drawRay.dir) < 0) dirRez_VIII = -dirRez_VIII;
        Vector3 dirRez_X = oriX.transform.position - RezTr.position;
        if (Vector3.Dot(dirRez_X, dirXToBrain.drawRay.dir) < 0) dirRez_X = -dirRez_X;
        //TODO
        camPivot.LookRezAndStraightX();
    }    

    [Header("Calibration")]
    public float stepBack = 1f;

    [Header("Stepback Direction")]
    public DrawARayAt_PosDir rayToGo = null;

    [ContextMenu("Stepback")]
    public void Stepback()
    {
        //TODO
        camPivot.LookRezAndStraightX();
    }

    bool CheckOnCam(VectorOnCam oncam, float minX, float maxX, float minY, float maxY)
    {
        Resolution currentRes = Screen.currentResolution;
        return (oncam.pRoot.x >= minX && oncam.pRoot.x <= maxX && oncam.pRoot.y >= minY && oncam.pRoot.y <= maxY);
    }

    bool CheckOnCam_SinusNotSameBrain(VectorOnCam oncamSinus, VectorOnCam oncamBrain)
    {
        Resolution currentRes = Screen.currentResolution;
        return (CheckOnCam(oncamSinus, 0f, currentRes.width / 2f, currentRes.height / 2f, currentRes.height))
            && !(CheckOnCam(oncamBrain, 0f, currentRes.width / 2f, currentRes.height / 2f, currentRes.height));
    }

    [Header("Output RotateInStep")]
    public float minAngleVIIVIII = Mathf.Infinity;    
    public Keyview_Possible possibleCam = null;    
}