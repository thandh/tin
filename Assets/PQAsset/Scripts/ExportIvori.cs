﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class ExportIvori : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Clickable")]
    public bool ClickHere = false;

    [Header("Inputs")]
    public LineRenderer line = null;
    public TINFileOutput ivoriFileIO = null;

    [ContextMenu("InitParams")]
    public void InitParams()
    {
        line = LineCut.Instance.line;
    }

    [Serializable]
    public class AnIvoriData
    {
        [Header("Debug")]
        public bool isDebug = false;

        [Header("Params")]
        public List<Vector3> lineList = new List<Vector3>();

        /// <summary>
        /// element 0 : General View
        /// element 1 : Middle View
        /// element 2 : Key View
        /// </summary>
        public List<TransStorage.aTransStorage> camPosList = new List<TransStorage.aTransStorage>() { new TransStorage.aTransStorage(), new TransStorage.aTransStorage(), new TransStorage.aTransStorage() };
        public TransStorage.aTransStorage heraCompare = new TransStorage.aTransStorage(null, TransStorage.Space.Local, Vector3.zero, Vector3.zero, Vector3.one);
        public TransStorage.aTransStorage DKHera = new TransStorage.aTransStorage(null, TransStorage.Space.Local, Vector3.zero, Vector3.zero, Vector3.one);
        public TransStorage.aTransStorage tempoCylinder = new TransStorage.aTransStorage(null, TransStorage.Space.World, Vector3.zero, Vector3.zero, Vector3.one);

        public AnIvoriData() { this.lineList = new List<Vector3>(); this.camPosList = new List<TransStorage.aTransStorage>() { new TransStorage.aTransStorage(), new TransStorage.aTransStorage(), new TransStorage.aTransStorage()}; }
        public AnIvoriData(Vector3[] lineList, List<TransStorage.aTransStorage> camPosList)
        {
            this.lineList = lineList.ToList();
            this.camPosList = camPosList;
        }

        public bool IsValid()
        {
            if (lineList.Count == 0) { if (isDebug) Debug.Log("Position count : " + lineList.Count); return false; }
            if (camPosList.Count == 3 || camPosList[0].position == Vector3.zero || camPosList[1].position == Vector3.zero || camPosList[2].position == Vector3.zero)
            { if (isDebug) Debug.Log("Invalid Cam Positions!!"); return false; }

            return true;
        }

        public override string ToString()
        {
            return "abc";
        }

        public string ToJson()
        {
            return JsonUtility.ToJson(this);
        }
    }    

    [ContextMenu("GetLineSet")]
    public void GetLineSet()
    {
        Vector3[] posAr = new Vector3[line.positionCount];
        line.GetPositions(posAr);
        currentIvoriScrip.currentData.lineList = posAr.ToList();
    }

    [ContextMenu("GetLineSet")]
    public void SetLineSet()
    {        
        Vector3[] verts = currentIvoriScrip.currentData.lineList.ToArray();
        line.SetVertexCount(verts.Length);
        line.SetPositions(verts);
    }

    [ContextMenu("ReSetLineSet")]
    public void ReSetLineSet()
    {   
        line.SetVertexCount(0);
        line.SetPositions(new Vector3[0]);
    }

    [ContextMenu("DebugCurrentIvori")]
    public void DebugCurrentIvori()
    {
        Debug.Log(currentIvoriScrip.currentData.ToJson());
    }

    [Header("From Ivori")]
    public IvoriCSVTemplate ivoriTemplate = null;

    [ContextMenu("ReadFromIvori")]
    public void ReadFromIvori()
    {
        if (isDebug) Debug.Log("Start GetFromIvori()");

        ivoriTemplate.ReadCSV();
        AfterReadCSV();

        if (isDebug) Debug.Log("Done GetFromIvori()");
    }

    [ContextMenu("ReadFromCase")]
    public void ReadFromCase()
    {
        StartCoroutine(C_ReadFromCase());
    }

    IEnumerator C_ReadFromCase()
    {
        if (isDebug) Debug.Log("Start C_ReadFromCase");
        ivoriFileIO.Import();
        yield return new WaitUntil(() => ivoriFileIO.doneImport == true);
        ivoriTemplate.ReadCSV(ivoriFileIO.data);
        AfterReadCSV();
        if (isDebug) Debug.Log("Done C_ReadFromCase");
        yield break;
    }

    void AfterReadCSV()
    {
        currentIvoriScrip.currentData = new AnIvoriData();
        IvoriCSVTemplate.IvoriLine line = new IvoriCSVTemplate.IvoriLine();

        //LineSet
        line = ivoriTemplate.ivoriContent.content.Find((x) => x.querris[0] == "LineSet");
        if (line == null) { if (isDebug) Debug.LogError("line null!!"); }
        else
            for (int i = 0; i < int.Parse(line.querris[2]); i++)
            {
                currentIvoriScrip.currentData.lineList.Add(new Vector3(float.Parse(line.querris[i * 3 + 3]), float.Parse(line.querris[i * 3 + 4]), float.Parse(line.querris[i * 3 + 5])));
            }

        //Demo
        int index = 0;
        for (int i = 0; i < ivoriTemplate.ivoriContent.content.Count; i++)
        {
            if (ivoriTemplate.ivoriContent.content[i].querris[0] == "Demo")
            {
                List<string> querris = ivoriTemplate.ivoriContent.content[i].querris;
                if (index < currentIvoriScrip.currentData.camPosList.Count)
                {
                    currentIvoriScrip.currentData.camPosList[index].position.x = float.Parse(querris[1]);
                    currentIvoriScrip.currentData.camPosList[index].position.y = float.Parse(querris[2]);
                    currentIvoriScrip.currentData.camPosList[index].position.z = float.Parse(querris[3]);
                    currentIvoriScrip.currentData.camPosList[index].euler.x = float.Parse(querris[4]);
                    currentIvoriScrip.currentData.camPosList[index].euler.y = float.Parse(querris[5]);
                    currentIvoriScrip.currentData.camPosList[index].euler.z = float.Parse(querris[6]);
                    index++;
                }
            }
        }

        //HeraCylinder
        line = ivoriTemplate.ivoriContent.content.Find((x) => x.querris[0] == "HeraCylinder");
        if (line != null)
        {
            currentIvoriScrip.currentData.heraCompare.position = new Vector3(float.Parse(line.querris[1]), float.Parse(line.querris[2]), float.Parse(line.querris[3]));
            currentIvoriScrip.currentData.heraCompare.euler = new Vector3(float.Parse(line.querris[4]), float.Parse(line.querris[5]), float.Parse(line.querris[6]));
            GetHera();
        }
    }

    [Header("CamPivot")]
    public CamPivot camPivot = null;

    [ContextMenu("ToCam0")]
    public void ToCam0()
    {
        SetCamAt(0);
    }

    [ContextMenu("ToCam1")]
    public void ToCam1()
    {
        SetCamAt(1);
    }

    [ContextMenu("ToCam2")]
    public void ToCam2()
    {
        SetCamAt(2);
    }

    void SetCamAt(int i)
    {        
        Transform mainCam = Camera.main.transform;
        mainCam.position = currentIvoriScrip.currentData.camPosList[i].position;
        mainCam.rotation = Quaternion.Euler(currentIvoriScrip.currentData.camPosList[i].euler);
    }

    [Header("Saving Process")]
    public IvoriScrip currentIvoriScrip = null;

    [ContextMenu("SaveCam0")]
    public void SaveCam0()
    {
        SaveCamAt(0);
    }

    [ContextMenu("SaveCam1")]
    public void SaveCam1()
    {
        SaveCamAt(1);
    }

    [ContextMenu("SaveCam2")]
    public void SaveCam2()
    {
        SaveCamAt(2);
    }

    void SaveCamAt(int i)
    {        
        Transform mainCam = Camera.main.transform;
        currentIvoriScrip.currentData.camPosList[i].position = mainCam.position;
        currentIvoriScrip.currentData.camPosList[i].euler = mainCam.rotation.eulerAngles;
    }

    [Header("Export")]
    public bool doneExport = true;

    [ContextMenu("Export")]
    public void Export()
    {
        StartCoroutine(C_Export());
    }

    IEnumerator C_Export()
    {
        doneExport = false;

        if (isDebug) Debug.Log("Start C_Export()");

        BeforeExport();

        if (!ivoriFileIO.Existed())
        {
            ivoriFileIO.Empty();
            yield return new WaitUntil(() => ivoriFileIO.doneEmpty == true);
        }

        ivoriFileIO.outFile(ivoriTemplate.ToTxtContent());
        yield return new WaitUntil(() => ivoriFileIO.doneEmpty == true);

        doneExport = true;

        if (isDebug) Debug.Log("Done C_Export() : " + ivoriFileIO.currentFolder);

        yield break;
    }

    void BeforeExport()
    {
        SyncLineSet_ScripToTemplate();
        SyncCamPos_ScripToTemplate();
        SyncHeraCompare_ScripToTemplate();
        SyncDKHera_ScripToTemplate();
    }

    void SyncLineSet_ScripToTemplate()
    {
        List<Vector3> lineList = new List<Vector3>();
        AnIvoriData currentData = currentIvoriScrip.currentData;

        IvoriCSVTemplate.IvoriLine line = new IvoriCSVTemplate.IvoriLine();
        line.querris.Add("LineSet");
        line.querris.Add("3");
        line.querris.Add((currentData.lineList.Count).ToString());
                
        for (int i = 0; i < currentData.lineList.Count; i++)
        {
            line.querris.Add(currentData.lineList[i].x.ToString());
            line.querris.Add(currentData.lineList[i].y.ToString());
            line.querris.Add(currentData.lineList[i].z.ToString());
        }

        int index = ivoriTemplate.ivoriContent.content.FindIndex((x) => x.querris[0] == "LineSet");
        ivoriTemplate.ivoriContent.content[index] = line;
    }

    void SyncCamPos_ScripToTemplate()
    {                
        AnIvoriData currentData = currentIvoriScrip.currentData;

        List<int> indexDemo = new List<int>();
        for (int i = 0; i < ivoriTemplate.ivoriContent.content.Count; i++)
            if (ivoriTemplate.ivoriContent.content[i].querris[0] == "Demo")
                indexDemo.Add(i);
        
        ivoriTemplate.ivoriContent.content[indexDemo[0]] = IvoriCamLine(currentData.camPosList[0]);
        ivoriTemplate.ivoriContent.content[indexDemo[1]] = IvoriCamLine(currentData.camPosList[1]);
        ivoriTemplate.ivoriContent.content[indexDemo[2]] = IvoriCamLine(currentData.camPosList[2]);
    }

    IvoriCSVTemplate.IvoriLine IvoriCamLine(TransStorage.aTransStorage atrans)
    {
        IvoriCSVTemplate.IvoriLine line = new IvoriCSVTemplate.IvoriLine();
        line.querris.Add("Demo");
        line.querris.Add(atrans.position.x.ToString());
        line.querris.Add(atrans.position.y.ToString());
        line.querris.Add(atrans.position.z.ToString());
        line.querris.Add(atrans.euler.x.ToString());
        line.querris.Add(atrans.euler.y.ToString());
        line.querris.Add(atrans.euler.z.ToString());
        line.querris.Add("0");
        line.querris.Add("0");
        line.querris.Add("30");
        return line;
    }

    void SyncHeraCompare_ScripToTemplate()
    {
        AnIvoriData currentData = currentIvoriScrip.currentData;
        int index = ivoriTemplate.ivoriContent.content.FindIndex((x) => x.querris[0] == "HeraCylinder");
        if (index == -1) { if (isDebug) Debug.Log("Khong co line HeraCylinder!!"); return; }
        ivoriTemplate.ivoriContent.content[index] = IvoriHeraCylinder(currentData.heraCompare);
    }

    IvoriCSVTemplate.IvoriLine IvoriHeraCylinder(TransStorage.aTransStorage atrans)
    {
        IvoriCSVTemplate.IvoriLine line = new IvoriCSVTemplate.IvoriLine();
        line.querris.Add("HeraCylinder");
        line.querris.Add(atrans.position.x.ToString());
        line.querris.Add(atrans.position.y.ToString());
        line.querris.Add(atrans.position.z.ToString());
        line.querris.Add(atrans.euler.x.ToString());
        line.querris.Add(atrans.euler.y.ToString());
        line.querris.Add(atrans.euler.z.ToString());
        return line;
    }

    void SyncDKHera_ScripToTemplate()
    {
        AnIvoriData currentData = currentIvoriScrip.currentData;
        int index = ivoriTemplate.ivoriContent.content.FindIndex((x) => x.querris[0] == "dkHera");
        if (index == -1) { if (isDebug) Debug.Log("Khong co line dkHera!!"); return; }
        ivoriTemplate.ivoriContent.content[index] = IvoriDKHera(currentData.DKHera);
    }

    IvoriCSVTemplate.IvoriLine IvoriDKHera(TransStorage.aTransStorage atrans)
    {
        IvoriCSVTemplate.IvoriLine line = new IvoriCSVTemplate.IvoriLine();
        line.querris.Add("dkHera");
        line.querris.Add(atrans.scale.x.ToString());
        line.querris.Add(atrans.scale.y.ToString());
        line.querris.Add(atrans.scale.z.ToString());
        return line;
    }

    [Header("Save Hera")]
    public TransRelationship heraRelation = null;
    public AutoTranslateTemplate ivoriTranslateTemplate = null;

    [ContextMenu("GetHera")]
    public void GetHera()
    {
        heraRelation.trStorageList[1].position = currentIvoriScrip.currentData.heraCompare.position;
        heraRelation.trStorageList[1].euler = currentIvoriScrip.currentData.heraCompare.euler;
        heraRelation.Compare();
        heraRelation.transform.localScale = Vector3.one;
        ivoriTranslateTemplate.GetBeginByCurrent();
    }

    [ContextMenu("SaveHera")]
    public void SaveHera()
    {
        ivoriTranslateTemplate.GetBeginByCurrent();

        heraRelation.SaveCompare();
        heraRelation.transform.localScale = Vector3.one;

        currentIvoriScrip.currentData.heraCompare.position = heraRelation.trStorageList[1].position;
        currentIvoriScrip.currentData.heraCompare.euler = heraRelation.trStorageList[1].euler;

        IvoriCSVTemplate.IvoriLine lineHeraCylinder = new IvoriCSVTemplate.IvoriLine();
        lineHeraCylinder.querris.Add("HeraCylinder");
        lineHeraCylinder.querris.Add(heraRelation.trStorageList[1].position.x.ToString());
        lineHeraCylinder.querris.Add(heraRelation.trStorageList[1].position.y.ToString());
        lineHeraCylinder.querris.Add(heraRelation.trStorageList[1].position.z.ToString());
        lineHeraCylinder.querris.Add(heraRelation.trStorageList[1].euler.x.ToString());
        lineHeraCylinder.querris.Add(heraRelation.trStorageList[1].euler.y.ToString());
        lineHeraCylinder.querris.Add(heraRelation.trStorageList[1].euler.z.ToString());

        int indexHeraCylinder = ivoriTemplate.ivoriContent.content.FindIndex((x) => x.querris[0] == "HeraCylinder");
        if (isDebug) Debug.Log("indexHeraCylinder : " + indexHeraCylinder);

        if (indexHeraCylinder == -1)
        {
            indexHeraCylinder = ivoriTemplate.ivoriContent.content.FindIndex((x) => x.querris[0] == "HeraView") + 1;
            ivoriTemplate.ivoriContent.content.Insert(indexHeraCylinder, lineHeraCylinder);
            if (isDebug) Debug.Log("Insert At: " + indexHeraCylinder);
        }
        else
        {
            ivoriTemplate.ivoriContent.content[indexHeraCylinder] = lineHeraCylinder;
            if (isDebug) Debug.Log("Change value At: " + indexHeraCylinder);
        }
    }

    [Header("Save DKHera")]
    public ApplybyLocalTransform applyDKHera = null;

    [ContextMenu("GetDKHera")]
    public void GetDKHera()
    {
        applyDKHera.applyScale = currentIvoriScrip.currentData.DKHera.scale;
        applyDKHera.ApplyScale();
    }

    [Header("HeraPush")]
    public int DefaultDKHeraPush = 10;

    [ContextMenu("SaveDKHera")]
    public void SaveDKHera()
    {
        applyDKHera.GetCurrentSca();
        currentIvoriScrip.currentData.DKHera.scale = applyDKHera.applyScale;

        IvoriCSVTemplate.IvoriLine lineDKHera = new IvoriCSVTemplate.IvoriLine();
        lineDKHera.querris.Add("dkHera");
        lineDKHera.querris.Add(applyDKHera.applyScale.x.ToString());
        lineDKHera.querris.Add(applyDKHera.applyScale.y.ToString());
        lineDKHera.querris.Add(applyDKHera.applyScale.z.ToString());
        lineDKHera.querris.Add(applyDKHera.applyScale.z.ToString());
        lineDKHera.querris.Add(DefaultDKHeraPush.ToString());

        int indexDKHera = ivoriTemplate.ivoriContent.content.FindIndex((x) => x.querris[0] == "dkHera");
        if (isDebug) Debug.Log("indexDKHera  : " + indexDKHera);

        if (indexDKHera == -1)
        {
            indexDKHera = ivoriTemplate.ivoriContent.content.FindIndex((x) => x.querris[0] == "HeraView") + 1;
            ivoriTemplate.ivoriContent.content.Insert(indexDKHera, lineDKHera);
            if (isDebug) Debug.Log("Insert At: " + indexDKHera);
        }
        else
        {
            ivoriTemplate.ivoriContent.content[indexDKHera] = lineDKHera;
            if (isDebug) Debug.Log("Change value At: " + indexDKHera);
        }
    }

    [Header("Tempo Cylinder")]
    public Craniotomy crain = null;

    [ContextMenu("GetTempoCylinder")]
    public void GetTempoCylinder()
    {
        crain.transform.position = currentIvoriScrip.currentData.tempoCylinder.position;
        crain.transform.rotation = Quaternion.Euler(currentIvoriScrip.currentData.tempoCylinder.euler);
    }

    [ContextMenu("SaveTempoCylinder")]
    public void SaveTempoCylinder()
    {
        currentIvoriScrip.currentData.tempoCylinder.position = crain.transform.position;
        currentIvoriScrip.currentData.tempoCylinder.euler = crain.transform.rotation.eulerAngles;
    }

    [ContextMenu("Stop")]
    public void Stop()
    {
        StopAllCoroutines();
        ivoriFileIO.StopAllCoroutines();
    }
}