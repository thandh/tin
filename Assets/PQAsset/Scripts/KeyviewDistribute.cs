﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using System.Linq;

public class KeyviewDistribute : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Input")]
    public CamPivot_Keyview keyview = null;
    public GameObject camviewPreb;
    public List<KeyviewMarker> markerList = new List<KeyviewMarker>();

    [ContextMenu("Create")]
    public void Create()
    {
        //List<Quaternion> quaterList
        //for (int i = 0; i < keyview.possibleCam)
    }

    [ContextMenu("DeleteAll")]
    public void DeleteAll()
    {
        markerList = GetComponentsInChildren<KeyviewMarker>().ToList();
        for (int i = 0; i < markerList.Count; i++)
        {
            if (Application.isPlaying) DestroyImmediate(markerList[i].gameObject); else Destroy(markerList[i].gameObject);
        }
        markerList = new List<KeyviewMarker>();
    }
}