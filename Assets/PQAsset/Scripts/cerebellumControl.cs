﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class cerebellumControl : Singleton<cerebellumControl>
{
	//MegaCurveDeform MCD;
	public float time = 0, timeMove = 0;
	public int line_index = 0;		
	public GameObject target, targetcy, line, heramove;
	public Button btnDraw;
	public Vector3[] linelist = new Vector3[100];
	public bool isdrawing, isTransform, isMove;
	public Transform rezTarget;	

	Vector3 moveUD;

	// Use this for initialization
	void Start ()
	{
		isdrawing = false;
		isTransform = false;		
		Mesh mesh = X.Instance.GetComponentInChildren<MeshFilter> ().mesh;
        
		float Mx = 0, mx = 0;
		float My = 0, my = 0;
		float Mz = 0, mz = 0;
       
		for (int i = 0; i < mesh.vertices.Length; i++) {
			if (i == 0) {
				Mx = mesh.vertices [i].x;
				My = mesh.vertices [i].y;
				Mz = mesh.vertices [i].z;
				mx = mesh.vertices [i].x;
				my = mesh.vertices [i].y;
				mz = mesh.vertices [i].z;
			} else {
				if (mesh.vertices [i].x > Mx)
					Mx = mesh.vertices [i].x; 
				if (mesh.vertices [i].y > My)
					My = mesh.vertices [i].y;
				if (mesh.vertices [i].z > Mz)
					Mz = mesh.vertices [i].z;
				if (mesh.vertices [i].x < mx)
					mx = mesh.vertices [i].x;   
				if (mesh.vertices [i].y < my)
					my = mesh.vertices [i].y;
				if (mesh.vertices [i].z < mz)
					mz = mesh.vertices [i].z;
			}
           

		}
		float MxU = 0, mxU = Mx;
		float MyU = 0, myU = My;
		float MzU = 0, mzU = Mz;
		float MxD = 0, mxD = Mx;
		float MyD = 0, myD = My;
		float MzD = 0, mzD = Mz;
		for (int i = 0; i < mesh.vertices.Length; i++) {
			if (mesh.vertices [i].y > (My + my) / 4) {
				if (mesh.vertices [i].x > MxU)
					MxU = mesh.vertices [i].x;
				if (mesh.vertices [i].y > MyU)
					MyU = mesh.vertices [i].y;
				if (mesh.vertices [i].z > MzU)
					MzU = mesh.vertices [i].z;
				if (mesh.vertices [i].x < mxU)
					mxU = mesh.vertices [i].x;
				if (mesh.vertices [i].y < myU)
					myU = mesh.vertices [i].y;
				if (mesh.vertices [i].z < mzU)
					mzU = mesh.vertices [i].z;

			} else {
				if (mesh.vertices [i].x > MxD)
					MxD = mesh.vertices [i].x;
				if (mesh.vertices [i].y > MyD)
					MyD = mesh.vertices [i].y;
				if (mesh.vertices [i].z > MzD)
					MzD = mesh.vertices [i].z;
				if (mesh.vertices [i].x < mxD)
					mxD = mesh.vertices [i].x;
				if (mesh.vertices [i].y < myD)
					myD = mesh.vertices [i].y;
				if (mesh.vertices [i].z < mzD)
					mzD = mesh.vertices [i].z;
			}


		}
		Vector3 midU = new Vector3 ((MxU + mxU) / 2, (MyU + myU) / 2, (MzU + mzU) / 2);
		Vector3 midD = new Vector3 ((MxD + mxD) / 2, (MyD + myD) / 2, (MzD + mzD) / 2);
		moveUD = midU - midD;
		// print(moveUD);
		//target.transform.position = new Vector3((Mx+mx)/2,(My+my)/2,(Mz+mz)/2);
		//  print(new Vector3((Mx + mx) / 2, (My + my) / 2, (Mz + mz) / 2));
	}
	// Update is called once per frame
	void Update ()
	{
		if (isMove) {
			artery.Instance.transform.position -= moveUD * Time.deltaTime / 3;
		}

        

		if (isTransform) {
			if (time < 5) {
				time += Time.deltaTime;
				//MCD.MaxDeviation = -time * 100;
				if (ChangerSceneMgr.Instance.isModel_A)
					heramove.transform.localPosition = new Vector3 (0, -2f + 0.16f * time, 0);
				if (ChangerSceneMgr.Instance.isModel_B)
					heramove.transform.localPosition = new Vector3 (0, -1.95f + 0.14f * time, 0);//1003データ
				heramove.transform.GetChild(0).transform.localRotation = Quaternion.Euler(20.8f,200,80-0.7f*time);

			} else {
				
				heramove.transform.localPosition = new Vector3(0, -2f, 0);
				 heramove.transform.localPosition = new Vector3(0, 0.6f, 0);
				//  MCD.MaxDeviation = -500;
				time = 5;
			}
		} else {
			if (time >= 5 && time < 10) {
				time += Time.deltaTime;
				//MCD.MaxDeviation = -500 + (time - 5) * 100;
				if (ChangerSceneMgr.Instance.isModel_A)
					heramove.transform.localPosition = new Vector3 (0, -1.2f - 0.16f * (time - 5), 0);
				if (ChangerSceneMgr.Instance.isModel_B)
					heramove.transform.localPosition = new Vector3 (0, -1.95f - 0.1f * (time - 10), 0);//1003データ

				heramove.transform.localRotation = Quaternion.Euler(22.8f,15.0f,0);
			} else {

				//MCD.MaxDeviation = -time * 5;
				heramove.transform.localPosition = new Vector3(0,-2.2f, 0);
				time = 0;
			}
			//heramove.transform.localRotation = Quaternion.LookRotation(cerebellumControl.Instance.rezTarget.position - heramove.transform.localPosition);
		}

		if (Input.GetMouseButton (0) && isdrawing)
        {
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit rayhit;
            if (Physics.Raycast(ray, out rayhit))
            {
                line.GetComponent<Renderer>().material.color = Color.red;
                line.GetComponent<LineRenderer>().SetVertexCount(line_index + 1);
                line.GetComponent<LineRenderer>().SetPosition(line_index, rayhit.point);
                line_index = Mathf.Min(line_index, 99);
                linelist[line_index] = rayhit.point;
                line_index++;
                // targetline.transform.position = rayhit.point;
            }
		}
		if (Input.GetMouseButtonDown (1) && isdrawing) {
			drawend ();
		}

	}

	public void HeraCerebellumDeformation ()
	{

		//MCD.MaxDeviation = -300;
		heramove.transform.localPosition = new Vector3 (0, -2f - 2, 0);
	}


	public void lineset (int n, Vector3[] k)
	{
		line.GetComponent<LineRenderer> ().SetVertexCount (n);
		line.GetComponent<LineRenderer> ().SetPositions (k);
		linelist = k;
		line_index = n;
		drawend ();
	}

	public void draw ()
	{
		if (isdrawing)
			drawend ();
		else {
			target.SetActive (false);
			line_index = 0;
			line.GetComponent<LineRenderer> ().SetVertexCount (0);
			isdrawing = true;
			// UI_draw.GetComponent<Image>().color = Color.red;
//			btnDraw.onClick.Invoke ();

		}
		Controls.Instance.holetarget.SetActive (true);
	}

	public void drawend ()
	{
//		target.SetActive (true);
		targetcy.SetActive (true);
		isdrawing = false;
		line.GetComponent<Renderer> ().material.color = Color.green;
//		UI_draw.GetComponent<Image> ().color = Color.white;
		btnDraw.GetComponent<SwitchImageButton> ().SwitchImage ();
	}

	public void arteryreset ()
	{
		artery.Instance.transform.localPosition = new Vector3 (0, 0, 0);		
	}

}

