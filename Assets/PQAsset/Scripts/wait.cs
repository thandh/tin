﻿using UnityEngine;
using System.Collections;

public class wait : MonoBehaviour
{
    public static bool isWait;
    static float wait_time;
    static float now_time;
    public static float timeError;
    public static bool k,isKeep;

    void Start ()
    {
        k = false;
        isWait = false;
        isKeep = false;
        wait_time = 0;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (isWait == true && isKeep == false)
        { 
            if (ManngerSystem.DebugInput == true)
            {
                timeError = 0;
                isWait = false;
            }
            else
            {
                now_time += Time.deltaTime;
                if(wait_time >= 0)
                {
                    timeError = wait_time - now_time;
                    if (now_time > wait_time)
                    {
                        timeError = 0;
                        if (k == false) ManngerSystem.Instance.start_number++;
                        isWait = false;                   
                    }
                }
                else
                {
                    timeError = -1;
                    if (Input.GetKeyDown(KeyCode.Space))
                    {
                        if (k == false) ManngerSystem.Instance.start_number++;
                        isWait = false;
                    }
                }
            }
        }
    }

    public static void setW(float i)
    {
        k = false;
        now_time = 0;
        wait_time = i;
        isWait = true;
    }

    public static void set(float i)
    {
        k = true;
        now_time = 0;
        wait_time = i;
        isWait = true;
    }
}
