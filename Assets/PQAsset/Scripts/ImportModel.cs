﻿using System.Collections;
using System.Collections.Generic;
using TriLib;
using TriLib.Samples;
using UnityEngine;

public class ImportModel : MonoBehaviour
{
    [Header("Debug")]
    public bool IsDebug = false;
    public bool useUpdate = true;

    [Header("Params")]
    public GameObject canvasImport = null;

    [Header("Load Process")]
    public bool Async = true;
    public GameObject _rootGameObject = null;
    
    [ContextMenu("OpenFileDialog")]
    public void OpenFileDialog()
    {
        StartCoroutine(C_OpenFileDialog());
    }

    IEnumerator C_OpenFileDialog()
    {
        if (IsDebug) Debug.Log("Start Open File Dialog");

        if (FileOpenDialog.Instance == null)
        {
            GameObject go = Instantiate(canvasImport as GameObject, Vector3.zero, Quaternion.identity);
            go.name = "CanvasImport";
            yield return new WaitUntil(() => FileOpenDialog.Instance != null);
        }

        LoadLocalAssetButtonClick();
        yield return new WaitUntil(() => _rootGameObject != null);


        if (IsDebug) Debug.Log("Done Open File Dialog");

        yield break;
    }

    /// <summary>
    /// Handles "Load local asset button" click event and tries to load an asset at chosen path.
    /// </summary>
    private void LoadLocalAssetButtonClick()
    {
        var fileOpenDialog = FileOpenDialog.Instance;
        fileOpenDialog.Title = "Please select a File";
        fileOpenDialog.Filter = AssetLoaderBase.GetSupportedFileExtensions() + "*.zip;";
#if (UNITY_WINRT && !UNITY_EDITOR_WIN)
                fileOpenDialog.ShowFileOpenDialog(delegate (byte[] fileBytes, string filename) 
                {
                    LoadInternal(filename, fileBytes);
#else
        fileOpenDialog.ShowFileOpenDialog(delegate (string filename)
        {
#endif
            LoadInternal(filename, null);
        }
        );
    }

    /// <summary>
    /// Loads a model from the given filename or given file bytes.
    /// </summary>
    /// <param name="filename">Model filename.</param>
    /// <param name="fileBytes">Model file bytes.</param>
    private void LoadInternal(string filename, byte[] fileBytes = null)
    {
        PreLoadSetup();
        var assetLoaderOptions = GetAssetLoaderOptions();
        if (!Async)
        {
            using (var assetLoader = new AssetLoader())
            {
                assetLoader.OnMetadataProcessed += AssetLoader_OnMetadataProcessed;
                try
                {
#if (UNITY_WINRT && !UNITY_EDITOR_WIN)
                        var extension = FileUtils.GetFileExtension(filename);
                        _rootGameObject = assetLoader.LoadFromMemoryWithTextures(fileBytes, extension, assetLoaderOptions, _rootGameObject);
#else
                    if (fileBytes != null && fileBytes.Length > 0)
                    {
                        var extension = FileUtils.GetFileExtension(filename);
                        _rootGameObject = assetLoader.LoadFromMemoryWithTextures(fileBytes, extension, assetLoaderOptions, _rootGameObject);
                    }
                    else if (!string.IsNullOrEmpty(filename))
                    {
                        _rootGameObject = assetLoader.LoadFromFileWithTextures(filename, assetLoaderOptions);
                    }
                    else
                    {
                        throw new System.Exception("File not selected");
                    }
#endif
                }
                catch (System.Exception exception)
                {
                    ErrorDialog.Instance.ShowDialog(exception.ToString());
                }
            }
            if (_rootGameObject != null)
            {
                PostLoadSetup();
            }
        }
        else
        {
            using (var assetLoader = new AssetLoaderAsync())
            {
                assetLoader.OnMetadataProcessed += AssetLoader_OnMetadataProcessed;
                try
                {
                    if (fileBytes != null && fileBytes.Length > 0)
                    {
                        var extension = FileUtils.GetFileExtension(filename);
                        assetLoader.LoadFromMemoryWithTextures(fileBytes, extension, assetLoaderOptions, null, delegate (GameObject loadedGameObject)
                        {
                            _rootGameObject = loadedGameObject;
                            if (_rootGameObject != null)
                            {
                                PostLoadSetup();
                            }
                        });
                    }
                    else if (!string.IsNullOrEmpty(filename))
                    {
                        assetLoader.LoadFromFileWithTextures(filename, assetLoaderOptions, null, delegate (GameObject loadedGameObject)
                        {
                            _rootGameObject = loadedGameObject;
                            if (_rootGameObject != null)
                            {
                                PostLoadSetup();
                            }
                        });
                    }
                    else
                    {
                        throw new System.Exception("File not selected");
                    }
                }
                catch (System.Exception exception)
                {
                    ErrorDialog.Instance.ShowDialog(exception.ToString());
                }
            }
        }
    }

    /// <summary>
    /// Hides user controls.
    /// </summary>
    private void HideControls()
    {

    }

    /// <summary>
    /// Pre Load setup.
    /// </summary>
    private void PreLoadSetup()
    {
        HideControls();
        if (_rootGameObject != null)
        {
            Destroy(_rootGameObject);
            _rootGameObject = null;
        }
    }

    /// <summary>
    /// Gets the asset loader options.
    /// </summary>
    /// <returns>The asset loader options.</returns>
    private AssetLoaderOptions GetAssetLoaderOptions()
    {
        var assetLoaderOptions = AssetLoaderOptions.CreateInstance();
        assetLoaderOptions.DontLoadCameras = false;
        assetLoaderOptions.DontLoadLights = false;
        assetLoaderOptions.AddAssetUnloader = true;
        return assetLoaderOptions;
    }

    /// <summary>
    /// Event assigned to FBX metadata loading. Editor debug purposes only.
    /// </summary>
    /// <param name="metadataType">Type of loaded metadata</param>
    /// <param name="metadataIndex">Index of loaded metadata</param>
    /// <param name="metadataKey">Key of loaded metadata</param>
    /// <param name="metadataValue">Value of loaded metadata</param>
    private void AssetLoader_OnMetadataProcessed(AssimpMetadataType metadataType, uint metadataIndex, string metadataKey, object metadataValue)
    {
        Debug.Log("Found metadata of type [" + metadataType + "] at index [" + metadataIndex + "] and key [" + metadataKey + "] with value [" + metadataValue + "]");
    }

    /// <summary>
    /// Post load setup.
    /// </summary>
    private void PostLoadSetup()
    {

    }
}
