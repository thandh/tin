﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Canel : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Params")]
    public CamPivot camPivot = null;
    public Follow camPivotFolowCanel = null;
    public VertClosestToTransform sinusCloseRez = null;
    public VertClosestToTransform brainCloseSinus = null;
    public VertClosestToTransform sinusCloseBrain = null;
    public CenterOfTransform canelCenter = null;
    
    [ContextMenu("State0")]
    public void State0()
    {
        sinusCloseRez.GetClose();
        brainCloseSinus.GetClose();
        sinusCloseBrain.GetClose();
        canelCenter.Center();
        camPivot.transform.position = canelCenter.transform.position;
        camPivot.SetZ(0);
        camPivot.LookRezAndStraightX();        
    }

    [Header("Stepback")]    
    public Transform dirBS = null;
    public float stepBack = 2f;

    public void Stepback()
    {   
        canelCenter.transform.position += dirBS.forward * stepBack;
        camPivot.transform.position = canelCenter.transform.position;
        camPivot.SetZ(0);
        camPivot.LookRezAndStraightX();
    }
}
