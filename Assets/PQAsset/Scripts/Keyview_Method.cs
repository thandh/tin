﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Keyview_Method : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Params")]
    public CamPivot_Keyview keyview = null;

    [ContextMenu("InitParams")]
    public void InitParams()
    {

    }

    [Serializable]
    public enum TypeKeyview
    {
        Center,
        Crain,
        CrainEditor
    }

    [Header("Type Keyview")]
    public TypeKeyview typeKeyview = TypeKeyview.CrainEditor;

    [ContextMenu("CurrentSet")]
    public void CurrentSet()
    {
        delInit = null; delState0 = null; delStepback = null;
        switch (typeKeyview)
        {
            case TypeKeyview.Center: delInit += keyview.InitParams; delState0 += keyview.State0; delStepback += keyview.Stepback; break;
            case TypeKeyview.Crain: delInit += keyview.InitParams_UseCrain; delState0 += keyview.State0_UseCrain; delStepback += keyview.Stepback_UseCrain; break;
            case TypeKeyview.CrainEditor: delInit += keyview.InitParams_UseCrainFile; delState0 += keyview.State0_UseCrainEditor; delStepback += keyview.Stepback_UseCrainEditor; break;
        }
    }

    //[Header("Init")]
    public delegate void delKeyviewInit();
    public delKeyviewInit delInit = null;

    [ContextMenu("Init")]
    public void Init()
    {
        if (delInit == null) CurrentSet();
        delInit.Invoke();

    }

    //[Header("State0")]
    public delegate void delKeyviewState0();
    public delKeyviewState0 delState0 = null;

    [ContextMenu("State0")]
    public void State0()
    {
        if (delState0 == null) CurrentSet();
        delState0.Invoke();
    }

    //[Header("Stepback")]
    public delegate void delKeyviewStepback();
    public delKeyviewStepback delStepback = null;

    [ContextMenu("Stepback")]
    public void Stepback()
    {
        if (delStepback == null) CurrentSet();
        delStepback.Invoke();
    }

    [ContextMenu("Stepforward")]
    public void Stepforward()
    {
        if (delStepback == null) CurrentSet();
        keyview.stepBack *= -1;
        delStepback.Invoke();
        keyview.stepBack *= -1;
    }
}
