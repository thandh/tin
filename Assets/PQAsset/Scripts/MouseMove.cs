﻿using UnityEngine;
using System.Collections;

public class MouseMove : MonoBehaviour
{
    public Transform target;
    public float x, y;
    public float xSpeed = 1;
    public float ySpeed = 1;
    public float distence;
    public float disSpeed = 1;
    public float minDistence = 1;
    public float maxDistence = 5;

    private Quaternion rotationEuler;
    private Vector3 cameraPosition;

    void Start()
    {

    }
        
    void Update()
    {
        x += Input.GetAxis("Mouse X") * xSpeed * 154 * Time.deltaTime;
        y -= Input.GetAxis("Mouse Y") * ySpeed * 154 * Time.deltaTime;

        print(x);

        if (x > 360) x -= 360; else if (x < 0) x += 360;
        
        distence -= Input.GetAxis("Mouse ScrollWheel") * disSpeed * 154 * Time.deltaTime;
        distence = Mathf.Clamp(distence, minDistence, maxDistence);

        rotationEuler = Quaternion.Euler(y, x, 0);
        cameraPosition = rotationEuler * new Vector3(0, 0, -distence) + target.position;

        transform.rotation = rotationEuler;
        transform.position = cameraPosition;
    }
}
