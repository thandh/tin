﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class IvoriCSVTemplate : MonoBehaviour
{
    [Header("From Ivori")]
    public TextAsset csv = null;

    [Serializable]
    public class IvoriLine
    {
        public List<string> querris = new List<string>();
        public string ToTxt()
        {
            string content = string.Empty;
            for (int i = 0; i < this.querris.Count - 1; i++)
            {
                content += querris[i] + ", ";
            }
            content += querris[querris.Count - 1];
            return content;
        }
    }

    [Serializable]
    public class IvoriContent
    {
        public List<IvoriLine> content = new List<IvoriLine>();
    }

    public IvoriContent ivoriContent = new IvoriContent();

    [ContextMenu("ReadCSV")]
    public void ReadCSV()
    {
        ReadCSV(csv.text);        
    }
    
    public void ReadCSV(string csvContent)
    {
        ivoriContent = new IvoriContent();

        StringReader readerString = new StringReader(csvContent);
        string text = readerString.ReadLine();
        while (text != null)
        {
            IvoriLine line = new IvoriLine();
            line.querris = text.Split(',').ToList();
            ivoriContent.content.Add(line);
            text = readerString.ReadLine();
        }
    }

    public string ToTxtContent()
    {
        string content = string.Empty;

        for (int i = 0; i < this.ivoriContent.content.Count - 1; i++)
        {
            content += this.ivoriContent.content[i].ToTxt() + '\n';   
        }
        content += this.ivoriContent.content[this.ivoriContent.content.Count - 1].ToTxt();

        return content;
    }

    [ContextMenu("DebugTxtContent")]
    public void DebugTxtContent()
    {
        Debug.Log(ToTxtContent());
    }
}
