﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using System.Text.RegularExpressions;
using UnityEngine.UI;
using System.Text;
using System.Windows.Forms;
using RockVR.Video;

public class ManngerSystem : Singleton<ManngerSystem>
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Params")]
    internal int ikK = 10;
    public GUISkin GameSkin;
    public static Vector3[] posXYZ;
    protected FileInfo theSourceFile = null;
    protected StreamReader reader = null;
    protected StringReader readerString = null;
    protected string text = " ";

    // assigned to allow first line to be read below
    protected String[] oringinData = new String[1000];
    protected char[] delimiterChars = { ',' };
    string[] queries = new string[100];
    string[][] arg;
    static string[] branchName = new string[1000];
    static int[] branchLine = new int[1000];
    public int start_number = 0;
    int max_number;
    int max_branchnumber;
    public static bool isEnd;
    public static bool isLoad;
    bool waitread;
    public GameObject cube, UI_ivori;
    string[] guiNumber = new string[10];
    string[] guiSec = new string[10];
    string[] gui = new string[10];
    string[] sysText = new string[1000];
    int a;
    static bool[] waitForTime = new bool[1000];
    public static bool ResponsesOK;
    public static int ResponsesInt;
    bool selectE;
    GameObject[] TET = new GameObject[100];
    public static bool DebugInput;
    public static bool Move_Active;
    public static bool Equalizer_Active;
    string[][] Dearg;
    string Loadpath;
    public string stringToEdit;
    public GameObject VR_Cam;
    public static GameObject VR_CamS;
    public bool AutoLoad = true;
    public bool AudioMode = false;
    public bool DebugMenu;
    public TextAsset csv;
    public CameraController cc;
    public ButtonManager btnManager;

    // Use this for initialization
    MeshDeformer meshDeform;

    tHera _thera = null;
    tHera thera { get { if (_thera == null) _thera = tHera.Instance; return _thera; } }

    [SerializeField]
    PaintVertices paintVertices;
    MeshFilter _meshPaint;

    [Header("PicaUp")]
    public bool boolUsePauseAfterGoSkull = false;
    PicaUp picaUp = null;

    void Awake()
    {
        VR_CamS = VR_Cam;
        Equalizer_Active = true;
        Move_Active = true;
        DebugInput = false;
        ResponsesInt = 0;
        ResponsesOK = true;
        a = 0;
        waitread = true;
        isEnd = true;
        isLoad = false;
        start_number = 0;
        selectE = false;

        if (AudioMode)
        {
            GameObject spot = GameObjectHardFind("SPOT").gameObject;
            GameObject dayLight = GameObjectHardFind("Directional light_Day").gameObject;
            GameObject nightLight = GameObjectHardFind("Directional light_Night").gameObject;
            GameObject SunLight = GameObjectHardFind("Sun").gameObject;
            SunLight.SetActive(false);
            spot.SetActive(false);
            dayLight.SetActive(false);
            nightLight.SetActive(false);
        }
    }

    private void Start()
    {
        meshDeform = cerebellum_180416.Instance.GetComponentInChildren<MeshDeformer>();
        picaUp = PICA.Instance.picaUp;
    }

    static public Transform GameObjectHardFind(string str)
    {
        Transform result = null;
        foreach (Transform root in GameObject.FindObjectsOfType(typeof(Transform)))
        {
            if (root.transform.parent == null)
            { // means it's a root GO
                result = GameObjectHardFind(root, str, 0);
                if (result != null)
                    break;
            }
        }
        return result;
    }

    static private Transform GameObjectHardFind(Transform item, string str, int index)
    {
        if (index == 0 && item.name == str)
            return item;
        if (index < item.transform.childCount)
        {
            Transform result = GameObjectHardFind(item.transform.GetChild(index), str, 0);
            if (result == null)
            {
                return GameObjectHardFind(item, str, ++index);
            }
            else
            {
                return result;
            }
        }
        return null;
    }

    [Header("Hera compare to Cylinder/From Ivori")]
    public TransRelationship heraCompare = null;
    public dkHera dkhera = null;

    [Header("Params")]
    public Craniotomy crain = null;
    public Ivori_ExportCylinder ivori_ExportCrain = null;

    [Header("Cam Fly")]
    public CamFly camFly = null;

    [Header("Auto Deform by Hera")]
    public CerebellumDeformPanel heraDeform = null;
    public dkHera_AutoPush dkHeraAutoPush = null;    

    [Header("Process")]
    public bool doneDeform = true;
    public bool doneRelease = true;

    void Update()
    {
        if (isBloodVesselMove)
        {
            bloodVessel2.transform.localPosition = Vector3.MoveTowards(bloodVessel2.transform.localPosition, new Vector3(0, -10, 0), 2.5f * Time.deltaTime);
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            StartCoroutine(Reset());
        }

        if (Input.GetKeyDown(KeyCode.PageDown))
        {
            start_number++;
        }

        if (Input.GetKeyDown(KeyCode.PageUp))
        {
            start_number--;
            waitForTime[start_number] = true;
        }

        if (isLoad == false && AutoLoad == true)
        {
            arg = new string[1000][];
            readerString = new StringReader(csv.text);
            max_number = 0;
            max_branchnumber = 0;
            text = readerString.ReadLine();
            while (text != null)
            {
                oringinData[max_number] = text;
                queries = text.Split(',');
                if (queries[0] == "Name")
                {

                }
                else if (queries[0] == "Branch")
                {
                    arg[max_number] = new string[10];
                    sysText[max_number] = text;
                    arg[max_number][0] = queries[0];
                    arg[max_number][1] = queries[1];
                    branchName[max_branchnumber] = queries[1];
                    branchLine[max_branchnumber] = max_number;
                    waitForTime[max_number] = true;
                    max_branchnumber++;
                    max_number++;
                }
                else
                {
                    sysText[max_number] = text;
                    int queries_number = 0;
                    arg[max_number] = new string[100];

                    while (queries_number < queries.Length)
                    {
                        arg[max_number][queries_number] = queries[queries_number];
                        queries_number++;
                    }

                    waitForTime[max_number] = true;
                    max_number++;
                }

                text = readerString.ReadLine();
            }

            isLoad = true;
            isEnd = false;
            a = 0;
        }

        if (Input.GetKeyDown(KeyCode.L) && AutoLoad == false && isLoad == false)
        {
            iVoRi_Load();
        }

        if (start_number - 2 >= 0 && sysText[start_number - 2] != null)
        {
            gui[0] = sysText[start_number - 2] + ".";
            guiNumber[0] = (start_number - 2).ToString() + ".";
        }

        if (start_number - 1 >= 0 && sysText[start_number - 1] != null)
        {
            gui[1] = sysText[start_number - 1];
            guiNumber[1] = (start_number - 1).ToString() + ".";
        }

        if (sysText[start_number] != null)
        {
            gui[2] = sysText[start_number];
            gui[3] = sysText[start_number + 1];
            gui[4] = sysText[start_number + 2];
            gui[5] = sysText[start_number + 3];
            guiNumber[2] = start_number.ToString() + ".";
            guiNumber[3] = (start_number + 1).ToString() + ".";
            guiNumber[4] = (start_number + 2).ToString() + ".";
            guiNumber[5] = (start_number + 3).ToString() + ".";
        }

        if (wait.isWait == false && isEnd == false && DebugInput == false)
        {
            if (start_number < max_number)
            {
                SwitchCurrentCommand();
            }
        }

        if (isEnd == true && a == 0)
        {
            a = 1;
            print("System End!");
        }
    }

    [UnityEngine.ContextMenu("SwitchCurrentCommand")]
    public void SwitchCurrentCommand()
    {
        if (isDebug) Debug.Log("Switching at : " + start_number);

        switch (arg[start_number][0])
        {
            case "Msg":
                if (isDebug) Debug.Log("Msg");

                if (arg[start_number][1].Contains("On"))
                {
                    TextMessage.instance.Show(arg[start_number][2]);
                }
                else
                {
                    TextMessage.instance.Hidden();
                }
                start_number++;
                break;

            case "VideoRecord":
                if (isDebug) Debug.Log("VideoRecord");
#if !UNITY_EDITOR
					    if (arg [start_number] [1].Contains ("Start")) {
						    VideoRecordController.instance.StartRecord ();
					    } else {
						    VideoRecordController.instance.StopRecord ();
					    }
#endif

                start_number++;
                break;

            case "Wait":
                if (isDebug) Debug.Log("Wait");

                wait.setW(Convert.ToSingle(arg[start_number][1]));
                break;

            case "Branch":
                if (isDebug) Debug.Log("Branch");

                start_number++;
                break;

            case "Goto":
                if (isDebug) Debug.Log("Goto");

                if (Convert.ToSingle(arg[start_number][1]) != 0 && waitForTime[start_number] == true)
                {
                    wait.set(Convert.ToSingle(arg[start_number][1]));
                    waitForTime[start_number] = false;
                }
                else
                {
                    Goto(arg[start_number][2]);
                }
                break;

            case "Select_Event":
                if (isDebug) Debug.Log("Select_Event");

                selectE = true;
                break;

            case "SkullView":
                if (isDebug) Debug.Log("SkullView");

                if (Convert.ToSingle(arg[start_number][1]) != 0 && waitForTime[start_number] == true)
                {
                    wait.set(Convert.ToSingle(arg[start_number][1]));
                    waitForTime[start_number] = false;
                }
                else
                {
                    Controls.Instance.skullOn();
                    start_number++;
                }
                break;

            case "HoleView":
                if (isDebug) Debug.Log("HoleView");

                if (ivori_ExportCrain.allowOutFile)
                {
                    crain.stepBack.StepbackFromRezCurrentDir();
                    ivori_ExportCrain.CylinderOut();
                    isEnd = true;
                    break;
                }

                if (Convert.ToSingle(arg[start_number][1]) != 0 && waitForTime[start_number] == true)
                {
                    wait.set(Convert.ToSingle(arg[start_number][1]));
                    waitForTime[start_number] = false;
                }
                else
                {
                    Controls.Instance.holeOn();
                    start_number++;
                }
                break;

            case "HeraView":
                if (isDebug) Debug.Log("HeraView");

                if (Convert.ToSingle(arg[start_number][1]) != 0 && waitForTime[start_number] == true)
                {
                    wait.set(Convert.ToSingle(arg[start_number][1]));
                    waitForTime[start_number] = false;
                }
                else
                {
                    Controls.Instance.heraOn();
                    start_number++;
                }
                break;

            case "HeraCylinder":
                if (isDebug) Debug.Log("HeraCylinder");

                Vector3 refPos = new Vector3(Convert.ToSingle(arg[start_number][1]), Convert.ToSingle(arg[start_number][2]), Convert.ToSingle(arg[start_number][3]));
                Vector3 refEul = new Vector3(Convert.ToSingle(arg[start_number][4]), Convert.ToSingle(arg[start_number][5]), Convert.ToSingle(arg[start_number][6]));

                heraCompare.SetPosAt(2, refPos);
                heraCompare.SetRotAt(2, refPos);

                heraCompare.Compare();
                start_number++;
                break;

            case "HeraScriptMove":
                if (isDebug) Debug.Log("HeraScriptMove");

                dkHera_AutoPush.AMove move = new dkHera_AutoPush.AMove();
                move.moveType = (dkHera_AutoPush.eMoveType)Enum.Parse(typeof(dkHera_AutoPush.eMoveType), arg[start_number][1]);
                move.countPush = Convert.ToInt16(arg[start_number][2]);
                dkHeraAutoPush.moveList.Add(move);

                start_number++;
                break;

            case "dkHera":
                if (isDebug) Debug.Log("dkHera");

                Vector3 refScaDK = new Vector3(Convert.ToSingle(arg[start_number][1]), Convert.ToSingle(arg[start_number][2]), Convert.ToSingle(arg[start_number][3]));
                dkhera.localScale.applyScale = refScaDK;
                dkhera.localScale.ApplyScale();                

                dkHeraAutoPush.moveList = new List<dkHera_AutoPush.AMove>();

                dkHera_AutoPush.AMove moveDK = new dkHera_AutoPush.AMove();
                moveDK.moveType = dkHera_AutoPush.eMoveType.Linear;
                moveDK.countPush = Convert.ToInt16(arg[start_number][4]);
                dkHeraAutoPush.moveList.Add(moveDK);

                start_number++;
                break;

            case "LineSet":
                if (isDebug) Debug.Log("LineSet");

                if (Convert.ToSingle(arg[start_number][1]) != 0 && waitForTime[start_number] == true)
                {
                    wait.set(Convert.ToSingle(arg[start_number][1]));
                    waitForTime[start_number] = false;
                }
                else
                {
                    Vector3[] v = new Vector3[Convert.ToInt32(arg[start_number][2])];
                    for (int i = 0; i < Convert.ToInt32(arg[start_number][2]); i++)
                    {
                        v[i] = new Vector3(Convert.ToSingle(arg[start_number][3 * i + 3]), Convert.ToSingle(arg[start_number][3 * i + 4]), Convert.ToSingle(arg[start_number][3 * i + 5]));
                    }

                    cerebellumControl.Instance.lineset(Convert.ToInt32(arg[start_number][2]), v);
                    start_number++;
                }
                break;

            case "CamReset":
                if (isDebug) Debug.Log("CamReset");

                if (Convert.ToSingle(arg[start_number][1]) != 0 && waitForTime[start_number] == true)
                {
                    wait.set(Convert.ToSingle(arg[start_number][1]));
                    waitForTime[start_number] = false;
                }
                else
                {
                    DemoCreate.Instance.resteOrigin();
                    start_number++;
                }
                break;

            case "HoleSet":
                if (isDebug) Debug.Log("HoleSet");

                if (Convert.ToSingle(arg[start_number][1]) != 0 && waitForTime[start_number] == true)
                {
                    wait.set(Convert.ToSingle(arg[start_number][1]));
                    waitForTime[start_number] = false;
                }
                else
                {
                    crain.ClickHoleSetButton();
                    start_number++;
                }
                break;

            case "CerebellumTrans":
                if (isDebug) Debug.Log("CerebellumTrans");

                if (Convert.ToSingle(arg[start_number][1]) != 0 && waitForTime[start_number] == true)
                {
                    wait.set(Convert.ToSingle(arg[start_number][1]));
                    waitForTime[start_number] = false;
                }
                else
                {
                    Controls.Instance.transOn();
                    start_number++;
                }
                break;

            case "DeformBrain":
                //if (doneDeform)
                {
                    doneDeform = false;

                    if (isDebug) Debug.Log("DeformBrain");

                    if (Convert.ToSingle(arg[start_number][1]) != 0 && waitForTime[start_number] == true)
                    {
                        wait.set(Convert.ToSingle(arg[start_number][1]));
                        waitForTime[start_number] = false;
                    }
                    else
                    {
                        //deform siêu cũ
                        //meshDeform.allowDeform = false;
                        //meshDeform.lockDeform = false;

                        //meshDeform.AddDeformingForce (new Vector3 (-8.9f, -54.3f, -21.3f), 4500);
                        //meshDeform.AddDeformingForce (new Vector3 (-15.6f, -48.6f, -24.1f), 4500);
                        //meshDeform.AddDeformingForce (new Vector3 (-17.7f, -43.4f, -32.2f), 4500);
                        //meshDeform.AddDeformingForce (new Vector3 (-18.7f, -53.2f, -38.6f), 4500);
                        //meshDeform.AddDeformingForce (new Vector3 (-16.2f, -51.0f, -29.4f), 4500);

                        //deform cũ
                        //meshDeform.AutoDeform();

                        //thera.ivoryTranslateTemplate.Paste();
                        //AutoTranslate oldTranslate = thera.tHeraTranslate;
                        //oldTranslate.MoveWithinSteps(oldTranslate.beginPos, oldTranslate.beginRot, oldTranslate.endPos, oldTranslate.endRot, true);

                        //deform mới                            
                        heraDeform.btnAutoPush_OnClick();

                        start_number++;
                    }
                }

                break;

            case "PaintVertices":
                if (isDebug) Debug.Log("PaintVertices");

                if (Convert.ToSingle(arg[start_number][1]) != 0 && waitForTime[start_number] == true)
                {
                    wait.set(Convert.ToSingle(arg[start_number][1]));
                    waitForTime[start_number] = false;
                }
                else
                {
                    picaUp.AutoGo();                
                    start_number++;
                }
                break;

            case "AteryRun":
                if (isDebug) Debug.Log("AteryRun");

                if (Convert.ToSingle(arg[start_number][1]) != 0 && waitForTime[start_number] == true)
                {
                    wait.set(Convert.ToSingle(arg[start_number][1]));
                    waitForTime[start_number] = false;
                }
                else
                {                    
                    AteryRun(Convert.ToUInt16(arg[start_number][2]), Convert.ToSingle(arg[start_number][3]), Convert.ToSingle(arg[start_number][4]), Convert.ToSingle(arg[start_number][5]), Convert.ToSingle(arg[start_number][6]));
                    start_number++;
                }
                break;

            case "ToggleTransparentX":
                if (isDebug) Debug.Log("ToggleTransparentX");

                if (Convert.ToSingle(arg[start_number][1]) != 0 && waitForTime[start_number] == true)
                {
                    wait.set(Convert.ToSingle(arg[start_number][1]));
                    waitForTime[start_number] = false;
                }
                else
                {
                    Material mX = X.Instance.mf.GetComponent<MeshRenderer>().sharedMaterial;
                    Color mXColor = mX.color;
                    mXColor.a = mX.color.a == 1f ? 0f : 1f;
                    mX.color = mXColor;

                    start_number++;
                }
                break;

            case "ReleaseDeformBrain":
                //if (doneRelease)
                {
                    doneRelease = false;

                    if (isDebug) Debug.Log("ReleaseDeformBrain");

                    if (Convert.ToSingle(arg[start_number][1]) != 0 && waitForTime[start_number] == true)
                    {
                        wait.set(Convert.ToSingle(arg[start_number][1]));
                        waitForTime[start_number] = false;
                    }
                    else
                    {
                        //release cũ
                        //meshDeform.allowDeform = true;
                        //meshDeform.lockDeform = false;
                        //AutoTranslate oldTranslate = thera.tHeraTranslate;
                        //oldTranslate.MoveWithinSteps(oldTranslate.endPos, oldTranslate.endRot, oldTranslate.beginPos, oldTranslate.beginRot, false);

                        //release mới
                        dkHeraAutoPush.brainDKRig.tRig.UpdateMeshMethod();
                        dkHeraAutoPush.brainTransparent.Set100();

                        heraDeform.btnReset_OnClick();

                        start_number++;
                    }
                }

                break;

            case "HideVagus":
                if (isDebug) Debug.Log("HideVagus");

                if (Convert.ToSingle(arg[start_number][1]) != 0 && waitForTime[start_number] == true)
                {
                    wait.set(Convert.ToSingle(arg[start_number][1]));
                    waitForTime[start_number] = false;
                }
                else
                {
                    //HideVagus ();//test用
                    start_number++;
                }
                break;

            case "ArteryMove":
                if (isDebug) Debug.Log("ArteryMove");

                if (Convert.ToSingle(arg[start_number][1]) != 0 && waitForTime[start_number] == true)
                {
                    wait.set(Convert.ToSingle(arg[start_number][1]));
                    waitForTime[start_number] = false;
                }
                else
                {
                    //Need to do art move here
                    start_number++;
                }
                break;

            case "BloodVesselMove":
                if (isDebug) Debug.Log("BloodVesselMove");

                if (Convert.ToSingle(arg[start_number][1]) != 0 && waitForTime[start_number] == true)
                {
                    wait.set(Convert.ToSingle(arg[start_number][1]));
                    waitForTime[start_number] = false;
                }
                else
                {
                    StartCoroutine(BloodVesselMove());
                    start_number++;
                }
                break;

            case "Demo":
                if (isDebug) Debug.Log("Demo");

                if (Convert.ToSingle(arg[start_number][1]) != 0 && waitForTime[start_number] == true)
                {
                    wait.set(Convert.ToSingle(arg[start_number][7]));
                    waitForTime[start_number] = false;
                }
                else
                {
                    // DemoCreate.Instance.demomodeivori (
                    // Convert.ToSingle (arg [start_number] [1]),
                    // Convert.ToSingle (arg [start_number] [2]),
                    // Convert.ToSingle (arg [start_number] [3]),
                    // Convert.ToSingle (arg [start_number] [4]),
                    // Convert.ToSingle (arg [start_number] [5]),
                    // Convert.ToSingle (arg [start_number] [6]),
                    // 0,
                    // Convert.ToSingle (arg [start_number] [8]),
                    // Convert.ToSingle (arg [start_number] [9]));

                    if (isDebug) Debug.Log("Fly : " +
                        new Vector3(Convert.ToSingle(arg[start_number][1]),
                        Convert.ToSingle(arg[start_number][2]),
                        Convert.ToSingle(arg[start_number][3])).ToString() + ", " +
                        new Vector3(Convert.ToSingle(arg[start_number][4]),
                        Convert.ToSingle(arg[start_number][5]),
                        Convert.ToSingle(arg[start_number][6])).ToString());

                    camFly.Fly
                        (
                        new Vector3(Convert.ToSingle(arg[start_number][1]),
                        Convert.ToSingle(arg[start_number][2]),
                        Convert.ToSingle(arg[start_number][3])),

                        new Vector3(Convert.ToSingle(arg[start_number][4]),
                        Convert.ToSingle(arg[start_number][5]),
                        Convert.ToSingle(arg[start_number][6]))
                        );

                    start_number++;
                }
                break;

            case "CamFocusREZ":
                if (isDebug) Debug.Log("CamFocusREZ");

                float zoomFactor = Convert.ToSingle(arg[start_number][1]);
                float duration = Convert.ToSingle(arg[start_number][2]);

                cc.FocusREZPoint(zoomFactor, duration);
                start_number++;
                break;

            case "Cam":
                if (isDebug) Debug.Log("Cam");

                float posX = Convert.ToSingle(arg[start_number][4]);
                float posY = Convert.ToSingle(arg[start_number][5]);
                float posZ = Convert.ToSingle(arg[start_number][6]);

                float rotX = Convert.ToSingle(arg[start_number][7]);
                float rotY = Convert.ToSingle(arg[start_number][8]);
                float rotZ = Convert.ToSingle(arg[start_number][9]);

                //					float zoomFactor = Convert.ToSingle (arg [start_number] [1]);
                //					float duration = Convert.ToSingle (arg [start_number] [2]);

                if (arg[start_number][1] == ("Move"))
                    cc.Move(posX, posY, posZ, Convert.ToSingle(arg[start_number][2]), Convert.ToSingle(arg[start_number][3]));
                else if (arg[start_number][1] == ("MoveAndRotate"))
                {
                    cc.MoveAndRotate(posX, posY, posZ, rotX, rotY, rotZ, Convert.ToSingle(arg[start_number][2]), Convert.ToSingle(arg[start_number][3]));
                }

                start_number++;
                break;

            case "Equalizer_On":
                if (isDebug) Debug.Log("Equalizer_On");

                Equalizer_Active = true;
                start_number++;
                break;

            case "Equalizer_Off":
                if (isDebug) Debug.Log("Equalizer_Off");

                Equalizer_Active = false;
                start_number++;
                break;

            case "Control_On":
                if (isDebug) Debug.Log("Control_On");

                Move_Active = true;
                start_number++;
                break;

            case "Control_Off":
                if (isDebug) Debug.Log("Control_Off");

                Move_Active = false;
                start_number++;
                break;

            case "Reset":
                if (isDebug) Debug.Log("Reset");

                Controls.Instance.reset();
                start_number++;
                break;

            case "End":
                if (isDebug) Debug.Log("End");

                isEnd = true;
                UI_ivori.GetComponent<Text>().color = Color.white;
                break;

            default:
                if (isDebug) Debug.Log("default");

                Debug.LogError("System load error! Line:" + start_number);
                start_number++;
                break;
        }
    }

    public void iVoRi_Load()
    {
        arg = new string[1000][];
        OpenFileDialog openLog = new OpenFileDialog();
        openLog.InitialDirectory = UnityEngine.Application.dataPath + "/Resources";
        openLog.Filter = "List Files(*.csv;*.txt)|*.csv;*.txt|All files (*.*)|*.*";
        openLog.Title = "iVoRi System File Load";
        DialogResult result = openLog.ShowDialog();

        result = DialogResult.OK;
        if (result == DialogResult.OK)
        {
            Loadpath = openLog.FileName;
            string path = openLog.FileName;
            //  Debug.Log(path);
            theSourceFile = new FileInfo(path);
            reader = theSourceFile.OpenText();
            max_number = 0;
            max_branchnumber = 0;
            text = reader.ReadLine();

            while (text != null)
            {
                //  print("xx");
                oringinData[max_number] = text;
                queries = text.Split(',');
                //  Debug.Log("oringinData:" + oringinData[i]);
                //  Debug.Log(queries[0] + "_" + queries[1] + "_" + queries[2] + "_" + queries[3] + "_" + queries[4] + "_" + queries[5] + "_" + queries[6]);
                if (queries[0] == "Name")
                {

                }
                else if (queries[0] == "Branch")
                {
                    arg[max_number] = new string[10];
                    sysText[max_number] = text;
                    arg[max_number][0] = queries[0];
                    arg[max_number][1] = queries[1];
                    branchName[max_branchnumber] = queries[1];
                    branchLine[max_branchnumber] = max_number;
                    waitForTime[max_number] = true;
                    max_branchnumber++;
                    max_number++;
                }
                else
                {
                    sysText[max_number] = text;
                    int queries_number = 0;
                    arg[max_number] = new string[100];
                    while (queries_number < queries.Length)
                    {
                        arg[max_number][queries_number] = queries[queries_number];
                        queries_number++;
                    }

                    //    Debug.Log("XXX:" + arg[i][0] +"__"+ arg[i][1] + "__" + arg[i][2] + "__" + arg[i][3] + "__" + arg[i][4]);
                    //   Debug.Log(queries[0] + "_" + queries[1] + "_" + queries[2] + "_" + queries[3] + "_" + queries[4]);
                    waitForTime[max_number] = true;
                    max_number++;
                }

                text = reader.ReadLine();
            }
            reader.Close();
        }

        isLoad = true;
        isEnd = false;
        a = 0;
    }

    void OnGUI()
    {
        GUI.skin = GameSkin;

        if (isLoad == false && DebugMenu == true)
            GUI.Box(new Rect(10, UnityEngine.Screen.height - 255, 800, 155), "シナリオエンジン");

        if (isLoad == true && DebugMenu == true)
        {
            GUI.Box(new Rect(10, UnityEngine.Screen.height - 255, 800, 155), "シナリオエンジン コメントリスト　　E:編集 ， ESC:リセット");

            if (DebugInput == true)
            {
                stringToEdit = GUI.TextField(new Rect((UnityEngine.Screen.width - 700) / 2, (UnityEngine.Screen.height - 20) / 2, 650, 20), stringToEdit);

                if (GUI.Button(new Rect((UnityEngine.Screen.width - 700) / 2 + 660, (UnityEngine.Screen.height - 20) / 2, 40, 20), "OK"))
                {
                    queries = stringToEdit.Split(',');
                    sysText[start_number] = stringToEdit;
                    int queries_number = 0;
                    arg[start_number] = new string[100];
                    while (queries_number < queries.Length)
                    {
                        arg[start_number][queries_number] = queries[queries_number];
                        queries_number++;
                    }
                    waitForTime[start_number] = true;
                    DebugInput = false;
                    Debug.Log("Edit Line " + start_number + " to: " + stringToEdit);
                    savefile();
                }
            }

            GUI.Label(new Rect(47, UnityEngine.Screen.height - 225, 700, 30), gui[0]);
            GUI.Label(new Rect(47, UnityEngine.Screen.height - 207, 700, 30), gui[1]);
            GUI.Label(new Rect(47, UnityEngine.Screen.height - 185, 700, 30), gui[2], "sec");
            if (isEnd == false)
            {
                if (wait.isKeep == true)
                {
                    GUI.Label(new Rect(750, UnityEngine.Screen.height - 185, 50, 30), "Waiting", "sec");
                }
                else
                {
                    if (wait.timeError == -1)
                        GUI.Label(new Rect(750, UnityEngine.Screen.height - 185, 50, 30), "Click", "sec");
                    else
                        GUI.Label(new Rect(750, UnityEngine.Screen.height - 185, 50, 30), wait.timeError.ToString("#0.00"), "sec");
                }


            }

            GUI.Label(new Rect(47, UnityEngine.Screen.height - 163, 700, 30), gui[3]);
            GUI.Label(new Rect(47, UnityEngine.Screen.height - 145, 700, 30), gui[4]);
            GUI.Label(new Rect(47, UnityEngine.Screen.height - 127, 700, 30), gui[5]);

            GUI.Label(new Rect(17, UnityEngine.Screen.height - 225, 30, 30), guiNumber[0]);
            GUI.Label(new Rect(17, UnityEngine.Screen.height - 207, 30, 30), guiNumber[1]);
            GUI.Label(new Rect(17, UnityEngine.Screen.height - 185, 30, 30), guiNumber[2], "sec");
            GUI.Label(new Rect(17, UnityEngine.Screen.height - 163, 30, 30), guiNumber[3]);
            GUI.Label(new Rect(17, UnityEngine.Screen.height - 145, 30, 30), guiNumber[4]);
            GUI.Label(new Rect(17, UnityEngine.Screen.height - 127, 30, 30), guiNumber[5]);

            if (selectE == true)
            {
                GUILayout.BeginArea(new Rect(5, 200, 100, 600));
                foreach (string s in branchName)
                {

                    if (s != null)
                    {
                        if (GUILayout.Button(s))
                        {
                            if (start_number > branchLine[branchNumber(s)])
                            {
                                for (int i = branchLine[branchNumber(s)]; i < start_number; i++)
                                {
                                    waitForTime[i] = true;
                                }
                            }
                            start_number = branchLine[branchNumber(s)];
                            selectE = false;
                        }
                    }

                }

                if (GUILayout.Button("Cencel"))
                {
                    start_number++;
                    selectE = false;
                }

                GUILayout.EndArea();
            }
        }
        else
        {
            if (DebugMenu == true) GUI.Label(new Rect(17, UnityEngine.Screen.height - 205, 585, 80), "Press ' L ' to Load System File !", "load");
        }
    }

    static int branchNumber(string n)
    {
        int j = 0;
        foreach (string s in branchName)
        {
            if (s == n)
            {
                //  Debug.Log("a_" + j);
                return j;
            }
            j++;
        }
        Debug.Log("Branch is not found!");
        return -1;
    }

    IEnumerator Reset()
    {
        AsyncOperation async = UnityEngine.Application.LoadLevelAsync("TIN");
        Debug.Log("Loading complete");
        yield return async;
    }

    public void Goto(string n)
    {
        if (n == "Cancel" || branchNumber(n) == -1) start_number++;
        else
        {
            if (start_number > branchLine[branchNumber(n)])
            {
                for (int i = branchLine[branchNumber(n)]; i < start_number; i++) waitForTime[i] = true;
            }

            start_number = branchLine[branchNumber(n)];
        }
    }

    public void AutoLoadOn()
    {
        if (isEnd)
        {
            start_number = 0;
            UI_ivori.GetComponent<Text>().color = Color.black;
            isEnd = false;
            AutoLoad = true;
            btnManager.OniVori(false);
        }
        else
        {
            UI_ivori.GetComponent<Text>().color = Color.white;
            AutoLoad = false;
            isEnd = true;
            btnManager.OniVori(true);
        }
    }

    public void SwitchOnOffIvori()
    {
        if (AutoLoad)
        {
            VideoRecordController.instance.StopRecord();
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            //			AutoLoadOff ();
        }
        else
        {
            TimeScale.Scale1();
            AutoLoadOn();
        }
    }

    public void AutoLoadOff()
    {
        AutoLoad = false;
        isEnd = true;
    }

    /// <summary>
    /// Testのため、Ivoriが終わると、迷走神経が非表示、責任血管２（緑）を移動する
    /// </summary>
    public bool isStartTest = false;
    public bool isBloodVesselMove = false;
    public GameObject bloodVessel2;
    public GameObject vagus;

    void HideVagus()
    {
        if (vagus != null)
            vagus.SetActive(false);
    }

    IEnumerator BloodVesselMove()
    {
        isBloodVesselMove = true;
        yield return new WaitForSeconds(5f);
        isBloodVesselMove = false;
    }

    /// <summary>
    /// 編集したシナリオを上書き
    /// </summary>
    public void savefile()
    {
        string path = Loadpath;
        if (AutoLoad == true) path = "C:/Users/tomoki-pq/Desktop/AVJ/Assets/Resources/TGS_Demo17.csv";
        if (AutoLoad == false)
        {
            queries = Regex.Split(path, ".csv", RegexOptions.IgnoreCase);
            queries = Regex.Split(queries[0], ".txt", RegexOptions.IgnoreCase);
            queries = Regex.Split(queries[0], "_" + DateTime.Now.ToString("yyyyMMdd"), RegexOptions.IgnoreCase);
            path = queries[0] + "_" + DateTime.Now.ToString("yyyyMMdd") + ".csv";
        }

        Debug.Log(path);

        FileStream fs = new FileStream(path, FileMode.Create);
        StreamWriter sw = new StreamWriter(fs);
        for (int i = 0; i < max_number; i++) sw.WriteLine(sysText[i]);

        sw.Flush();
        sw.Close();
        fs.Close();
    }

    #region Specific Ivori Command

    [Header("Params")]
    public AteryRun ateryRun = null;

    [UnityEngine.ContextMenu("AteryRun")]
    public void AteryRun(int childID, float distance, float xPos, float yPos, float zPos)
    {
        ateryRun.RunAt(childID, distance, xPos, yPos, zPos);
    }    
    #endregion
}
