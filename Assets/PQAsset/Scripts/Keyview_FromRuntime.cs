﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Keyview_FromRuntime : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Params")]
    public CamPivot_Keyview keyview = null;
    public Keyview_Method keyviewMethod = null;
    public Keyview_AutoStepback keyviewAutoStepback = null;
    public Keyview_Possible possible = null;

    [ContextMenu("KeyviewFromRuntime")]
    public void KeyviewFromRuntime()
    {           
        keyviewMethod.typeKeyview = Keyview_Method.TypeKeyview.Crain;
        keyviewMethod.CurrentSet();
        keyviewMethod.Init();
        keyviewMethod.State0();
        keyviewAutoStepback.AutoStepback_FixedTurn();
    }
}
