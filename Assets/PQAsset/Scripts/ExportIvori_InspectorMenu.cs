﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class ExportIvori_InspectorMenu : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Inputs")]
    public ExportIvori exportIvori = null;

    [ContextMenu("ReadExisted")]
    public void ReadExisted()
    {
        exportIvori.ReadFromIvori();
    }

    [ContextMenu("ReadCase")]
    public void ReadCase()
    {
        exportIvori.ReadFromCase();
    }

    [ContextMenu("Export")]
    public void Export()
    {
        exportIvori.Export();
    }

    [ContextMenu("SaveLine")]
    public void SaveLine()
    {
        exportIvori.GetLineSet();
    }

    [ContextMenu("SetLine")]
    public void SetLine()
    {
        exportIvori.SetLineSet();
    }

    [ContextMenu("ResetLine")]
    public void ResetLine()
    {
        exportIvori.ReSetLineSet();
    }

    [ContextMenu("ToCam0")]
    public void ToCam0()
    {
        exportIvori.ToCam0();
    }

    [ContextMenu("ToCam1")]
    public void ToCam1()
    {
        exportIvori.ToCam1();
    }

    [ContextMenu("ToCam2")]
    public void ToCam2()
    {
        exportIvori.ToCam2();
    }

    [ContextMenu("SaveCam0")]
    public void SaveCam0()
    {
        exportIvori.SaveCam0();
    }

    [ContextMenu("SaveCam1")]
    public void SaveCam1()
    {
        exportIvori.SaveCam1();
    }

    [ContextMenu("SaveCam2")]
    public void SaveCam2()
    {
        exportIvori.SaveCam2();
    }

    [ContextMenu("GetHera")]
    public void GetHera()
    {
        exportIvori.GetHera();
    }

    [ContextMenu("SaveHera")]
    public void SaveHera()
    {
        exportIvori.SaveHera();
    }

    [ContextMenu("GetDKHera")]
    public void GetDKHera()
    {
        exportIvori.GetDKHera();
    }

    [ContextMenu("SaveDKHera")]
    public void SaveDKHera()
    {
        exportIvori.SaveDKHera();
    }
}
