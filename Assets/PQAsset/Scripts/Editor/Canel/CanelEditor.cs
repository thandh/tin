﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Canel))]
public class CanelEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        Canel myScript = (Canel)target;

        if (GUILayout.Button("Stepback"))
        {
            myScript.Stepback();
        }
    }
}