﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class TINIvoriWindow : EditorWindow
{
    [MenuItem("TIN/Ivori %#_i")]

    public static void ShowWindow()
    {
        EditorWindow window = EditorWindow.GetWindow(typeof(TINIvoriWindow));
        window.maxSize = new Vector2(200f, 300f);
        //window.minSize = new Vector2(200f, 300f);
    }

    bool showBtn = true;
    Vector2 scrollPos = Vector2.zero;

    void OnGUI()
    {
       scrollPos = EditorGUILayout.BeginScrollView(scrollPos, false, false);

       showBtn = EditorGUILayout.Toggle("OnOff to help Run", showBtn);

        GUILayout.Label("Read From CSV");

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Existed"))
        {
            ExportIvori_InspectorMenu exportIvori = FindObjectOfType<ExportIvori_InspectorMenu>();
            exportIvori.ReadExisted();
        }

        if (GUILayout.Button("Case"))
        {
            ExportIvori_InspectorMenu exportIvori = FindObjectOfType<ExportIvori_InspectorMenu>();
            exportIvori.ReadCase();
        }

        if (GUILayout.Button("Export"))
        {
            ExportIvori_InspectorMenu exportIvori = FindObjectOfType<ExportIvori_InspectorMenu>();
            exportIvori.Export();
        }
        GUILayout.EndHorizontal();

        GUILayout.Label("Line");

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Save"))
        {
            ExportIvori_InspectorMenu exportIvori = FindObjectOfType<ExportIvori_InspectorMenu>();
            exportIvori.SaveLine();
        }

        if (GUILayout.Button("Set"))
        {
            ExportIvori_InspectorMenu exportIvori = FindObjectOfType<ExportIvori_InspectorMenu>();
            exportIvori.SetLine();
        }

        if (GUILayout.Button("Reset"))
        {
            ExportIvori_InspectorMenu exportIvori = FindObjectOfType<ExportIvori_InspectorMenu>();
            exportIvori.ResetLine();
        }
        GUILayout.EndHorizontal();

        GUILayout.Label("Cam");
        GUILayout.Label("To");

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Cam0"))
        {
            ExportIvori_InspectorMenu exportIvori = FindObjectOfType<ExportIvori_InspectorMenu>();
            exportIvori.ToCam0();
        }

        if (GUILayout.Button("Cam1"))
        {
            ExportIvori_InspectorMenu exportIvori = FindObjectOfType<ExportIvori_InspectorMenu>();
            exportIvori.ToCam1();
        }

        if (GUILayout.Button("Cam2"))
        {
            ExportIvori_InspectorMenu exportIvori = FindObjectOfType<ExportIvori_InspectorMenu>();
            exportIvori.ToCam2();
        }
        GUILayout.EndHorizontal();

        GUILayout.Label("Save");

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Cam0"))
        {
            ExportIvori_InspectorMenu exportIvori = FindObjectOfType<ExportIvori_InspectorMenu>();
            exportIvori.SaveCam0();
        }

        if (GUILayout.Button("Cam1"))
        {
            ExportIvori_InspectorMenu exportIvori = FindObjectOfType<ExportIvori_InspectorMenu>();
            exportIvori.SaveCam1();
        }

        if (GUILayout.Button("Cam2"))
        {
            ExportIvori_InspectorMenu exportIvori = FindObjectOfType<ExportIvori_InspectorMenu>();
            exportIvori.SaveCam2();
        }
        GUILayout.EndHorizontal();

        GUILayout.Label("Hera");

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Get"))
        {
            ExportIvori_InspectorMenu exportIvori = FindObjectOfType<ExportIvori_InspectorMenu>();
            exportIvori.GetHera();
        }

        if (GUILayout.Button("Save"))
        {
            ExportIvori_InspectorMenu exportIvori = FindObjectOfType<ExportIvori_InspectorMenu>();
            exportIvori.SaveHera();
        }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("GetDK"))
        {
            ExportIvori_InspectorMenu exportIvori = FindObjectOfType<ExportIvori_InspectorMenu>();
            exportIvori.GetDKHera();
        }

        if (GUILayout.Button("SaveDK"))
        {
            ExportIvori_InspectorMenu exportIvori = FindObjectOfType<ExportIvori_InspectorMenu>();
            exportIvori.SaveDKHera();
        }
        GUILayout.EndHorizontal();

        GUILayout.Label("Cylinder Reflection");

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Reset"))
        {
            tCraniotomy.Instance.crain.localPrefab.ApplyPosition();
            tCraniotomy.Instance.crain.localPrefab.ApplyRotation_byEuler();
        }

        if (GUILayout.Button("Get"))
        {
            ExportIvori exportIvori = FindObjectOfType<ExportIvori>();
            exportIvori.GetTempoCylinder();
        }        
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Save"))
        {
            ExportIvori exportIvori = FindObjectOfType<ExportIvori>();
            exportIvori.SaveTempoCylinder();
        }

        if (GUILayout.Button("Export"))
        {
            Ivori_ExportCylinder exportIvori = FindObjectOfType<Ivori_ExportCylinder>();
            exportIvori.CylinderOut();
        }

        if (GUILayout.Button("CheckImport"))
        {
            Ivori_ExportCylinder exportIvori = FindObjectOfType<Ivori_ExportCylinder>();
            exportIvori.CheckImport();
        }
        GUILayout.EndHorizontal();

        GUILayout.EndScrollView();
    }
}
