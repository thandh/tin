﻿//using UnityEngine;
//using System.Collections;
//using UnityEditor;

//[CustomEditor(typeof(Ivori_ExportCylinder))]
//public class Ivori_ExportCylinderEditor : Editor
//{
//    [MenuItem("TIN/Camera Keyview/Manual %#_k")]

//    public override void OnInspectorGUI()
//    {
//        DrawDefaultInspector();
//        Ivori_ExportCylinder myScript = (Ivori_ExportCylinder)target;        

//        GUILayout.BeginHorizontal("");
//        if (GUILayout.Button("Previous")) myScript.ViewPrevious();
//        if (GUILayout.Button("Next")) myScript.ViewNext();
//        if (GUILayout.Button("Output")) myScript.Output();
//        GUILayout.EndHorizontal();
//    }
//}