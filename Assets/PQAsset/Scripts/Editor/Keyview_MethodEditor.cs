﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Keyview_Method))]
public class Keyview_MethodEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        Keyview_Method myScript = (Keyview_Method)target;

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("CurrentSet")) { myScript.CurrentSet(); }
        if (GUILayout.Button("Init")) { myScript.Init(); }
        if (GUILayout.Button("State0")) { myScript.State0(); }
        if (GUILayout.Button("StepBack")) myScript.Stepback();        
        GUILayout.EndHorizontal();
    }
}