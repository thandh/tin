﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(CamPivot_Keyview))]
public class CamPivot_KeyviewEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        CamPivot_Keyview myScript = (CamPivot_Keyview)target;

        GUILayout.BeginHorizontal();
        GUILayout.Label("Use center");
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("State0")) { myScript.InitParams(); myScript.State0(); }
        if (GUILayout.Button("StepBack")) myScript.Stepback();        
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Use crain");
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("State0")) { myScript.InitParams_UseCrain(); myScript.State0_UseCrain(); }
        if (GUILayout.Button("StepBack")) myScript.Stepback_UseCrain();        
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Use crain Editor");
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("State0")) { myScript.InitParams_UseCrainFile(); myScript.State0_UseCrainEditor(); }
        if (GUILayout.Button("StepBack")) myScript.Stepback_UseCrainEditor();        
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label("Output");
        GUILayout.EndHorizontal();

        if (GUILayout.Button("SaveState0")) myScript.SaveState0();
        if (GUILayout.Button("SaveKeyView")) myScript.SaveKeyView();
    }
}