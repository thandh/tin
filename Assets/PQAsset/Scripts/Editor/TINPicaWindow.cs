﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class TINPicaWindow : EditorWindow
{
    [MenuItem("TIN/PicaPanel %#_p")]

    public static void ShowWindow()
    {
        EditorWindow window = EditorWindow.GetWindow(typeof(TINPicaWindow));
        window.maxSize = new Vector2(200f, 300f);
        //window.minSize = new Vector2(200f, 300f);
    }
    
    Vector2 scrollPos = Vector2.zero;

    void OnGUI()
    {
        scrollPos = EditorGUILayout.BeginScrollView(scrollPos, false, false);

        GUILayout.Label("Rig");
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("FullAuto"))
        {
            Pica_AutoRig auto = FindObjectOfType<Pica_AutoRig>();
            auto.FullAuto();
        }

        if (GUILayout.Button("Distribute"))
        {
            Pica_AutoRig auto = FindObjectOfType<Pica_AutoRig>();
            auto.distribute.Distribute();
        }

        if (GUILayout.Button("Rig"))
        {
            Pica_AutoRig auto = FindObjectOfType<Pica_AutoRig>();
            auto.Rig();
        }
        GUILayout.EndHorizontal();

        GUILayout.Label("Scip");
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Save"))
        {
            Pica_AutoRig auto = FindObjectOfType<Pica_AutoRig>();
            auto.SaveScrip();
        }

        if (GUILayout.Button("Load"))
        {
            Pica_AutoRig auto = FindObjectOfType<Pica_AutoRig>();
            auto.LoadScrip();
        }
        GUILayout.EndHorizontal();

        GUILayout.Label("Spread Vert");
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Save"))
        {
            Pica_AutoRig auto = FindObjectOfType<Pica_AutoRig>();
            auto.SaveScrip();
        }

        if (GUILayout.Button("Load"))
        {
            Pica_AutoRig auto = FindObjectOfType<Pica_AutoRig>();
            auto.LoadScrip();
        }
        GUILayout.EndHorizontal();

        GUILayout.EndScrollView();
    }
}
