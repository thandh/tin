﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class TINCameraViewWindow : EditorWindow
{
    [MenuItem("TIN/Setup Camera/Manual %#_k")]

    public static void ShowWindow()
    {
        EditorWindow window = EditorWindow.GetWindow(typeof(TINCameraViewWindow));
        window.maxSize = new Vector2(200f, 300f);
        //window.minSize = new Vector2(200f, 300f);
    }

    bool showBtn = true;
    Vector2 scrollPos = Vector2.zero;

    void OnGUI()
    {
        scrollPos = EditorGUILayout.BeginScrollView(scrollPos, false, false);

        showBtn = EditorGUILayout.Toggle("OnOff to help Run", showBtn);

        if (GUILayout.Button("General View"))
        {
            Debug.Log("General View");
            HeadInit_CamState0 camState0Init = FindObjectOfType<HeadInit_CamState0>();
            camState0Init.DoInit();
        }

        GUILayout.Label("Keyview from Ivori");

        GUILayout.BeginHorizontal();        
        if (GUILayout.Button("Check Import"))
        {
            Debug.Log("Check Import");
            Ivori_ExportCylinder ivoriExport = FindObjectOfType<Ivori_ExportCylinder>();
            ivoriExport.CheckImport();
        }

        if (GUILayout.Button("Extract"))
        {
            Debug.Log("Extract Ivori");
            Ivori_ExportCylinder ivoriExport = FindObjectOfType<Ivori_ExportCylinder>();
            if (ivoriExport.hasOutput)
            {
                Debug.Log("Has data already!! Must delete!!");
            }
            else
            {
                ivoriExport.GetCylinder();
            }
        }

        if (GUILayout.Button("Keying!"))
        {
            Debug.Log("Keyview from Ivori");
            Ivori_ExportCylinder ivoriExport = FindObjectOfType<Ivori_ExportCylinder>();
            ivoriExport.KeyviewFromIvori();
            ivoriExport.allowOutFile = false;
        }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Step Forward"))
        {
            Keyview_Method method = FindObjectOfType<Keyview_Method>();
            method.Stepforward();
        }
        if (GUILayout.Button("Step Back"))
        {
            Keyview_Method method = FindObjectOfType<Keyview_Method>();
            method.Stepback();
        }
        GUILayout.EndHorizontal();

        GUILayout.Label("Campivot Keyview");

        GUILayout.Label("Crain Editor");

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Keyview0"))
        {
            Keyview_Method method = FindObjectOfType<Keyview_Method>();
            method.State0();
        }

        if (GUILayout.Button("Auto"))
        {
            Keyview_AutoStepback method = FindObjectOfType<Keyview_AutoStepback>();
            method.AutoStepback_FixedTurn();            
        }
        GUILayout.EndHorizontal();

        GUILayout.Label("Output");

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("SaveState0"))
        {
            CamPivot_Keyview method = FindObjectOfType<CamPivot_Keyview>();
            method.SaveState0();
            Keyview_Possible possible = FindObjectOfType<Keyview_Possible>();
            possible.Output();
        }

        if (GUILayout.Button("SaveKeyview"))
        {
            CamPivot_Keyview method = FindObjectOfType<CamPivot_Keyview>();
            method.SaveKeyView();
            Keyview_Possible possible = FindObjectOfType<Keyview_Possible>();
            possible.Output();
        }
        GUILayout.EndHorizontal();

        GUILayout.EndScrollView();
    }
}
