﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class TINTimeScaleWindow : EditorWindow
{
    [MenuItem("TIN/TimeScale %#_t")]

    public static void ShowWindow()
    {
        EditorWindow window = EditorWindow.GetWindow(typeof(TINTimeScaleWindow));
        window.maxSize = new Vector2(200f, 300f);
        window.minSize = new Vector2(10f, 10f);
    }
    
    Vector2 scrollPos = Vector2.zero;

    void OnGUI()
    {
        scrollPos = EditorGUILayout.BeginScrollView(scrollPos, false, false);

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("0.1"))
        {
            TimeScale time = FindObjectOfType<TimeScale>();
            time.vScale0();
        }

        if (GUILayout.Button("1"))
        {
            TimeScale time = FindObjectOfType<TimeScale>();
            time.vScale1();
        }
        GUILayout.EndHorizontal();        

        GUILayout.EndScrollView();
    }
}
