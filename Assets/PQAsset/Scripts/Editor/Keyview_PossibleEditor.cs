﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Keyview_Possible))]
public class Keyview_PossibleEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        Keyview_Possible myScript = (Keyview_Possible)target;        

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Previous")) myScript.ViewPrevious();
        if (GUILayout.Button("Next")) myScript.ViewNext();
        if (GUILayout.Button("Output")) myScript.Output();
        GUILayout.EndHorizontal();
    }
}