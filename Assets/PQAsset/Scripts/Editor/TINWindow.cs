﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class TINWindow : EditorWindow
{
    [MenuItem("TIN/ControlPanel %#_y")]

    public static void ShowWindow()
    {
        EditorWindow window = EditorWindow.GetWindow(typeof(TINWindow));
        window.maxSize = new Vector2(200f, 300f);
        //window.minSize = new Vector2(200f, 300f);
    }

    bool showBtn = true;
    Vector2 scrollPos = Vector2.zero;

    void OnGUI()
    {
        scrollPos = EditorGUILayout.BeginScrollView(scrollPos, false, false);

        showBtn = EditorGUILayout.Toggle("OnOff to help Run", showBtn);
                
        GUILayout.Label("Skull/Cylinder");

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("OnOffMesh"))
        {
            skull.Instance.MeshOnOff();
        }

        if (GUILayout.Button("OnOffMesh"))
        {
            Craniotomy.Instance.shapeJob.ToggleShowShape();
        }
        GUILayout.EndHorizontal();
        GUILayout.Label("X");

        GUILayout.BeginHorizontal();       
        if (GUILayout.Button("FullAuto"))
        {
            X.Instance.auto.FullAuto();
        }

        if (GUILayout.Button("ResetMesh"))
        {
            X.Instance.auto.ResetMesh();
        }

        if (GUILayout.Button("Stop"))
        {
            X.Instance.auto.Stop();
        }
        GUILayout.EndHorizontal();

        GUILayout.Label("X _ Snap Has 2");

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Step Bit"))
        {
            X.Instance.auto.xPointControl.snap2.RotateStepBit();
        }

        if (GUILayout.Button("Step Nbit"))
        {
            X.Instance.auto.xPointControl.snap2.RotateNStepBit();
        }
        GUILayout.EndHorizontal();

        GUILayout.Label("Brain");

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("OnOffMesh"))
        {
            List<ShapeJob> shapeList = tCerebellum.Instance.GetComponentsInChildren<ShapeJob>().ToList();
            foreach (ShapeJob sj in shapeList) sj.ToggleShowShape();
        }        
        GUILayout.EndHorizontal();

        GUILayout.Label("Brain dkNodes");

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Auto Setup"))
        {
            tRig_autoCreate_Brain brain = FindObjectOfType<tRig_autoCreate_Brain>();
            brain.FullAuto();
        }

        if (GUILayout.Button("DeleteAll"))
        {
            dkNodes nodes = FindObjectOfType<dkNodes>();
            nodes.DeleteAll();
        }

        if (GUILayout.Button("UpdateMesh"))
        {
            tRig_autoCreate_Brain brain = FindObjectOfType<tRig_autoCreate_Brain>();
            brain.tRig.UpdateMeshMethod();
        }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("ResetBrain"))
        {
            BrainResetMesh brain = FindObjectOfType<BrainResetMesh>();
            brain.BrainReset();
        }
        GUILayout.EndHorizontal();

        GUILayout.Label("Brain dkNodes _ Shape");

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Get"))
        {
            dkNodes_ShapesJob dknode_shapeJob = FindObjectOfType<dkNodes_ShapesJob>();
            dknode_shapeJob.GetList();
        }

        if (GUILayout.Button("Show"))
        {
            dkNodes_ShapesJob dknode_shapeJob = FindObjectOfType<dkNodes_ShapesJob>();
            dknode_shapeJob.ShowShape();
        }

        if (GUILayout.Button("Toggle"))
        {
            dkNodes_ShapesJob dknode_shapeJob = FindObjectOfType<dkNodes_ShapesJob>();
            dknode_shapeJob.ToggleShowShape();
        }
        GUILayout.EndHorizontal();

        GUILayout.Label("Pica");

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("ShowOn"))
        {
            Pica_AutoSetup autoPica = FindObjectOfType<Pica_AutoSetup>();
            autoPica.UpdateShape();
            autoPica.ShowOnShape();
        }

        if (GUILayout.Button("Off"))
        {
            Pica_AutoSetup autoPica = FindObjectOfType<Pica_AutoSetup>();
            autoPica.ShowOffShape();
        }        
        GUILayout.EndHorizontal();

        GUILayout.EndScrollView();
    }
}
