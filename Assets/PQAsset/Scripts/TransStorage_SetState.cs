﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransStorage_SetState : MonoBehaviour
{
    [Header("Input")]
    public TransStorage storage = null;

    [Header("Process")]
    public List<int> idStorageList = new List<int>();

    [ContextMenu("SetStorageFollowID")]
    public void SetStorageFollowID()
    {
        for (int i = 0; i < idStorageList.Count; i++)
        {
            storage.SetState(idStorageList[i]);
        }
    }
}
