﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangerSceneMgr : Singleton<ChangerSceneMgr> {

	[SerializeField]
	public bool isModel_A = false;
	[SerializeField]
	public bool isModel_B = false;

	void Awake() {
		DontDestroyOnLoad(transform.gameObject);
	}


	public void ClickStart()
	{
		SceneManager.LoadScene ("SelectModel");
	}

	public void ClickModelA()
	{
		SceneManager.LoadScene ("Model_A");
	}

	public void ClickModelB()
	{
		SceneManager.LoadScene ("Model_B");
	}

	public void ClickReturn()
	{

		SceneManager.LoadScene ("SelectModel");
		isModel_A = false;
		isModel_B = false;
	}
}
