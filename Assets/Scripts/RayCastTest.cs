﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class RayCastTest : MonoBehaviour
{
	List<int> listIdBeCut = new List<int> ();
	public GameObject target;

	// Use this for initialization
	void Start ()
	{
		CircleRay (transform.position, 2f, segment, angle);
		CircleRay (transform.position, 1.5f, segment, angle);
		CircleRay (transform.position, 1f, segment, angle);
		CircleRay (transform.position, 0.5f, segment, angle);

		loopCut (target);
	}
	
	// Update is called once per frame
	void Update ()
	{
		Debug.DrawLine (transform.position, transform.forward * -length + transform.position, Color.green);
//		Debug.DrawLine (hole.transform.position, hole.transform.up * -50 + hole.transform.position, Color.yellow);
	}

	public float length = 1;
	int numOfPoints = 10;

	void OnDrawGizmos ()
	{
		Gizmos.DrawLine (transform.position, transform.up * -length + transform.position);

//		float angleStep = 360.0f / (float)numOfPoints;
//
//		Quaternion quaternion = Quaternion.Euler (0.0f, 0.0f, angleStep);
//		for (int i = 0; i < numOfPoints - 1; i++) {
//			GameObject a = new GameObject ("a");
//			a.transform.position = quaternion * transform.position;
//		}

		Circle (transform.position, 2f, segment, angle);
		Circle (transform.position, 1.5f, segment, angle);
		Circle (transform.position, 1f, segment, angle);
		Circle (transform.position, 0.5f, segment, angle);
	}

	public int angle = 0;
	public int segment = 10;
	public float radius = 3;

	void Circle (Vector3 center, float radius, int segments, float angle)
	{
		float xo = 0;
		float yo = 0;
		float xn = 0;
		float yn = 0;

		for (int i = 0; i < (segments + 1); i++) {
			xn = Mathf.Sin (Mathf.Deg2Rad * angle) * radius;
			yn = Mathf.Cos (Mathf.Deg2Rad * angle) * radius;

			if (i > 0)
				Gizmos.DrawLine (new Vector3 (center.x + xn, center.y + yn, center.z), new Vector3 (center.x + xo, center.y + yo, center.z));

			xo = xn;
			yo = yn;

			Vector3 newV = new Vector3 (xo, yo, 0);
			angle += (360f / segments);

			Debug.DrawLine (newV + transform.position, transform.up * -length + newV + transform.position, Color.green);

		}
	}

	void CircleRay (Vector3 center, float radius, int segments, float angle)
	{
		float xo = 0;
		float yo = 0;
		float xn = 0;
		float yn = 0;

		for (int i = 0; i < (segments + 1); i++) {
			xn = Mathf.Sin (Mathf.Deg2Rad * angle) * radius;
			yn = Mathf.Cos (Mathf.Deg2Rad * angle) * radius;

//			if (i > 0) {
//				Gizmos.DrawLine (new Vector3 (center.x + xn, center.y + yn, center.z), new Vector3 (center.x + xo, center.y + yo, center.z));
//			}
			xo = xn;
			yo = yn;

			Vector3 newV = new Vector3 (xo, yo, 0);
			angle += (360f / segments);

//			Debug.DrawLine (newV + transform.position, transform.up * -length + newV + transform.position, Color.green);
			Ray ray = new Ray (newV + transform.position, transform.up * -length + newV + transform.position);

			RaycastHit hit;

			if (Physics.Raycast (ray, out hit, length)) {
				Debug.Log (hit.transform.name + ", " + hit.triangleIndex);

//				if (hit.transform != this.transform)
//					continue;
				if (!listIdBeCut.Contains (hit.triangleIndex))
					listIdBeCut.Add (hit.triangleIndex);
			}

		}
	}


	void loopCut (GameObject go)
	{
		Destroy (go.GetComponent<MeshCollider> ());

		Mesh mesh = go.GetComponent<MeshFilter> ().mesh;
		List<int> oldTriangles = mesh.triangles.ToList ();

		Debug.Log ("listIdBeCut: " + listIdBeCut.Count);
		Debug.Log ("oldTriangles: " + oldTriangles.Count);
		listIdBeCut.Sort ();

		for (int i = listIdBeCut.Count - 1; i >= 0; i--) {
			if (oldTriangles.Count <= listIdBeCut [i] * 3)
				continue;
			oldTriangles.RemoveAt (listIdBeCut [i] * 3);
			oldTriangles.RemoveAt (listIdBeCut [i] * 3);
			oldTriangles.RemoveAt (listIdBeCut [i] * 3);

		}
		listIdBeCut = new List<int> ();

		go.GetComponent<MeshFilter> ().mesh.triangles = oldTriangles.ToArray ();
		go.AddComponent<MeshCollider> ();
	}
}
