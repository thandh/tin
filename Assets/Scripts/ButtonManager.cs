﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonManager : MonoBehaviour
{
	[Space ()]
	public Button btnSkull;
	public Button btnHole;
	public Button btnHera;

	[Space ()]
	public Button btnDraw;
	public Button btnHoleset;
	public Button btnTrans;
	public Button btnArtMove;

	[Space ()]
	public Button btnCamFirst;
	public Button btnCamNext;

	[Space ()]
	public Toggle btnCut;
	// Use this for initialization
	void Start ()
	{
		btnSkull.GetComponent<SwitchImageButton> ().SwitchImage ();	
	}

	public void OniVori (bool value)
	{
		btnSkull.interactable = value;
		btnHole.interactable = value;
		btnHera.interactable = value;

		btnDraw.interactable = value;
		btnHoleset.interactable = value;
		btnTrans.interactable = value;
		btnArtMove.interactable = value;

		btnCamFirst.interactable = value;
		btnCamNext.interactable = value;

		btnCut.interactable = value;
	}
}
