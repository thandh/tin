﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RockVR.Video;
using System.IO;
using UnityEngine.UI;

public class VideoRecordController : MonoBehaviour
{
    [Header("Debug")]
    public bool isDebug = false;

	static VideoRecordController _instance;

	public static VideoRecordController instance{ get { return _instance; } }

    [Header("Params")]
    public GameObject canvasView;

	// Use this for initialization
	void Awake ()
	{
		_instance = this;
		Screen.SetResolution (1920, 1080, false);
	}

	void Start ()
	{
		if (isDebug) Debug.Log (Directory.GetCurrentDirectory ());
		string path = Path.Combine (Directory.GetCurrentDirectory (), "Videos/");
		SetSavePath (path);
        if (isDebug) Debug.Log ("Set SavePath " + path);
	}

	void SetSavePath (string newPath)
	{
		PathConfig.SaveFolder = newPath;
		if (!Directory.Exists (newPath)) Directory.CreateDirectory (newPath);
	}

	public void StartRecord ()
	{
        if (isDebug) Debug.Log ("VideoRecord Start");
		if (canvasView) canvasView.SetActive (true);		
		VideoCaptureCtrl.instance.StartCapture ();
	}

	public void StopRecord ()
	{
        if (isDebug) Debug.Log ("VideoRecord Stop");
        if (isDebug) Debug.Log ("SaveFolder " + PathConfig.SaveFolder);

		if (canvasView) canvasView.SetActive (false);
		
		VideoCaptureCtrl.instance.StopCapture ();
	}
}
