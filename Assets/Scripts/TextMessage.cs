﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextMessage : MonoBehaviour
{

	static TextMessage _instance;

	public static TextMessage instance{ get { return _instance; } }

	public GameObject msgObjecg;
	public Text content;

	// Use this for initialization
	void Awake ()
	{
		_instance = this;
	}

	public void Show (string msg)
	{
		msgObjecg.SetActive (true);
		content.text = msg;
	}

	public void Hidden ()
	{
		msgObjecg.SetActive (false);
	}

	IEnumerator IEShow (string msg, float delayTime)
	{
		content.text = msg;
		yield return new WaitForSeconds (delayTime);
		msgObjecg.SetActive (false);
	}
}
