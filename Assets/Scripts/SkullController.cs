﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SkullController : Singleton<SkullController>
{
    [Header("Debug")]
    public bool isDebug = false;

    [Header("Params")]
    public bool isCanCut = false;

    [ContextMenu("SwitchCutMode")]
    public void SwitchCutMode()
    {
        isCanCut = !isCanCut;
        UndoPanel.Instance.OnOff(isCanCut);
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                if (isDebug)
                    Debug.Log("Clicked on the UI " + EventSystem.current.currentSelectedGameObject);

                if (isCanCut)
                    CreateCircleRaycast(Input.mousePosition);
            }
        }
    }

    public float radius = 30;
    float distanceRaycast = 0.5f;

    List<Vector3> listDrawPos = new List<Vector3>();
    Vector3 direction;

    void CreateCircleRaycast(Vector3 center)
    {
        RaycastHit hit;

        List<CutHole> lstCutHole = new List<CutHole>();

        for (float i = 0; i < radius / 2; i += distanceRaycast)
        {
            for (float j = 0; j < 360; j += distanceRaycast)
            {
                float x = i * Mathf.Sin(j * Mathf.Deg2Rad) + center.x;

                float y = i * Mathf.Cos(j * Mathf.Deg2Rad) + center.y;

                Vector3 vec = new Vector3(x, y, 0);

                listDrawPos.Add(vec);

                Ray ray = Camera.main.ScreenPointToRay(vec);

                if (Physics.Raycast(ray, out hit, 1000.0f))
                {
                    CutHole cuthole = hit.collider.gameObject.GetComponent<CutHole>();
                    if (cuthole)
                    {
                        cuthole.SetIDCut(hit.triangleIndex);
                        cuthole.lastHole.position = hit.point;
                        if (!lstCutHole.Contains(cuthole))
                            lstCutHole.Add(cuthole);
                    }
                }
            }
        }

        Debug.Log("listCut.Count " + lstCutHole.Count);

        if (lstCutHole.Count > 0)
        {
            UndoManager.Instance.Register(lstCutHole[0]);
            lstCutHole[0].UndoRegister();

            lstCutHole[0].enabled = true;
            lstCutHole[0].loopCut(true);
        }

        for (int i = 1; i < lstCutHole.Count; i++)
        {
            UndoManager.Instance.RegisterMember(lstCutHole[i]);
            lstCutHole[i].UndoRegister();

            lstCutHole[i].enabled = true;
            lstCutHole[i].loopCut(true);
        }

        lstCutHole.Clear();
        UndoPanel.Instance.Check();
    }
}
