﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CameraController : MonoBehaviour
{
	public Camera cam;
	public Transform REZ;
	public Transform CraniotomyObj;

	Vector3 cacheCamPos;
	//	Vector3 cacheRootCamPos;

	Vector3 cacheRootViewPos;
	Vector3 cacheRootViewRotation;
	Transform cacheRootView;

	[Header ("ZOOM")]

	public float zoomFactor;

	public float zoomDistance = 20;
	public int minZoom = 200;
	public int maxZoom = 200;

	//	[Header ("MOVE")]
	//	public float duration = 1f;

	Coroutine coroutineMove;

	// Use this for initialization
	void Awake ()
	{
		cacheCamPos = cam.transform.localPosition;

		cacheRootViewPos = transform.position;
		cacheRootViewRotation = transform.eulerAngles;
		cacheRootView = this.transform;

		//		Debug.Log ("it :" + transform.localEulerAngles);
//		Debug.Log ("it :" + transform.eulerAngles);
//		Debug.Log ("it :" + transform.rotation.eulerAngles);
//		Debug.Log ("it :" + transform.localRotation.eulerAngles);

	}
	
	// Update is called once per frame
	void Update ()
	{
		leftClickHandle ();

		rightClickHandle ();

		MiddleDragHandle ();

		ScrollwheelHandle ();
	}

	Vector3 screenSpace;
	Vector3 offset;

	float lineSpawnCacheZ = 0;
	float lineSpawnRootZ = 0;

	void MiddleDragHandle ()
	{
//		if (Input.GetMouseButtonDown (2)) {
//			mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
//			offset = transform.position - new Vector3 (mousePos.x, mousePos.y, transform.position.z);
//		}
//
//		if (Input.GetMouseButton (2)) {
////			mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
//			transform.position = Camera.main.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, transform.position.z)) + offset;
//		}

//		if (Input.GetMouseButtonDown (0)) {
//			lineSpawnRootZ = lineSpawnCacheZ;
//			screenSpace = new Vector3 (transform.position.x, transform.position.y, lineSpawnCacheZ);
//			offset = new Vector3 (transform.position.x, transform.position.y, lineSpawnCacheZ) - Camera.main.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, lineSpawnCacheZ));
//		}
//
//		if (Input.GetMouseButton (0)) {
//			var curScreenSpace = Camera.main.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, lineSpawnRootZ));    
//			var curPosition = new Vector3 (curScreenSpace.x, curScreenSpace.y, lineSpawnRootZ) + offset;
//			transform.position = curPosition;
//		}
	}

	void leftClickHandle ()
	{
		
	}

	void rightClickHandle ()
	{
		if (Input.GetMouseButton (1)) {
			Rotate ();
		}
	}

	void ScrollwheelHandle ()
	{
		if (Input.GetAxis ("Mouse ScrollWheel") > 0) {
			zoomFactor += zoomDistance;
			ZoomUpdate (ref zoomFactor);
		}

		if (Input.GetAxis ("Mouse ScrollWheel") < 0) {
			zoomFactor -= zoomDistance;
			ZoomUpdate (ref zoomFactor);
		}
	}

	void Rotate ()
	{
		transform.Rotate (Vector3.up * Input.GetAxis ("Mouse X") * 80 * Time.deltaTime);
		transform.Rotate (new Vector3 (-1, 0, 0) * Input.GetAxis ("Mouse Y") * 80 * Time.deltaTime);
	}

	void ZoomUpdate (ref float value)
	{
		value = Mathf.Clamp (value, -minZoom, maxZoom);
		cam.transform.localPosition = new Vector3 (cacheCamPos.x, cacheCamPos.y, cacheCamPos.z + value);
	}

	public void Reset ()
	{
		cam.transform.localPosition = cacheCamPos;
		cam.transform.localRotation = Quaternion.Euler (0, 0, 0);

		transform.rotation = Quaternion.Euler (cacheRootViewRotation.x, cacheRootViewRotation.y, cacheRootViewRotation.z);

		ViewPoint (cacheRootViewPos, 0, 1);
//		ViewPoint (cacheRootView);
	}

	float zoomFactorView = 400;

	public void FocusREZPoint ()
	{
		ViewPoint (CraniotomyObj, zoomFactorView, 1);
//		transform.eulerAngles = Cylinder_target.parent.eulerAngles;
//		transform.transform.position = Cylinder_target.parent.transform.position;
//		transform.LookAt (REZ);
//		transform.eulerAngles = new Vector3 (transform.eulerAngles.x - 90, transform.eulerAngles.y, transform.eulerAngles.z);
	}

	public void Move (float posX, float posY, float posZ, float zoomFactorView, float duration)
	{
		ViewPoint (new Vector3 (posX, posY, posZ), zoomFactorView, duration);
		//		transform.eulerAngles = Cylinder_target.parent.eulerAngles;
		//		transform.transform.position = Cylinder_target.parent.transform.position;
		//		transform.LookAt (REZ);
		//		transform.eulerAngles = new Vector3 (transform.eulerAngles.x - 90, transform.eulerAngles.y, transform.eulerAngles.z);
	}

	public void MoveAndRotate (float posX, float posY, float posZ, float rotX, float rotY, float rotZ, float zoomFactorView, float duration)
	{
		ViewPoint (new Vector3 (posX, posY, posZ), new Vector3 (rotX, rotY, rotZ), zoomFactorView, duration);
		//		transform.eulerAngles = Cylinder_target.parent.eulerAngles;
		//		transform.transform.position = Cylinder_target.parent.transform.position;
		//		transform.LookAt (REZ);
		//		transform.eulerAngles = new Vector3 (transform.eulerAngles.x - 90, transform.eulerAngles.y, transform.eulerAngles.z);
	}

	public void FocusREZPoint (float zoomFactorView, float duration)
	{
		ViewPoint (CraniotomyObj, zoomFactorView, duration);
		//		transform.eulerAngles = Cylinder_target.parent.eulerAngles;
		//		transform.transform.position = Cylinder_target.parent.transform.position;
		//		transform.LookAt (REZ);
		//		transform.eulerAngles = new Vector3 (transform.eulerAngles.x - 90, transform.eulerAngles.y, transform.eulerAngles.z);
	}

	public void ViewPoint (Transform tf, float zoomView, float duration)
	{
		if (coroutineMove != null)
			StopCoroutine (coroutineMove);

		coroutineMove = StartCoroutine (IEViewPoint (tf, zoomView, duration));
	}

	public void ViewPoint (Vector3 pos, float zoomView, float duration = 1)
	{
		if (coroutineMove != null)
			StopCoroutine (coroutineMove);

		coroutineMove = StartCoroutine (IEViewPoint (pos, zoomView, duration));
	}

	public void ViewPoint (Vector3 pos, Vector3 rot, float zoomView, float duration = 1)
	{
		if (coroutineMove != null)
			StopCoroutine (coroutineMove);

		coroutineMove = StartCoroutine (IEViewPoint (pos, rot, zoomView, duration));
	}

	IEnumerator IEViewPoint (Transform target, float zoomView, float duration)
	{
		Vector3 destination = new Vector3 (target.position.x, target.position.y, target.position.z);
		Vector3 rotation = new Vector3 (target.eulerAngles.x, target.eulerAngles.y, target.eulerAngles.z);

		float counter = 0;
//		while (transform.position != destination) {
		while (counter < duration) {
			counter += Time.deltaTime;
			transform.position = Vector3.Lerp (transform.position, destination, counter / duration);
//			transform.eulerAngles = Vector3.MoveTowards (transform.eulerAngles, rotation, counter / duration);
			transform.rotation = Quaternion.Lerp (transform.rotation, Quaternion.Euler (rotation), counter / duration);

			transform.LookAt (REZ);
			zoomFactor = Mathf.Lerp (zoomFactor, zoomView, counter / duration);
			ZoomUpdate (ref zoomFactor);
			yield return null;
		}

		transform.LookAt (REZ);
	}

	IEnumerator IEViewPoint (Vector3 pos, float zoomView, float duration = 1)
	{
		Vector3 destination = pos;
		float counter = 0;
		while (counter < duration) {
			counter += Time.deltaTime;
			transform.position = Vector3.Lerp (transform.position, destination, counter / duration);

			zoomFactor = Mathf.Lerp (zoomFactor, zoomView, counter / duration);
			ZoomUpdate (ref zoomFactor);
			yield return null;
		}
	}

	IEnumerator IEViewPoint (Vector3 pos, Vector3 rotation, float zoomView, float duration = 1)
	{
		Vector3 destination = pos;
		float counter = 0;
		while (counter < duration) {
			counter += Time.deltaTime;
			transform.position = Vector3.Lerp (transform.position, destination, counter / duration);
//			transform.eulerAngles = Vector3.MoveTowards (transform.eulerAngles, rotation, counter / duration);
			transform.rotation = Quaternion.Lerp (transform.rotation, Quaternion.Euler (rotation), counter / duration);

			zoomFactor = Mathf.Lerp (zoomFactor, zoomView, counter / duration);
			ZoomUpdate (ref zoomFactor);
			yield return null;
		}
	}
}
