﻿using UnityEngine;
using System;
using System.Collections;

[RequireComponent(typeof(MeshFilter))]
public class MeshDeformer : MonoBehaviour
{
    public bool isDebug = false;

    [Header("Param")]
    public float springForce = 20f;
	public float damping = 5f;

    [Header("Input")]
    Mesh deformingMesh;
	Vector3[] originalVertices, displacedVertices;
	Vector3[] vertexVelocities;

	float uniformScale = 1f;

    [Header("Control")]
    public bool allowDeform = false;
    public bool idle = true;
    public bool lockDeform = true;

    void Start () 
	{
		deformingMesh = GetComponent<MeshFilter>().mesh;
		originalVertices = deformingMesh.vertices;
		displacedVertices = new Vector3[originalVertices.Length];
		for (int i = 0; i < originalVertices.Length; i++)
        {
			displacedVertices[i] = originalVertices[i];
		}
		vertexVelocities = new Vector3[originalVertices.Length];
	}

    [Header("iExam")]    
    public MeshDeformer_ExamInfo.UpdateIExamODV IExam_UpdateODV = null;
    public MeshDeformer_ExamInfo.UpdateIExamPAV IExam_UpdatePAV = null;
    public MeshDeformer_ExamInfo.UpdateIExamProcess updateIExamProcess = null;
    public MeshDeformer_SlowestVert.UpdateISlowest updateISlowest = null;    

    void Update ()
	{
        if (lockDeform) return;

        if (isDebug) Debug.Log("UpdateVertex");

        uniformScale = transform.localScale.x;
		for (int i = 0; i < displacedVertices.Length; i++)
		{
			UpdateVertex (i);            
		}

        if (IExam_UpdateODV != null) IExam_UpdateODV.Invoke(originalVertices, displacedVertices, vertexVelocities);
        if (IExam_UpdateODV != null) updateISlowest.Invoke(vertexVelocities);

        deformingMesh.vertices = displacedVertices;
		deformingMesh.RecalculateNormals ();		

        if (Input.GetKey(KeyCode.N))
        {
            for (int i = 0; i < displacedVertices.Length; i++)
            {
                UpdateVertex_byStep(i);
            }

            deformingMesh.vertices = displacedVertices;
            deformingMesh.RecalculateNormals();
        }

        if (Input.GetKey(KeyCode.M))
        {            
            _posDraw = transform.TransformPoint(displacedVertices[0]);
            _dirDraw = point - transform.TransformPoint(displacedVertices[0]);
            
            Debug.DrawRay(_posDraw, _dirDraw, Color.green, Time.deltaTime);
        }
    }

	void UpdateVertex (int i)
    {
		Vector3 velocity = vertexVelocities[i];
		Vector3 displacement = displacedVertices[i] - originalVertices[i];        

		if (allowDeform == true)
            displacement = displacedVertices[i] - originalVertices[i];
		else displacement = Vector3.zero;

		displacement *= uniformScale;
		velocity -= displacement * springForce * Time.deltaTime;

		velocity *= 1f - damping * Time.deltaTime;

		vertexVelocities[i] = velocity;
		displacedVertices[i] += velocity * (Time.deltaTime / uniformScale);

        if (idle && velocity != Vector3.zero) idle = false;
        else if (!idle && velocity == Vector3.zero) idle = true;

        if (updateIExamProcess != null) updateIExamProcess.Invoke(i, velocity.magnitude, displacedVertices[i]);
    }

    [Header("Deform no explosion")]
    public float stepPos = 0.1f;
    public Vector3 point = Vector3.zero;

    Vector3 _dirDraw = Vector3.zero;
    Vector3 _posDraw = Vector3.zero;
    
    public float radius100 = 1f;
    public float radius0 = 2f;
    public float scaleSphere = 10f;
        
    void UpdateVertex_byStep(int i)
    {
        Vector3 dir = point - transform.TransformPoint(displacedVertices[i]);
        Vector3 localDir = transform.InverseTransformDirection(dir);
        float length = (float)(localDir.magnitude / scaleSphere);
        if (length > radius0) return;
        float percent = 1f;
        if (length > radius100 && length <= radius0) percent = 1 - (float)((float)(length - radius0) / (float)(radius0 - radius100));
        displacedVertices[i] += localDir * stepPos * percent;
    }

    public Transform _point = null;
    public float _force = -1f;

    [ContextMenu("AddDeformingForce")]
    public void AddDeformingForce()
    {
        if (_point != null && _force != -1f)
            AddDeformingForce(_point.position, _force);
    }

    public void AddDeformingReverseForce()
    {
        if (_point != null && _force != -1f)
            AddDeformingForce(_point.position, -_force);
    }

    public void AddDeformingForce (Vector3 point, float force)
    { 
		point = transform.InverseTransformPoint(point);
		for (int i = 0; i < displacedVertices.Length; i++)
        {
			AddForceToVertex(i, point, force);
		}
	}

    void AddForceToVertex (int i, Vector3 point, float force)
    {
		Vector3 pointToVertex = displacedVertices[i] - point;
		pointToVertex *= uniformScale;
		float attenuatedForce = force / (1f + pointToVertex.sqrMagnitude);
		float velocity = attenuatedForce * Time.deltaTime;
		vertexVelocities[i] += pointToVertex.normalized * velocity;

        if (IExam_UpdatePAV != null) IExam_UpdatePAV.Invoke(i, pointToVertex, attenuatedForce, velocity, vertexVelocities[i]);
	}

    public void AddDirectionForce(Vector3 direction, float force)
    {
        for (int i = 0; i < displacedVertices.Length; i++)
        {
            AddDirectForceToVertex(i, direction, force);
        }
    }

    void AddDirectForceToVertex(int i, Vector3 direction, float force)
    {

    }

    [Header("Reset Deform")]
    public float resetSpringForce = 40f;
    public float resetDamping = 4f;
    
    [ContextMenu("ResetDeform")]
    public void ResetDeform()
    {
        Debug.Log("ResetDeform");
        StopAllCoroutines();
        StartCoroutine(C_ResetDeform());
    }

    public delegate void delAfterResetDeform();
    public delAfterResetDeform delafterResetDeform = null;

    IEnumerator C_ResetDeform()
    {
        lockDeform = false;        

        springForce = resetSpringForce;
        damping = resetDamping;

        allowDeform = true;
        yield return new WaitForSeconds(3f);

        if (delafterResetDeform != null) delafterResetDeform.Invoke();

        yield break;
    }

    [Header("Auto Deform")]
    public float autoSpringForce = 4f;
    public float autoDamping = 3f;

    [ContextMenu("AutoDeform")]
    public void AutoDeform()
    {
        Debug.Log("AutoDeform");
        StopAllCoroutines();
        StartCoroutine(C_Deformer());
    }

    public delegate void delAfterAutoDeform();
    public delAfterAutoDeform delafterAutoDeform = null;

    IEnumerator C_Deformer()
    {
        lockDeform = false;

        springForce = autoSpringForce;
        damping = autoDamping;

        allowDeform = false;

        for (int i = 0; i < 13; i++)
        {
            AddDeformingForce(new Vector3(-8.9f, -54.3f, -21.3f), 2500);
            AddDeformingForce(new Vector3(-15.6f, -48.6f, -24.1f), 2500);
            AddDeformingForce(new Vector3(-17.7f, -43.4f, -32.2f), 2500);
            AddDeformingForce(new Vector3(-18.7f, -53.2f, -38.6f), 2500);
            AddDeformingForce(new Vector3(-16.2f, -51.0f, -29.4f), 2500);

            yield return new WaitForSeconds(0.2f);
        }

        yield return new WaitForSeconds(1f);

        yield return new WaitUntil(() => idle == true);

        if (delafterAutoDeform != null) delafterAutoDeform.Invoke();

        lockDeform = true;

        yield break;
    }
}