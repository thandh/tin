﻿using UnityEngine;
using System;
using System.Collections;

public class AddforceToBrain : MonoBehaviour
{
    public MeshDeformer mdBrain = null;

    [Header("Control")]
    public bool canAdd = true;

    [Header("Input")]
    public float force = -3000f;
    public int timeAdd = 1;
    public float timeStep = 0.2f;

    [Header("AddThatForce")]
    public bool doneAdd = true;

    [ContextMenu("Add that force")]
    public void AddThatForce()
    {
        if (!canAdd) return;
        StopAllCoroutines();
        StartCoroutine(C_AddThatForce());
    }
    
    IEnumerator C_AddThatForce()
    {
        doneAdd = false;

        for (int i = 0; i < timeAdd; i++)
        {
            mdBrain.AddDeformingForce(transform.position, force);
            yield return new WaitForSeconds(timeStep);
        }

        doneAdd = true;

        yield break;
    }
}