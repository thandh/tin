﻿using UnityEngine;
using System;
using System.Collections;

public class MeshDeformer_ExamInfo : MonoBehaviour
{
    public bool isDebug = false;

    [Header("Param")]
    public MeshDeformer md = null;

    [Header("Example Info - attenuated")]
    public int iExam = 0;
    public Vector3 oriPos_iExam = Vector3.zero;
    public Vector3 disPos_iExam = Vector3.zero;
    public Vector3 velocity_iExam = Vector3.zero;

    public delegate void UpdateIExamODV(Vector3[] ori, Vector3[] dis, Vector3[] velo);
    public delegate void UpdateIExamPAV(int VertID, Vector3 pointToVertex, float attenuatedForce, float velo0, Vector3 vertexVelo0);
    public delegate void UpdateIExamProcess(int VertID, float velo, Vector3 currentPos);

    public void IExam_Update_ODV(Vector3[] ori, Vector3[] dis, Vector3[] velo)
    {
        oriPos_iExam = ori[iExam];
        disPos_iExam = dis[iExam];
        velocity_iExam = velo[iExam];
    }

    [Header("Example Info - beginning")]
    public Vector3 pointToVertex = Vector3.zero;
    public float attenuatedForce = 0f;
    public float velo0 = 0;
    public Vector3 vertexVelo0 = Vector3.zero;
    
    public void IExam_Update_PAV(int VertID, Vector3 pointToVertex, float attenuatedForce, float velo0, Vector3 vertexVelo0)
    {
        if (VertID != this.iExam) return;
        this.pointToVertex = pointToVertex;
        this.attenuatedForce = attenuatedForce;
        this.velo0 = velo0;
        this.vertexVelo0 = vertexVelo0;

        ResetProcessing();
        doneProcessing = false; ///Start Processing
    }

    [Header("Example Info - Processing")]
    public bool doneProcessing = true;
    public int countAnnuated = 0;
    public float disTravel = 0f;
    public float deltaVelo = 0.01f;
    
    void ResetProcessing()
    {
        countAnnuated = 0;
        disTravel = 0f;
    }

    public void IExam_Update_Processing(int VertID, float velo, Vector3 currentPos)
    {
        if (doneProcessing) return;
        if (VertID != this.iExam) return;
        if (velo < deltaVelo)
        {
            if (disTravel == 0f)
            {
                disTravel = (currentPos - oriPos_iExam).magnitude;
                doneProcessing = true;
            }
            return;
        }
        countAnnuated++;        
    }

    private void OnEnable()
    {
        md.IExam_UpdateODV += IExam_Update_ODV;
        md.IExam_UpdatePAV += IExam_Update_PAV;
        md.updateIExamProcess += IExam_Update_Processing;
    }

    private void OnDisable()
    {
        if (!isQuiting) md.IExam_UpdateODV -= IExam_Update_ODV;
        if (!isQuiting) md.IExam_UpdatePAV -= IExam_Update_PAV;
        if (!isQuiting) md.updateIExamProcess -= IExam_Update_Processing;
    }

    bool isQuiting = false;

    private void OnApplicationQuit()
    {
        isQuiting = true;    
    }
}