﻿using UnityEngine;
using UnityEngine.UI;

public class SwitchImageButton : MonoBehaviour 
{
	public Sprite buttonOn;
	public Sprite buttonOff;

	public void SwitchImage()
	{
		if(GetComponent<Image>().sprite == buttonOn)
		{
			GetComponent<Image>().sprite = buttonOff;
		} else
		{
			GetComponent<Image>().sprite = buttonOn;
		}
	}	
}
