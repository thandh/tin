﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CereImpactPoint : MonoBehaviour
{
    public AddforceToBrain force = null;
    public HitByRay hitByRay = null;
    public MeshRenderer mr = null;

    private void Start()
    {
        Init();
    }

    [ContextMenu("Init")]
    public void Init()
    {
        force.mdBrain = cerebellum_180416.Instance.md;
    }

    public void StepFromCere(float distance)
    {
        bool hit = hitByRay.CheckHitUseFinalToFinal();
        if (hit)
        {
            transform.position += (hitByRay.trFrom.position - hitByRay.trTo.position).normalized * distance;
            force.canAdd = true;
        }
        else
        {
            force.canAdd = false;
        }
    }

    [Header("Visible")]
    public bool boolVisible = false;

    [SerializeField] Renderer _r = null;
    Renderer r { get { if (_r == null) _r = GetComponent<Renderer>(); return _r; } }

    public Color vColor = Color.green;
    public Color ivColor = Color.black;

    void Update()
    {
        if (boolVisible) CheckVisible();        
    }

    void CheckVisible()
    {
        if (r.IsVisibleFrom(Camera.main)) r.sharedMaterials[0].color = vColor;
        else r.sharedMaterials[0].color = ivColor;
    }

    public void SetColor(Color c)
    {
        r.material.color = c;
    }

    public void ShowRenderer(bool show)
    {
        mr.enabled = show;
    }

    [ContextMenu("GetMR")]
    public void GetMR()
    {
        mr = GetComponent<MeshRenderer>();
    }
}
