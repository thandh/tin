﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;

public class CutHole : MonoBehaviour,IUndo
{
	[System.Serializable]
	public struct MeshHistory
	{
		public int[] triangles;

		public MeshHistory (int[] triangles)
		{
			this.triangles = triangles;
		}
	}
    	
	public List<MeshHistory> history = new List<MeshHistory> ();

    public Transform lastHole = null;

    void Start ()
	{

	}
    
	List<int> listIdBeCut = new List<int> ();

	public float radius = 100;
	public float distanceRaycast = 0.5f;

	List<Vector3> listDrawPos = new List<Vector3> ();
	Vector3 direction;
    
	public void SetIDCut (int id)
	{
		if (!listIdBeCut.Contains (id))
			listIdBeCut.Add (id);
	}	

	public void loopCut (bool reset)
	{
		if (this.gameObject.GetComponent<MeshCollider> ())
			Destroy (this.gameObject.GetComponent<MeshCollider> ());

		Mesh mesh = transform.GetComponent<MeshFilter> ().mesh;
		List<int> oldTriangles = mesh.triangles.ToList ();
		listIdBeCut.Sort ();
		
		for (int i = listIdBeCut.Count - 1; i >= 0; i--) {
			if (oldTriangles.Count <= listIdBeCut [i] * 3)
				continue;
			oldTriangles.RemoveAt (listIdBeCut [i] * 3);
			oldTriangles.RemoveAt (listIdBeCut [i] * 3);
			oldTriangles.RemoveAt (listIdBeCut [i] * 3);
			
		}

		listIdBeCut = new List<int> ();

		transform.GetComponent<MeshFilter> ().mesh.triangles = oldTriangles.ToArray ();
		StartCoroutine (AddMeshCollider (reset));
	}

	IEnumerator AddMeshCollider (bool reset)
	{
		yield return null;
		if (!this.gameObject.GetComponent<MeshCollider> ())
			this.gameObject.AddComponent<MeshCollider> ();

		if (reset)
			this.enabled = false;
	}

	void OnDrawGizmosSelected ()
	{
		// Display the explosion radius when selected
		Gizmos.color = new Color (1, 1, 0, 0.75F);
		foreach (Vector3 vec in listDrawPos) {
			Gizmos.DrawRay (vec, direction * 1000.0f);
		}
	}

	public void RestartScene ()
	{
		SceneManager.LoadScene (0);
	}

	public void UndoRegister ()
	{
		history.Add (new MeshHistory (transform.GetComponent<MeshFilter> ().mesh.triangles));
	}

	#region IUndo

	[ContextMenu ("Undo")]
	public void Undo ()
	{
		if (this.gameObject.GetComponent<MeshCollider> ())
			Destroy (this.gameObject.GetComponent<MeshCollider> ());

		if (history.Count > 0)
			transform.GetComponent<MeshFilter> ().mesh.triangles = history [history.Count - 1].triangles;

		if (history.Count > 0)
			history.RemoveAt (history.Count - 1);

//		if (!this.gameObject.GetComponent<MeshCollider> ())
//			this.gameObject.AddComponent<MeshCollider> ();

		StartCoroutine (AddMeshCollider (true));

	}

	#endregion
}
