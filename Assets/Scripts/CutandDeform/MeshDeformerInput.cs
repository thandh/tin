﻿using UnityEngine;

public class MeshDeformerInput : MonoBehaviour {
	
	public float force = 10f;
	public float forceOffset = 0.1f;

	public float distanceRay = 1f;

	[SerializeField] Camera _camera;
	
	void Update () {
		if (Input.GetMouseButton(0)) {
			HandleInput();
		}
	}

	void HandleInput () {
		Ray inputRay = _camera.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		
		if (Physics.Raycast(inputRay, out hit)) {
			
			MeshDeformer deformer = hit.collider.GetComponent<MeshDeformer>();
			if (deformer) {
				
				Vector3 point = hit.point;
				
				
				point += hit.normal * forceOffset;
				Debug.Log("point "+point);
				deformer.AddDeformingForce(point, force);
			}
		}
	}

	public void HandleInput (Vector3 posBegin,Vector3 direction) 
	{
		RaycastHit hit;
		
		if (Physics.Raycast(posBegin, direction, out hit,distanceRay)) {
			MeshDeformer deformer = hit.collider.GetComponent<MeshDeformer>();
			if (deformer) {
				Vector3 point = hit.point;
				point += hit.normal * forceOffset;
				deformer.AddDeformingForce(point, force);
			}
		}
	}
}