﻿using UnityEngine;

public delegate void delAfterDeform();

public class MeshDeformerMethod : MonoBehaviour
{
    [Header("Inputs")]
    public MeshDeformer md = null;    

    public delAfterDeform delafterDeform = null;

    [ContextMenu("SetPoint")]
    public void SetPoint()
    {
        md._point = transform;
    }

    [Header("Deform")]
    public bool UseKeyBoardColon = false;

    [ContextMenu("Deform")]
    public void Deform()
    {
        Debug.Log(transform.name + " is deforming Cerebellum");

        SetPoint();
        md.AddDeformingForce();
    }

    [ContextMenu("DeformReverse")]
    public void DeformReverse()
    {
        Debug.Log(transform.name + " is deforming Cerebellum");

        SetPoint();
        md.AddDeformingReverseForce();
    }    

    private void Update()
    {
        if (UseKeyBoardColon)
        {
            if (Input.GetKey(KeyCode.Space) && (!Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift)))
            {
                Deform();
            } else if (Input.GetKey(KeyCode.Space) && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
            {
                DeformReverse();
            }
        }
    }
}