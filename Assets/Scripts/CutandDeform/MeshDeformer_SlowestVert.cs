﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class MeshDeformer_SlowestVert : MonoBehaviour
{
    public bool isDebug = false;

    [Header("Param")]
    public MeshDeformer md = null;

    [Header("Example Info")]
    public int iSlowest = 0;    

    public delegate void UpdateISlowest(Vector3[] velo);

    public void DoUpdateISlowest(Vector3[] velo)
    {
        float maxMagnitude = 999999f;
        for (int i = 0; i < velo.Length; i++)
        {
            if (velo[i].magnitude < maxMagnitude)
            {
                iSlowest = i;
                maxMagnitude = velo[i].magnitude;
            }
        }
        
    }

    private void OnEnable()
    {
        md.updateISlowest += DoUpdateISlowest;
    }

    private void OnDisable()
    {
        if (!isQuiting) md.updateISlowest -= DoUpdateISlowest;
    }

    bool isQuiting = false;

    private void OnApplicationQuit()
    {
        isQuiting = true;
    }
}