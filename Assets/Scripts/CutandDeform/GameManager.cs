﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
	[SerializeField] MeshDeformerInput deformerInput;

	[SerializeField] PaintVertices paintVertices;	

	MeshFilter _meshPaint;

	bool deformerKey = false;
	bool paintKey = false;

    private void Start()
    {        
        //_meshPaint = PICA.Instance.mf;
    }

	public void SwitchPaintActive (Image _imageButton)
	{
		//if (paintKey) {
		//	paintKey = false;
		//	foreach (PaintDeformer pd in FindObjectsOfType<PaintDeformer>()) {
		//		pd.enabled = true;
		//	}
		//} else {
		//	paintKey = true;
		//	foreach (PaintDeformer pd in FindObjectsOfType<PaintDeformer>()) {
		//		pd.enabled = false;
		//	}

		//	paintVertices.PullAnimation (_meshPaint.mesh, new Vector3 (19.1f, 24.9f, -569.2f), new Vector3 (-0.4f, -0.9f, 0.3f));
		//	paintVertices.PullAnimation (_meshPaint.mesh, new Vector3 (26.5f, 32.0f, -570.3f), new Vector3 (0.7f, -0.6f, -0.3f));
		//}
	}

	bool isCutHole = false;

	public void SwitchCutHole ()
	{
		if (isCutHole) {
			foreach (CutHole ch in FindObjectsOfType<CutHole>()) {
				ch.enabled = false;
			}
			isCutHole = false;
		} else {
			foreach (CutHole ch in FindObjectsOfType<CutHole>()) {
				ch.enabled = true;
			}
			isCutHole = true;
		}
	}

	public void ResetScene ()
	{
        TimeScale.Scale1();
        Destroy(DontDestroy.Instance.gameObject);
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
	}
}
