﻿using UnityEngine;

public class MeshDeformerSphere : MonoBehaviour
{
    MeshDeformer _md = null;
    MeshDeformer md { get { if (_md == null) _md = cerebellum_180416.Instance.md; return _md; } }

    public UseRayToMove useRay = null;

    private void Start()
    {
        UpdateMeshDeform_SpherePosition();
        md.scaleSphere = transform.localScale.x;
    }

    private void OnEnable()
    {
        useRay.delAfter += UpdateMeshDeform_SpherePosition;
    }

    private void OnDisable()
    {
        useRay.delAfter -= UpdateMeshDeform_SpherePosition;
    }

    void UpdateMeshDeform_SpherePosition()
    {
        md.point = transform.position;
    }
}