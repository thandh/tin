﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PaintVertices : MonoBehaviour
{
    public float radius = 1.0f;
    public float pull = 10.0f;
    private MeshFilter unappliedMesh;

    enum FallOff { Gauss, Linear, Needle }
	FallOff fallOff = FallOff.Needle;

    static float LinearFalloff(float distance, float inRadius)
    {
        return Mathf.Clamp01(1.0f - distance / inRadius);
    }

    static float GaussFalloff(float distance, float inRadius)
    {
        return Mathf.Clamp01(Mathf.Pow(360.0f, -Mathf.Pow(distance / inRadius, 2.5f) - 0.01f));
    }

    float NeedleFalloff(float dist, float inRadius)
    {
        return -(dist * dist) / (inRadius * inRadius) + 1.0f;
    }

	List<Vector3> pullingPoint = new List<Vector3>();

	[SerializeField] Transform direct;

	[SerializeField] PaintDeformer paintDeformer;

	[SerializeField] Camera _camera;

    void DeformMesh(Mesh mesh, Vector3 position, float power, float inRadius)
    {
        Vector3[] vertices = mesh.vertices;
        Vector3[] normals = mesh.normals;
        float sqrRadius = inRadius * inRadius;

        // Calculate averaged normal of all surrounding vertices	
        Vector3 averageNormal = Vector3.zero;
        for (int i = 0; i < vertices.Length; i++)
        {
            float sqrMagnitude = (vertices[i] - position).sqrMagnitude;
            // Early out if too far away
            if (sqrMagnitude > sqrRadius)
                continue;

            float distance = Mathf.Sqrt(sqrMagnitude);
            float falloff = LinearFalloff(distance, inRadius);
            averageNormal += falloff * normals[i];
        }
        averageNormal = averageNormal.normalized;

        // Deform vertices along averaged normal
        for (int i = 0; i < vertices.Length; i++)
        {
            float sqrMagnitude = (vertices[i] - position).sqrMagnitude;
            // Early out if too far away
            if (sqrMagnitude > sqrRadius)
                continue;

            float distance = Mathf.Sqrt(sqrMagnitude);
            float falloff;

            switch (fallOff)
            {
                case FallOff.Gauss:
                    falloff = GaussFalloff(distance, inRadius);
                    break;
                case FallOff.Needle:
                    falloff = NeedleFalloff(distance, inRadius);
                    break;
                default:
                    falloff = LinearFalloff(distance, inRadius);
                    break;
            }

            vertices[i] += averageNormal * falloff * power;
        }

        mesh.vertices = vertices;
        mesh.RecalculateNormals();
        mesh.RecalculateBounds();
    }

	void DeformMesh(Mesh mesh, Vector3 position,Vector3 direction, float power, float inRadius)
	{
		Vector3[] vertices = mesh.vertices;
		Vector3[] normals = mesh.normals;
		float sqrRadius = inRadius * inRadius;

		// Deform vertices along averaged normal
		for (int i = 0; i < vertices.Length; i++)
		{
			float sqrMagnitude = (vertices[i] - position).sqrMagnitude;
			// Early out if too far away
			if (sqrMagnitude > sqrRadius)
				continue;

			float distance = Mathf.Sqrt(sqrMagnitude);
			float falloff;

			switch (fallOff)
			{
			case FallOff.Gauss:
				falloff = GaussFalloff(distance, inRadius);
				break;
			case FallOff.Needle:
				falloff = NeedleFalloff(distance, inRadius);
				break;
			default:
				falloff = LinearFalloff(distance, inRadius);
				break;
			}

			vertices[i] += direction * falloff * power;
		}

		mesh.vertices = vertices;
		mesh.RecalculateNormals();
		mesh.RecalculateBounds();
	}

	float pullPower = 1;

	Vector3 pointStart,pointEnd;

    void Update()
    {
		if (Input.GetMouseButtonDown (0))
		{
			RaycastHit hitStart;
			Ray rayStart = _camera.ScreenPointToRay(Input.mousePosition);

			if (Physics.Raycast (rayStart, out hitStart))
			{
				PaintDeformer deformer = hitStart.collider.GetComponent<PaintDeformer>();

				if(deformer)
				{
					paintDeformer = deformer;
				} else
				{
					return;
				}
				pointStart = hitStart.point;
				MeshFilter filter = hitStart.collider.GetComponent<MeshFilter>();
				
				if (filter)
				{
					pointStart = filter.transform.InverseTransformPoint(hitStart.point);
					unappliedMesh = filter;
					// pullingPoint.Add (startPoint);
					// StartCoroutine (PullAnimation(startPoint,false));
				}
			}
		}

		if(Input.GetMouseButton(0))
		{
			RaycastHit hit;
			Ray rayStart = _camera.ScreenPointToRay(Input.mousePosition);

			if (Physics.Raycast (rayStart, out hit))
			{
				PaintDeformer deformer = hit.collider.GetComponent<PaintDeformer>();

				if(deformer != paintDeformer)
				{
					return;
				} else
				{
					MeshFilter filter = hit.collider.GetComponent<MeshFilter>();
				
					if (filter)
					{
						pointEnd = filter.transform.InverseTransformPoint(hit.point);
					}
				}
			}
		}

		if(Input.GetMouseButtonUp(0))
		{
			if(paintDeformer)
			{
				MeshFilter filter = paintDeformer.GetComponent<MeshFilter>();
				
				if (filter)
				{
					unappliedMesh = filter;
					// pullingPoint.Add (startPoint);
					Vector3 direct = pointEnd-pointStart;
					direct.Normalize();
					Debug.Log ("pointStart "+pointStart);
					Debug.Log ("direct "+direct);
					StartCoroutine (PullAnimation(pointStart,direct,false));
				}
			}
		}
    }

	public void PullAnimation(Mesh mesh,Vector3 p,Vector3 direct)
	{
		StartCoroutine (PullAnimation(mesh,p,direct,false));
	}

	IEnumerator PullAnimation(Mesh mesh,Vector3 p,Vector3 direct,bool lastObj)
	{
		float pulled = 0;
		while (pulled < pull)
		{
			yield return null;
			DeformMesh (mesh
				, p
				, direct,
				pulled * Time.deltaTime, 
				radius);
			pulled += pullPower;
		}
		if (lastObj)
		{
			ApplyMeshCollider ();
			paintDeformer.allowedDeformed = true;
		}
	}

	IEnumerator PullAnimation(Vector3 p,Vector3 direct,bool lastObj)
	{
		float pulled = 0;
		while (pulled < pull)
		{
			yield return null;
			DeformMesh (unappliedMesh.mesh
				, p
				, direct,
				pulled * Time.deltaTime, 
				radius);
			pulled += pullPower;
		}
		if (lastObj)
		{
			ApplyMeshCollider ();
			paintDeformer.allowedDeformed = true;
		}
	}

    void ApplyMeshCollider()
    {
        if (unappliedMesh && unappliedMesh.GetComponent<MeshCollider>())
        {
            unappliedMesh.GetComponent<MeshCollider>().sharedMesh = unappliedMesh.sharedMesh;
        }
        unappliedMesh = null;
    }
}