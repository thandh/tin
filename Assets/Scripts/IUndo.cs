﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUndo
{
	void Undo ();
}
