﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class InputController : MonoBehaviour
{
	public GameObject target;
	public Transform camTranform;

	public float sensitivityY = 2;
	public float sensitivityX = 4;
	public float sensitivityZ = 4;
	float rotationY = 0f;
	float rotationX = 0f;
	float rotationZ = 0f;

	bool isDrag;
	Vector3 rootPositionCam;

	//Get touch
	Vector2 startTouch0, startTouch1;
	Vector2 curTouch0, curTouch1;

	float disStartTouch;
	float disCurTouch;

	public Vector2 startPos, curPos;
	public float startDis;
	public float curDis;
	public float zoomSpeed;
	public float zoomFactor;
	public float startZoomFactor;
	//	public ModelAndCamera _modelAndCamera;

	[Range (-15, 0)]
	public int minZoom = -10;
	[Range (0, 15)]
	public int maxZoom = 10;

	bool canActive = true;

	void Start ()
	{
		if (Application.isEditor) {
			sensitivityY = 4;
			sensitivityX = 6;
			sensitivityZ = 6;
		}

		rootPositionCam = camTranform.localPosition;
	}

	void Rotate ()
	{
		rotationY -= Input.GetAxis ("Mouse Y") * sensitivityY;
		rotationY = Mathf.Clamp (rotationY, 0, 90);

		rotationX -= Input.GetAxis ("Mouse X") * sensitivityX;
		//			rotationX = Mathf.Clamp (rotationX, 0, 90);

		target.transform.localEulerAngles = new Vector3 (rotationY, -rotationX, rotationZ);
	}

	void Update ()
	{
		if (Application.isEditor) {
			if (!EventSystem.current.IsPointerOverGameObject ()) {
				if (Input.GetMouseButton (0))
					Rotate ();

				if (Input.GetMouseButtonDown (1)) {
					startPos = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
					startDis = Vector2.Distance (Vector2.zero, startPos);
					startZoomFactor = zoomFactor;
				} else if (Input.GetMouseButton (1)) {
					curPos = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
					curDis = Vector2.Distance (Vector2.zero, curPos);
					zoomFactor = Mathf.Clamp (startZoomFactor + (curDis - startDis) / 40, minZoom, maxZoom);
//					Debug.Log (zoomFactor);
					camTranform.localPosition = new Vector3 (rootPositionCam.x, rootPositionCam.y, rootPositionCam.z + zoomFactor);
//					Debug.Log ("zoom factor" + zoomFactor);
//					_modelAndCamera.addDistance (rootPositionCam.z + zoomFactor);
				}

			}
		} else {
			if (Input.touchCount > 0) {
				Touch touch0 = Input.GetTouch (0);

				if (Input.touchCount == 1) {
					if (!EventSystem.current.IsPointerOverGameObject (Input.GetTouch (0).fingerId)) {
					
						if (touch0.phase == TouchPhase.Began) {
							canActive = true;
						}

						if (touch0.phase == TouchPhase.Moved) {
							if (canActive)
								Rotate ();
						}

						startZoomFactor = zoomFactor;
						startTouch0 = touch0.position;

						if (touch0.phase == TouchPhase.Ended || touch0.phase == TouchPhase.Canceled)
							canActive = true;

					}
				} else if (Input.touchCount >= 2) {
					if (!EventSystem.current.IsPointerOverGameObject (Input.GetTouch (1).fingerId)) {

						if (touch0.phase == TouchPhase.Began)
							startTouch0 = touch0.position;

						Touch touch1 = Input.GetTouch (1);

						if (touch1.phase == TouchPhase.Began)
							startTouch1 = touch1.position;

						disStartTouch = Vector2.Distance (startTouch0, startTouch1);

						if (touch0.phase == TouchPhase.Moved || touch1.phase == TouchPhase.Moved) {
							disCurTouch = Vector2.Distance (touch0.position, touch1.position);

							zoomFactor = Mathf.Clamp (startZoomFactor + (disCurTouch - disStartTouch) / 40, minZoom, maxZoom);

							camTranform.localPosition = new Vector3 (rootPositionCam.x, rootPositionCam.y, rootPositionCam.z + zoomFactor);
//							_modelAndCamera.addDistance (rootPositionCam.z + zoomFactor);
							Debug.Log ("zoom factor" + zoomFactor);
						}

//					if (touch0.phase == TouchPhase.Ended && touch1.phase == TouchPhase.Ended ||
//					    touch0.phase == TouchPhase.Canceled && touch1.phase == TouchPhase.Canceled) {
//						canActive = true;
//						startZoomFactor = zoomFactor;
//					}
//
						if (touch0.phase == TouchPhase.Ended || touch1.phase == TouchPhase.Ended) {
							canActive = false;
							startZoomFactor = zoomFactor;
						}
					}
				}
			}
		}
	}
}
