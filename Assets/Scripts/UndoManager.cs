﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UndoManager : MonoBehaviour
{
	static UndoManager instance;

	public static UndoManager Instance{ get { return instance; } }

	[System.Serializable]
	public class Queue
	{
		[SerializeField]
		public List<IUndo> undos = new List<IUndo> ();

		public void Add (IUndo undo)
		{
			undos.Add (undo);
		}
	}

	public List<Queue> stack = new List<Queue> ();

	void Awake ()
	{
		instance = this;
	}

	public void RemoveRegister ()
	{
		stack.RemoveAt (stack.Count - 1);
	}

	public void Register (IUndo undo)
	{
		stack.Add (new Queue ());
		stack [stack.Count - 1].undos.Add (undo);
	}

	public void RegisterMember (IUndo undo)
	{
		stack [stack.Count - 1].undos.Add (undo);
	}

	[ContextMenu ("Undo")]
	public void Undo ()
	{
		if (stack.Count > 0) {
			for (int i = 0; i < stack [stack.Count - 1].undos.Count; i++) {
				stack [stack.Count - 1].undos [i].Undo ();
			}

			RemoveRegister ();
			Debug.Log ("Undo");
		}
	}

	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.Z))
			Undo ();
	}
}
